﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ConfirmationDetails.xaml
    /// </summary>
    public partial class ConfirmationDetails : Window
    {
        private Settings settings = Properties.Settings.Default;
        private string heading;
        private string ticketid;
        private string value;
        private string box;
        private string ticketname;
        private string changeBox;
        private string frombox;
        private int soldoutstatus;
        private string packno;
        private long InactiveTicketValue;
        public ConfirmationDetails()
        {
            InitializeComponent();
            color();
            btnyes.Focus();
        }

        public void color()
        {
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbheading.Foreground = new SolidColorBrush(settings.Theme);
            btnno.Foreground = new SolidColorBrush(settings.Font);
            btnno.Background = new SolidColorBrush(settings.Tab);
            btnyes.Foreground = new SolidColorBrush(settings.Font);
            btnyes.Background = new SolidColorBrush(settings.Tab);
            txtTicktname.Foreground = new SolidColorBrush(settings.Font);
            txtvalue.Foreground = new SolidColorBrush(settings.Font);
            txtbox.Foreground = new SolidColorBrush(settings.Font);
            txttobox.Foreground = new SolidColorBrush(settings.Font);

            lbheading.FontFamily = new FontFamily(settings.Fontstyle);
                
        }

        public ConfirmationDetails(string heading, string ticketname, string value, string box, long InactiveTicketValue=0)     //Activate Ticket //soldout
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnyes.Focus();
            color();
            this.heading = heading;
            this.ticketname = ticketname;
            this.value = value;
            this.box = box;
            this.InactiveTicketValue = InactiveTicketValue;
            lbheading.Content = heading;
            txtTicktname.Content = ticketname;
            txtvalue.Content = value;
            txtbox.Content = box;
            if (heading == "Do you want to Inactivate?")
            txtinactive.Content = "Inactive Ticket Value : " + InactiveTicketValue;
        }

        public ConfirmationDetails(string heading, string ticketname, string value, string changeBox, string frombox)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnyes.Focus();
            color();
            this.heading = heading;
            this.ticketname = ticketname;
            this.value = value;
            this.changeBox = changeBox;
            this.frombox = frombox;
            lbbox.Content = "From Box";
            lbtobox.Visibility = Visibility.Visible;
            txttobox.Visibility = Visibility.Visible;

            lbheading.Content = heading;
            txtTicktname.Content = ticketname;
            txtvalue.Content = value;
            txtbox.Content = frombox;
          //  txttobox = changeBox;
            txttobox.Content = changeBox;

        }

        public ConfirmationDetails(string heading, string ticketname, string value, long InactiveTicketValue=0)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnyes.Focus();
            color();
            this.heading = heading;
            this.ticketname = ticketname;
            this.value = value;
            lbheading.Content = heading;
            lbbox.Visibility = Visibility.Hidden;
            txtbox.Visibility = Visibility.Hidden;
            txtTicktname.Content = ticketname;
            txtvalue.Content = value;   
            if(heading == "Do you want to Return?")
            {
                txtinactive.Content = "Return Ticket Value : " + InactiveTicketValue;
            }
           
        }

        public ConfirmationDetails(string heading, int soldoutstatus, string ticketid, string packno)       //full pack soldout
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnyes.Focus();
            color();
            this.heading = heading;
            this.soldoutstatus = soldoutstatus;
            this.ticketid = ticketid;
            this.packno = packno;
            lbheading.Content = heading;
            lbbox.Visibility = Visibility.Hidden;
            txtbox.Visibility = Visibility.Hidden;
            //lbticketname.Content = "Ticket Id";
            //lbvalue.Content = "Pack No.";
            txtTicktname.Content = ticketid;
            txtvalue.Content = packno;
        }

        public ConfirmationDetails(string heading, int soldoutstatus, string ticketname, string value, string box)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnyes.Focus();
            color();
            this.heading = heading;
            this.soldoutstatus = soldoutstatus;
            this.ticketname = ticketname;
            this.value = value;
            this.box = box;
            lbheading.Content = heading;
            txtbox.Content = box;
            //lbticketname.Content = "Ticket Id";
            //lbvalue.Content = "Pack No.";
            txtTicktname.Content = ticketname;
            txtvalue.Content = value;
        }


        private void btnno_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void deletebttnclicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.58;
        }     

       
    }
}
