﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for DialogWithCalender.xaml
    /// </summary>
    public partial class DialogWithCalender : Window
    {
    
        private Settings settings = Properties.Settings.Default; 
        private string heading;

        public DialogWithCalender()
        {
            InitializeComponent();
            btnStatus1.Focus();
            btnStatus1.Background = new SolidColorBrush(settings.Tab);
            
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            btnStatus1.Foreground = new SolidColorBrush(settings.Font);
      
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.labelfont);
        }

        public DialogWithCalender(string heading)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnStatus1.Focus();
            this.heading = heading;           
            lblTittle.Text = heading;
            btnStatus1.Background = new SolidColorBrush(settings.Tab);
        
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            btnStatus1.Foreground = new SolidColorBrush(settings.Font);
          
          
            if (settings.Background.ToString () != "#00FFFFFF") 
           {
               this.Background = new SolidColorBrush(settings.Background);
           }
            lblTittle.FontFamily = new FontFamily(settings.labelfont);
   
        }

        private void btnStatus1_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnStatus_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public DateTime ResponseData
        {
            get
            {


                return dpDate.SelectedDate.Value;
            }

            set
            {
                dpDate.SelectedDate = value;
             
            }
        }
    }
}
