﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace lai2.Bal
{
    class Utility
    {
      
    }

      

    public static class IndexFinder
    {
        public static int index { get; set; }
    }

    

    public class LoginList
    {

        public bool status { get; set; }
        public User user { get; set; }
        public List<Shop> shops { get; set; }
    }

    public static class LoginDetail
    {

        public static bool status { get; set; }
        public static User user { get; set; }
        public static List<Shop> shops { get; set; }
    }


    public class LatitudeList
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class ScanData
    {
        public string PackNo { get; set; }
        public string TicketId { get; set; }
        public int PackPosition { get; set; }
    }

    
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string Password { get; set; }

    }

    public class Shop
    {
        public int ShopId { get; set; }
        public string LocationName { get; set; }
        public string LegalName { get; set; }
        public double Distance { get; set; }
        public int BoxCount { get; set; }
        public int BoxAge { get; set; }
        public int InventoryAge { get; set; }
        public string ShopCloseTime { get; set; }
        public double AIResultPercetage { get; set; }
        public int TicketValueOrder { get; set; }
        public double InstantProfit { get; set; }
        public double OnlineProfit { get; set; }
        public string State { get; set; }
        public int rank { get; set; }
        public string SupportId { get; set; }
        public string Expiredate { get; set; }
        public string TicketIdCount { get; set; }
        public string PackNoCount { get; set; }
        public string PackPositionCount { get; set; }
        public int SettlementDays { get; set; }

        public int ShopTime { get; set; }
        public int HasAtvm { get; set; }
        public string Registerdate { get; set; }
    }

    //public class LoginList
    //{

    //    public bool status { get; set; }
    //    public User user { get; set; }
    //    public List<Shop> shops { get; set; }
    //}

    public class Result
    {
        public bool Status { get; set; }
        public string Description { get; set; }
        public string DateTime { get; set; }

    }
    public class ResultWithOldTicket
    {
        public bool Status { get; set; }
        public string Description { get; set; }
        public string DateTime { get; set; }

        public ReportData OldTicketInfo { get; set; }
    }

    public class ResultWithBox
    {
        public bool Status { get; set; }
        public string Description { get; set; }
        public string DateTime { get; set; }
        public List<int> BoxList { get; set; }
    }

    public class TicketDetails
    {
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int MaxNo { get; set; }
        public int MinNo { get; set; }
        public int Value { get; set; }
        public long TicketInventoryId { get; set; }
        public string ExpDate { get; set; }

    }

    public class TicketStatus
    {
        public bool Status { get; set; }
        public List<TicketDetails> TicketInfo { get; set; }

    }

    public class ViewTicket
    {
        public long TicketInventoryId { get; set; }
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public string InventoryDate { get; set; }
        public int rank { get; set; }

    }

    public class ViewTicketList
    {
        public List<ViewTicket> ViewInventoryList { get; set; }
        public int NoTicketInInvetory { get; set; }
        public double TotalInventoryValue { get; set; }
    }

    public class ReportData
    {
        public long ReportId { get; set; }
        public long TicketInventoryId { get; set; }
        public int Box { get; set; }
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public int OpenNo { get; set; }
        public int CloseNo { get; set; }
        public int Total { get; set; }
        public int ActiveAge { get; set; }
        public int InactiveStatus { get; set; }
        public int RetuenStatus { get; set; }
        public int SoldoutStatus { get; set; }
        public int TransactionId { get; set; }
        public int MaxNo { get; set; }
        public int MinNo { get; set; }
        public int Count { get; set; }
        public int rank { get; set; }
        public string SalePercentage { get; set; }
        

    }
    public class SpecificTicket
    {
        public long TicketInventoryId { get; set; }
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public int Box { get; set; }
        public string ActiveDate { get; set; }
        public int rank { get; set; }


    }


    public class ReportDataList
    {

        public Result ExpiryCheck { get; set; }

        public List<ReportData> ShiftData { get; set; }
        public List<SpecificTicket> ActiveData { get; set; }
        public List<SpecificTicket> InactiveData { get; set; }
        public List<SpecificTicket> ReturnData { get; set; }
        public List<SpecificTicket> SoldOutData { get; set; }
        public double InstantSales { get; set; }
        public double InstantCashout { get; set; }
        public double OnlineSales { get; set; }
        public double OnlineCashout { get; set; }
        public double Debit { get; set; }
        public double Credit { get; set; }
        public double Topup { get; set; }
        public double TopUpCancel { get; set; }
        public string StartDate { get; set; }
        public string CloseDate { get; set; }
        public double ActiveTicketNo { get; set; }
        public double InventoryTicketNo { get; set; }
        public double SystemActiveTicketNo { get; set; }
        public double SystemActiveTicketValue { get; set; }
        public double SystemInventoryTicketNo { get; set; }
        public double SystemInventoryTicketValue { get; set; }
        public double CashInHand { get; set; }

        public double DayInstantSales { get; set; }
        public double DayInstantCashout { get; set; }
        public double DayOnlineSales { get; set; }
        public double DayOnlineCashout { get; set; }
        public double DayDebit { get; set; }
        public double DayCredit { get; set; }
        public double DayTopup { get; set; }
        public double DayTopUpCancel { get; set; }

        public double InstantProfit { get; set; }
        public double OnlineProfit { get; set; }
        public double DayInstantProfit { get; set; }
        public double DayOnlineProfit { get; set; }
        public string ActualCloseDate { get; set; }
        public string EmptyBoxes { get; set; }

        //vignesh
        public double Issued { get; set; }
        public double Loan { get; set; }
        
    }
 
   
    public class SettlementTicket
    {
        public long TicketInventoryId { get; set; }
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public int SettlementStatus { get; set; }
        public string SettlementDate { get; set; }
        public int rank { get; set; }
        public string SettlementStatusChar { get; set; }
    }
    public class ResultWithSettlementTicket
    {
        public bool Status { get; set; }
        public string Description { get; set; }
        public List<SettlementTicket> SettlementTicket { get; set; }

    }

    public class emaillist
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public long EmailId { get; set; }
    }

    public class Emails
    {
        public long EmailId { get; set; }
        public string Email { get; set; }

    }

    public class ReportIdList
    {
        public long ReportId { get; set; }
        public string shift { get; set; }

    }

    public class Report
    {
        public double InstantSales { get; set; }
        public double InstantCashout { get; set; }
        public double OnlineSales { get; set; }

        public double OnlineCashout { get; set; }
        public double Debit { get; set; }
        public double Credit { get; set; }
        public double Topup { get; set; }
        public double TopUpCancel { get; set; }
        public int ActtiveNo { get; set; }
        public int SettlementNo { get; set; }
        public string Day { get; set; }
        public double OnlineProfit { get; set; }
        public double InstantProfit { get; set; }
        public int rank { get; set; }

        public double ActualMoneyDrop { get; set; }
        public double CalculatedMoneyDrop { get; set; }
        public double ShortOrOver { get; set; }
        // public string DayEnd { get; set; }


    }
    public class AITicket
    {
        public int TicketReferenceId { get; set; }
        public string TicketId { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public double Point { get; set; }
        public int NoInventoryTickets { get; set; }
        public int Rank { get; set; }
        public int TotalSales { get; set; }

    }

    public class AITicketList
    {
        public List<string> Value { get; set; }
        public Dictionary<string, List<AITicket>> Data { get; set; }
    }

    public class SellingTickets
    {
        public List<string> ValuesList { get; set; }
        public string Data { get; set; }
    }

    public class PieData
    {
        public string Value { get; set; }
        public double Space { get; set; }
        public double Sales { get; set; }
        public string SalesPercentage { get; set; }
        public string SpacePercentage { get; set; }
        public string Result { get; set; }
        public string Difference { get; set; }
        public int rank { get; set; }
    }

    public class AIOrder
    {
        public long TicketReferenceId { get; set; }
        public string TicketId { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public double SalesPerWeek { get; set; }
        public int TicketInInventory { get; set; }
        public int NextWeekOrder { get; set; }
        public int rank { get; set; }

    }

    public class BoxCount
    {
        public int Value { get; set; }
        public int Count { get; set; }

    }

    public class ShopDetails
    {
        public string LocationName { get; set; }
        public string LegalName { get; set; }
        public string StreetNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }

    public class TicketsView
    {
        public long TicketInventoryId { get; set; }
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public string Date { get; set; }
        public int rank { get; set;}


    }
    public class PieData2
    {

        public string Box { get; set; }
        public double Space { get; set; }
        public string SpacePercentage { get; set; }

        public int Sales { get; set; }
        public string SalesPercentage { get; set; }
        public string Result { get; set; }
        public string TicketId { get; set; }
        public string Difference { get; set; }
        public int rank { get; set; }
        public string LessBox { get; set; }


    }
    public class SpaceDivision
    {
        public int BoxCount { get; set; }
        public int Value { get; set; }
    }
    public class AutoComplete
    {
        public List<string> ticketIdList { get; set; }
        public Result result { get; set; }
    }


    public class GetPackNo
    {
        public string PackNo { get; set; }

    }


    public class ReportDataATVM : INotifyPropertyChanged
    {
        public long ReportId { get; set; }
        public long TicketInventoryId { get; set; }
        public int Box { get; set; }
        public string TicketId { get; set; }
        public string PackNo { get; set; }
        public string TicketName { get; set; }
        public int Value { get; set; }
        public int OpenNo { get; set; }
        public int CloseNo { get; set; }
        public int Total { get; set; }
        public int ActiveAge { get; set; }
        public int InactiveStatus { get; set; }
        public int RetuenStatus { get; set; }
        public int SoldoutStatus { get; set; }
        public int TransactionId { get; set; }
        public int MaxNo { get; set; }
        public int MinNo { get; set; }
        private int _count;
        public int Count
        {
            get { return _count; }
            set
            {
                if (this._count != value)
                {
                    this._count = value;
                    this.CloseNo = this.OpenNo + _count;
                    this.Total = this.Value * _count;

                    this.NotifyPropertyChanged("Name");
                    this.NotifyPropertyChanged("Total");
                    this.NotifyPropertyChanged("CloseNo");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        public int rank { get; set; }
        public string SalePercentage { get; set; }


    }


    public class Vendors
    {
        public long Rank { get; set; }
        public long VendorID { get; set; }
        public string VendorName { get; set; }
        public string Department { get; set; }
        public string ContactNumber { get; set; }

        public string EmailId { get; set; }

    }
    public class Payouts
    {
        public long Rank { get; set; }      

        public int PayoutId { get; set; }
        public int VendorId { get; set; }
        public int DepartmentId { get; set; }
        public string VendorName { get; set; }
        public string DeptName { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string InvoiceNo { get; set; }
        public string ModeOfPayment { get; set; }
        public double TotalAmount { get; set; }
        public string InvoiceImage { get; set; }
        public ImageSource imageSource { get; set; }

    }
    public class Loans
    {
        public long Rank { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }       
        public string ModeOfPayment { get; set; }
        public string Towards { get; set; }
        public string Amount { get; set; }
    }
    public class Departments
    {
        public long Rank { get; set; }
        public long DeptId { get; set; }
        public string Dept { get; set; }
     
    }
}
