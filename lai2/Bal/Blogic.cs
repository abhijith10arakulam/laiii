﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using lai2.Bal;
using System.Windows;
using System.Windows.Navigation;
using System.IO;
using Newtonsoft.Json.Linq;
using lai2.Properties;
using System.Collections.Specialized;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace lai2.Bal
{

    class Blogic
    {
        public static string opendate { get; set; }
        public static int aifalg { get; set; }
        public static int updateInventoryfalg { get; set; }
        public static bool closeShiftButtonStatus { get; set; }
        public static int ExpireFlag { get; set; }
        public static int CurrentRow { get; set; }

        public static AITicketList aishop { get; set; }
        public static AITicketList aishopDaily { get; set; }
        public static AITicketList aishopMontly { get; set; }
        public static List<AIOrder> aiNextweekorder { get; set; }

        public static bool IsAtvm { get; set; }
        public static int  TempBoxCount { get; set; }

        //public string baseUrl = "https://www.realtnetworking.com/API/LAI2";
        public string baseUrl = "http://localhost/lai";
        
//public string baseUrl = "http://localhost:53766";

        //public string ShopManeger { get; set; }
        //public string ShopAdmin { get; set; }
        LoginList l = new LoginList();

        LatitudeList ll = new LatitudeList();
        User u = new User();
        Shop s = new Shop();
        private Settings settings = Properties.Settings.Default;
        // UserDetails ud = new UserDetails();

        //Complete cp = new Complete();
       
        public bool internetconnection()
        {
            var client = new WebClient();
            try
            {
               // var link = client.DownloadString(new Uri(baseUrl+"/Test.aspx"));
                return true;
            }
            catch
            {
                return false;
            }
        }


        public LoginList LoginCheck(string username, string password)
        {
            l.user = new User();
            //var request = (HttpWebRequest)WebRequest.Create("http://ifconfig.me");          //Global ip 
            //request.UserAgent = "curl"; // this simulate curl linux command
            //string publicIPAddress;
            //request.Method = "GET";
            //using (WebResponse response = request.GetResponse())
            //{
            //    using (var reader = new StreamReader(response.GetResponseStream()))
            //    {
            //        publicIPAddress = reader.ReadToEnd();                               //contains the ip address
            //    }
            //}

            //var client = new WebClient();
            //var latresponse = client.DownloadString(new Uri("http://iptolatlng.com?ip=" + publicIPAddress));      //contains the latitude and longitude of the given ip
            //ll = JsonConvert.DeserializeObject<LatitudeList>(latresponse);
            var client = new WebClient();
            ll.lat = 0.0;
            ll.lng = 0.0;
            try
            {
                string link = baseUrl + "/Login.aspx?UserName=" + username + "&Passcode=" + password + "&Lattitude=" + ll.lat + "&Longitude=" + ll.lng + GetAtvmStatus();
                var logresponse = client.DownloadString(new Uri(link));
                if (logresponse != null)
                {
                    l = JsonConvert.DeserializeObject<LoginList>(logresponse);            //logresponse contains the json formated o/p
                    LoginDetail.shops = l.shops;
                    LoginDetail.status = l.status;
                    LoginDetail.user = l.user;
                    for (int i = 0; i < LoginDetail.shops.Count; i++)
                    {
                        LoginDetail.shops[i].rank = i + 1;
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return l;
        }


        public ReportDataList TodaysData(string date, string time)
        {
            ReportDataList rd = new ReportDataList();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/GetLatestData.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + date + "&Time=" + time + GetAtvmStatus();
                var reportresponse = client.DownloadString(new Uri(link));


                if (reportresponse != null)
                {
                    rd = JsonConvert.DeserializeObject<ReportDataList>(reportresponse);

                    for (int i = 0; i < rd.ActiveData.Count; i++)
                    {
                        rd.ActiveData[i].rank = i + 1;
                    }
                    for (int i = 0; i < rd.InactiveData.Count; i++)
                    {
                        rd.InactiveData[i].rank = i + 1;
                    }
                    for (int i = 0; i < rd.ReturnData.Count; i++)
                    {
                        rd.ReturnData[i].rank = i + 1;
                    }
                    for (int i = 0; i < rd.SoldOutData.Count; i++)
                    {
                        rd.SoldOutData[i].rank = i + 1;
                    }

                    for (int i = 0; i < rd.ShiftData.Count; i++)
                    {
                        rd.ShiftData[i].rank = i + 1;
                        double percentge = (rd.ShiftData[i].OpenNo * 100 / (rd.ShiftData[i].MaxNo + 1));

                      //  if (rd.ShiftData[i].ActiveAge > 0 && rd.ShiftData[i].ReportId == rd.ShiftData[i].Activer)

                        if (rd.ShiftData[i].SoldoutStatus == 1)
                        {
                            rd.ShiftData[i].ActiveAge = -51;
                        }
                        else if (rd.ShiftData[i].InactiveStatus == 1)
                        {
                            rd.ShiftData[i].ActiveAge = -52;
                        }
                        else if (rd.ShiftData[i].RetuenStatus == 1)
                        {
                            rd.ShiftData[i].ActiveAge = -53;
                        }

                        rd.ShiftData[i].SalePercentage = rd.ShiftData[i].ActiveAge + "/" + percentge;

                    }
                }
            }
            catch
            { }
            return rd;
        }

        public ViewTicketList ViewInventoryDetails()
        {
            ViewTicketList td = new ViewTicketList();
            try
            {
                var viewclient = new WebClient();
                var viewresponse = viewclient.DownloadString(new Uri(baseUrl + "/ViewInventory.aspx?UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (viewresponse != null)
                {
                    td = JsonConvert.DeserializeObject<ViewTicketList>(viewresponse);
                    for (int i = 0; i < td.ViewInventoryList.Count; i++)
                    {
                        td.ViewInventoryList[i].rank = i + 1;
                    }
                }
            }
            catch { }
            return td;
        }

        internal TicketStatus CheckTicketInfo(string ticketid)
        {
            TicketStatus tds = new TicketStatus();
            try
            {
                var client = new WebClient();
                var inforesponse = client.DownloadString(new Uri(baseUrl + "/GetTicketInfo.aspx?TicketId=" + ticketid + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (inforesponse != null)
                {
                    tds = JsonConvert.DeserializeObject<TicketStatus>(inforesponse);
                }
            }
            catch { }
            return tds;
        }

        internal Result updatestatus(string p1, string p2, string p3, string p4, string p5, string minNo,string date, string expDate)        //update Inventory
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string s = baseUrl + "/UpdateInventory.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&TicketName=" + p3 + "&MaxNo=" + p4 + "&MinNo=" + minNo + "&Value=" + p5 + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + date + "&ExpDate=" + expDate + "" + GetAtvmStatus();
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(updateresponse);

                }
            }
            catch { }
            return r;
        }

        internal Result deletedata(long p)
        {
            Result r1 = new Result();
            try
            {
                var client = new WebClient();
                string s = baseUrl + "/DeleteInventory.aspx?TicketInventoryId=" + p + "&UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var deleterespponse = client.DownloadString(new Uri(s));
                if (deleterespponse != null)
                {
                    r1 = JsonConvert.DeserializeObject<Result>(deleterespponse);
                }
            }
            catch { }
            return r1;
        }


        internal ScanData GetTicket(string barcode)
        {
            ScanData tt = new ScanData();
            switch (LoginDetail.shops[IndexFinder.index].State)
            {
                case "New Jersey":
                    {
                        barcode = barcode.Substring(0, barcode.Length - 10);

                        if (barcode.Length == 14)
                        {
                            tt.TicketId = Convert.ToString(barcode.Substring(0, 5));
                            tt.PackNo = barcode.Substring(5, 6);
                            tt.PackPosition = Convert.ToInt32(barcode.Substring(11, 3));

                        }
                        break;
                    }

                case "Kentucky":
                    {
                        barcode = barcode.Substring(0, barcode.Length - 11);

                        if (barcode.Length == 13)
                        {
                            tt.TicketId = Convert.ToString(barcode.Substring(0, 3));
                            tt.PackNo = barcode.Substring(4, 6);
                            tt.PackPosition = Convert.ToInt32(barcode.Substring(10, 3));

                        }
                        break;
                    }

                case "Illinois":

                case "North Carolina":
                    {
                        barcode = barcode.Substring(0, barcode.Length - 8);

                        if (barcode.Length == 12)
                        {
                            tt.TicketId = Convert.ToString(barcode.Substring(0, 3));
                            tt.PackNo = barcode.Substring(3, 6);
                            tt.PackPosition = Convert.ToInt32(barcode.Substring(9, 3));

                        }
                        break;
                    }
                case "Ohio":
                    {
                        barcode = barcode.Substring(0, barcode.Length - 6);

                        if (barcode.Length == 12)
                        {
                            tt.TicketId = Convert.ToString(barcode.Substring(0, 3));
                            tt.PackNo = barcode.Substring(3, 6);
                            tt.PackPosition = Convert.ToInt32(barcode.Substring(9, 3));

                        }
                        break;
                    }

                case "Virginia":
                        {
                            barcode = barcode.Substring(0, barcode.Length - 11);

                            if (barcode.Length == 13)
                            {
                                tt.TicketId = Convert.ToString(barcode.Substring(0, 4));
                                tt.PackNo = barcode.Substring(4, 6);
                                tt.PackPosition = Convert.ToInt32(barcode.Substring(10, 3));

                            }
                            break;

                        }

                default:
                    {
                        barcode = barcode.Substring(0, barcode.Length - 10);

                        if (barcode.Length == 14)
                        {
                            tt.TicketId = Convert.ToString(barcode.Substring(0, 4));
                            tt.PackNo = barcode.Substring(4, 7);
                            tt.PackPosition = Convert.ToInt32(barcode.Substring(11, 3));

                        }
                        else if (barcode.Length == 12)
                        {
                            tt.TicketId = Convert.ToString(barcode.Substring(0, 3));
                            tt.PackNo = barcode.Substring(3, 6);
                            tt.PackPosition = Convert.ToInt32(barcode.Substring(9, 3));
                        }
                        break;
                    }
            }

            return tt;
        }

        internal TicketStatus ActiveTickets_Fill(string p)
        {
            TicketStatus ts = new TicketStatus();
            try
            {
                string jp = baseUrl + "/GetTicketInfo.aspx?TicketId=" + p + "&UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/GetTicketInfo.aspx?TicketId=" + p + "&UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    ts = JsonConvert.DeserializeObject<TicketStatus>(ticketinforesponse);
                }
            }
            catch { }
            return ts;
        }

        internal ResultWithOldTicket Activate_Tickets(string p1, string p2, string p3, string p4, string p5, int ScanOrder)
        {
            ResultWithOldTicket rt = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ActiveTicket.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&PackPosition=" + p3 + "&Box=" + p4 + "&Date=" + p5 + " &ScanOrder=" + ScanOrder + "&UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithOldTicket>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal List<SpecificTicket> ActiveTickets_view()
        {
            List<SpecificTicket> st = new List<SpecificTicket>();
            try
            {
                var client = new WebClient();
                var activeticketresponse = client.DownloadString(new Uri(baseUrl + "/ViewActiveTickets.aspx?UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (activeticketresponse != null)
                {
                    st = JsonConvert.DeserializeObject<List<SpecificTicket>>(activeticketresponse);
                    for (int i = 0; i < st.Count; i++)
                    {
                        st[i].rank = i + 1;
                    }

                }
            }
            catch { }
            return st;
        }



        internal Result InActivate_Tickets(string p1, string p2, string p3, string s)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                var link = baseUrl + "/InactiveTicket.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&PackPosition=" + p3 + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + s + GetAtvmStatus();
                var Inactiveticketresponse = client.DownloadString(new Uri(link));
                if (Inactiveticketresponse != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(Inactiveticketresponse);
                }
            }
            catch { }
            return r;
        }


        internal ResultWithOldTicket InActiveTickets_Fill(string p1, string p2)
        {
            ResultWithOldTicket r = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/GetTicketDetailsForSoldout.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus();
                var Inactiveticketfill = client.DownloadString(new Uri(link));
                if (Inactiveticketfill != null)
                {
                    r = JsonConvert.DeserializeObject<ResultWithOldTicket>(Inactiveticketfill);
                }
            }
            catch { } return r;
        }

        internal List<TicketsView> ViewInactive_Tickets(string fromdate, string todate)
        {
            List<TicketsView> vt = new List<TicketsView>();
            try
            {
                var client = new WebClient();
                var ViewInactiveticket = client.DownloadString(new Uri(baseUrl + "/ViewInactiveTickets.aspx?FromDate=" + fromdate + "&ToDate=" + todate + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ViewInactiveticket != null)
                {
                    vt = JsonConvert.DeserializeObject<List<TicketsView>>(ViewInactiveticket);
                    for (int i = 0; i < vt.Count; i++)
                    {
                        vt[i].rank = i + 1;
                    }

                }
            }
            catch { }
            return vt;
        }

        internal Result Return_Tickets(string p1, string p2, string p3, string s)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string temp = baseUrl + "/ReturnTicket.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&PackPosition=" + p3 + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + s + GetAtvmStatus();
                var Status = client.DownloadString(new Uri(temp));
                if (Status != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(Status);
                }
            }
            catch { }
            return r;
        }

        internal List<TicketsView> ViewActive(string fromdate, string todate)
        {
            List<TicketsView> vt = new List<TicketsView>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewReturnTicket.aspx?FromDate=" + fromdate + "&ToDate=" + todate + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus();
                var ViewInactiveticket = client.DownloadString(new Uri(link));
                if (ViewInactiveticket != null)
                {
                    vt = JsonConvert.DeserializeObject<List<TicketsView>>(ViewInactiveticket);
                    for (int i = 0; i < vt.Count; i++)
                    {
                        vt[i].rank = i + 1;
                    }
                }
            }
            catch { }
            return vt;
        }

        internal Result SoldOut_Tickets(string p1, string p2, string p3, string s)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string temp = baseUrl + "/SoldoutTicket.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + s + GetAtvmStatus();
                var Status = client.DownloadString(new Uri(temp));
                if (Status != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(Status);
                }
            }
            catch { }
            return r;
        }

        internal List<TicketsView> ViewSoldOut_Tickets(string fromdate, string todate)
        {
            List<TicketsView> vt = new List<TicketsView>();
            try
            {
                var client = new WebClient();
                var ViewInactiveticket = client.DownloadString(new Uri(baseUrl + "/ViewSoldoutTicket.aspx?FromDate=" + fromdate + "&ToDate=" + todate + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ViewInactiveticket != null)
                {
                    vt = JsonConvert.DeserializeObject<List<TicketsView>>(ViewInactiveticket);
                    for (int i = 0; i < vt.Count; i++)
                    {
                        vt[i].rank = i + 1;
                    }
                }
            }
            catch { }
            return vt;
        }

        internal ResultWithSettlementTicket Settlementby_date(string fromdate, string todate)
        {
            ResultWithSettlementTicket rt = new ResultWithSettlementTicket();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl+"/ViewsettlementByDate.aspx?FromDate=" + fromdate + "&ToDate=" + todate + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithSettlementTicket>(value);
                    for (int i = 0; i < rt.SettlementTicket.Count; i++)
                    {
                        rt.SettlementTicket[i].rank = i + 1;
                    }
                }
            }
            catch { }
            return rt;
        }

        internal ResultWithSettlementTicket Settlementby_week(string date)
        {
            ResultWithSettlementTicket rt = new ResultWithSettlementTicket();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/ViewsettlementByWeek.aspx?Date=" + date + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithSettlementTicket>(value);
                    for (int i = 0; i < rt.SettlementTicket.Count; i++)
                    {
                        rt.SettlementTicket[i].rank = i + 1;
                    }
                }
            }
            catch { }
            return rt;
        }

        internal TicketStatus Settlementtxt_autofill(string p)
        {
            TicketStatus ts = new TicketStatus();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/GetTicketInfo.aspx?TicketId=" + p + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    ts = JsonConvert.DeserializeObject<TicketStatus>(value);
                }
            }
            catch { }
            return ts;
        }

        internal Result Settlement_save(string p1, string p2, string s)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/TicketSettlement.aspx?TicketId=" + p1 + "&PackNo=" + p2 + "&Date=" + s + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        internal ResultWithOldTicket AutoFill_BeggingNo(string box)
        {
            ResultWithOldTicket r = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/GetTicketByBox.aspx?Box=" + box + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<ResultWithOldTicket>(value);
                }
            }
            catch { }
            return r;
        }

        internal Result CheckStatus_FixBeggingNo(string tcktid, string packno, string packposition)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/FixOpeningNo.aspx?TicketId=" + tcktid + "&PackNo=" + packno + "&PackPosition=" + packposition + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        internal ResultWithOldTicket AutoFill_ChangeBox(string box)
        {
            ResultWithOldTicket r = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/GetTicketByBox.aspx?Box=" + box + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<ResultWithOldTicket>(value);
                }
            }
            catch { }
            return r;
        }

        internal ResultWithOldTicket CheckStatus_ChangeBox(string tcktid, string packno, string packpositionclose, string changeBox, string date)
        {
            ResultWithOldTicket r = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ChangeBox.aspx?TicketId=" + tcktid + "&PackNo=" + packno + "&PackPosition=" + packpositionclose + "&NewBox=" + changeBox + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + date + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<ResultWithOldTicket>(value);
                }
            }
            catch { }
            return r;
        }

        internal ResultWithOldTicket CloseShiftAuto_fill(string ticketid, string packno, string packpositionopen)
        {
            ResultWithOldTicket r = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string uri = baseUrl+"/CloseTicket.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&PackPosition=" + packpositionopen + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId;
                var value = client.DownloadString(new Uri(baseUrl+"/CloseTicket.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&PackPosition=" + packpositionopen + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<ResultWithOldTicket>(value);
                }
            }
            catch { }
            return r;
        }

        internal ResultWithBox ViewSkippedBox()
        {
            ResultWithBox ro = new ResultWithBox();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/ViewSkipedBox.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (value != null)
                {
                    ro = JsonConvert.DeserializeObject<ResultWithBox>(value);
                }
            }
            catch { }
            return ro;
        }

        internal ResultWithBox EmptyBox()
        {
            ResultWithBox rb = new ResultWithBox();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewEmptyBox.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    rb = JsonConvert.DeserializeObject<ResultWithBox>(value);
                }
            }
            catch { }
            return rb;
        }

        internal string EmptyBox_Report(string reportId="new")
        {
            string boxes = "";
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/Emptybox_Report.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&ReportId=" + reportId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    boxes = value;
                }
            }
            catch { }
            return boxes;
        }


        internal Result CloseShiftStatus_check(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string total, string cashinhand, string closedate, string credit, string debit, string topup, string topupcancel, string date, string time, string loan, string issued)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/CloseShift.aspx?InstantCashOut=" + Cashout + "&OnlineSale=" + onlinesale + "&OnlineCashOut=" + onlineCashout + "&CashInHand=" + cashinhand + "&ActiveTicketNo=" + active + "&InventoryTicketNo=" + inventory + "&Credit=" + credit + "&Debit=" + debit + "&TopUp=" + topup + "&TopUpCancel=" + topupcancel + "&Date=" + date + "&Time=" + time + "&CloseDate=" + closedate + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&Loan=" + loan + "&Issued=" + issued + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch
            { }
            return r;
        }

        internal Result AddEmail(string email)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/AddEmail.aspx?Email=" + email + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        internal List<emaillist> View_Emails()
        {
            List<emaillist> e = new List<emaillist>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewEmail.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    e = JsonConvert.DeserializeObject<List<emaillist>>(value);
                }
                for (int i = 0; i < e.Count; i++)
                {
                    e[i].Id = i + 1;

                }
            }
            catch { }
            return e;
        }

        internal Result delete_emails(string p1, long p2)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/DeleteEmail.aspx?EmailId=" + p2 + "&Email=" + p1 + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { } return r;
        }

        internal Result UpdateInventoryAge(string inventoryage)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/UpdateInventoryAge.aspx?InventoryAge=" + inventoryage + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        internal Result Updateboxage(string boxage)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/UpdateBoxAge.aspx?BoxAge=" + boxage + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        internal Result Change_Password(string oldpsswrd, string nwpsrwrd, string confirmpsrd)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ChangePassword.aspx?NewPassword=" + nwpsrwrd + "&OldPassword=" + oldpsswrd + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        //internal Result AddEmployee(string username, string password)
        //{
        //   //  Result r = new Result();
        // //   var client = new WebClient();
        //   //string link = baseUrl+"/AddUser.aspx?UserName=" +username+ "&Password=" +password+ "&UserTypeId=" ++ "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId;
        //   // var value = client.DownloadString(new Uri(link));
        //   // r = JsonConvert.DeserializeObject<Result>(value);
        //   // return r;
        //}

        internal List<User> viewAddEmployee()
        {
            List<User> r = new List<User>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewUsers.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<User>>(value);
                }
            }
            catch { }
            return r;
        }
        internal List<Departments> viewAddDepartment()
        {
            List<Departments> r = new List<Departments>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/DeptView.aspx?departmentId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<Departments>>(value);
                }
            }
            catch { }
            return r;
        }
        internal List<User> viewAddVendor()
        {
            List<User> r = new List<User>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewUsers.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<User>>(value);
                }
            }
            catch { }
            return r;
        }
        internal List<User> viewaddPayout()
        {
            List<User> r = new List<User>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewUsers.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<User>>(value);
                }
            }
            catch { }
            return r;
        }
        internal List<User> viewaddLoan()
        {
            List<User> r = new List<User>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/ViewUsers.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<User>>(value);
                }
            }
            catch { }
            return r;
        }
        internal Result DeleteAddedEmployees(string deleteUserid)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/DeleteUser.aspx?DeleteUserId=" + deleteUserid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }


        internal Result AddEmployees(string username, string password, int usertypeid)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/AddUser.aspx?UserName=" + username + "&Password=" + password + "&UserTypeId=" + usertypeid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }

        internal Result UpdateBoxNumber(string Boxnumber)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/UpdateBoxcount.aspx?Boxcount=" + Boxnumber + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;

        }

        internal AITicketList MostSellingTicket(string type, string date, string State_Shop)
        {


            AITicketList aITList = new AITicketList();


            if (type == null || date == null)
            {
                return aITList;
            }
            try
            {
                aITList.Data = new Dictionary<string, List<AITicket>>();
                var client = new WebClient();
                string link = baseUrl + "/GetMostSellingTicketValue.aspx?Date=" + date + "&Type=" + type + "&State_Shop=" + State_Shop + "&Value=OverAll &UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var response = client.DownloadString(new Uri(link));
                response = response.Substring(1, response.Length - 2);
                string value = response.Substring(0, response.IndexOf('}') + 1);
                string data = response.Substring(response.IndexOf('}') + 2, response.Length - (value.Length + 1));
                value = value.Substring(value.IndexOf('['), value.Length - (value.IndexOf('[') + 1));
                List<string> valueList = JsonConvert.DeserializeObject<List<string>>(value);
                aITList.Value = valueList;
                JObject root = JObject.Parse(data);
                JArray items = (JArray)root["Data"];

                for (int i = 0; i < aITList.Value.Count; i++)
                {
                    JArray items1 = (JArray)items[i][aITList.Value[i]];
                    List<AITicket> tlist = JsonConvert.DeserializeObject<List<AITicket>>(items1.ToString());

                    for (int j = 0; j < tlist.Count; j++)
                    {
                        tlist[j].Rank = j + 1;
                    }
                    aITList.Data.Add(aITList.Value[i], tlist);

                }
            }
            catch
            { }
            return aITList;
        }

        internal List<ReportIdList> ViewShift(string date1)
        {
            List<ReportIdList> r = new List<ReportIdList>();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/GetReportIdsByDate.aspx?Date=" + date1 + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<ReportIdList>>(value);
                }
            }
            catch { }
            return r;
        }

        internal ReportDataList ShiftReportData(string reportid)
        {
            ReportDataList r = new ReportDataList();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/GetShiftReport.aspx?ReportId=" + reportid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<ReportDataList>(value);
                    for (int i = 0; i < r.ShiftData.Count; i++)
                    {
                        r.ShiftData[i].rank = i + 1;
                    }
                    for (int i = 0; i < r.ActiveData.Count; i++)
                    {
                        r.ActiveData[i].rank = i + 1;
                    }
                    for (int i = 0; i < r.InactiveData.Count; i++)
                    {
                        r.InactiveData[i].rank = i + 1;
                    }
                    for (int i = 0; i < r.SoldOutData.Count; i++)
                    {
                        r.SoldOutData[i].rank = i + 1;
                    }
                    for (int i = 0; i < r.ReturnData.Count; i++)
                    {
                        r.ReturnData[i].rank = i + 1;
                    }

                }
            }
            catch { }
            return r;
        }


        internal List<Report> weeklyReport(string date)
        {
            List<Report> r = new List<Report>();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/WeeklyReport.aspx?Date=" + date + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<Report>>(value);
                }
            }
            catch { }
            return r;
        }

        internal List<Report> MonthlyReport(string date)
        {
            List<Report> r = new List<Report>();
            try
            {
                var client = new WebClient();
                var value = client.DownloadString(new Uri(baseUrl + "/MonthlyReport.aspx?Date=" + date + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<List<Report>>(value);
                }
            }
            catch { }
            return r;
        }

        internal ResultWithOldTicket FullPackSoldOut_Tickets(string ticketid, string packno, string packpositionopen, string box, string date, int ScanOrder)
        {
            ResultWithOldTicket rt = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string uri = baseUrl + "/FullPackSoldout.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&Date=" + date + "&Box=" + box + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/FullPackSoldout.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&Date=" + date + "&Box=" + box + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithOldTicket>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result UpdateProfitPercentage(string instantProfit, string onlineprofit)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/UpdateProfit.aspx?InstantProfit=" + instantProfit + "&OnlineProfit=" + onlineprofit + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal List<AIOrder> AI_NextweekOrderDetails()
        {
            List<AIOrder> rt = new List<AIOrder>();
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/AINextWeekOrder.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<List<AIOrder>>(ticketinforesponse);
                    for (int i = 0; i < rt.Count; i++)
                    {
                        rt[i].rank = i + 1;
                    }

                }
            }
            catch { }
            return rt;
        }

        internal Result DeleteInventory(string deleteData)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/DeleteInventory.aspx?TicketInventoryId=" + deleteData + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal ResultWithOldTicket FixSoldOut(string ticketid, string packno, string packposition)
        {
            ResultWithOldTicket rt = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/FixSoldout_ReturnTicket.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&PackPosition=" + packposition + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithOldTicket>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }


        internal List<Report> WeeklyReportView(string fromdate, string todate)
        {
            List<Report> rt = new List<Report>();
            Report r = new Report();
            //double s = 0;
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/WeeklyReport.aspx?FromDate=" + fromdate + "&ToDate=" + todate + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<List<Report>>(ticketinforesponse);

                    for (int i = 0; i < rt.Count; i++)
                    {
                        rt[i].rank = i + 1;
                        r.InstantSales += rt[i].InstantSales;
                        r.InstantCashout += rt[i].InstantCashout;
                        r.OnlineSales += rt[i].OnlineSales;
                        r.OnlineCashout += rt[i].OnlineCashout;
                        r.ActtiveNo += rt[i].ActtiveNo;
                        r.SettlementNo += rt[i].SettlementNo;
                        r.InstantProfit += rt[i].InstantProfit;
                        r.OnlineProfit += rt[i].OnlineProfit;
                        r.Credit += rt[i].Credit;
                        r.Debit += rt[i].Debit;
                        r.ActualMoneyDrop += rt[i].ActualMoneyDrop;
                        r.CalculatedMoneyDrop += rt[i].CalculatedMoneyDrop;
                        r.ShortOrOver += rt[i].ShortOrOver;
                    }
                    r.Day = "Total";
                    rt.Add(r);

                }
            }
            catch { }
            return rt;
        }



        internal Result backtoinventory(string ticketid, string packno)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string url = baseUrl + "/BackToInventory.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(url));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal List<PieData> PieDataAI1(string date, string type)
        {
            List<PieData> data = new List<PieData>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/PieDataAI1.aspx?Date=" + date + "&Type=" + type + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    data = JsonConvert.DeserializeObject<List<PieData>>(ticketinforesponse);
                    for (int i = 0; i < data.Count; i++)
                    {
                        data[i].Difference = Convert.ToString((Convert.ToDouble(data[i].SalesPercentage)) - (Convert.ToDouble(data[i].SpacePercentage)));
                        data[i].Difference = (Math.Round(Convert.ToDouble(data[i].Difference.ToString()), 2, MidpointRounding.AwayFromZero)).ToString("F2");
                        data[i].rank = i + 1;
                        if (data[i].Difference.Contains("%"))
                        { }
                        else
                            data[i].Difference = data[i].Difference + "%";

                    }
                }
            }
            catch { }
            return data;
        }


        internal List<PieData2> PieDataAI2(string date, string type, string value)
        {
            List<PieData2> data = new List<PieData2>();
            try
            {
                var client = new WebClient();
                string link = string.Format(baseUrl + "/PieDataAI2.aspx?Date=" + date + "&Type=" + type + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Value=" + value + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/PieDataAI2.aspx?Date=" + date + "&Type=" + type + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Value=" + value + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    data = JsonConvert.DeserializeObject<List<PieData2>>(ticketinforesponse);
                    for (int i = 0; i < data.Count; i++)
                    {
                        data[i].Difference = Convert.ToString(Convert.ToDouble(data[i].SalesPercentage) - Convert.ToDouble(data[i].SpacePercentage));
                        data[i].Difference = (Math.Round(Convert.ToDouble(data[i].Difference.ToString()), 2, MidpointRounding.AwayFromZero)).ToString("F2");
                        data[i].rank = i + 1;
                        if (data[i].Difference.Contains("%"))
                        { }
                        else
                        {
                            data[i].Difference = data[i].Difference + "%";
                        }
                        if (data[i].SpacePercentage.Contains("%"))
                        { }
                        else
                        {
                            data[i].SpacePercentage = data[i].SpacePercentage + "%";
                        }
                        if (data[i].SalesPercentage.Contains("%"))
                        { }
                        else
                        {
                            data[i].SalesPercentage = data[i].SalesPercentage + "%";
                        }

                    }
                }
            }
            catch { }
            return data;
        }


        internal ResultWithOldTicket ManualCloseShift(string ticketid, string packno, string packposition)
        {
            ResultWithOldTicket rt = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                string linl = baseUrl + "/CloseTicket.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&PackPosition=" + packposition + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/CloseTicket.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&PackPosition=" + packposition + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithOldTicket>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal List<AITicket> Aisugestion(string box)
        {

            List<AITicket> rt = new List<AITicket>();

            if (box == "Box")
            {

                return rt;
            }
            try
            {
                var client = new WebClient();
                var link = baseUrl + "/AISugeestion.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Box=" + box + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<List<AITicket>>(ticketinforesponse);
                    for (int i = 0; i < rt.Count; i++)
                    {
                        rt[i].Rank = i + 1;
                    }
                }
            }
            catch { }
            return rt;
        }

        internal ResultWithOldTicket AddShiftReport_Tickets(string tbTicketId, string tbPackno, string tbPackPositionopen, string tbPackPositionclose, string cbboxcount, string reportid, string s, int ScanOrder)
        {
            ResultWithOldTicket rt = new ResultWithOldTicket();
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/ReportFixAddTicket.aspx?TicketId=" + tbTicketId + "&PackNo=" + tbPackno + "&PackPositionOpen=" + tbPackPositionopen + "&PackPositionClose=" + tbPackPositionclose + "&Box=" + cbboxcount + "&ScanOrder=" + ScanOrder + "&Date=" + s + "&ReportId=" + reportid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithOldTicket>(ticketinforesponse);
                }
            }
            catch(Exception e)
            { }
            return rt;
        }



        internal Result DeleteReport(string ticketinventoryid, string transactionid, string reportid)
        {
            string date = DateTime.Now.ToString("yyyy/MM/dd");
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string url = baseUrl + "/ReportFixDelete.aspx?ReportId=" + reportid + "&TicketInventoryId=" + ticketinventoryid + "&TransactionId=" + transactionid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + date + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/ReportFixDelete.aspx?ReportId=" + reportid + "&TicketInventoryId=" + ticketinventoryid + "&TransactionId=" + transactionid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&Date=" + date + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;


        }

        internal Result UpdateAiresultPercentage(string inventoryage)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/UpdateAIResultPercetage.aspx?AIResultPercetage" + inventoryage + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result Ascendingorder(int ticketvalueorder)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/UpdateTicketValueOrder.aspx?TicketValueOrder=" + ticketvalueorder + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result FactoryReset()
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/FactoryReset.aspx?" + "ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/FactoryReset.aspx?" + "ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result AddShop(string username, string password, string shopEmail)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/AddExstingShop.aspx?UserName=" + username + "&Password=" + password + "&ShopEmail=" + shopEmail + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(baseUrl + "/AddExstingShop.aspx?UserName=" + username + "&Password=" + password + "&ShopEmail=" + shopEmail + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus()));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result RemoveShop()
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/RemoveShop.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }



        internal bool CheckPackNoLenght(int ticketPackNoLenght)
        {
            switch (LoginDetail.shops[IndexFinder.index].State)
            {
                case "New Jersey":
                    {
                        if (ticketPackNoLenght == 6)
                            return true;
                        else

                            return false;
                        break;
                    }

                case "Kentucky":
                    {
                        if (ticketPackNoLenght == 6)
                            return true;
                        else

                            return false;
                        break;
                    }

                default:
                    {
                        if (ticketPackNoLenght == 6 || ticketPackNoLenght == 7)
                            return true;
                        else

                            return false;
                        break;

                    }
            }
        }
        internal bool CheckTicketIdLenght(int ticketIdLenght)
        {
            switch (LoginDetail.shops[IndexFinder.index].State)
            {
                case "New Jersey":
                    {
                        if (ticketIdLenght == 5)
                            return true;
                        else

                            return false;
                        break;
                    }

                case "Kentucky":
                    {
                        if (ticketIdLenght == 3)
                            return true;
                        else

                            return false;
                        break;
                    }

                default:
                    {
                        if (ticketIdLenght == 3 || ticketIdLenght == 4)
                            return true;
                        else

                            return false;
                        break;

                    }
            }
        }

        internal Result UpdateSettlementDays(string setlementDays)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateSettlementDays.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&SettlementDays=" + setlementDays + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }


        internal Result EditReportData(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/EditReportData.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=" + Cashout + "&OnlineSale=" + onlinesale + "&OnlineCashOut=" + onlineCashout + "&CashInHand=" + cashinhand + "&ActiveTicketNo=" + active + "&InventoryTicketNo=" + inventory + "&Credit=" + credit + "&Debit=" + debit + "&TopUp=" + topup + "&TopUpCancel=" + topupcancel + "&ReportId=" + reportId + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal void SendDeviceDetails(string device, string version)
        {
            // Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/SendDeviceInfo.aspx?DeviceToken=" + device + "&Version=" + version + "&OS=Windows" + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    //  rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }


        }

        internal Result FixInactiveCloseNo(string ticketid, string packno, string packposition)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/FixInactiveTicket.aspx?TicketId=" + ticketid + "&PackNo=" + packno + "&PackPosition=" + packposition + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal AutoComplete AutoCompliteData(int operation = 1)
        {
            AutoComplete rt = new AutoComplete();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/AutoCompleteActiveTicket.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&TicketId=" + "&operation=" + operation + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<AutoComplete>(ticketinforesponse);
                }
            }
            catch { }

            return rt;
        }

        internal Result RecoverLastShift()
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/RecoverShift.aspx?userid=" + LoginDetail.user.UserId + "&shopid=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;


        }

        internal List<GetPackNo> GetPackNos(string data, int from = 1)
        {
            List<GetPackNo> packnos = new List<GetPackNo>();

            List<string> str_packno = new List<string>();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/GetPackNo_TicketId.aspx?TicketId=" + data + "&shopid=" + LoginDetail.shops[IndexFinder.index].ShopId + "&ticketStatus=" + from + GetAtvmStatus();
                var ticketinforesponse = client.DownloadString(new Uri(link));
                if (ticketinforesponse != null)
                {
                    str_packno = JsonConvert.DeserializeObject<List<string>>(ticketinforesponse);
                    foreach (string packno in str_packno)
                    {
                        GetPackNo rt = new GetPackNo();
                        rt.PackNo = packno;
                        packnos.Add(rt);
                    }
                }
            }
            catch { }
            return packnos;
        }

        internal Result UpdateStatus_PrintReport(int shopId, int flag)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string s = baseUrl + "/PrintReportSetttings.aspx?ShopId=" + shopId + "&Status=" + flag + GetAtvmStatus();
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(updateresponse);

                }
            }
            catch { }
            return r;
        }

        internal int GetStatus_PrintReport(int shopId)
        {
            try
            {
                int status = 0;
                var client = new WebClient();
                string s = baseUrl + "/GetPrintReport_Status.aspx?ShopId=" + shopId + GetAtvmStatus();
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    status = JsonConvert.DeserializeObject<int>(updateresponse);
                }
                return status;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal Result UpdateStatus_AutoSettled_SoldOut(int shopId, int flag)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string s = baseUrl + "/PrintReportSetttings.aspx?ShopId=" + shopId + "&Status=" + flag + "&Opreation=1" + GetAtvmStatus();
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(updateresponse);

                }
            }
            catch { }
            return r;
        }

        internal int GetStatus_AutoSettled_SoldOut(int shopId)
        {
            try
            {
                int status = 0;
                var client = new WebClient();
                string s = baseUrl + "/GetPrintReport_Status.aspx?ShopId=" + shopId + "&Opreation=1" + GetAtvmStatus(); ;
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    status = JsonConvert.DeserializeObject<int>(updateresponse);
                }
                return status;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal void ResetSkipping()
        {
            try
            {
                Result rs = new Result();
                var client = new WebClient();
                string s = baseUrl + "/ResetScaning.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    rs = JsonConvert.DeserializeObject<Result>(updateresponse);
                }

            }
            catch (Exception e)
            {
               
            }
        }

        internal bool CheckScan()
        {
            Result rs = new Result();
            try
            {

                var client = new WebClient();
                string s = baseUrl + "/CheckStartScan.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var updateresponse = client.DownloadString(new Uri(s));
                if (updateresponse != null)
                {
                    rs = JsonConvert.DeserializeObject<Result>(updateresponse);
                }

            }
            catch (Exception e)
            {
              
            }
            return rs.Status;
        }

        internal Result UpdateOnlineSales(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "OnlineSale";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=0&OnlineSale=" + onlinesale + "&OnlineCashOut=0&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=0&Debit=0&TopUp=0&TopUpCancel=0&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result UpdateOnlineCashout(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "OnlineCashout";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=0&OnlineSale=0&OnlineCashOut=" + onlineCashout + "&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=0&Debit=0&TopUp=0&TopUpCancel=0&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result UpdateInstantCashout(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "InstantCashout";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=" + Cashout + "&OnlineSale=0&OnlineCashOut=0&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=0&Debit=0&TopUp=0&TopUpCancel=0&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());
                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result UpdateReport(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId, string loan, string issued)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportAll.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=" + Cashout + "&OnlineSale=" + onlinesale + "&OnlineCashOut=" + onlineCashout + "&CashInHand=" + cashinhand + "&ActiveTicketNo=" + active + "&InventoryTicketNo=" + inventory + "&Credit=" + credit + "&Debit=" + debit + "&TopUp=" + topup + "&TopUpCancel=" + topupcancel + "&ReportId=" + reportId + "&Loan=" + loan + "&Issued=" + issued + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result UpdateCredit(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "Credit";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=0&OnlineSale=0&OnlineCashOut=0&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=" + credit + "&Debit=0&TopUp=0&TopUpCancel=0&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }
        internal Result UpdateDebit(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "Debit";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=0&OnlineSale=0&OnlineCashOut=0&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=0&Debit=" + debit + "&TopUp=0&TopUpCancel=0&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }
        internal Result UpdateTopup(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "Topup";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=0&OnlineSale=0&OnlineCashOut=0&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=0&Debit=0&TopUp=" + topup + "&TopUpCancel=0&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }
        internal Result UpdateTopupCancel(string onlinesale, string onlineCashout, string Cashout, string active, string inventory, string cashinhand, string credit, string debit, string topup, string topupcancel, string reportId)
        {
            Result rt = new Result();
            try
            {
                string type = "TopUpCancel";
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateReportSpecific.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&InstantCashOut=0&OnlineSale=0&OnlineCashOut=0&CashInHand=0&ActiveTicketNo=0&InventoryTicketNo=0&Credit=0&Debit=0&TopUp=0&TopUpCancel=" + topupcancel + "&ReportId=" + reportId + "&type=" + type + GetAtvmStatus());

                var ticketinforesponse = client.DownloadString(new Uri(uri));
                if (ticketinforesponse != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(ticketinforesponse);
                }
            }
            catch { }
            return rt;
        }

        internal Result ChangeShiftDate(string reportId,string toDate)
        {

            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/ChangeShiftDate.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&ReportId=" + reportId + "&ToDate=" + toDate + GetAtvmStatus());

                var response = client.DownloadString(new Uri(uri));
                if (response != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(response);
                }
            }
            catch { }
            return rt;

           
        }

        internal void UpdateShopTime()
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/UpdateShopTime.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&ShopTime=" + LoginDetail.shops[IndexFinder.index].ShopTime + GetAtvmStatus());

                var response = client.DownloadString(new Uri(uri));
                if (response != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(response);
                }
            }
            catch { }
            

        }

        internal ResultWithOldTicket CloseShiftAtvm(List<ScanData> tickets)
        {
            ResultWithOldTicket rt = new ResultWithOldTicket();
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["Tickets"] = JsonConvert.SerializeObject( tickets);
              
                var response = client.UploadValues(baseUrl + "/CloseShiftAtvm.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId, values);

                var responseString = Encoding.Default.GetString(response);
                if (response != null)
                {
                    rt = JsonConvert.DeserializeObject<ResultWithOldTicket>(responseString);
                }
            }
            return rt;
        }




        #region commonLinkData

        public string GetAtvmStatus()
        {
            return "&IsAtvm=" + IsAtvm;
        }
        #endregion


        internal Result UpdateStatus_ATVMSettings( int status)
        {
            Result rt = new Result();
            try
            {
                var client = new WebClient();
                string uri = string.Format(baseUrl + "/ATVMSettings.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&Status=" + status + GetAtvmStatus());

                var response = client.DownloadString(new Uri(uri));
                if (response != null)
                {
                    rt = JsonConvert.DeserializeObject<Result>(response);
                }
            }
            catch { }

            return rt;
        }

        //POS

        internal List<Vendors> Vendors_view()
        {
            List<Vendors> st = new List<Vendors>();
            try
            {
                var client = new WebClient();
                var vendors = client.DownloadString(new Uri(baseUrl + "/VendorView.aspx?UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (vendors != null)
                {
                    st = JsonConvert.DeserializeObject<List<Vendors>>(vendors);
                    for (int i = 0; i < st.Count; i++)
                    {
                        st[i].Rank = i + 1;
                    }

                }
            }
            catch(Exception ex) { }
            return st;
        }
        internal List<Payouts> Payouts_view()
        {
            List<Payouts> st = new List<Payouts>();
            try
            {
                var client = new WebClient();
                var payouts = client.DownloadString(new Uri(baseUrl + "/PayoutView.aspx?UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (payouts != null)
                {
                    st = JsonConvert.DeserializeObject<List<Payouts>>(payouts);
                    for (int i = 0; i < st.Count; i++)
                    {
                        st[i].Rank = i + 1;
                        //st[i].imageSource = SetImageData(Encoding.ASCII.GetBytes(st[i].InvoiceImage));
                    }

                }
            }
            catch (Exception ex) { }
            return st;
        }
        public ImageSource SetImageData(byte[] binaryData)
        {
            //var source = new BitmapImage();
            //source.BeginInit();
            //source.StreamSource = new MemoryStream(data);
            //source.EndInit();

            //// use public setter
            //return source;

           // byte[] binaryData = Convert.FromBase64String(bgImage64);

            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = new MemoryStream(binaryData);
            bi.EndInit();

            //Image img = new Image();
            //img.Source = bi;
            return bi;
        }
        internal List<Loans> Loans_view()
        {
            List<Loans> st = new List<Loans>();
            try
            {
                var client = new WebClient();
                var loans = client.DownloadString(new Uri(baseUrl + "/LoanView.aspx?UserID=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus()));
                if (loans != null)
                {
                    st = JsonConvert.DeserializeObject<List<Loans>>(loans);
                    for (int i = 0; i < st.Count; i++)
                    {
                        st[i].Rank = i + 1;
                        st[i].Time = Convert.ToDateTime(st[i].Time).ToString("h tt");
                        st[i].Date = Convert.ToDateTime(st[i].Date).ToString("dd/MM/yyyy");
                        //DateTime Time = DateTime.Now;

                    }

                }
            }
            catch (Exception ex) { }
            return st;
        }
        internal Result DeleteAddedDepartment(string deleteDeptId)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/DeptDelete.aspx?deletedeptId=" + deleteDeptId + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }
        internal Result AddDepartment(string departmentName)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                string link = baseUrl + "/DeptAdd.aspx?Department=" + departmentName +  "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }
        internal Result AddPayout(string vendorId,  string departmentId, string invoiceNo, string modeOfPayment, string totalAmount, string invoiceImage)
        {
            Result r = new Result();
            try
            {
                //PayoutView.aspx?vendorId=1&userId=241&departmentId=21&invoiceNo=34234&modeOfPayment=cash&totalAmount=3454&invoiceImage=null
                var client = new WebClient();
                string link = baseUrl + "/PayoutAdd.aspx?vendorId=" + vendorId + "&invoiceNo=" + invoiceNo + "&modeOfPayment=" + modeOfPayment + "&departmentId=" + departmentId + "&totalAmount=" + totalAmount + "&invoiceImage=" + invoiceImage + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch(Exception ex ) {}
            return r;
        }
        internal Result AddVendor(string vendorName, string DepartmentId, string contactNumber, string emailId)
        {
            Result r = new Result();
            try
            {
             
                var client = new WebClient();
                string link = baseUrl + "/VendorAdd.aspx?vendorName=" + vendorName + "&DepartmentId=" + DepartmentId + "&contactNumber=" + contactNumber + "&emailId=" + emailId + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }
        internal Result AddLoan(string date, string time, string modeOfPayment, string amount, string towards)
        {
            Result r = new Result();
            try
            {
                var client = new WebClient();
                //LoanAdd.aspx?userId=241&date=2017-05-14&time=2017-05-14&modeOfPayment=cash&amount=233&towards=testing
                string link = baseUrl + "/LoanAdd.aspx?date=" + date + "&time=" + time + "&modeOfPayment=" + modeOfPayment + "&amount=" + amount + "&towards=" + towards + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + GetAtvmStatus();
                var value = client.DownloadString(new Uri(link));
                if (value != null)
                {
                    r = JsonConvert.DeserializeObject<Result>(value);
                }
            }
            catch { }
            return r;
        }
    }
}

