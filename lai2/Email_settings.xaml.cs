﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using System.Text.RegularExpressions;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Email_settings.xaml
    /// </summary>
    /// 
   
    public partial class Email_settings : Window
    {
            
        Blogic bl=new Blogic();
        private Settings settings = Properties.Settings.Default; 
       
        public Email_settings()
        {
            InitializeComponent();
            color();          
            List<emaillist> eml = new List<emaillist>();
            eml = bl.View_Emails();
            gdEmailList.DisplayMemberPath = "Email";
            gdEmailList.SelectedValuePath = "EmailId";
            if (eml == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                gdEmailList.ItemsSource = eml;               
            }
        }
        public void color()
        {
            button1.Background = new SolidColorBrush(settings.Tab);
            button2.Background = new SolidColorBrush(settings.Tab);
            emailbutton.Background = new SolidColorBrush(settings.Tab);
            btnBack.Background = new SolidColorBrush(settings.Tab);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            label3.Foreground = new SolidColorBrush(settings.Theme);
            button1.Foreground = new SolidColorBrush(settings.Font);
            button2.Foreground = new SolidColorBrush(settings.Font);
            emailbutton.Foreground = new SolidColorBrush(settings.Font);
            btnBack.Foreground = new SolidColorBrush(settings.Font);           
            label2.Foreground = new SolidColorBrush(settings.Font);
            gdEmailList.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            label3.FontFamily = new FontFamily(settings.Fontstyle);
            gdEmailList.FontFamily = new FontFamily(settings.datagridfont);
            gdEmailList.BorderBrush = new SolidColorBrush(settings.Border);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            sp1main.Visibility = Visibility.Hidden;
            spaddemail.Visibility = Visibility.Visible;
            lbError.Content = "";
        }

        private void Add1_Click(object sender, RoutedEventArgs e)
        {
            if (EmailValid(txtEmail.Text) == "1")
            {
                string email = txtEmail.Text;
                Result r = bl.AddEmail(email);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        sp1main.Visibility = Visibility.Visible;
                        spaddemail.Visibility = Visibility.Hidden;
                        List<emaillist> em = bl.View_Emails();
                        gdEmailList.ItemsSource = em;
                        txtEmail.Text = "";
                    }
                    else
                    {
                        lbError.Content = r.Description;
                    }
                }
            }
            else
            {
                // MessageBox.Show("Incorrect Format");
                string heading = "Incorrect Format";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
        }

        public string EmailValid(string email)
        {
            // Allows only valid EmailID
            string pattern = @"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$";
            System.Text.RegularExpressions.Match match = Regex.Match(email.Trim(), pattern, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return ("1");
            }
            else
            {

                return null;
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (gdEmailList.SelectedItem != null)
            {
                if (gdEmailList.SelectedItems.Count == 1)
                {
                    emaillist es = (emaillist)gdEmailList.SelectedItem;
                    string heading = "Delete Email";
                    string content = "Do you want to delete " + es.Email +"?";

                    var dialog = new DeleteWindow(heading,content);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        Result r = bl.delete_emails(es.Email, es.EmailId);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                List<emaillist> em = bl.View_Emails();
                                if (em == null && bl.internetconnection() == false)
                                {
                                    string heading1 = "        Poor Connectivity";
                                    var dialog1 = new DialogBox_BoxNo(heading1);
                                    dialog1.ShowDialog();
                                }
                                else
                                    gdEmailList.ItemsSource = em;
                            }
                        }
                    }
                }
                else
                {
                    string heading = "Select one at a time";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
            else
            {
                // MessageBox.Show("Select One");
                string heading = "Invalid Selection";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
        }

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex()).ToString();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            sp1main.Visibility = Visibility.Visible;
            spaddemail.Visibility = Visibility.Hidden;
            List<emaillist> em = bl.View_Emails();
            if (em == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            gdEmailList.ItemsSource = em;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        } 
    }
}
