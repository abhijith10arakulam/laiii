﻿using lai2.Properties;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ColorFont
{
    public partial class ColorFontChooser : UserControl
    {
        private Settings settings = lai2.Properties.Settings.Default;
        public ColorFontChooser()
        {
            InitializeComponent();
            this.Background = new SolidColorBrush(settings.Background);
            lstFamily.Foreground = new SolidColorBrush(settings.Font);
            lstTypefaces.Foreground = new SolidColorBrush(settings.Font);
            this.txtSampleText.IsReadOnly = true;
        }

        public FontInfo SelectedFont
        {
            get
            {
                return new FontInfo(this.txtSampleText.FontFamily,
                                    this.txtSampleText.FontSize,
                                    this.txtSampleText.FontStyle,
                                    this.txtSampleText.FontStretch,
                                    this.txtSampleText.FontWeight,
                                    this.colorPicker.SelectedColor.Brush);
            }

        }

        private void colorPicker_ColorChanged(object sender, RoutedEventArgs e)
        {
            this.txtSampleText.Foreground = this.colorPicker.SelectedColor.Brush;
        }
    }
}
