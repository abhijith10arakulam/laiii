﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Media;

namespace lai2
{
    /// <summary>
    /// Interaction logic for 
    /// Tickets.xaml
    /// </summary>
    /// 
    public partial class ActiveTickets : Window
    {
        private Settings settings = Properties.Settings.Default;
        int asndngtemp = 0;

        string ticketnametemp;
        string valuetemp;
        string TicketValue;
        private int UIvalue;
        //bool mLeftCtrlDown = false;
        //bool mScanShiftDown = false;
        //bool mScanning = false;
        int returnFlag = 1;

        string barcode = "";
        StringBuilder mScanData = new StringBuilder();
        KeyConverter mScanKeyConverter = new KeyConverter();
        //  private string p;
        //  private Result res;

        int ScanOrder;
        Blogic bl = new Blogic();
        int flag = 0;
        private string box;
        private string ticketname;
        private string tcktvalue;
        private string openno;
        private string closeno;
        private string total;
        private string tcktid;
        private string pckno;
        private string pckposopen;
        int temp;
        private int zero;
        public static int maxno;
        public string expdate;
        int ss;
        int canupdateFlag = 0;
        //private Settings settings = Properties.Settings.Default;
        public ActiveTickets()
        {
            InitializeComponent();
            Colour();
            this.DataContext = new ViewModel();
            InVisibleLabel();
            if (settings.checkbox == 1)
            {
                CheckBox1.IsChecked = true;
            }
            else
            {
                CheckBox1.IsChecked = false;
            }
            if (settings.TicketActivationOrder == 0)
            {

                rdbtnascending.IsChecked = true;
            }
            else
            {
                rdbtndescending.IsChecked = true;

            }

            if(Blogic.IsAtvm)
            {
                rdbtnascending.IsChecked = true;
                btfullpackSoldout.Visibility = Visibility.Hidden;
            }
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            ss = Convert.ToInt32(DateTime.Now.ToString("ss"));
            var am = DateTime.Now.ToString("tt");
            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;

            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                cbboxcount.Items.Add(i);
            }
            //cbboxcount.SelectedItem = 1;
            // tbPackPositionopen.Text = "0";           
            cbboxcount.SelectedItem = settings.box;

            if (settings.box > LoginDetail.shops[IndexFinder.index].BoxCount)
            {
                settings.box = 1;
                cbboxcount.SelectedItem = settings.box;
            }

            tbPackPositionopen.Text = "";
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
        }


        public void Colour()
        {
            btfullpackSoldout.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            btClose.Background = new SolidColorBrush(settings.Tab);
            cbboxcount.Background = new SolidColorBrush(settings.Tab);
            comboboxhour.Background = new SolidColorBrush(settings.Tab);
            comboboxam.Background = new SolidColorBrush(settings.Tab);
            comboboxminute.Background = new SolidColorBrush(settings.Tab);
            cbboxcount.Foreground = new SolidColorBrush(settings.Font);
            comboboxhour.Foreground = new SolidColorBrush(settings.Font);
            comboboxam.Foreground = new SolidColorBrush(settings.Font);
            comboboxminute.Foreground = new SolidColorBrush(settings.Font);

            lbactivationheading.Foreground = new SolidColorBrush(settings.Theme);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            btfullpackSoldout.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            btClose.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbactivationheading.FontFamily = new FontFamily(settings.Fontstyle);
            lbdescription.FontFamily = new FontFamily(settings.labelfont);
        }

        public ActiveTickets(string tcktid, string pckno, string pckposopen)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            Colour();
            this.DataContext = new ViewModel();
            InVisibleLabel();
            if (settings.checkbox == 1)
            {
                CheckBox1.IsChecked = true;
            }
            else
            {
                CheckBox1.IsChecked = false;
            }
            this.tcktid = tcktid;
            this.pckno = pckno;
            this.pckposopen = pckposopen;
            tbTicketId.SearchText = tcktid;
            tbPackno.SearchText = pckno;

            //  if(settings.)

            if (settings.ActiveScan == 0 && settings.TicketActivationOrder == 0)
            {

                tbPackPositionopen.Text = "000";
            }
            else if (settings.ActiveScan == 0 && settings.TicketActivationOrder == 1)
            {
                tbPackPositionopen.Text = "";
            }

            else
            { 
                tbPackPositionopen.Text = pckposopen;
            }

            if (settings.TicketActivationOrder == 0)
                rdbtnascending.IsChecked = true;
            else
                rdbtndescending.IsChecked = true;

            if(Blogic.IsAtvm)
            {
                rdbtnascending.IsChecked = true;
                btfullpackSoldout.Visibility = Visibility.Hidden;
            }
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            ss = Convert.ToInt32(DateTime.Now.ToString("ss"));

            var am = DateTime.Now.ToString("tt");
            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;
            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                cbboxcount.Items.Add(i);
            }
            //  cbboxcount.SelectedItem = 1;
            // temp = 1;
            cbboxcount.SelectedItem = settings.box;
            if (settings.box >= LoginDetail.shops[IndexFinder.index].BoxCount+1)
            {
                settings.box = 1;
                cbboxcount.SelectedItem = settings.box;
            }
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
        }

        public ActiveTickets(string box)
        {
            InitializeComponent();
            Colour();
            this.DataContext = new ViewModel();
            InVisibleLabel();
            if (settings.checkbox == 1)
            {
                CheckBox1.IsChecked = true;
            }
            else
            {
                CheckBox1.IsChecked = false;
            }
            this.box = box;


            if (settings.TicketActivationOrder == 0)
                rdbtnascending.IsChecked = true;
            else
                rdbtndescending.IsChecked = true;
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            ss = Convert.ToInt32(DateTime.Now.ToString("ss"));
            var am = DateTime.Now.ToString("tt");
            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;

            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                cbboxcount.Items.Add(i);
            }

            cbboxcount.SelectedItem = Convert.ToInt32(box);
            settings.box = Convert.ToInt32(box);
            settings.Save();
            if (settings.box >= LoginDetail.shops[IndexFinder.index].BoxCount+1)
            {
                settings.box = 1;
                cbboxcount.SelectedItem = settings.box;
            }
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
        }


        public void InVisibleLabel()
        {
            lbvaldtcktid.Visibility = Visibility.Hidden;
            lbvaldtcktname.Visibility = Visibility.Hidden;
            lbvaldpackposOPen.Visibility = Visibility.Hidden;
            lbvaldtcktpackno.Visibility = Visibility.Hidden;
            lbvaldtcktvalue.Visibility = Visibility.Hidden;
            lbvaldbox.Visibility = Visibility.Hidden;
            //lbvaldBox.Visibility = Visibility.Hidden;
        }


        private void TicketId_textchanged(object sender, TextChangedEventArgs e)
        {
            if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
            //   if (tbTicketId.Text.Length >= 3)
            {
                if (TryToParse(tbTicketId.SearchText) == true)
                {
                    //if (ScanOrder == 1)
                    //{
                    //    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.Text);
                    //    if (ts.Status == true)
                    //    {
                    //        maxno = ts.TicketInfo[0].MaxNo;
                    //        tbPackPositionopen.Text = Convert.ToString(maxno);
                    //    }
                    //}
                    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.SearchText);
                    if (ts == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (ts.Status == true)
                        {
                            InVisibleLabel();
                            tbPackno.SearchText = Convert.ToString(ts.TicketInfo[0].PackNo);
                            tbTicketName.Text = Convert.ToString(ts.TicketInfo[0].TicketName);
                            tbValue.Text = Convert.ToString(ts.TicketInfo[0].Value);
                            maxno = ts.TicketInfo[0].MaxNo;
                            ticketnametemp = tbTicketName.Text;
                            expdate = ts.TicketInfo[0].ExpDate;
                            canupdateFlag = 1;
                            if (ScanOrder == 0 && settings.ActiveScan == 0)
                            {
                                asndngtemp++;
                                tbPackPositionopen.Text = "000";
                            }
                            else if(ScanOrder == 0 && settings.ActiveScan == 1)
                            {
                                asndngtemp++;
                                tbPackPositionopen.Text = Convert.ToString(ts.TicketInfo[0].MaxNo); 
                            }

                        }
                        else
                        {
                            //tbPackno.SearchText = "";
                            //tbTicketName.Text = "";
                            //tbValue.Text = "";
                            //tbPackPositionopen.Text = "";
                            //tbBox.Text = "";                  
                        }
                    }
                }
            }

            if (tbTicketId.SearchText == "")
            {
                InVisibleLabel();
                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackno.SearchText = "";
                maxno = new int();
                lbdescription.Visibility = Visibility.Hidden;
                canupdateFlag = 0;
            }
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)        //Ascending Clicked
        {
            ScanOrder = 0;
            if (asndngtemp != 0 && settings.ActiveScan == 0)
            {
                tbPackPositionopen.Text = "000";
                //  tbPackPositionopen.Text = "";             
            }
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)        //Descending Clicked
        {
            ScanOrder = 1;
            if (settings.ActiveScan == 0)
            {
                tbPackPositionopen.Text = Convert.ToString(maxno);
                //    tbPackPositionopen.Text = "000";                
            }
        }

        private void setData(ScanData tt)
        {
            tbTicketId.SearchText = Convert.ToString(tt.TicketId);
            tbPackno.SearchText = tt.PackNo;

            if (settings.ActiveScan == 1)
            {
                tbPackPositionopen.Text = Convert.ToString(tt.PackPosition);
            }
            btSubmit.Focus();

        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {
            Active();

        }

        private void Active()
        {
            btSubmit.Focusable = false;
            flag = 0;
            InVisibleLabel();
            if (tbTicketName.Text == "")
            {
                lbvaldtcktname.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketId.SearchText == "")
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackno.SearchText == "")
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbValue.Text == "")
            {
                lbvaldtcktvalue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPositionopen.Text == "")
            {
                lbvaldpackposOPen.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbTicketId.SearchText) == false)
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if ((TryToParse(tbPackno.SearchText) == false))
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if ((TryToParse(tbPackPositionopen.Text) == false))
            {
                lbvaldpackposOPen.Visibility = Visibility.Visible;
                flag = 1;
            }
            if ((TryToParse(TicketValue) == false))
            {
                lbvaldtcktvalue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckPackNoLenght(tbPackno.SearchText.Length))
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (CheckBox1.IsChecked == true)
            {
                if (flag == 0)
                {
                    if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                    // if (tbTicketId.Text.Length >= 3 && tbTicketId.Text.Length <= 5 && tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7)
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                        string heading = "Do you want to Activate?";
                        string ticketname = tbTicketName.Text;
                        string value = tbValue.Text;
                        string box = cbboxcount.SelectedItem.ToString();
                        var dialog1 = new ConfirmationDetails(heading, ticketname, value, box);
                        if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
                        {

                            lbdescription.Content = "";
                            string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                            String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + ":" + ss + " " + comboboxam.SelectedItem.ToString();
                            ResultWithOldTicket rt = bl.Activate_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, cbboxcount.SelectedItem.ToString(), s, ScanOrder);
                            //                            ResultWithOldTicket rt = bl.Activate_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, cbboxcount.SelectedItem.ToString(), s, ScanOrder);

                            if (rt == null && bl.internetconnection() == false)
                            {
                                string heading1 = "        Poor Connectivity";
                                var dialog = new DialogBox_BoxNo(heading1);
                                dialog.ShowDialog();
                            }
                            else
                            {
                                if (rt.Status == true)
                                {
                                    snd = new SoundPlayer(Properties.Resources.done);
                                    snd.Play();

                                    if (settings.box <= LoginDetail.shops[IndexFinder.index].BoxCount)
                                    {
                                        settings.box = Convert.ToInt32(cbboxcount.SelectedItem);
                                        settings.box++;
                                        settings.Save();
                                    }
                                    else
                                    {
                                        settings.box = 1;
                                        settings.Save();
                                        cbboxcount.SelectedItem = settings.box;
                                    }
                                    this.DialogResult = true;
                                    lbdescription.Content = rt.Description;
                                    tbTicketId.SearchText = "";
                                    tbPackno.SearchText = "";
                                    tbTicketName.Text = "";
                                    tbPackPositionopen.Text = "";
                                    tbValue.Text = "";
                                }
                                else if (rt.Description == "Another Ticket is Activated in this Box")
                                {
                                    var dialog = new OldTicketInfo(rt);
                                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                                    {
                                        ResultWithOldTicket rs = bl.Activate_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, cbboxcount.SelectedItem.ToString(), s, ScanOrder);
                                        //   ResultWithOldTicket rs = bl.Activate_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, cbboxcount.SelectedItem.ToString(), s, ScanOrder);

                                        if (rs == null && bl.internetconnection() == false)
                                        {
                                            string heading2 = "        Poor Connectivity";
                                            var dialog2 = new DialogBox_BoxNo(heading2);
                                            dialog2.ShowDialog();
                                        }
                                        else
                                        {
                                            if (rs.Status == true)
                                            {
                                                snd = new SoundPlayer(Properties.Resources.done);
                                                snd.Play();
                                                if (settings.box <= LoginDetail.shops[IndexFinder.index].BoxCount)
                                                {
                                                    settings.box = Convert.ToInt32(cbboxcount.SelectedItem);
                                                    settings.box++;
                                                    settings.Save();
                                                }
                                                else
                                                {
                                                    settings.box = 1;
                                                    settings.Save();
                                                    cbboxcount.SelectedItem = settings.box;
                                                }
                                                this.DialogResult = true;
                                                lbdescription.Content = rs.Description;
                                                tbTicketId.SearchText = "";
                                                tbPackno.SearchText = "";
                                                tbTicketName.Text = "";
                                                tbPackPositionopen.Text = "";
                                                tbValue.Text = "";
                                            }
                                            else
                                            {
                                                lbdescription.Content = rs.Description;
                                                flag = 0;
                                                tbTicketId.Focus();
                                            }
                                        }
                                    }
                                }
                                else if (rt.Description == "Please Update Inventory")
                                {
                                    if (canupdateFlag == 1)
                                    {
                                        var dialog = new DialogBox("Would you like to update inventory?");
                                        if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                                        {
                                            string dateUpdate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                                            if (expdate == "" || expdate == null)
                                                expdate = "never";

                                            Result rsup = bl.updatestatus(tbTicketId.SearchText, tbPackno.SearchText, tbTicketName.Text, maxno.ToString(), tbValue.Text.Replace("$", ""),"0", dateUpdate, expdate);
                                            if (rsup.Status == true)
                                            {
                                                Active();
                                            }

                                        }

                                    }
                                    else
                                    {
                                        var dialog = new Message(rt.Description, rt.DateTime);
                                        if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                                        {
                                        }
                                        //  lbdescription.Visibility = Visibility.Visible;
                                        //lbdescription.Content = rt.Description;
                                        flag = 0;
                                        tbTicketId.Focus();
                                    }
                                }
                                else
                                {

                                    var dialog = new Message(rt.Description, rt.DateTime);
                                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                                    {
                                    }
                                    //  lbdescription.Visibility = Visibility.Visible;
                                    //lbdescription.Content = rt.Description;
                                    flag = 0;
                                    tbTicketId.Focus();
                                }
                            }
                        }
                        else
                        {
                            tbTicketId.Focus();
                        }
                    }
                }
            }

            else
            {
                lbvaldbox.Visibility = Visibility.Visible;
                var dialog = new DialogBox_BoxNo();
                dialog.ShowDialog();
            }
        }

        void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return" && returnFlag == 1 & barcode.Length != 0)
            {
                //bl = new BAL();
                //returnFlag = 0;
                if ((barcode.Length <= 24 && barcode.Length >= 22) || (barcode.Length == 20) || (barcode.Length == 18))
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
                    snd.Play();
                    ScanData tt = bl.GetTicket(barcode);
                    setData(tt);
                    btSubmit.Focus();
                }

                barcode = "";

                if (UIvalue == 0)
                {
                    // EnableTextBoxes();
                    //if (TbValidation())
                    //{

                    //    //this.DialogResult = true;
                    //}
                    //else
                    //{
                    //    btUISubmit.Visibility = Visibility.Visible;
                    //    btUISubmit.IsEnabled = true;
                    //}
                }
            }

            if (e.Key.ToString().StartsWith("D") && returnFlag == 1)
            {
                barcode = barcode + e.Key.ToString().Replace("D", string.Empty);

            }
            //listBox1.Items.Add(e.Key.ToString().Replace("D",string.Empty));
        }



        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void tbValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = tbValue.Text.Replace("$", "");
            if (s != "")
            {
                tbValue.Text = "$" + s;
                valuetemp = tbValue.Text;
            }
            TicketValue = tbValue.Text.Replace("$", "");
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                return result;
                //if (value == null) value = "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        }


        private void btFullpackSoldOut_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            InVisibleLabel();
            //if (tbTicketName.Text == "")
            //{
            //    lbvaldtcktname.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            if (tbTicketId.SearchText == "")
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackno.SearchText == "")
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            //if (tbValue.Text == "")
            //{
            //    lbvaldtcktvalue.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (tbPackPositionopen.Text == "")
            //{
            //    lbvaldpackposOPen.Visibility = Visibility.Visible;
            //    flag = 1;
            //}

            if (TryToParse(tbTicketId.SearchText) == false)
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if ((TryToParse(tbPackno.SearchText) == false))
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            //if ((TryToParse(tbPackPositionopen.Text) == false))
            //{
            //    lbvaldpackposOPen.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if ((TryToParse(TicketValue) == false))
            //{
            //    lbvaldtcktvalue.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            if (!bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckPackNoLenght(tbPackno.SearchText.Length))
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (CheckBox1.IsChecked == true)
            {
                if (flag == 0)
                {
                    if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                    //  if (tbTicketId.Text.Length >= 3 && tbTicketId.Text.Length <= 5 && (tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7))
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                        string heading = "Are you sure the ticket is soldout?";
                        int soldoutstatus = 1;
                        string ticketname = ticketnametemp;
                        string value = valuetemp;

                        var dialog1 = new ConfirmationDetails(heading, soldoutstatus, ticketname, value);
                        if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
                        {
                            lbdescription.Content = "";
                            tbValue.Text = tbValue.Text.Replace("$", "");
                            string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                            String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + " " + comboboxam.SelectedItem.ToString();
                            ResultWithOldTicket rt = bl.FullPackSoldOut_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, cbboxcount.SelectedItem.ToString(), s, ScanOrder);
                            if (rt == null && bl.internetconnection() == false)
                            {
                                string heading1 = "        Poor Connectivity";
                                var dialog2 = new DialogBox_BoxNo(heading1);
                                dialog2.ShowDialog();
                            }
                            else
                            {
                                if (rt.Status == true)
                                {
                                    snd = new SoundPlayer(Properties.Resources.done);
                                    snd.Play();
                                    lbdescription.Content = rt.Description;
                                    tbTicketId.SearchText = "";
                                    tbPackno.SearchText = "";
                                    tbTicketName.Text = "";
                                    tbPackPositionopen.Text = "";
                                    tbValue.Text = "";
                                    this.DialogResult = true;
                                }

                                else
                                {
                                    var dialog = new Message(rt.Description, rt.DateTime);
                                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                                    {
                                    }
                                    //      lbdescription.Visibility = Visibility.Visible;
                                    //    lbdescription.Content = rt.Description;
                                }
                            }
                        }
                    }
                }
               

            }
            else
            {
                lbvaldbox.Visibility = Visibility.Visible;
                var dialog = new DialogBox_BoxNo();
                dialog.ShowDialog();
            }
        }



        private void cbbox_checked(object sender, RoutedEventArgs e)
        {
            if (CheckBox1.IsChecked == true)
            {
                lbvaldbox.Visibility = Visibility.Hidden;
            }
            else
            {
                lbvaldbox.Visibility = Visibility.Visible;
            }
           // btSubmit.Focus();
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1;
        }

        private void cbboxcount_selctionchanged(object sender, SelectionChangedEventArgs e)
        {
            cbboxcount.Focusable = false;
        }

        private void tbTicketId_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key.ToString() == "Down" || e.Key.ToString() == "Up")
            //{
            //    string data = Convert.ToString(tbTicketId.SelectedItem);
            //    //this.DataContext = new ViewModel(data);
            //    if (data != string.Empty)

            //    {
            //        tbTicketId.SearchText = data;
            //        tbTicketIdChangeEvent();
            //    }

            //}
            //else
            //{
            //string data = tbTicketId.SearchText;
            //this.DataContext = new ViewModel(data);

            //tbTicketIdChangeEvent();
            //}
        }


        public void tbTicketIdChangeEvent()
        {
            var data = (AutoCompleteItem)tbTicketId.SelectedItem;

            if (data != null)
            {
                tbTicketId.SearchText = data.Name;
            }
            if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
            //   if (tbTicketId.Text.Length >= 3)
            {
                if (TryToParse(tbTicketId.SearchText) == true)
                {
                    //if (ScanOrder == 1)
                    //{
                    //    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.Text);
                    //    if (ts.Status == true)
                    //    {
                    //        maxno = ts.TicketInfo[0].MaxNo;
                    //        tbPackPositionopen.Text = Convert.ToString(maxno);
                    //    }
                    //}
                    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.SearchText);
                    if (ts == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (ts.Status == true)
                        {
                            InVisibleLabel();
                            tbPackno.SearchText = Convert.ToString(ts.TicketInfo[0].PackNo);
                            tbTicketName.Text = Convert.ToString(ts.TicketInfo[0].TicketName);
                            tbValue.Text = Convert.ToString(ts.TicketInfo[0].Value);
                            maxno = ts.TicketInfo[0].MaxNo;
                            ticketnametemp = tbTicketName.Text;
                            expdate = ts.TicketInfo[0].ExpDate;
                            canupdateFlag = 1;
                            if (ScanOrder == 0 && settings.ActiveScan == 0)
                            {
                                asndngtemp++;
                                tbPackPositionopen.Text = "000";
                            }
                            else if(ScanOrder == 1 && settings.ActiveScan == 0)
                            {
                                tbPackPositionopen.Text =Convert.ToString( ts.TicketInfo[0].MaxNo);

                            }
                        }
                        else
                        {
                            //tbPackno.SearchText = "";
                            //tbTicketName.Text = "";
                            //tbValue.Text = "";
                            //tbPackPositionopen.Text = "";
                            //tbBox.Text = "";                  
                        }
                    }
                }
            }

            if (tbTicketId.SearchText == "")
            {
                InVisibleLabel();
                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackno.SearchText = "";
                maxno = new int();
                lbdescription.Visibility = Visibility.Hidden;
                canupdateFlag = 0;
            }
        }

        private void tbTicketId_SearchTextChanged(object sender, EventArgs e)
        {
            tbTicketIdChangeEvent();
        }

        private void tbTicketId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tbTicketIdChangeEvent();

        }

        private void tbTicketId_LostFocus(object sender, RoutedEventArgs e)
        {
            var data = tbTicketId.SearchText;
            List<GetPackNo> items = new List<GetPackNo>();
            items = bl.GetPackNos(data);
            tbPackno.ItemsSource = items;


        }

        private void tbPackno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tbPackno.SelectedItem != null)
                tbPackno.SearchText = ((GetPackNo)tbPackno.SelectedItem).PackNo;

        }
    }
}
