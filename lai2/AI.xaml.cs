﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AI.xaml
    /// </summary>
    public partial class AI : Page
    {
        string ailevel1 = "AI level1";


        string datestate;   //store
        string typestate;   //store

        string date_state;      //state
        string type_state;          //state

        List<AIOrder> ai = new List<AIOrder>();     //nextweek order

        List<PieData2> pieDataAI2 = new List<PieData2>();
        List<PieData> pieDataAI1 = new List<PieData>();     //ai analysis
        string dateai1;
        string cbanalysisai1;
        string dateai2;

        string box;
        List<AITicket> aisugestion = new List<AITicket>();       //aisugestion

        System.ComponentModel.BackgroundWorker mWorkernxtweekorder;
        System.ComponentModel.BackgroundWorker mWorker;
        System.ComponentModel.BackgroundWorker mWorkerstate;
        System.ComponentModel.BackgroundWorker mWorkeraaisugestion;
        System.ComponentModel.BackgroundWorker mWorkeraaianalysis;

        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        AITicketList aishop = new AITicketList();
        AITicketList aistate = new AITicketList();
        string value;
        List<string> cbvalue = new List<string>();
        string ai2value;
        string cbai2strng;
        List<string> aity = new List<string> { "Daily", "Weekly", "Monthly" };
        int boxcount = LoginDetail.shops[IndexFinder.index].BoxCount;
        int[] a = { 0, 0, 0, 0, 0 };
        int valuecount;
        public AI()
        {

            try {
                InitializeComponent();
                valuecount = 0;

                color();
                DateTime date = Convert.ToDateTime(Blogic.opendate);
                cbboxtype.ItemsSource = aity;
                cbValueShop.SelectedItem = "OverAll";
                datepicker1.SelectedDate = date;//DateTime.Now;
                datestate = Blogic.opendate; //DateTime.Now.ToString();
                typestate = "Weekly";


                cbValueNetwork.SelectedValue = "OverAll";
                cbboxtypestate.ItemsSource = aity;
              //  cbboxtypestate.SelectedValue = "Weekly";
                datepicker_state.SelectedDate = date;// DateTime.Now;

                cbAi2.ItemsSource = aity;
                cbAi2.SelectedItem = "Weekly";
                cbAnalysis.ItemsSource = aity;
                cbAnalysis.SelectedItem = "Weekly";
                List<string> ailvl = new List<string> { "AI level1", "AI level2" };
                cbAiLevel1.ItemsSource = ailvl;
                cbAiLevel1.SelectedItem = "AI level1";
                cbAiLevel2.ItemsSource = ailvl;
                cbAiLevel2.SelectedItem = "AI level1";

                datepickerAI1.SelectedDate = date;// DateTime.Now;
                datepicker2_graph.SelectedDate = date;// DateTime.Now;

                for (int i = 1; i <= boxcount; i++)
                {
                    cbBoxActive.Items.Add(i);
                }
                cbBoxActive.Items.Add("Box");
                cbBoxActive.SelectedValue = "Box";

                datepicker_state.DisplayDateEnd = date;// DateTime.Now;
                datepickerAI1.DisplayDateEnd = date;// DateTime.Now;
                datepicker2_graph.DisplayDateEnd = date;// DateTime.Now;
                datepicker1.DisplayDateEnd = date;// DateTime.Now;
                dateai1 = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
                dateai2 = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
                cbanalysisai1 = "Weekly";
                cbai2strng = "Weekly";
                if (Blogic.aishop != null)
                {
                    cbboxtype.SelectedValue = "Weekly";
                }
                cbboxtype.SelectionChanged += new SelectionChangedEventHandler(cbboxtype_SelectionChanged);//shop
                datepicker1.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(datepicker_selecteddatechanged);//shop
                cbValueShop.SelectionChanged += new SelectionChangedEventHandler(Cbvalue_selectionChanged);//shop


                cbAnalysis.SelectionChanged += new SelectionChangedEventHandler(cbAnalysis_SelectionChanged);
                datepickerAI1.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(cbAnalysis_SelectionChanged);

                cbAi2.SelectionChanged += new SelectionChangedEventHandler(cbai2_selectionChanged);
                datepicker2_graph.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(cbai2_selectionChanged);
                cbAi2Value.SelectionChanged += new SelectionChangedEventHandler(cbai2_selectionChanged);

                cbAiLevel1.SelectionChanged += new SelectionChangedEventHandler(cbAiLevel1_SelectionChanged);
                cbAiLevel2.SelectionChanged += new SelectionChangedEventHandler(cbAiLevel2_selectionChanged);
                cbValueNetwork.SelectionChanged += new SelectionChangedEventHandler(cbValueNetwork_selectionChanged);
                datepicker_state.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(datepickerstate_selecteddatechanged);

                cbboxtypestate.SelectionChanged += new SelectionChangedEventHandler(cbboxtypestate_SelectionChanged);
                if (Blogic.aishop != null)
                {
                    cbboxtypestate.SelectedValue = "Weekly";
                    aishop = Blogic.aishop;
                       //aistate = Blogic.aistate;
                //   dataLoadState();
                    dataLoadStore();
                    checkCall[0] = 1;
                    //     checkCall[4] = 1;
                //    cbboxtypestate.SelectedValue= "Weekly";

                }
                else
                {
                    cbboxtypestate.SelectedValue = "Weekly";

                    cbboxtype.SelectedValue = "Weekly";
                }
            }
            catch(Exception e)
            {
                loadingtelerickstore.IsBusy = false;
                cbValueNetwork.ItemsSource = cbValueShop.ItemsSource;
            }
}

        int[] checkCall = { 0, 0, 0, 0, 0 };
        private void Mostsellingticketinstore_gotfocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkCall[0] == 0)
                {
                    loading();

                }
                checkCall[0] = 1;
            }
            catch { }
        }

        private void AIanalysis_gotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkCall[1] == 0)
                {
                    if (cbAi2Value.Items.Count == 0)
                    {
                        checkCall[0] = 0;
                        loading();
                          
                    }
                    checkCall[1] = 1;
                    loadinganalysis();
                }
            }
            catch { }
        }

        private void nextweekorder_gotfocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkCall[2] == 0)
                    loadingnextweekorder();
                checkCall[2] = 1;
            }
            catch { }
        }

        private void AIsugestion_gotfocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkCall[3] == 0)
                    loadingaisugesstion();
                checkCall[3] = 1;
            }
            catch { }
        }
        private void mostsellingticketinstate_gotfocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkCall[4] == 0)
                   loadingstate();
                checkCall[4] = 1;
            }
            catch { }
        }

        private void loadinganalysis()
        {
            try
            {
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
                mWorkeraaianalysis = new System.ComponentModel.BackgroundWorker();
                mWorkeraaianalysis.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWorkaianalysis);
                mWorkeraaianalysis.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChangedaianalysis);
                mWorkeraaianalysis.WorkerReportsProgress = true;
                mWorkeraaianalysis.WorkerSupportsCancellation = true;
                mWorkeraaianalysis.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompletedaianalysis);
                mWorkeraaianalysis.RunWorkerAsync();
                if (!mWorkeraaianalysis.CancellationPending)
                {
                    try
                    {
                        loadingtelerickaianaalysis.IsBusy = true;
                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }

                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }

            catch { }
        }

        private void worker_DoWorkaianalysis(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (checkCall[1] != 0)
            {
                try
                {
                    //  pieDataAI1 = bl.PieDataAI1(Convert.ToDateTime(datepickerAI1.SelectedDate).ToString("yyyy-MM-dd"), cbAnalysis.SelectedItem.ToString());

                    if (ailevel1 == "AI level1")
                    {
                        if (dateai1 != null && cbanalysisai1 != null)
                        {
                            pieDataAI1 = bl.PieDataAI1(dateai1, cbanalysisai1);
                        }
                    }

                    if (ailevel1 == "AI level2")
                    {
                        if (dateai2 != null && cbai2strng != null && ai2value != null)
                        {
                            pieDataAI2 = bl.PieDataAI2(dateai2, cbai2strng, ai2value);
                        }
                    }
                }
                catch { }
            }
        }

        private void worker_ProgressChangedaianalysis(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            // pbProcessing.Value = e.ProgressPercentage;
        }

        private void worker_RunWorkerCompletedaianalysis(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorkeraaianalysis.CancelAsync();
            loadingtelerickaianaalysis.IsBusy = false;
            if (checkCall[1] != 0)
            {
                try
                {
                    if (pieDataAI1 == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (ailevel1 == "AI level1")
                        {
                            if (dateai1 != null && cbanalysisai1 != null)
                            {
                                for (int i = 0; i < pieDataAI1.Count; i++)
                                {
                                    if (pieDataAI1[i].SalesPercentage.Contains("%"))
                                    { }
                                    else
                                        pieDataAI1[i].SalesPercentage = pieDataAI1[i].SalesPercentage + "%";

                                    if (pieDataAI1[i].SpacePercentage.Contains("%"))
                                    { }
                                    else
                                        pieDataAI1[i].SpacePercentage = pieDataAI1[i].SpacePercentage + "%";

                                    if (pieDataAI1[i].Value.Contains("$"))
                                    { }
                                    else
                                        pieDataAI1[i].Value = "$" + pieDataAI1[i].Value;
                                }
                                //  if (pieDataAI1.Count > 0)
                                {
                                    pieSales.Palette = MakePalette(pieDataAI1.Count);
                                    pieSpace.Palette = MakePalette(pieDataAI1.Count);
                                }
                                pieSales.DataContext = pieDataAI1;
                                pieSpace.DataContext = pieDataAI1;
                                dgDataSalesDivision.ItemsSource = pieDataAI1;
                                dgDataSpaceDivision.ItemsSource = pieDataAI1;
                                dgDataResult.ItemsSource = pieDataAI1;
                            }
                        }
                        else
                            if (ailevel1 == "AI level2")
                            {
                                if (dateai2 != null && cbai2strng != null && ai2value != null)
                                {
                                    dgDataSalesDivisionAI2.ItemsSource = pieDataAI2;
                                    dgDataSpaceDivisionAI2.ItemsSource = pieDataAI2;
                                    dgDataResultAI2.ItemsSource = pieDataAI2;
                                    {

                                        for (int i = 0; i < pieDataAI2.Count; i++)
                                        {
                                            if (pieDataAI2[i].Box.Length > 6)
                                        {
                                            pieDataAI2[i].LessBox = pieDataAI2[i].Box.Substring(0, 4) + "..";
                                        }
                                            else
                                                pieDataAI2[i].LessBox = pieDataAI2[i].Box;

                                        }
                                       
                                        

                                        //  if (pieDataAI1.Count > 0)
                                        {
                                            pieSpaceAI2.Palette = MakePalette(pieDataAI2.Count);
                                            pieSalesAI2.Palette = MakePalette(pieDataAI2.Count);
                                           
                                            pieSpaceAI2.DataContext = pieDataAI2;
                                            pieSalesAI2.DataContext = pieDataAI2;
                                        }
                                        valuecount++;
                                    }
                                }
                            }
                    }
                }
                catch { }
            }
        }

        private void cbai2_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbAi2Value.SelectedItem != null)
            {
                ai2value = cbAi2Value.SelectedItem.ToString();
                ai2value = ai2value.Replace("$", "");
                dateai2 = Convert.ToDateTime(datepicker2_graph.SelectedDate).ToString("yyyy-MM-dd");

                cbai2strng = cbAi2.SelectedItem.ToString();
                tab8AI2.IsSelected = true;
                loadinganalysis();
            }
        }

        private void cbAiLevel1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbAiLevel1.SelectedItem.ToString() == "AI level2")
            {
                ailevel1 = cbAiLevel1.SelectedItem.ToString();
                spAi1.Visibility = Visibility.Hidden;
                spAi2.Visibility = Visibility.Visible;
                cbAiLevel2.SelectedItem = "AI level2";
                loadinganalysis();
            }
        }
        private void cbAiLevel2_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbAiLevel2.SelectedItem.ToString() == "AI level1")
            {
                ailevel1 = cbAiLevel2.SelectedItem.ToString();
                spAi1.Visibility = Visibility.Visible;
                spAi2.Visibility = Visibility.Hidden;
                cbAiLevel1.SelectedItem = "AI level1";
                loadinganalysis();
            }
        }

        private void cbAnalysis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dateai1 = Convert.ToDateTime(datepickerAI1.SelectedDate).ToString("yyyy-MM-dd");
            cbanalysisai1 = cbAnalysis.SelectedItem.ToString();
            datepicker2_graph.SelectedDate = datepickerAI1.SelectedDate;
            cbAi2.SelectedItem = cbAnalysis.SelectedItem;
            cbai2strng = cbAnalysis.SelectedItem.ToString();
            tab8.IsSelected = true;
            loadinganalysis();
        }



        private void loading()
        {
            try
            {
                //pbProcessing.Value = 0;
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
                mWorker = new System.ComponentModel.BackgroundWorker();
                mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
                mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
                mWorker.WorkerReportsProgress = true;
                mWorker.WorkerSupportsCancellation = true;
                mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                mWorker.RunWorkerAsync();

                //  while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {
                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            //  loadingtelerickstore.Visibility = Visibility.Visible;
                            loadingtelerickstore.IsBusy = true;
                            // pbProcessing.Value = (pbProcessing.Value + 0.05) % 100;
                            // pbProcessing.Value = (pbProcessing.Value + 1);
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //  break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }

            
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            {
                if (datestate == Convert.ToDateTime(Blogic.opendate).ToString("yyyy/MM/dd"))
                {
                    if (typestate == "Weekly" && Blogic.aishop != null)
                        aishop = Blogic.aishop;
                    else if (typestate == "Daily" && Blogic.aishopDaily != null)
                        aishop = Blogic.aishopDaily;
                    else if (typestate == "Monthly" && Blogic.aishopMontly != null)
                        aishop = Blogic.aishopMontly;
                    else
                        aishop = bl.MostSellingTicket(typestate, datestate, "shop");
                }
                else
                {
                    aishop = bl.MostSellingTicket(typestate, datestate, "shop");
                }
            }
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            //  pbProcessing.Value = e.ProgressPercentage;
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation 
            mWorker.CancelAsync();
            loadingtelerickstore.IsBusy = false;
            //   loadingtelerickstore.Visibility = Visibility.Hidden;
            //pbProcessing.Visibility = Visibility.Hidden;
            //lblProcessing.Visibility = Visibility.Hidden;
            try                 //shop
            {
              
                if (aishop == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    dataLoadStore();
                }
            }
            catch
            {

            }
            Blogic.aifalg = 0;
        }

        private void cbboxtype_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                datestate = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                typestate = cbboxtype.SelectedItem.ToString();
                loading();
            }
            catch { }
        }

        private void datepicker_selecteddatechanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                datestate = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                typestate = cbboxtype.SelectedItem.ToString();
                loading();
            }
            catch { }
        }

        private void Cbvalue_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbValueShop.SelectedItem != null)
                {
                    value = cbValueShop.SelectedItem.ToString();
                    value = value.Replace("$", "");
                    List<AITicket> aiTicket = aishop.Data[value];
                    dgDataShop.ItemsSource = aiTicket;
                }
            }
            catch
            { }
        }

        private void loadingstate()
        {
            try
            {
                // pbProcessing.Value = 0;
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
                mWorkerstate = new System.ComponentModel.BackgroundWorker();
                mWorkerstate.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWorkstate);
                mWorkerstate.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChangedstate);
                mWorkerstate.WorkerReportsProgress = true;
                mWorkerstate.WorkerSupportsCancellation = true;
                mWorkerstate.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompletedstate);
                mWorkerstate.RunWorkerAsync();

                // while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {
                    if (!mWorkerstate.CancellationPending)
                    {
                        try
                        {
                            //  loadingtelerickinstate.Visibility = Visibility.Visible;
                            loadingtelerickinstate.IsBusy = true;
                            //  pbProcessing.Value = (pbProcessing.Value + 0.05) % 100;
                            //    pbProcessing.Value = (pbProcessing.Value + 1);
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    //  else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //  break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }
        }

        private void worker_DoWorkstate(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            {
                aistate = bl.MostSellingTicket(type_state, date_state, "state");
            }
        }

        private void worker_ProgressChangedstate(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            //  pbProcessing.Value = e.ProgressPercentage;
        }

        private void worker_RunWorkerCompletedstate(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation 
            mWorkerstate.CancelAsync();
     
            loadingtelerickinstate.IsBusy = false;
            //  loadingtelerickinstate.Visibility = Visibility.Hidden;


            try                         //state
            {
             
                if (aistate == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    dataLoadState();
                    // cbValueNetwork.SelectedItem = "OverAll";
                }
            }
            catch
            { }
        }


   private void     dataLoadState()
        {
            List<string> valuestate = new List<string>();
            for (int i = 0; i < aistate.Value.Count - 1; i++)
            {
                valuestate.Add("$" + aistate.Value[i]);
            }

            valuestate.Add("OverAll");
            cbValueNetwork.ItemsSource = valuestate;


            value = cbValueNetwork.SelectedItem.ToString();
            value = value.Replace("$", "");
            List<AITicket> aiTicket = aistate.Data[value];
            dgDataNetwork.ItemsSource = aiTicket;
        
     
            loadingtelerickinstate.IsBusy = false;
        }

   private void dataLoadStore()
   {

       List<string> valueshop = new List<string>();
       if (aishop.Value!=null) 
           for (int i = 0; i < aishop.Value.Count - 1; i++)
       {
           valueshop.Add("$" + aishop.Value[i]);
       }
       else
       {
           aishop.Value = new List<string>();
       }
       valueshop.Add("OverAll");
       cbValueShop.ItemsSource = valueshop;
        

            //if (valuecount == 0)
            {
           List<string> ai2value = new List<string>();
           for (int i = 0; i < (aishop.Value.Count) - 1; i++)
           {
               // cbAi2Value.Items.Add("$" + aishop.Value[i]);
               ai2value.Add("$" + aishop.Value[i]);
           }

           if (checkCall[1] == 0)
               cbAi2Value.ItemsSource = ai2value;

           //  cbAi2Value.SelectedIndex = 0;
           //  valuecount++;
       }

       if (cbValueShop.SelectedItem != null)
       {
           try
           {
               value = cbValueShop.SelectedItem.ToString();
               value = value.Replace("$", "");
               List<AITicket> aiTicket = aishop.Data[value];
               dgDataShop.ItemsSource = aiTicket;
           }
           catch { }
       }

          //  cbValueNetwork.ItemsSource = valueshop;
            loadingtelerickstore.IsBusy = false;
   }
        private void cbValueNetwork_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbValueNetwork.SelectedItem != null)
            {
                value = cbValueNetwork.SelectedItem.ToString();
                value = value.Replace("$", "");
                List<AITicket> aiTicket = aistate.Data[value];
                dgDataNetwork.ItemsSource = aiTicket;
            }
        }

        private void cbboxtypestate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //  List<string> valuestate = new List<string>();
                date_state = Convert.ToDateTime(datepicker_state.SelectedDate).ToString("yyyy/MM/dd");
                type_state = cbboxtypestate.SelectedItem.ToString();
                loadingstate();
            }
            catch { }
        }

        private void datepickerstate_selecteddatechanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //  List<string> valuestate = new List<string>();
                date_state = Convert.ToDateTime(datepicker_state.SelectedDate).ToString("yyyy/MM/dd");
                type_state = cbboxtypestate.SelectedItem.ToString();
                loadingstate();
            }
            catch { }
        }


        private void loadingnextweekorder()
        {
            try
            {
                // pbProcessing.Value = 0;
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
                mWorkernxtweekorder = new System.ComponentModel.BackgroundWorker();
                mWorkernxtweekorder.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWorknxtweekorder);
                mWorkernxtweekorder.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChangednextweekorder);
                mWorkernxtweekorder.WorkerReportsProgress = true;
                mWorkernxtweekorder.WorkerSupportsCancellation = true;
                mWorkernxtweekorder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompletednextweekorder);
                mWorkernxtweekorder.RunWorkerAsync();


                //  while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {
                    if (!mWorkernxtweekorder.CancellationPending)
                    {
                        try
                        {
                            //   loadingtelericknextweekorder.Visibility = Visibility.Visible;
                            loadingtelericknextweekorder.IsBusy = true;
                            // pbProcessing.Value = (pbProcessing.Value + 0.05) % 100;
                            // pbProcessing.Value = (pbProcessing.Value + 1);
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //  break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }
        }

        private void worker_DoWorknxtweekorder(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

            if (Blogic.aiNextweekorder == null)
                ai = bl.AI_NextweekOrderDetails();

            else
                ai = Blogic.aiNextweekorder;
        }

        private void worker_ProgressChangednextweekorder(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            //  pbProcessing.Value = e.ProgressPercentage;
        }

        private void worker_RunWorkerCompletednextweekorder(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation 
            mWorkernxtweekorder.CancelAsync();
            loadingtelericknextweekorder.IsBusy = false;
            // loadingtelericknextweekorder.Visibility = Visibility.Hidden;
            //pbProcessing.Visibility = Visibility.Hidden;
            //lblProcessing.Visibility = Visibility.Hidden;
            try
            {
                if (ai == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                    dgDataOrder.ItemsSource = ai;
            }
            catch { }
        }

        private void loadingaisugesstion()
        {
            try
            {
                // pbProcessing.Value = 0;
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
                mWorkeraaisugestion = new System.ComponentModel.BackgroundWorker();
                mWorkeraaisugestion.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWorkaisugetion);
                mWorkeraaisugestion.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChangedaisugesttion);
                mWorkeraaisugestion.WorkerReportsProgress = true;
                mWorkeraaisugestion.WorkerSupportsCancellation = true;
                mWorkeraaisugestion.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompletedaisugestion);
                mWorkeraaisugestion.RunWorkerAsync();


                // while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {
                    if (!mWorkeraaisugestion.CancellationPending)
                    {
                        try
                        {
                            // loadingtelericksugestion.Visibility = Visibility.Visible;
                            loadingtelericksugestion.IsBusy = true;
                            //  pbProcessing.Value = (pbProcessing.Value + 0.05) % 100;
                            //   pbProcessing.Value = (pbProcessing.Value + 1);
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //  break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }
        }

        private void cbboxactive_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            box = cbBoxActive.SelectedItem.ToString();
            if (TryToParse(box) == true)
            {
                loadingaisugesstion();
            }
            //List<AITicket> ai = bl.Aisugestion(box);
            //if (ai == null && bl.internetconnection() == false)
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();  
            //}
            // else
            //dgDataActive.ItemsSource = ai;
            //}
        }

        private void worker_DoWorkaisugetion(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            {
                aisugestion = bl.Aisugestion(box);
            }
        }

        private void worker_ProgressChangedaisugesttion(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            //  pbProcessing.Value = e.ProgressPercentage;
        }

        private void worker_RunWorkerCompletedaisugestion(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation 
            mWorkeraaisugestion.CancelAsync();
            loadingtelericksugestion.IsBusy = false;
            //  loadingtelericksugestion.Visibility = Visibility.Hidden;
            //pbProcessing.Visibility = Visibility.Hidden;
            //lblProcessing.Visibility = Visibility.Hidden;
            try
            {
                if (aisugestion == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                    dgDataActive.ItemsSource = aisugestion;
            }
            catch { }
        }

        public void color()
        {
            loadingtelerickstore.Foreground = new SolidColorBrush(settings.Font);
            loadingtelerickaianaalysis.Foreground = new SolidColorBrush(settings.Font);
            loadingtelericknextweekorder.Foreground = new SolidColorBrush(settings.Font);
            loadingtelericksugestion.Foreground = new SolidColorBrush(settings.Font);
            loadingtelerickinstate.Foreground = new SolidColorBrush(settings.Font);
            cbAi2Value.Background = new SolidColorBrush(settings.Tab);
            cbAi2Value.Foreground = new SolidColorBrush(settings.Font);
            cbValueShop.Background = new SolidColorBrush(settings.Tab);
            cbboxtype.Background = new SolidColorBrush(settings.Tab);
            cbValueNetwork.Background = new SolidColorBrush(settings.Tab);
            cbboxtypestate.Background = new SolidColorBrush(settings.Tab);
            cbAnalysis.Background = new SolidColorBrush(settings.Tab);
            cbAiLevel1.Background = new SolidColorBrush(settings.Tab);
            cbBoxActive.Background = new SolidColorBrush(settings.Tab);
            btAiActive.Background = new SolidColorBrush(settings.Tab);
            tab1.Background = new SolidColorBrush(settings.Tab);
            tab2.Background = new SolidColorBrush(settings.Tab);
            tab3.Background = new SolidColorBrush(settings.Tab);
            tab4.Background = new SolidColorBrush(settings.Tab);
            tab5.Background = new SolidColorBrush(settings.Tab);
            tab6.Background = new SolidColorBrush(settings.Tab);
            tab7.Background = new SolidColorBrush(settings.Tab);
            tab8.Background = new SolidColorBrush(settings.Tab);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            tab1.Foreground = new SolidColorBrush(settings.Font);
            tab2.Foreground = new SolidColorBrush(settings.Font);
            tab3.Foreground = new SolidColorBrush(settings.Font);
            tab4.Foreground = new SolidColorBrush(settings.Font);
            tab5.Foreground = new SolidColorBrush(settings.Font);
            tab6.Foreground = new SolidColorBrush(settings.Font);
            tab7.Foreground = new SolidColorBrush(settings.Font);
            tab8.Foreground = new SolidColorBrush(settings.Font);
            cbValueShop.Foreground = new SolidColorBrush(settings.Font);
            cbboxtype.Foreground = new SolidColorBrush(settings.Font);
            cbValueNetwork.Foreground = new SolidColorBrush(settings.Font);
            cbboxtypestate.Foreground = new SolidColorBrush(settings.Font);
            cbAnalysis.Foreground = new SolidColorBrush(settings.Font);
            cbAiLevel1.Foreground = new SolidColorBrush(settings.Font);
            cbBoxActive.Foreground = new SolidColorBrush(settings.Font);
            btAiActive.Foreground = new SolidColorBrush(settings.Font);
            dgDataShop.Foreground = new SolidColorBrush(settings.Font);
            dgDataNetwork.Foreground = new SolidColorBrush(settings.Font);
            dgDataActive.Foreground = new SolidColorBrush(settings.Font);
            dgDataOrder.Foreground = new SolidColorBrush(settings.Font);
            cbAiLevel2.Background = new SolidColorBrush(settings.Tab);
            cbAiLevel2.Foreground = new SolidColorBrush(settings.Font);
            cbAi2.Background = new SolidColorBrush(settings.Tab);
            cbAi2.Foreground = new SolidColorBrush(settings.Font);
            tab6AI2.Background = new SolidColorBrush(settings.Tab);
            tab7AI2.Background = new SolidColorBrush(settings.Tab);
            tab8AI2.Background = new SolidColorBrush(settings.Tab);
            tab6AI2.Foreground = new SolidColorBrush(settings.Font);
            tab7AI2.Foreground = new SolidColorBrush(settings.Font);
            tab8AI2.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            btnprintstore.Background = new SolidColorBrush(settings.Tab);
            btnprintstore.Foreground = new SolidColorBrush(settings.Font);
            btnprintstate.Background = new SolidColorBrush(settings.Tab);
            btnprintstate.Foreground = new SolidColorBrush(settings.Font);
            btnprintpieai1.Background = new SolidColorBrush(settings.Tab);
            btnprintpieai1.Foreground = new SolidColorBrush(settings.Font);
            btnprintpieai2.Background = new SolidColorBrush(settings.Tab);
            btnprintpieai2.Foreground = new SolidColorBrush(settings.Font);
            btnextweekorderpdf.Background = new SolidColorBrush(settings.Tab);
            btnextweekorderpdf.Foreground = new SolidColorBrush(settings.Font);
            dgDataShop.BorderBrush = new SolidColorBrush(settings.Border);
            pieSales.BorderBrush = new SolidColorBrush(settings.Border);
            pieSpace.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataSalesDivision.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataSpaceDivision.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataResult.BorderBrush = new SolidColorBrush(settings.Border);
            pieSalesAI2.BorderBrush = new SolidColorBrush(settings.Border);
            pieSpaceAI2.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataSalesDivisionAI2.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataSpaceDivisionAI2.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataResultAI2.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataOrder.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataActive.BorderBrush = new SolidColorBrush(settings.Border);
            dgDataNetwork.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);

            dgDataShop.FontFamily = new FontFamily(settings.datagridfont);
            dgDataSalesDivision.FontFamily = new FontFamily(settings.datagridfont);
            dgDataSpaceDivision.FontFamily = new FontFamily(settings.datagridfont);
            dgDataResult.FontFamily = new FontFamily(settings.datagridfont);
            dgDataOrder.FontFamily = new FontFamily(settings.datagridfont);
            dgDataActive.FontFamily = new FontFamily(settings.datagridfont);
            dgDataNetwork.FontFamily = new FontFamily(settings.datagridfont);

        }

        private void btAiActive_Click(object sender, RoutedEventArgs e)
        {
            string box = cbBoxActive.SelectedItem.ToString();
            if (TryToParse(box) == true)
            {
                bool flag = false;
                while (!flag)                         //means flag=false
                {
                    var dialog = new ActiveTickets(box);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        this.NavigationService.Refresh();
                        dialog.Close();
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
            }
            else
            {
                return result;
            }
        }

        private System.Windows.Controls.DataVisualization.ResourceDictionaryCollection MakePalette(int Count)
        {
            string[] str = new string[7];
            int i = 0;
            System.Windows.Controls.DataVisualization.ResourceDictionaryCollection palette = new System.Windows.Controls.DataVisualization.ResourceDictionaryCollection();

            ResourceDictionary rd = new ResourceDictionary();
            Style style = new Style(typeof(Control));
            //Style style1 = new Style(typeof(Control));
            //Style style2 = new Style(typeof(Control));
            SolidColorBrush brush = null;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;


            brush = new SolidColorBrush(Colors.Blue);
            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);
            if (Count == 1)
                return palette;
            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.Magenta);
            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 2)
                return palette;


            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.Green);
            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 3)
                return palette;


            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.Purple);
            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 4)
                return palette;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;


            brush = new SolidColorBrush(Colors.Yellow);
            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 5)
                return palette;


            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.OrangeRed);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 6)
                return palette;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.MistyRose);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 7)
                return palette;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.LimeGreen);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 8)
                return palette;


            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.DarkGray);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 9)
                return palette;


            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;


            brush = new SolidColorBrush(Colors.PaleGreen);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);


            if (Count == 10)
                return palette;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.Plum);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 11)
                return palette;



            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.RosyBrown);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);


            if (Count == 12)
                return palette;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.Tomato);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 13)
                return palette;



            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.Wheat);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 14)
                return palette;
            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.YellowGreen);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);

            if (Count == 15)
                return palette;

            rd = new ResourceDictionary();
            style = new Style(typeof(Control));
            brush = null;

            brush = new SolidColorBrush(Colors.OliveDrab);

            style.Setters.Add(new Setter() { Property = Control.BackgroundProperty, Value = brush });
            rd.Add("DataPointStyle", style);
            palette.Add(rd);
            return palette;
        }

        private void btnprintstore_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                string value = cbValueShop.SelectedItem.ToString();
                value = value.Replace("$", "");
                string type = cbboxtype.SelectedItem.ToString();
                string link = " https://www.realtnetworking.com/API/LAI2/GetMostSellingTicketValuePDF.aspx?Date=" + date + "&Type=" + type + "&State_Shop=Shop&UserId=" + LoginDetail.user.UserId + "&Value=" + value + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&print=yes";
                var response = client.DownloadData(new Uri(String.Format(link)));
                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\Mostsellingticketstorepdf.pdf", response);
            }
            catch
            { return; }
            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\Mostsellingticketstorepdf.pdf");
            Process.Start(sInfo);

        }

        private void btnprintstate_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string date = Convert.ToDateTime(datepicker_state.SelectedDate).ToString("yyyy/MM/dd");
                string value = cbValueNetwork.SelectedItem.ToString();
                value = value.Replace("$", "");
                string type = cbboxtypestate.SelectedItem.ToString();
                string url = String.Format(" https://www.realtnetworking.com/API/LAI2/GetMostSellingTicketValuePDF.aspx?Date=" + date + "&Type=" + type + "&State_Shop=State&UserId=" + LoginDetail.user.UserId + "&Value=" + value + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&print=yes");
                var response = client.DownloadData(new Uri(String.Format(" https://www.realtnetworking.com/API/LAI2/GetMostSellingTicketValuePDF.aspx?Date=" + date + "&Type=" + type + "&State_Shop=State&UserId=" + LoginDetail.user.UserId + "&Value=" + value + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&print=yes")));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\MostsellingticketStatepdf.pdf", response);

            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\MostsellingticketStatepdf.pdf");
            Process.Start(sInfo);
        }

        private void btnprintai2_click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string date = Convert.ToDateTime(datepicker2_graph.SelectedDate).ToString("yyyy/MM/dd");
                string value = cbAi2Value.SelectedItem.ToString();
                value = value.Replace("$", "");
                string type = cbAi2.SelectedItem.ToString();
                var response = client.DownloadData(new Uri(String.Format(" https://www.realtnetworking.com/API/LAI2/PiechartAI2pdf.aspx?Date=" + date + "&Type=" + type + "&Value=" + value + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&print=yes")));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\PieChartAi2pdf.pdf", response);

            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\PieChartAi2pdf.pdf");
            Process.Start(sInfo);
        }

        private void btnprintai1_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string date = Convert.ToDateTime(datepickerAI1.SelectedDate).ToString("yyyy/MM/dd");
                string type = cbAnalysis.SelectedItem.ToString();
                var response = client.DownloadData(new Uri(String.Format(" https://www.realtnetworking.com/API/LAI2/PiechartAI1Pdf.aspx?Date=" + date + "&Type=" + type + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&print=yes")));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\PieChartAi1pdf.pdf", response);

            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\PieChartAi1pdf.pdf");
            Process.Start(sInfo);
        }

        private void btnextweekorderpdf_click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string link = " https://www.realtnetworking.com/API/LAI2/AInextweekorderpdf.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&print=yes";
                var response = client.DownloadData(new Uri(String.Format(link)));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\AiNextWeekOrderpdf.pdf", response);

            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\AiNextWeekOrderpdf.pdf");
            Process.Start(sInfo);
        }





    }
}
