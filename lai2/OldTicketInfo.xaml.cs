﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for OldTicketInfo.xaml
    /// </summary>
    public partial class OldTicketInfo : Window
    {
        private Settings settings = Properties.Settings.Default;
        private Bal.ResultWithOldTicket rt;
        private int fixsoldoutstatus;
        Blogic bl = new Blogic();

        public OldTicketInfo()
        {
            InitializeComponent();
            color();
            btInactivefix.Visibility = Visibility.Hidden;
            btback.Visibility = Visibility.Hidden;

        }

        public void color()
        {
            btSolidout.Background = new SolidColorBrush(settings.Tab);
            btInactive.Background = new SolidColorBrush(settings.Tab);
            btReturn.Background = new SolidColorBrush(settings.Tab);
            lbtext.Foreground = new SolidColorBrush(settings.Theme);
            lbTicketname.Foreground = new SolidColorBrush(settings.Font);
            lbTicketId.Foreground = new SolidColorBrush(settings.Font);
            lbBox.Foreground = new SolidColorBrush(settings.Font);
            lbpackno.Foreground = new SolidColorBrush(settings.Font);
            lbOpenno.Foreground = new SolidColorBrush(settings.Font);
            btSolidout.Foreground = new SolidColorBrush(settings.Font);
            btInactive.Foreground = new SolidColorBrush(settings.Font);
            btReturn.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            btSolidout.Foreground = new SolidColorBrush(settings.Font);
            btInactive.Foreground = new SolidColorBrush(settings.Font);
            btReturn.Foreground = new SolidColorBrush(settings.Font);
            btback.Background = new SolidColorBrush(settings.Tab);
            btback.Foreground = new SolidColorBrush(settings.Font);
            btInactivefix.Foreground = new SolidColorBrush(settings.Font);
            btInactivefix.Background = new SolidColorBrush(settings.Tab);
            lbvalue.Foreground = new SolidColorBrush(settings.Font);
            lbtext.FontFamily = new FontFamily(settings.Fontstyle);
            lbstatus.FontFamily = new FontFamily(settings.labelfont);
        }

        public OldTicketInfo(Bal.ResultWithOldTicket rt)
        {
            InitializeComponent();
            // TODO: Complete member initialization
            color();
            this.rt = rt;
            lbTicketId.Content = rt.OldTicketInfo.TicketId;
            lbTicketname.Content = rt.OldTicketInfo.TicketName;
            lbpackno.Content = rt.OldTicketInfo.PackNo;
            lbBox.Content = rt.OldTicketInfo.Box;
            lbOpenno.Content = rt.OldTicketInfo.OpenNo;
            lbvalue.Content = rt.OldTicketInfo.Value.ToString("C");
            btInactivefix.Visibility = Visibility.Hidden;
            btback.Visibility = Visibility.Hidden;
            btSolidout.Focus();

        }

        public OldTicketInfo(Bal.ResultWithOldTicket rt, int fixsoldoutstatus)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();
            this.rt = rt;
            this.fixsoldoutstatus = fixsoldoutstatus;
            lbstatus.Visibility = Visibility.Hidden;
            lbTicketId.Content = rt.OldTicketInfo.TicketId;
            lbTicketname.Content = rt.OldTicketInfo.TicketName;
            lbpackno.Content = rt.OldTicketInfo.PackNo;
            lbBox.Content = rt.OldTicketInfo.Box;
            lbOpenno.Content = rt.OldTicketInfo.OpenNo;
            lbvalue.Content = rt.OldTicketInfo.Value.ToString("C");
            lbtext.Content = "Fix Sold Out in Progress";
            if (fixsoldoutstatus == 1)
            {
                btInactive.Visibility = Visibility.Hidden;
                btSolidout.Visibility = Visibility.Hidden;
                btReturn.Visibility = Visibility.Hidden;
                btInactivefix.Visibility = Visibility.Visible;
                btback.Visibility = Visibility.Visible;
               // btInactive.Content = "Inactive";
                btInactivefix.Focus();
            }
            else

            btSolidout.Focus();          
        }

        private void btSolidout_Click(object sender, RoutedEventArgs e)
        {
            string ticketid = Convert.ToString(lbTicketId.Content);
            string ticketname = Convert.ToString(lbTicketname.Content);
            string packno = Convert.ToString(lbpackno.Content);
            string box = Convert.ToString(lbBox.Content);
            string openno = Convert.ToString(lbOpenno.Content);
            var dialog = new SoldoutTickets(ticketid, ticketname, packno, box, openno);           
            if (dialog.ShowDialog() == true && dialog.DialogResult==true)
            {
                this.DialogResult = true;
            }
        }

        private void btInactive_Click(object sender, RoutedEventArgs e)
        {           
                string ticketid = Convert.ToString(lbTicketId.Content);
                string ticketname = Convert.ToString(lbTicketname.Content);
                string packno = Convert.ToString(lbpackno.Content);
                string box = Convert.ToString(lbBox.Content);
                string openno = Convert.ToString(lbOpenno.Content);
                var dialog = new InactiveWindow(ticketid, packno, openno);

                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    this.DialogResult = true;
                }
            
        }

        private void btReturn_Click(object sender, RoutedEventArgs e)
        {
            string ticketid = Convert.ToString(lbTicketId.Content);
            string ticketname = Convert.ToString(lbTicketname.Content);
            string packno = Convert.ToString(lbpackno.Content);
            string box = Convert.ToString(lbBox.Content);
            string openno = Convert.ToString(lbOpenno.Content);
            var dialog = new ReturnTickets(ticketid, packno, openno);

            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                this.DialogResult = true;
            }
        }

        private void btback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btinactivefix_click(object sender, RoutedEventArgs e)
        {
            if (fixsoldoutstatus == 1)
            {
                string ticketid = Convert.ToString(lbTicketId.Content);
                string ticketname = Convert.ToString(lbTicketname.Content);
                string packno = Convert.ToString(lbpackno.Content);
                string box = Convert.ToString(lbBox.Content);
                string openno = Convert.ToString(lbOpenno.Content);
                var dialog = new InactiveWindow(ticketid, packno, openno);

                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    this.DialogResult = true;
                }
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.34;
        } 
    }
}
