﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ActiveScan.xaml
    /// </summary>
    public partial class ActiveScan : Window
    {
        private Settings settings = Properties.Settings.Default;
        public ActiveScan()
        {
            InitializeComponent();

            if(settings.TicketActivationOrder==0)
            {
                rdbtnzero.Content = "Zero";
            }
            else
            {
                rdbtnzero.Content = "Maximum Number";
            }
            if (settings.ActiveScan == 0)
            {
                rdbtnzero.IsChecked = true;

            }
            else
            {
                rdbtnscnnumber.IsChecked = true;
            
            }
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            rdbtnzero.Foreground = new SolidColorBrush(settings.Font);
            rdbtnscnnumber.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);
            rdbtnzero.FontFamily = new FontFamily(settings.labelfont);
            rdbtnscnnumber.FontFamily = new FontFamily(settings.labelfont);

        }

        private void rdbtnzero_checked(object sender, RoutedEventArgs e)
        {
           
          //  settings.Save();
        }

        private void rdbtnscannumber_checked(object sender, RoutedEventArgs e)
        {
          //  settings.ActiveScan = 1;
           // settings.Save();
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            if (rdbtnzero.IsChecked == true)
            {
                settings.ActiveScan = 0;
            }
            else
            {
                settings.ActiveScan = 1;
            }
            settings.Save();
            this.DialogResult = true;
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        } 
    }
}
