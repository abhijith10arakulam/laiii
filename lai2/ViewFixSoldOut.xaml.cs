﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewFixSoldOut.xaml
    /// </summary>
    public partial class ViewFixSoldOut : Page
    {
        ReportDataList r = new ReportDataList();
        System.ComponentModel.BackgroundWorker mWorker; 
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        string date = DateTime.Now.ToString();
        string time = DateTime.Now.ToString();
        public ViewFixSoldOut()
        {
            InitializeComponent();

            lbstartdate.Content = Blogic.opendate;
            txtlegal.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin1.Content = LoginDetail.user.UserName.ToString();

            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            dtInactiveTickets.Foreground = new SolidColorBrush(settings.Complite);
            dtReturnTickets.Foreground = new SolidColorBrush(settings.Complite);
            dtSoldoutTickets.Foreground = new SolidColorBrush(settings.Complite);
            this.Background = new SolidColorBrush(settings.Background);
            dtInactiveTickets.BorderBrush = new SolidColorBrush(settings.Border);
            dtReturnTickets.BorderBrush = new SolidColorBrush(settings.Border);
            dtSoldoutTickets.BorderBrush = new SolidColorBrush(settings.Border);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin1.Foreground = new SolidColorBrush(settings.Font);
            dtInactiveTickets.FontFamily = new FontFamily(settings.datagridfont);
            dtReturnTickets.FontFamily = new FontFamily(settings.datagridfont);
            dtSoldoutTickets.FontFamily = new FontFamily(settings.datagridfont);
            lblcnnme.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            lblegalname.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin1.FontFamily = new FontFamily(settings.labelfont);
            lbuserlogin.FontFamily = new FontFamily(settings.labelfont);
            lbopndte.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            loadingtelerick.Foreground = new SolidColorBrush(settings.Font);

            tab1.Background = new SolidColorBrush(settings.Tab);
            tab2.Background = new SolidColorBrush(settings.Tab);
            tab3.Background = new SolidColorBrush(settings.Tab);

            tab1.BorderBrush = new SolidColorBrush(settings.Complite);
            tab2.BorderBrush = new SolidColorBrush(settings.InactiveC);
            tab3.BorderBrush = new SolidColorBrush(settings.ReturnC);

            tab1.Foreground = new SolidColorBrush(settings.Font);
            tab2.Foreground = new SolidColorBrush(settings.Font);
            tab3.Foreground = new SolidColorBrush(settings.Font);

            date = DateTime.Now.ToString();
            time = DateTime.Now.ToString();

            dtInactiveTickets.Foreground= new SolidColorBrush(settings.InactiveC);
            dtReturnTickets.Foreground = new SolidColorBrush(settings.ReturnC);
            dtSoldoutTickets.Foreground= new SolidColorBrush(settings.Complite);
            
            loading();
        }        
        
        public void loading()
        {
            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
            new System.Threading.ThreadStart(delegate { }));
            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;
                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }
                else
                {
                    // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                    //   break;
                }

                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            r = bl.TodaysData(date, time);
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            var ticketsSoldOut = r.ShiftData.Where(a => a.SoldoutStatus == 1).ToList();
            var ticketsInactive = r.ShiftData.Where(a => a.InactiveStatus == 1).ToList();
            var ticketsReturn = r.ReturnData;

            for (int i = 0; i < ticketsInactive.Count; i++)
            {
                ticketsInactive[i].rank = i + 1;
            }

            for (int i = 0; i < ticketsSoldOut.Count; i++)
            {
                ticketsSoldOut[i].rank = i + 1;
            }

            for (int i = 0; i < ticketsReturn.Count; i++)
            {
                ticketsReturn[i].rank = i + 1;
            }
            dtSoldoutTickets.ItemsSource = ticketsSoldOut;
            dtReturnTickets.ItemsSource = ticketsReturn;
                
            dtInactiveTickets.ItemsSource = ticketsInactive;
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }       

        private void DatagridSelection_Changed(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReportData r = (ReportData)dtSoldoutTickets.SelectedItem;
                var dialog = new FixSoldOut(r);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {                   
                  //  this.NavigationService.Refresh();
                    //tab1.Focus();
                    dtSoldoutTickets.UnselectAll();
                    loading();
                }
            }
            catch
            {
                //string heading = "Invalid Selection";
                //var dialog = new DialogBox_BoxNo(heading);
                //dialog.ShowDialog();
            }
        }

        private void dtInactiveTickets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReportData r = (ReportData)dtInactiveTickets.SelectedItem;
                var dialog = new FixSoldOut(r,"Inactive");
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    dtInactiveTickets.UnselectAll();
                    loading();
                    //this.NavigationService.Refresh();
                    //tabctrl.SelectedIndex = 2;// tabctrl.SelectedValue = tab3;
                }
            }
            catch
            {
                //string heading = "Invalid Selection";
                //var dialog = new DialogBox_BoxNo(heading);
                //dialog.ShowDialog();
            }
        }

        private void dtReturnTickets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SpecificTicket r = (SpecificTicket)dtReturnTickets.SelectedItem;

                ReportData rr = new ReportData();
                rr.TicketId = r.TicketId;
                rr.PackNo = r.PackNo;
                rr.OpenNo=0;
                rr.TicketName = r.TicketName;
                rr.Box=r.Box;
                
                
                var dialog = new FixSoldOut(rr,"Return");
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                  //  this.NavigationService.Refresh();
                    //tabctrl.SelectedIndex = 2;
                    dtReturnTickets.UnselectAll();
                    loading();
                }
            }
            catch
            {
                //string heading = "Invalid Selection";
                //var dialog = new DialogBox_BoxNo(heading);
                //dialog.ShowDialog();
            }
        }
    }
}
