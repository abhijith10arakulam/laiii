﻿using lai2.Bal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using Telerik.Windows.Controls;

namespace lai2
{
    public class ViewModel : ViewModelBase
    {
        private string data;
        //public ViewModel(string data)
        //{
        //    this.data = data;
        //}

        private AutoCompleteItem selectedItem;

        public AutoCompleteItem MySelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                OnPropertyChanged("MySelectedItem");
            }
        }

     
        
        private ObservableCollection<AutoCompleteItem> collection;
        public ObservableCollection<AutoCompleteItem> Collection
        {
            get
            {
                AutoComplete rt = new AutoComplete();


                  if (collection == null)
                {
                    Blogic bl = new Blogic();
                      rt=bl.AutoCompliteData();

                  
                    collection = new ObservableCollection<AutoCompleteItem>();
                    for (int i = 0; rt.ticketIdList!=null && i < rt.ticketIdList.Count; i++)
                    {
                        var newItem = new AutoCompleteItem() { Name = rt.ticketIdList[i], ID = i };
                        collection.Add(newItem);
                    }
                }
                return collection;
            }

        }



    }
}
