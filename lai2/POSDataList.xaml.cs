﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Windows.Media.Animation;
using System.Media;
using System.Collections;
using System.Text.RegularExpressions;

namespace lai2
{
    /// <summary>
    /// Interaction logic for DataList.xaml
    /// </summary>
    public partial class POSDataList : Page
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        ReportDataList rl = new ReportDataList();

        System.ComponentModel.BackgroundWorker mWorker;

        public POSDataList()
        {
            InitializeComponent();

            if (LoginDetail.user.UserType == "ShopAdmin")
            {
                try
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.none);
                    snd.Play();
                }
                catch { }
            }

            //lbinsatntsale.Visibility = Visibility.Visible;
            //lbinsatntsaleval.Visibility = Visibility.Visible;

            //tab1.Background = new SolidColorBrush(settings.Tab);
            //tab2.Background = new SolidColorBrush(settings.Tab);
            //tab3.Background = new SolidColorBrush(settings.Tab);
            //tab4.Background = new SolidColorBrush(settings.Tab);
            //tab5.Background = new SolidColorBrush(settings.Tab);

            ////  tab1.BorderBrush = new SolidColorBrush(settings.Tab);
            //tab2.BorderBrush = new SolidColorBrush(settings.ActiveC);
            //tab3.BorderBrush = new SolidColorBrush(settings.Complite);
            //tab4.BorderBrush = new SolidColorBrush(settings.InactiveC);
            //tab5.BorderBrush = new SolidColorBrush(settings.ReturnC);

            //  tab1.BorderThickness = new Thickness(5);

            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin.Foreground = new SolidColorBrush(settings.Font);
            //tab1.Foreground = new SolidColorBrush(settings.Font);
            //tab2.Foreground = new SolidColorBrush(settings.Font);
            //tab3.Foreground = new SolidColorBrush(settings.Font);
            //tab4.Foreground = new SolidColorBrush(settings.Font);
            //tab5.Foreground = new SolidColorBrush(settings.Font);
            ////lbinsatntsaleval.Foreground = new SolidColorBrush(settings.Font);
            //activatedTickets.Foreground = new SolidColorBrush(settings.Font);
            //soldoutTickets.Foreground = new SolidColorBrush(settings.Font);
            //activatedTickets.Foreground = new SolidColorBrush(settings.ActiveC);
            //soldoutTickets.Foreground = new SolidColorBrush(settings.Complite);
            //InactiveTickets.Foreground = new SolidColorBrush(settings.InactiveC);
            //ReturnTickets.Foreground = new SolidColorBrush(settings.ReturnC);
            this.Background = new SolidColorBrush(settings.Background);
            //telerickgrid.Background = new SolidColorBrush(settings.Background);
            //loading.Background = new SolidColorBrush(settings.Background);
            //loading.Foreground = new SolidColorBrush(settings.Font);
            //LatestData.BorderBrush = new SolidColorBrush(settings.Border);
            //activatedTickets.BorderBrush = new SolidColorBrush(settings.Border);
            //soldoutTickets.BorderBrush = new SolidColorBrush(settings.Border);
            //InactiveTickets.BorderBrush = new SolidColorBrush(settings.Border);
            //ReturnTickets.BorderBrush = new SolidColorBrush(settings.Border);
            txtlegal.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin.Content = LoginDetail.user.UserName.ToString();

            //LatestData.FontFamily = new FontFamily(settings.datagridfont);
            //activatedTickets.FontFamily = new FontFamily(settings.datagridfont);
            //soldoutTickets.FontFamily = new FontFamily(settings.datagridfont);
            //InactiveTickets.FontFamily = new FontFamily(settings.datagridfont);
            //ReturnTickets.FontFamily = new FontFamily(settings.datagridfont);

            lblglname.FontFamily = new FontFamily(settings.labelfont);
            lbloctnname.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            lbopndate.FontFamily = new FontFamily(settings.labelfont);
            lbuserlogn.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin.FontFamily = new FontFamily(settings.labelfont);
            //tbEmptyBoxes.Foreground = new SolidColorBrush(settings.Font);
            //     lbApp.FontFamily = new FontFamily(settings.Fontstyle);
            //   lbApp.Foreground = new SolidColorBrush(settings.Theme);

            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
            //  pbProcessing.Value = 0;

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();

            //Duration duration = new Duration(TimeSpan.FromSeconds(20));

            //DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
            //pbProcessing.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
            try
            {
                //while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {

                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            //loading.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }

            loadPOSData();
            
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string time = DateTime.Now.ToString("hh:mm:sstt");
            rl = bl.TodaysData(date, time);


        }


        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            // pbProcessing.Value = e.ProgressPercentage;
            //if (pbProcessing.Value == 0.05)
            //{
            //    mWorker.CancelAsync();
            //}
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation  
            mWorker.CancelAsync();
            //loading.IsBusy = false;
            if (rl.ShiftData == null)
            {
                if (bl.internetconnection() == false)
                {
                    string heading = "     Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    mWorker.RunWorkerAsync();

                    //Duration duration = new Duration(TimeSpan.FromSeconds(20));

                    //DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
                    //pbProcessing.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
                    try
                    {
                        //while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                        {

                            if (!mWorker.CancellationPending)
                            {
                                try
                                {
                                    //loading.IsBusy = true;
                                }
                                catch (System.Exception ex)
                                {
                                    // No action required
                                }
                            }
                            else
                            {
                                // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                                //   break;
                            }

                            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                                   new System.Threading.ThreadStart(delegate { }));
                        }
                    }
                    catch { }

                }
            }
            else
            {


                if (rl.ExpiryCheck.Status == false)
                {
                    var dialog = new Expired(rl.ExpiryCheck.Description);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                    }
                    Blogic.ExpireFlag = 1;
                }
                else
                {
                    Blogic.ExpireFlag = 0;
                    //lbinsatntsaleval.Content = rl.InstantSales.ToString("C");
                    lbstartdate.Content = rl.StartDate;
                    Blogic.opendate = rl.StartDate;
                    //LatestData.ItemsSource = rl.ShiftData;
                    //activatedTickets.ItemsSource = rl.ActiveData.OrderByDescending(a => a.ActiveDate);
                    //soldoutTickets.ItemsSource = rl.SoldOutData.OrderByDescending(a => a.ActiveDate);
                    //InactiveTickets.ItemsSource = rl.InactiveData.OrderByDescending(a => a.ActiveDate);
                    //ReturnTickets.ItemsSource = rl.ReturnData.OrderByDescending(a => a.ActiveDate);
                    //tbEmptyBoxes.Text = rl.EmptyBoxes;



                    ////LatestData.FirstDisplayedScrollingRowIndex = Blogic.CurrentRow;
                    //if (Blogic.CurrentRow < rl.ShiftData.Count)
                    //{
                    //    if (Blogic.CurrentRow + 9 < rl.ShiftData.Count)
                    //        LatestData.ScrollIntoView(LatestData.Items[Blogic.CurrentRow + 9]);
                    //    else if ((Blogic.CurrentRow + 4 < rl.ShiftData.Count))
                    //        LatestData.ScrollIntoView(LatestData.Items[Blogic.CurrentRow + 4]);
                    //    else
                    //        LatestData.ScrollIntoView(LatestData.Items[Blogic.CurrentRow]);

                    //    Blogic.CurrentRow = 0;
                    //}
                }

            }
            hideValues();
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }



        private void alltickets_focused(object sender, RoutedEventArgs e)
        {
            //lbinsatntsale.Visibility = Visibility.Visible;
            //lbinsatntsaleval.Visibility = Visibility.Visible;
        }

        private void ActivatedTicket_focued(object sender, RoutedEventArgs e)
        {
            //lbinsatntsale.Visibility = Visibility.Hidden;
            //lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void ReturnTicket_Focussed(object sender, RoutedEventArgs e)
        {
            //lbinsatntsale.Visibility = Visibility.Hidden;
            //lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void inactiveTicket_gotfocussed(object sender, RoutedEventArgs e)
        {
            //lbinsatntsale.Visibility = Visibility.Hidden;
            //lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void SoldOutTicket_focussed(object sender, RoutedEventArgs e)
        {
            //lbinsatntsale.Visibility = Visibility.Hidden;
            //lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void cmdStart_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Frame_Navigated_1(object sender, NavigationEventArgs e)
        {

        }

        private void LatestData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            // Blogic.CurrentRow = LatestData.SelectedIndex;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        //private void alltickets_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    lbinsatntsale.Visibility = Visibility.Hidden;
        //    lbinsatntsaleval.Visibility = Visibility.Hidden;
        //}

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void dataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {

        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string content = (sender as Button).Tag.ToString();
                //MessageBox.Show("Event " + content);

                if (content.Contains("Cheque"))
                {
                    var dialog = new AddCheque();
                    dialog.ShowDialog();
                }
                else if (content.Contains("Money"))
                {
                    var dialog = new AddMoneyOrder();
                    dialog.ShowDialog();
                }
                else if (content.Contains("Cash"))
                {
                    var dialog = new AddCashInHand();
                    dialog.ShowDialog();
                }
            }
            catch (Exception ex) { }

        }

        private void btPOS_Click(object sender, RoutedEventArgs e)
        {
            LoginList ll = bl.LoginCheck(settings.username, settings.Password);

            if (ll.user.UserName != null)
            {
                Blogic.closeShiftButtonStatus = true;


                //btcloseshift.Visibility = Visibility.Visible;

                NavigationService navService = NavigationService.GetNavigationService(this);
                HomeAdmin nextPage = new HomeAdmin();
                navService.Navigate(nextPage);
            }
            else
            {

                settings.Password = "";
                settings.Save();
                NavigationService navService = NavigationService.GetNavigationService(this);
                Login nextPage = new Login();
                navService.Navigate(nextPage);
            }
        }

        private void btPOS_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void loadPOSData()
        {


            List<FillControls> items = new List<FillControls>();

            items.Add(new FillControls() { Department = "Fuel", Reg1 = "", Reg2 = "", Total = "" });
            items.Add(new FillControls() { Department = "Beer", Reg1 = "", Reg2 = "", Total = "" });
            items.Add(new FillControls() { Department = "Money Order", Reg1 = "", Reg2 = "", Total = "" });
            items.Add(new FillControls() { Department = "Tobaco", Reg1 = "", Reg2 = "", Total = "" });
            items.Add(new FillControls() { Department = "Grocery", Reg1 = "", Reg2 = "", Total = "" });
            items.Add(new FillControls() { Department = "Non Food", Reg1 = "$500", Reg2 = "$1500", Total = "$2000" });
            items.Add(new FillControls() { Department = "Phone Card", Reg1 = "$500", Reg2 = "$1000", Total = "$1500" });
            items.Add(new FillControls() { Department = "Lottery Scratch", Reg1 = "$500", Reg2 = "$500", Total = "$1000" });
            items.Add(new FillControls() { Department = "Lottery Online", Reg1 = "$310", Reg2 = "$310", Total = "$620" });

            items.Add(new FillControls() { Department = "", Reg1 = "", Reg2 = "", Total = "" });
            items.Add(new FillControls() { Department = "", Reg1 = "", Reg2 = "", Total = "" });

            items.Add(new FillControls() { Department = "No Tax", Reg1 = "$500", Reg2 = "$1500", Total = "$2000" });
            items.Add(new FillControls() { Department = "High Tax", Reg1 = "$500", Reg2 = "$1000", Total = "$1500" });
            items.Add(new FillControls() { Department = "Low Tax", Reg1 = "$500", Reg2 = "$500", Total = "$1000" });
            items.Add(new FillControls() { Department = "Total Income", Reg1 = "$2500", Reg2 = "$2100", Total = "$4600" });


            List<Departments> u = bl.viewAddDepartment();
            u.Add(new Departments() { Dept = "" });
            u.Add(new Departments() { Dept = "" });

            u.Add(new Departments() { Dept = "No Tax" });
            u.Add(new Departments() { Dept = "High Tax" });
            u.Add(new Departments() { Dept = "Low Tax" });
            u.Add(new Departments() { Dept = "Total Income" });

            if (u == null && bl.internetconnection() == false)
            {
                string heading1 = "        Poor Connectivity";
                var dialog1 = new DialogBox_BoxNo(heading1);
                dialog1.ShowDialog();
            }
            else
            {
                //gdDepartment.ItemsSource = u;
                dataGrid.ItemsSource = u;
            }
            //dataGrid.ItemsSource = items;




            List<FillSettlements> settlements = new List<FillSettlements>();

            settlements.Add(new FillSettlements() { Settlements = "Network Revenue", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "Loto Online Cash", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "Lotto Instant Cash", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "EBT", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "Coupon", TotalExp = "", Visible = "Hidden" });

            settlements.Add(new FillSettlements() { Settlements = "", TotalExp = "", Visible = "Hidden" });

            settlements.Add(new FillSettlements() { Settlements = "Cheque", TotalExp = "", Visible = "Visible" });
            settlements.Add(new FillSettlements() { Settlements = "Money Order", TotalExp = "", Visible = "Visible" });
            settlements.Add(new FillSettlements() { Settlements = "Cash in Hand", TotalExp = "", Visible = "Visible" });

            settlements.Add(new FillSettlements() { Settlements = "Total Drops", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "Loan", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "Total Settlement", TotalExp = "", Visible = "Hidden" });
            settlements.Add(new FillSettlements() { Settlements = "Short/Over", TotalExp = "", Visible = "Hidden" });



            dataGridSettlement.ItemsSource = settlements;



           
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            var textboxes1 = dataGrid.FindAllVisualDescendants()
    .Where(elt => elt.Name == "Reg1")
    .OfType<TextBox>();
            var textboxes2 = dataGrid.FindAllVisualDescendants()
  .Where(elt => elt.Name == "Reg2")
  .OfType<TextBox>();
            var total = dataGrid.FindAllVisualDescendants()
 .Where(elt => elt.Name == "Total")
 .OfType<TextBox>();

            for (int i = 0; i < dataGrid.Items.Count - 1; i++)
            {
                // string total1 = total.ElementAt(i).Text;
                string t1 = (textboxes1.ElementAt(i).Text.Length == 0) ? "0" : textboxes1.ElementAt(i).Text;
                string t2 = (textboxes2.ElementAt(i).Text.Length == 0) ? "0" : textboxes2.ElementAt(i).Text;

                total.ElementAt(i).Text = (Convert.ToDouble(t1) + Convert.ToDouble(t2)).ToString();
            }

          
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void hideValues()
        {
            var textboxes1 = dataGrid.FindAllVisualDescendants()
   .Where(elt => elt.Name == "Reg1")
   .OfType<TextBox>();
            var textboxes2 = dataGrid.FindAllVisualDescendants()
  .Where(elt => elt.Name == "Reg2")
  .OfType<TextBox>();
            var total = dataGrid.FindAllVisualDescendants()
 .Where(elt => elt.Name == "Total")
 .OfType<TextBox>();

            for (int i = 0; i < dataGrid.Items.Count-1; i++)
            {
                if (((Departments)dataGrid.Items[i]).Dept.Length == 0)
                {
                    TextBox tx = textboxes1.ElementAt(i);
                    tx.Visibility = Visibility.Hidden;
                    TextBox tx1 = textboxes2.ElementAt(i);
                    tx1.Visibility = Visibility.Hidden;
                    TextBox tt = total.ElementAt(i);
                    tt.Visibility = Visibility.Hidden;

                    
                    //  ((TextBox)textboxes1.ElementAt(i)).Visibility = Visibility.Hidden;
                }
                //// string total1 = total.ElementAt(i).Text;
                //string t1 = (textboxes1.ElementAt(i).Text.Length == 0) ? "0" : textboxes1.ElementAt(i).Text;
                //string t2 = (textboxes2.ElementAt(i).Text.Length == 0) ? "0" : textboxes2.ElementAt(i).Text;

                //total.ElementAt(i).Text = (Convert.ToDouble(t1) + Convert.ToDouble(t2)).ToString();
            }
        }
    }
    public class TodoItem
    {
        public string Title { get; set; }
        public int Completion { get; set; }
    }
    public class FillControls
    {
        public string Department { get; set; }
        public string Reg1 { get; set; }
        public string Reg2 { get; set; }
        public string Total { get; set; }
    }

    public class FillSettlements
    {
        public string Settlements { get; set; }
        public string TotalExp { get; set; }
        public string Visible { get; set; }
    }
    public static class VisualTreeHelperExtension
    {
        struct StackElement
        {
            public FrameworkElement Element { get; set; }
            public int Position { get; set; }
        }
        public static IEnumerable<FrameworkElement> FindAllVisualDescendants(this FrameworkElement parent)
        {
            if (parent == null)
                yield break;
            Stack<StackElement> stack = new Stack<StackElement>();
            int i = 0;
            while (true)
            {
                if (i < VisualTreeHelper.GetChildrenCount(parent))
                {
                    FrameworkElement child = VisualTreeHelper.GetChild(parent, i) as FrameworkElement;
                    if (child != null)
                    {
                        if (child != null)
                            yield return child;
                        stack.Push(new StackElement { Element = parent, Position = i });
                        parent = child;
                        i = 0;
                        continue;
                    }
                    ++i;
                }
                else
                {
                    // back at the root of the search
                    if (stack.Count == 0)
                        yield break;
                    StackElement element = stack.Pop();
                    parent = element.Element;
                    i = element.Position;
                    ++i;
                }
            }
        }
    }
}