﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddVendor : Window
    {
        string Selectedvalue;
        private Settings settings = Properties.Settings.Default;

        int validation = 0;
        int flag;
        Blogic bl = new Blogic();
        public AddVendor()
        {            
            InitializeComponent();
            color();
            Hiddenvalidation();
            txtboxVendorName.Focus();
            if (LoginDetail.user.UserType == "ShopAdmin")
            {
               // cbselectuser.Items.Add("Employee");

               //cbselectuser.Items.Add("Shop Manager");             
            }
            else if (LoginDetail.user.UserType == "ShopManeger")
            {
                //cbselectuser.Items.Add("Employee");
            }

            //cbselectuser.SelectedItem = "Employee";

           

            List<Departments> dept = bl.viewAddDepartment();

            cbDepartment.ItemsSource = dept;
            this.cbDepartment.SelectedValuePath = "DeptId";
            this.cbDepartment.DisplayMemberPath = "Dept";
        }

        public void color()
        {
            btnregister.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            lbdecription.Foreground = new SolidColorBrush(settings.Font);
            btnregister.Foreground = new SolidColorBrush(settings.Font);
            cbDepartment.Background = new SolidColorBrush(settings.Tab);
            btnregister.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            cbDepartment.Foreground = new SolidColorBrush(settings.Font);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            btnregister_Copy.Background = new SolidColorBrush(settings.Tab);
            btnregister_Copy.Foreground=new SolidColorBrush(settings.Font);
        }

        public void Hiddenvalidation()
        {
            lbvalusername.Visibility = Visibility.Hidden;
            lbvalpassword.Visibility = Visibility.Hidden;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Hiddenvalidation();
            validation = 0;
            if (txtboxVendorName.Text == "")
            {
                lbvalpassword.Visibility = Visibility.Visible;
                validation = 1;
            }
            if (txtboxPhone.Text == "")
            {
                lbvalusername.Visibility = Visibility.Visible;
                validation = 1;
            }
            else if (validation == 0)
            {
                //string username = txtboxusername.Text;
                //string password = txtboxpassword.Text;
                Result r = bl.AddVendor(txtboxVendorName.Text, cbDepartment.SelectedValue.ToString(),txtboxPhone.Text,txtboxEmail.Text);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        this.DialogResult = true;
                    }
                    else
                    {
                        lbdecription.Content = r.Description;
                    }
                }
            }
        }

        private void CbselectUser_selectionChanged(object sender, SelectionChangedEventArgs e)
        {

            // Selectedvalue =Convert.ToString(cbselectuser.SelectedItem);

            //if (Selectedvalue == "Shop Manager")
            //{
            //    flag = 4;
            //}
            //if (Selectedvalue == "Employee")
            //{
            //    flag = 5;
            //}
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        } 

       
    }
}
