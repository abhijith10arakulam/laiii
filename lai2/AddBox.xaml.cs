﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AddBox.xaml
    /// </summary>
    public partial class AddBox : Window
    {
        string text1;
        string text;
        int flag = 0;
        Blogic bl = new Blogic();
        private string type;
        private string oldpswrd;
        private string newpswrd;
        private string Confirmpswrd;
       
        private Settings settings = Properties.Settings.Default;
        public AddBox()
        {
            InitializeComponent();
           txtbox.KeyDown += new KeyEventHandler(tb_keydown);
           txtbox_Copy.KeyDown += new KeyEventHandler(tb_keydown);

        }

        private void tb_keydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Add_Click(sender, e);
            }
        }
        public void HiddenValidation()
        {
            lbvalcnfrmpswrd.Visibility = Visibility.Hidden;
            lbvalnwpsrd.Visibility = Visibility.Hidden;
            lbvaloldpswrd.Visibility = Visibility.Hidden;
        }

        public void color()
        {
            button1.Background = new SolidColorBrush(settings.Tab);
            button1.Foreground = new SolidColorBrush(settings.Font);
            button1_Copy.Background = new SolidColorBrush(settings.Tab);           
            button1_Copy.Foreground = new SolidColorBrush(settings.Font);
            lbTitle.Foreground = new SolidColorBrush(settings.Theme);
            buttonnback.Background = new SolidColorBrush(settings.Tab);
            buttonnback.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btnback.Foreground = new SolidColorBrush(settings.Font);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnsubmit2.Background = new SolidColorBrush(settings.Tab);
            btnsubmit2.Foreground = new SolidColorBrush(settings.Font);
            buttonnbackprofitpercentage.Background = new SolidColorBrush(settings.Tab);
            buttonnbackprofitpercentage.Foreground = new SolidColorBrush(settings.Font);
            lbtitle2.Foreground = new SolidColorBrush(settings.Theme);
            lbtitle2.FontFamily = new FontFamily(settings.Fontstyle);
            lbTitle.FontFamily = new FontFamily(settings.Fontstyle);
            
        }

        public AddBox(string title, string type)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();
            txtbox.KeyDown += new KeyEventHandler(tb_keydown);
            txtbox_Copy.KeyDown += new KeyEventHandler(tb_keydown);
            HiddenValidation();
            this.Title = title;
            this.type = type;
            lbTitle.Content = title;
            lbtype.Content = type;
            if (Title == "Update Inventory Age")
            {
                txtbox.Text = LoginDetail.shops[IndexFinder.index].InventoryAge.ToString();
              
            }
            else if (Title == "Update Box Age")
            {
                txtbox.Text = LoginDetail.shops[IndexFinder.index].BoxAge.ToString();
            }
            else if (Title == "Update Profit Percentage")
            {
              //  txtbox.Text = LoginDetail.shops[IndexFinder.index].BoxAge.ToString();
                txtbox_Copy.Visibility = Visibility.Visible;
              //  button1_Copy.Visibility = Visibility.Visible;
                lbonlineprofit.Visibility = Visibility.Visible;
              //  button1.Visibility = Visibility.Hidden;
               // buttonnbackprofitpercentage.Visibility = Visibility.Visible;

                buttonnback.Visibility = Visibility.Hidden;
                button1.Visibility = Visibility.Hidden;
                button1_Copy.Visibility = Visibility.Visible;
                buttonnbackprofitpercentage.Visibility = Visibility.Visible;

                txtbox.Text = LoginDetail.shops[IndexFinder.index].InstantProfit.ToString();
               txtbox_Copy.Text= LoginDetail.shops[IndexFinder.index].OnlineProfit.ToString();
               
            }
            else if (Title == "Update Box Count")
            {
                txtbox.Text = LoginDetail.shops[IndexFinder.index].BoxCount.ToString();
            }

            else if (Title == "Update AI Result Percentage")
            {
                txtbox.Text = LoginDetail.shops[IndexFinder.index].AIResultPercetage.ToString();
            }

            else if (Title == "Update Settlement Days")
            {
                txtbox.Text = LoginDetail.shops[IndexFinder.index].SettlementDays.ToString();
            }
        }

        public AddBox(string title, string oldpswrd, string newpswrd, string Confirmpswrd)      //Change Password
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();

            HiddenValidation();
            this.Title = title;
            this.oldpswrd = oldpswrd;
            this.newpswrd = newpswrd;
            this.Confirmpswrd = Confirmpswrd;

            spcaddbox.Visibility = Visibility.Hidden;
            spcadd.Visibility = Visibility.Visible;
            
            lbtitle2.Content = Title;
            lboldpswrd.Content = oldpswrd;
            lbnwpsrd.Content = newpswrd;
            lbcnfrmpaswrd.Content = Confirmpswrd;

            tbnwpsrdorg.Visibility = Visibility.Visible;
            tboldpswrdorg.Visibility = Visibility.Visible;
            tbconfrmpaswrdorg.Visibility = Visibility.Visible;

            tbconfrmpaswrd.Visibility = Visibility.Hidden;          
            tbnwpsrd.Visibility = Visibility.Hidden;
            tboldpswrd.Visibility = Visibility.Hidden;
            btnsubmit2.Content = "Submit";
           
        }


        private void Add_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            lbval1.Visibility = Visibility.Hidden;

            if (Title == "Update Inventory Age")
            {
                if (txtbox.Text == "")
                {
                    lbval1.Visibility = Visibility.Visible;
                }

                if (TryToParse(text) == false)
                {
                    lbvaltxt.Visibility = Visibility.Visible;
                    flag = 1;
                }
                if (txtbox.Text != "" && flag == 0)
                {
                    string inventoryage = text;
                    Result r = bl.UpdateInventoryAge(inventoryage);
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            LoginDetail.shops[IndexFinder.index].InventoryAge = Convert.ToInt32(inventoryage);

                            this.DialogResult = true;
                        }
                    }
                }
            }

                if (Title == "Update AI Result Percentage")
                {
                    if (txtbox.Text == "")
                    {
                        lbval1.Visibility = Visibility.Visible;
                    }

                    if (TryToParse(text) == false)
                    {
                        lbvaltxt.Visibility = Visibility.Visible;
                        flag = 1;
                    }
                    if (txtbox.Text != "" && flag == 0)
                    {
                        string inventoryage = text;
                        Result r = bl.UpdateAiresultPercentage(inventoryage);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                LoginDetail.shops[IndexFinder.index].AIResultPercetage = Convert.ToInt32(inventoryage);
                                
                                this.DialogResult = true;
                            }
                        }
                    }
                }


                if (Title == "Update Box Age")
                {
                    flag = 0;
                    lbval1.Visibility = Visibility.Hidden;
                    if (TryToParse(text) == false)
                    {
                        lbvaltxt.Visibility = Visibility.Visible;
                        flag = 1;
                    }
                    if (txtbox.Text == "")
                    {
                        lbval1.Visibility = Visibility.Visible;
                    }

                    if (txtbox.Text != "" && flag == 0)
                    {
                        string boxage = text;
                        Result r = bl.Updateboxage(boxage);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                LoginDetail.shops[IndexFinder.index].BoxAge = Convert.ToInt32(boxage);
                                this.DialogResult = true;
                            }
                        }
                    }
                }

                if (Title == "Update Profit Percentage")
                {
                    flag = 0;
                    lbval1.Visibility = Visibility.Hidden;
                    lbval1_Copy.Visibility = Visibility.Hidden;

                    if (TryToParse(text1) == false)
                    {
                        lbvaltxt.Visibility = Visibility.Visible;
                        flag = 1;
                    }
                    if (TryToParse(text) == false)
                    {
                        lbval1_Copy.Visibility = Visibility.Visible;
                        flag = 1;
                    }

                    if (txtbox.Text == "")
                    {
                        lbval1.Visibility = Visibility.Visible;
                    }
                    if (txtbox_Copy.Text == "")
                    {
                        lbval1_Copy.Visibility = Visibility.Visible;
                    }

                    if (txtbox.Text != "" && txtbox_Copy.Text != "" && flag == 0)
                    {
                        string instantProfit = text;
                        string onlineprofit = text1;
                        Result r = bl.UpdateProfitPercentage(instantProfit, onlineprofit);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                LoginDetail.shops[IndexFinder.index].InstantProfit = Convert.ToDouble(instantProfit);
                                LoginDetail.shops[IndexFinder.index].OnlineProfit = Convert.ToDouble(onlineprofit);

                                this.DialogResult = true;
                            }
                        }
                    }
                }

                if (Title == "Update Box Count")
                {
                    //text-->txtbox.Text

                    text = txtbox.Text;
                    flag = 0;
                    lbval1.Visibility = Visibility.Hidden;
                    if (TryToParse(text) == false)
                    {
                        lbvaltxt.Visibility = Visibility.Visible;
                        flag = 1;
                    }
                   

                    if (txtbox.Text == "")
                    {
                        lbval1.Visibility = Visibility.Visible;
                    }
                    
                    if (txtbox.Text != "" && flag == 0)
                    {
                        if (Convert.ToInt32(txtbox.Text) > 0)
                        {
                            string Boxnumber = text;
                            Result r = bl.UpdateBoxNumber(Boxnumber);
                            if (r == null && bl.internetconnection() == false)
                            {
                                string heading1 = "        Poor Connectivity";
                                var dialog1 = new DialogBox_BoxNo(heading1);
                                dialog1.ShowDialog();
                            }
                            else
                            {
                                if (r.Status == true)
                                {
                                    LoginDetail.shops[IndexFinder.index].BoxCount = Convert.ToInt32(Boxnumber);
                                    this.DialogResult = true;
                                }
                            }
                        }
                        else
                        {
                            lbval1.Visibility = Visibility.Visible;
                        }
                    }
                }

                if (Title == "Update Settlement Days")
                {
                    text = txtbox.Text;
                    flag = 0;
                    lbval1.Visibility = Visibility.Hidden;
                    
                    if (txtbox.Text == "")
                    {
                        lbval1.Visibility = Visibility.Visible;
                    }
                   else if (TryToParse(text) == false)
                    {
                        lbvaltxt.Visibility = Visibility.Visible;
                        flag = 1;
                    }
                    else if (Convert.ToInt32(text) < 0)
                    {
                        lbval1.Visibility = Visibility.Visible;
                        flag = 1;
                    }
                    else if (txtbox.Text != "" && flag == 0)
                    {
                        string setlementDays = text;
                        Result r = bl.UpdateSettlementDays(setlementDays);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                LoginDetail.shops[IndexFinder.index].SettlementDays = Convert.ToInt32(setlementDays);
                                this.DialogResult = true;
                            }
                        }
                    }
                }

            
        }




        private void ChangePasswordBtn_Click(object sender, RoutedEventArgs e)
        {
            HiddenValidation();
            if ( tbconfrmpaswrdorg.Password == "")
            {
                lbvalcnfrmpswrd.Visibility = Visibility.Visible;
            }
            if (tbnwpsrdorg.Password == "")
            {
                lbvalnwpsrd.Visibility = Visibility.Visible;
            }
            if (tboldpswrdorg.Password == "")
            {
                lbvaloldpswrd.Visibility = Visibility.Visible;
            }
            else
            {
                string oldpsswrd = tboldpswrdorg.Password;
                string nwpsrwrd = tbnwpsrdorg.Password;
                string confirmpsrd = tbconfrmpaswrdorg.Password;
                if (nwpsrwrd == confirmpsrd)
                {
                    string heading = "Change Password";
                    string content = "Do you want to change password" + "?";

                    var dialogalert = new DeleteWindow(heading, content);
                    if (dialogalert.ShowDialog() == true && dialogalert.DialogResult == true)
                    {
                        Result r = bl.Change_Password(oldpsswrd, nwpsrwrd, confirmpsrd);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {

                                settings.IsPasswordChanged = true;
                                //string status = "Successfully Changed";
                                //var dialog = new DialogBox_BoxNo(status);
                                //if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                                //{
                                this.DialogResult = true;
                                // MessageBox.Show(r.Description);
                                // }
                            }
                            else
                            {
                                // MessageBox.Show(r.Description);
                                var dialog = new DialogBox_BoxNo(r.Description);
                                dialog.ShowDialog();
                            }
                        }
                    }
                }
                else
                {
                    string warning = "Mismatch Password";
                    var dialog = new DialogBox_BoxNo(warning);
                    dialog.ShowDialog();
                    // MessageBox.Show("New password and confirm password should be same");
                }               
            }
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                return result;
                //if (value == null) value = "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        } 

        private void AddEmployee_clicked(object sender, RoutedEventArgs e)
        {

        }

        private void inputtext_changed(object sender, TextChangedEventArgs e)
        {                                                   //  || Title == "Update Box Count"
            if (Title == "Update Box Age" || Title == "Update Inventory Age" )
            {
                string s = txtbox.Text.Replace("days", "");
                s = s.Replace("d", "");
                s = s.Replace("a", "");
                s = s.Replace("y", "");
                s = s.Replace("s", "");

                if (s != "")
                {
                    txtbox.Text = s + "days";
                }
                text = txtbox.Text.Replace("days", "");
            }

            else if (Title == "Update Profit Percentage")
            {
                string ss = txtbox.Text.Replace("%", "");
                ss = ss.Replace("%", "");

                if (ss != "")
                {
                    double x;
                    if (double.TryParse(ss, out  x))
                    {
                        if (Convert.ToDouble(ss) <= 100)
                            ss += "%";
                        else
                            ss = "0%";

                    }
                    else
                        ss = "0%";

                    txtbox.Text = ss;

                }
                text = txtbox.Text.Replace("%", "");
            }

            else if (Title == "Update AI Result Percentage")
            {
                string ss = txtbox.Text.Replace("%", "");
                ss = ss.Replace("%", "");

                if (ss != "")
                {
                    double x;
                    if (double.TryParse(ss, out  x))
                    {
                        if (Convert.ToDouble(ss) <= 100)
                            ss += "%";
                        else
                            ss = "0%";
                    }
                    else
                        ss = "0%";

                    txtbox.Text = ss;

                }
                text = txtbox.Text.Replace("%", "");            
            }
        }

        private void txtbox_Copy_textChanged(object sender, TextChangedEventArgs e)
        {
            string ss = txtbox_Copy.Text.Replace(".00%", "");
             ss = ss.Replace("%", "");
     
            if (ss != "")
            {
                double x;
               if(double .TryParse (ss,out  x ))
               {
                   if (Convert.ToDouble(ss) <= 100)
                       ss += "%";
                   else
                       ss = "0%";
               
               }
               else
                   ss = "0%";


            }
            txtbox_Copy.Text = ss;

            text1 = txtbox_Copy.Text.Replace("%", "");
        }

        private void Back_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        } 

        
    }
}
