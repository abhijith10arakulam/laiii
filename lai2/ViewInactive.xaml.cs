﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewInactive.xaml
    /// </summary>
    public partial class ViewInactive : Page
    {
        string fromdate;
        string todate;
        System.ComponentModel.BackgroundWorker mWorker; 
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        List<TicketsView> vt = new List<TicketsView>();
        public ViewInactive()
        {
            InitializeComponent();
            fromdatepicker.SelectedDate = DateTime.Now.AddDays (-7);
            todatepicker.SelectedDate = DateTime.Now;
            todatepicker.DisplayDateEnd = DateTime.Now;

            lbstartdate.Content = Blogic.opendate;
            txtlegal.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin.Text = LoginDetail.user.UserName.ToString();
            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin.Foreground = new SolidColorBrush(settings.Font);           
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);           
            dgInactive.Foreground = new SolidColorBrush(settings.InactiveC);
            this.Background = new SolidColorBrush(settings.Background);
            dgInactive.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            dgInactive.FontFamily = new FontFamily(settings.datagridfont);
            lblcntnme.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            lbfromdate.FontFamily = new FontFamily(settings.labelfont);
            lbtodate.FontFamily = new FontFamily(settings.labelfont);
            lbopndatee.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            lbuserloginn.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin.FontFamily = new FontFamily(settings.labelfont);
            loadingtelerick.Foreground = new SolidColorBrush(settings.Font);
            tbEmptyBoxes.Foreground = new SolidColorBrush(settings.Font);
            tbEmptyBoxes.Text = bl.EmptyBox_Report().Replace("\"", "");

           
            loading();
        }
         
        public void loading()
        {        
            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
            new System.Threading.ThreadStart(delegate { }));       

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            try
            {             
                if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loadingtelerick.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }            
            catch { }
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            vt = bl.ViewInactive_Tickets(fromdate, todate);
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
           
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {            
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            if (vt == null && bl.internetconnection()==false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
                dgInactive.ItemsSource = vt;  
           
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }   
    
        private void ToDate_Changed(object sender, SelectionChangedEventArgs e)
        {
            if (fromdatepicker.SelectedDate != null && todatepicker.SelectedDate != null)
            {
                 fromdate = Convert.ToDateTime(fromdatepicker.SelectedDate).ToString("yyyy/MM/dd");
                 todate = Convert.ToDateTime(todatepicker.SelectedDate).ToString("yyyy/MM/dd");
                 loading();                        
            }
        }
    }
}
