﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Windows.Media.Animation;
using System.Media;


namespace lai2
{
    /// <summary>
    /// Interaction logic for DataList.xaml
    /// </summary>
    public partial class DataList : Page
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        ReportDataList rl = new ReportDataList();

        System.ComponentModel.BackgroundWorker mWorker;

        public DataList()
        {
            InitializeComponent();

            if (LoginDetail.user.UserType == "ShopAdmin")
            {
                try
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.none);
                    snd.Play();
                }
                catch { }
            }
            
            lbinsatntsale.Visibility = Visibility.Visible;
            lbinsatntsaleval.Visibility = Visibility.Visible;

            tab1.Background = new SolidColorBrush(settings.Tab);
            tab2.Background = new SolidColorBrush(settings.Tab);
            tab3.Background = new SolidColorBrush(settings.Tab);
            tab4.Background = new SolidColorBrush(settings.Tab);
            tab5.Background = new SolidColorBrush(settings.Tab);

            //  tab1.BorderBrush = new SolidColorBrush(settings.Tab);
            tab2.BorderBrush = new SolidColorBrush(settings.ActiveC);
            tab3.BorderBrush = new SolidColorBrush(settings.Complite);
            tab4.BorderBrush = new SolidColorBrush(settings.InactiveC);
            tab5.BorderBrush = new SolidColorBrush(settings.ReturnC);

            //  tab1.BorderThickness = new Thickness(5);

            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin.Foreground = new SolidColorBrush(settings.Font);
            tab1.Foreground = new SolidColorBrush(settings.Font);
            tab2.Foreground = new SolidColorBrush(settings.Font);
            tab3.Foreground = new SolidColorBrush(settings.Font);
            tab4.Foreground = new SolidColorBrush(settings.Font);
            tab5.Foreground = new SolidColorBrush(settings.Font);
            lbinsatntsaleval.Foreground = new SolidColorBrush(settings.Font);
            //activatedTickets.Foreground = new SolidColorBrush(settings.Font);
            //soldoutTickets.Foreground = new SolidColorBrush(settings.Font);
            activatedTickets.Foreground = new SolidColorBrush(settings.ActiveC);
            soldoutTickets.Foreground = new SolidColorBrush(settings.Complite);
            InactiveTickets.Foreground = new SolidColorBrush(settings.InactiveC);
            ReturnTickets.Foreground = new SolidColorBrush(settings.ReturnC);
            this.Background = new SolidColorBrush(settings.Background);
            //telerickgrid.Background = new SolidColorBrush(settings.Background);
            //loading.Background = new SolidColorBrush(settings.Background);
            loading.Foreground = new SolidColorBrush(settings.Font);
            LatestData.BorderBrush = new SolidColorBrush(settings.Border);
            activatedTickets.BorderBrush = new SolidColorBrush(settings.Border);
            soldoutTickets.BorderBrush = new SolidColorBrush(settings.Border);
            InactiveTickets.BorderBrush = new SolidColorBrush(settings.Border);
            ReturnTickets.BorderBrush = new SolidColorBrush(settings.Border);
            txtlegal.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin.Content = LoginDetail.user.UserName.ToString();

            LatestData.FontFamily = new FontFamily(settings.datagridfont);
            activatedTickets.FontFamily = new FontFamily(settings.datagridfont);
            soldoutTickets.FontFamily = new FontFamily(settings.datagridfont);
            InactiveTickets.FontFamily = new FontFamily(settings.datagridfont);
            ReturnTickets.FontFamily = new FontFamily(settings.datagridfont);

            lblglname.FontFamily = new FontFamily(settings.labelfont);
            lbloctnname.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            lbopndate.FontFamily = new FontFamily(settings.labelfont);
            lbuserlogn.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin.FontFamily = new FontFamily(settings.labelfont);
            tbEmptyBoxes.Foreground = new SolidColorBrush(settings.Font);
       //     lbApp.FontFamily = new FontFamily(settings.Fontstyle);
        //   lbApp.Foreground = new SolidColorBrush(settings.Theme);
            
            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
            //  pbProcessing.Value = 0;

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();

            //Duration duration = new Duration(TimeSpan.FromSeconds(20));

            //DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
            //pbProcessing.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
            try
            {
                //while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {

                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loading.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string time = DateTime.Now.ToString("hh:mm:sstt");
            rl = bl.TodaysData(date, time);


        }


        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            // pbProcessing.Value = e.ProgressPercentage;
            //if (pbProcessing.Value == 0.05)
            //{
            //    mWorker.CancelAsync();
            //}
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation  
            mWorker.CancelAsync();
            loading.IsBusy = false;
            if (rl.ShiftData == null)
            {
                if (bl.internetconnection() == false)
                {
                    string heading = "     Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    mWorker.RunWorkerAsync();

                    //Duration duration = new Duration(TimeSpan.FromSeconds(20));

                    //DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
                    //pbProcessing.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
                    try
                    {
                        //while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                        {

                            if (!mWorker.CancellationPending)
                            {
                                try
                                {
                                    loading.IsBusy = true;
                                }
                                catch (System.Exception ex)
                                {
                                    // No action required
                                }
                            }
                            else
                            {
                                // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                                //   break;
                            }

                            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                                   new System.Threading.ThreadStart(delegate { }));
                        }
                    }
                    catch { }

                }
            }
            else
            {


                if (rl.ExpiryCheck.Status == false)
                {
                    var dialog = new Expired(rl.ExpiryCheck.Description);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                    }
                    Blogic.ExpireFlag = 1;
                }
                else
                {
                    Blogic.ExpireFlag = 0;
                    lbinsatntsaleval.Content = rl.InstantSales.ToString("C");
                    lbstartdate.Content = rl.StartDate;
                    Blogic.opendate = rl.StartDate;
                    LatestData.ItemsSource = rl.ShiftData;
                    activatedTickets.ItemsSource = rl.ActiveData.OrderByDescending(a=>a.ActiveDate);
                    soldoutTickets.ItemsSource = rl.SoldOutData.OrderByDescending(a => a.ActiveDate);
                    InactiveTickets.ItemsSource = rl.InactiveData.OrderByDescending(a => a.ActiveDate);
                    ReturnTickets.ItemsSource = rl.ReturnData.OrderByDescending(a => a.ActiveDate);
                    tbEmptyBoxes.Text = rl.EmptyBoxes;

                    

                    //LatestData.FirstDisplayedScrollingRowIndex = Blogic.CurrentRow;
                    if (Blogic.CurrentRow < rl.ShiftData.Count)
                    {
                        if (Blogic.CurrentRow + 9 < rl.ShiftData.Count)
                            LatestData.ScrollIntoView(LatestData.Items[Blogic.CurrentRow + 9]);
                        else if ((Blogic.CurrentRow + 4 < rl.ShiftData.Count))
                            LatestData.ScrollIntoView(LatestData.Items[Blogic.CurrentRow + 4]);
                        else
                            LatestData.ScrollIntoView(LatestData.Items[Blogic.CurrentRow]);

                        Blogic.CurrentRow = 0;
                    }
                }

            }

        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }



        private void alltickets_focused(object sender, RoutedEventArgs e)
        {
            lbinsatntsale.Visibility = Visibility.Visible;
            lbinsatntsaleval.Visibility = Visibility.Visible;
        }

        private void ActivatedTicket_focued(object sender, RoutedEventArgs e)
        {
            lbinsatntsale.Visibility = Visibility.Hidden;
            lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void ReturnTicket_Focussed(object sender, RoutedEventArgs e)
        {
            lbinsatntsale.Visibility = Visibility.Hidden;
            lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void inactiveTicket_gotfocussed(object sender, RoutedEventArgs e)
        {
            lbinsatntsale.Visibility = Visibility.Hidden;
            lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void SoldOutTicket_focussed(object sender, RoutedEventArgs e)
        {
            lbinsatntsale.Visibility = Visibility.Hidden;
            lbinsatntsaleval.Visibility = Visibility.Hidden;
        }

        private void cmdStart_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Frame_Navigated_1(object sender, NavigationEventArgs e)
        {

        }

        private void LatestData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Blogic.CurrentRow = LatestData.SelectedIndex;
        }

        //private void alltickets_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    lbinsatntsale.Visibility = Visibility.Hidden;
        //    lbinsatntsaleval.Visibility = Visibility.Hidden;
        //}

    }
}
