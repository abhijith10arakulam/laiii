﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Media;

namespace lai2
{
    /// <summary>
    /// Interaction logic for SettlementbyTicket.xaml
    /// </summary>
    public partial class SettlementbyTicket : Window
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        int flag = 0;
        string TicketValue;

        private int UIvalue;
        //bool mLeftCtrlDown = false;
        //bool mScanShiftDown = false;
        //bool mScanning = false;
        int returnFlag = 1;

        string barcode = "";
        StringBuilder mScanData = new StringBuilder();
        KeyConverter mScanKeyConverter = new KeyConverter();
        //  private string p;
        //  private Result res;
       
        int ScanOrder;
        public SettlementbyTicket()
        {
            InitializeComponent();
            HiddenField();
            color();
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            var am = DateTime.Now.ToString("tt");

            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;           
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
        }

        public void color()
        {
            btClose.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            btClose.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            comboboxhour.Background = new SolidColorBrush(settings.Tab);
            comboboxminute.Background = new SolidColorBrush(settings.Tab);
            comboboxam.Background = new SolidColorBrush(settings.Tab);
            comboboxhour.Foreground = new SolidColorBrush(settings.Font);
            comboboxminute.Foreground = new SolidColorBrush(settings.Font);
            comboboxam.Foreground = new SolidColorBrush(settings.Font);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            lbdescription.FontFamily = new FontFamily(settings.labelfont);
        }

        void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return" && returnFlag == 1 & barcode.Length != 0)
            {
                //bl = new BAL();
                //returnFlag = 0;
                if ((barcode.Length <= 24 && barcode.Length >= 22) || (barcode.Length == 20)||(barcode.Length == 18))
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
                    snd.Play();
                    ScanData tt = bl.GetTicket(barcode);

                    setData(tt);
                    // btSubmit.Focus();
                }

                barcode = "";

                if (UIvalue == 0)
                {
                    // EnableTextBoxes();
                    //if (TbValidation())
                    //{

                    //    //this.DialogResult = true;
                    //}
                    //else
                    //{
                    //    btUISubmit.Visibility = Visibility.Visible;
                    //    btUISubmit.IsEnabled = true;
                    //}
                }
            }
            // else 
            if (e.Key.ToString().StartsWith("D") && returnFlag == 1)
            {
                barcode = barcode + e.Key.ToString().Replace("D", string.Empty);

            }
            //listBox1.Items.Add(e.Key.ToString().Replace("D",string.Empty));
        }

        private void setData(ScanData tt)
        {
            tbTicketId.Text = Convert.ToString(tt.TicketId);
            tbPackno.Text = tt.PackNo;
          //  tbPackPosition.Text = Convert.ToString(tt.PackPosition);
        }

        public void HiddenField()
        {
            lbvalpackno.Visibility = Visibility.Hidden;
            lbvaltcktname.Visibility = Visibility.Hidden;
            lbvalTicketid.Visibility = Visibility.Hidden;
            lbvalValue.Visibility = Visibility.Hidden;
        }

        private void TicketId_textchanged(object sender, TextChangedEventArgs e)
        {
            TicketStatus ts = new TicketStatus();
            if (bl.CheckTicketIdLenght(tbTicketId.Text.Length))
         //   if (tbTicketId.Text.Length >= 3 && tbTicketId.Text.Length <= 5)
            {
                if (TryToParse(tbTicketId.Text) == true)
                {
                    ts = bl.Settlementtxt_autofill(tbTicketId.Text);
                    if (ts == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (ts.Status == true)
                        {
                            HiddenField();
                            tbPackno.Text = ts.TicketInfo[0].PackNo;
                            tbTicketName.Text = ts.TicketInfo[0].TicketName;
                            tbValue.Text = Convert.ToString(ts.TicketInfo[0].Value);
                        }
                    }
                }
            }

            if (tbTicketId.Text == "")
            {
                HiddenField();
                tbPackno.Text = "";
                tbValue.Text = "";
                tbTicketName.Text = "";
                lbdescription.Visibility = Visibility.Hidden;
            }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {
            btSubmit.Focusable = false;
            flag = 0;
            HiddenField();
            if (tbTicketId.Text == "")
            {
                lbvalTicketid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackno.Text == "")
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketName.Text == "")
            {
                lbvaltcktname.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbValue.Text == "")
            {
                lbvalValue.Visibility = Visibility.Visible;
                flag = 1;
            }

            if ((TryToParse(tbTicketId.Text) == false)) 
            {
                lbvalTicketid.Visibility = Visibility.Visible;
                flag = 1;
            }

            if ((TryToParse(tbPackno.Text) == false))
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(TicketValue) == false)
            {
                lbvalValue.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (!bl.CheckTicketIdLenght(tbTicketId.Text.Length))
            {
                lbvalTicketid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckPackNoLenght(tbPackno .Text.Length))
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }

            else if (flag == 0)
            {
                if (bl.CheckTicketIdLenght(tbTicketId.Text.Length) && bl.CheckPackNoLenght(tbPackno.Text.Length))
               // if (tbTicketId.Text.Length >= 3 && tbTicketId.Text.Length <= 5 && (tbPackno.Text.Length == 6 || tbPackno.Text.Length == 7))
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();

                    string heading = "Do you want to settle?";
                    string ticketname = tbTicketName.Text;
                    string value = tbValue.Text;
                    var dialog = new ConfirmationDetails(heading, ticketname, value);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        Result r = new Result();
                        string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                        String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + comboboxam.SelectedItem.ToString();
                        r = bl.Settlement_save(tbTicketId.Text, tbPackno.Text, s);
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading1 = "        Poor Connectivity";
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                this.DialogResult = true;
                                lbdescription.Content = r.Description;
                                tbPackno.Text = "";
                                tbTicketId.Text = "";
                                tbValue.Text = "";
                                tbTicketName.Text = "";

                            }
                            else
                            {
                                tbTicketId.Focus();
                                lbdescription.Visibility = Visibility.Visible;
                                lbdescription.Content = r.Description;
                            }
                        }
                    }
                    else
                    {
                        tbTicketId.Focus();
                    }
                }
            }
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                return result;
                //if (value == null) value = "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        }

        private void tbValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = tbValue.Text.Replace("$", "");
            if (s != "")
            {
                tbValue.Text = "$" + s;
            }
            TicketValue = tbValue.Text.Replace("$", "");
            tbValue.CaretIndex = TicketValue.Length + 1;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.05;
        }


       
    }
}
