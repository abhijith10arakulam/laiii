﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for FixBegingNo.xaml
    /// </summary>
    public partial class FixBegingNo : Window
    {
        private Settings settings = Properties.Settings.Default; 
        Blogic bl = new Blogic();
        int flag = 0;

        public FixBegingNo(int box)
        {
            InitializeComponent();
            color();
            HideLabel();
            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                cbBox.Items.Add(i);
            }
            cbBox.SelectedItem = box;
            btSubmit.Focus();
        }
        public int ResponseData
        {
            get
            {
                return Convert .ToInt32( cbBox.SelectedItem);
            }
        }
        public void color()
        {
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btClose.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            cbBox.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            cbBox.Foreground = new SolidColorBrush(settings.Font);
            btClose.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            lbdescription.FontFamily = new FontFamily(settings.labelfont);
        }

        public void HideLabel()
        {
            lbvalpackno.Visibility = Visibility.Hidden;
            lbvalpackpositionclose.Visibility = Visibility.Hidden;
            lbvalPackPositionopen.Visibility = Visibility.Hidden;
            lbvaltcktid.Visibility = Visibility.Hidden;
            lbvaltcktname.Visibility = Visibility.Hidden;
        }
        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {
            submit();
        }

        private void submit()
        {
            flag = 0;
            HideLabel();
            if (tbTicketId.Text == "")
            {
                lbvaltcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketName.Text == "")
            {
                lbvaltcktname.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPositionopen.Text == "")
            {
                lbvalPackPositionopen.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPosition.Text == "")
            {
                lbvalpackpositionclose.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackno.Text == "")
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbTicketId.Text) == false)
            {
                lbvaltcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbPackno.Text) == false)
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbPackPosition.Text) == false)
            {
                lbvalpackpositionclose.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbPackPositionopen.Text) == false)
            {
                lbvalPackPositionopen.Visibility = Visibility.Visible;
                flag = 1;
            }

            else if (flag == 0)
            {
                Result r = new Result();
                string tcktid = tbTicketId.Text;
                string packno = tbPackno.Text;
                string packpositionopen = tbPackPositionopen.Text;
                r = bl.CheckStatus_FixBeggingNo(tcktid, packno, packpositionopen);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        this.DialogResult = true;
                        lbdescription.Content = r.Description;
                        tbPackno.Text = "";
                        tbPackPosition.Text = "";
                        tbPackPositionopen.Text = "";
                        tbTicketId.Text = "";
                        tbTicketName.Text = "";

                    }
                    else
                    {
                        lbdescription.Visibility = Visibility.Visible;
                        lbdescription.Content = r.Description;

                    }
                }
            }
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                return result;
                //if (value == null) value = "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        } 


        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }      


        private void BoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ResultWithOldTicket rs = new ResultWithOldTicket();
            string box = Convert.ToString(cbBox.SelectedItem);
            rs = bl.AutoFill_BeggingNo(box);
            if (rs == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rs.Status == true)
                {
                    //  lbdescription.Content = rs.Description;
                    tbTicketId.Text = Convert.ToString(rs.OldTicketInfo.TicketId);
                    tbPackno.Text = Convert.ToString(rs.OldTicketInfo.PackNo);
                    tbTicketName.Text = Convert.ToString(rs.OldTicketInfo.TicketName);
                    tbPackPositionopen.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                    tbPackPosition.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                    btSubmit.Focus();
                }
                else
                {
                    //  lbdescription.Content = rs.Description;
                    tbTicketId.Text = "";
                    tbPackno.Text = "";
                    tbTicketName.Text = "";
                    tbPackPositionopen.Text = "";
                    tbPackPosition.Text = "";
                }
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.15;
        }


        private void packpositionopen_keydown(object sender, KeyEventArgs e)
        {             
            if (e.Key == Key.Enter)
            {
                submit();
              
            }       
        }
 
    }

}
