﻿using lai2.Properties;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ColorFont
{
    /// <summary>
    /// Interaction logic for ColorFontDialog.xaml
    /// </summary>
public partial class ColorFontDialog : Window
{
    private FontInfo selectedFont;
    private Settings settings = lai2.Properties.Settings.Default;
    public ColorFontDialog()
    {
        this.selectedFont = null; // Default
        InitializeComponent();

        if (settings.Background.ToString() != "#00FFFFFF")
        {
            this.Background = new SolidColorBrush(settings.Background);
        }
        btnOk.Foreground = new SolidColorBrush(settings.Font);
        btncancel.Foreground = new SolidColorBrush(settings.Font);
        btnOk.Background = new SolidColorBrush(settings.Tab);
        btncancel.Background = new SolidColorBrush(settings.Tab);
    }

    public FontInfo Font
    {
        get
        {
            return this.selectedFont;
        }

        set
        {
            FontInfo fi = value;
            this.selectedFont = fi;
        }
    }

    private void SyncFontName()
    {
        string fontFamilyName = this.selectedFont.Family.Source;
        int idx = 0;
        foreach (var item in this.colorFontChooser.lstFamily.Items)
        {
            string itemName = item.ToString();
            if (fontFamilyName == itemName)
            {
                break;
            }
            idx++;
        }
        this.colorFontChooser.lstFamily.SelectedIndex = idx;
        this.colorFontChooser.lstFamily.ScrollIntoView(this.colorFontChooser.lstFamily.Items[idx]);
    }

    private void SyncFontSize()
    {
        double fontSize = this.selectedFont.Size;
        this.colorFontChooser.fontSizeSlider.Value = fontSize;
    }

    private void SyncFontColor()
    {
        int colorIdx = AvailableColors.GetFontColorIndex(this.Font.Color);
        this.colorFontChooser.colorPicker.superCombo.SelectedIndex = colorIdx;
        this.colorFontChooser.txtSampleText.Foreground = this.Font.Color.Brush;
        this.colorFontChooser.colorPicker.superCombo.BringIntoView();
    }

    private void SyncFontTypeface()
    {
        string fontTypeFaceSb = FontInfo.TypefaceToString(this.selectedFont.Typeface);
        int idx = 0;
        foreach (var item in this.colorFontChooser.lstTypefaces.Items)
        {
            FamilyTypeface face = item as FamilyTypeface;
            if (fontTypeFaceSb == FontInfo.TypefaceToString(face))
            {
                break;
            }
            idx++;
        }
        this.colorFontChooser.lstTypefaces.SelectedIndex = idx;
    }

    private void btnOk_Click(object sender, RoutedEventArgs e)
    {
        this.Font = this.colorFontChooser.SelectedFont;
        this.DialogResult = true;
    }

    private void Window_Loaded_1(object sender, RoutedEventArgs e)
    {
        this.SyncFontColor();
        this.SyncFontName();
        this.SyncFontSize();
        this.SyncFontTypeface();

        Application curApp = Application.Current;
        Window mainWindow = curApp.MainWindow;
        this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
        this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
    }
}
}
