﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace lai2
{
    /// <summary>
    /// Interaction logic for DailyReport.xaml
    /// </summary>
    public partial class DailyReport : Page
    {
        Blogic bl = new Blogic();
        string date1;
        private Settings settings = Properties.Settings.Default;
        ReportIdList r = new ReportIdList();
        string reportid;
        string heading;
        System.ComponentModel.BackgroundWorker mWorker;
        List<ReportIdList> rl = new List<ReportIdList>();
        ReportDataList rll = new ReportDataList();

        public DailyReport()
        {
            InitializeComponent();
            color();
            spcshiftdata.Visibility = Visibility.Hidden;
            spcshift.Visibility = Visibility.Visible;
            datepicker1.SelectedDate = DateTime.Now;
            datepicker1.DisplayDateEnd = DateTime.Now;

            lbCompany.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            lbLocation.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            lbUserName.Content = LoginDetail.user.UserName.ToString();

            if (Blogic.IsAtvm)
            {
                btEdit.Visibility = Visibility.Hidden;
                lblInstntTotl_Copy.Visibility = Visibility.Hidden;
                lblInstCashot_Copy.Visibility = Visibility.Hidden;
                lbtpup.Visibility = Visibility.Hidden;
                lbtpupcncl.Visibility = Visibility.Hidden;
                lbcredit.Visibility = Visibility.Hidden;
                lbdebit.Visibility = Visibility.Hidden;
                lbtopup.Visibility = Visibility.Hidden;
                lbtopupcancel.Visibility = Visibility.Hidden;
                lbnetonlinesalee.Visibility = Visibility.Hidden;
                lbonlinescshout.Visibility = Visibility.Hidden;
                lblInstCashot.Visibility = Visibility.Hidden;
                lbosale.Visibility = Visibility.Hidden;
                lbinsCashout.Visibility = Visibility.Hidden;
                lboCashout.Visibility = Visibility.Hidden;


            }

           

            
            loading();

        }

        private void color()
        {
            loadingtelerick.Foreground = new SolidColorBrush(settings.Font);
            btPdf.Background = new SolidColorBrush(settings.Tab);
            tab1.Background = new SolidColorBrush(settings.Tab);
            tab2.Background = new SolidColorBrush(settings.Tab);
            tab3.Background = new SolidColorBrush(settings.Tab);
            tab4.Background = new SolidColorBrush(settings.Tab);
            tab5.Background = new SolidColorBrush(settings.Tab);
            btPdf.Background = new SolidColorBrush(settings.Tab);

            //  tab1.BorderBrush = new SolidColorBrush(settings.Tab);
            tab2.BorderBrush = new SolidColorBrush(settings.ActiveC);
            tab3.BorderBrush = new SolidColorBrush(settings.Complite);
            tab4.BorderBrush = new SolidColorBrush(settings.InactiveC);
            tab5.BorderBrush = new SolidColorBrush(settings.ReturnC);

            tab1.Foreground = new SolidColorBrush(settings.Font);
            tab2.Foreground = new SolidColorBrush(settings.Font);
            tab3.Foreground = new SolidColorBrush(settings.Font);
            tab4.Foreground = new SolidColorBrush(settings.Font);
            tab5.Foreground = new SolidColorBrush(settings.Font);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            lbUserName.Foreground = new SolidColorBrush(settings.Font);
            lbLocation.Foreground = new SolidColorBrush(settings.Font);
            lbCompany.Foreground = new SolidColorBrush(settings.Font);
            lbOpenDate.Foreground = new SolidColorBrush(settings.Font);
            lbClosedate.Foreground = new SolidColorBrush(settings.Font);
            LatestData.Foreground = new SolidColorBrush(settings.Font);
            activatedTickets.Foreground = new SolidColorBrush(settings.ActiveC);
            soldoutTickets.Foreground = new SolidColorBrush(settings.Complite);
            InactiveTickets.Foreground = new SolidColorBrush(settings.InactiveC);
            ReturnTickets.Foreground = new SolidColorBrush(settings.ReturnC);
            lbinsTotal.Foreground = new SolidColorBrush(settings.Font);
            lbinsCashout.Foreground = new SolidColorBrush(settings.Font);
            lbosale.Foreground = new SolidColorBrush(settings.Font);
            lboCashout.Foreground = new SolidColorBrush(settings.Font);
            btPdf.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            lbcredit.Foreground = new SolidColorBrush(settings.Font);
            lbdebit.Foreground = new SolidColorBrush(settings.Font);
            lbtopup.Foreground = new SolidColorBrush(settings.Font);
            lbtopupcancel.Foreground = new SolidColorBrush(settings.Font);
            //lbdaycredit.Foreground = new SolidColorBrush(settings.Font);
            //lbdaydebit.Foreground = new SolidColorBrush(settings.Font);
            //lbdaytopup.Foreground = new SolidColorBrush(settings.Font);
            //lbdaytopupcancel.Foreground = new SolidColorBrush(settings.Font);
            dgshift.Foreground = new SolidColorBrush(settings.Font);
            btAdd.Foreground = new SolidColorBrush(settings.Font);
            btAdd.Background = new SolidColorBrush(settings.Tab);
            btDelete.Foreground = new SolidColorBrush(settings.Font);
            btDelete.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            dgshift.BorderBrush = new SolidColorBrush(settings.Border);
            LatestData.BorderBrush = new SolidColorBrush(settings.Border);
            activatedTickets.BorderBrush = new SolidColorBrush(settings.Border);
            soldoutTickets.BorderBrush = new SolidColorBrush(settings.Border);
            InactiveTickets.BorderBrush = new SolidColorBrush(settings.Border);
            ReturnTickets.BorderBrush = new SolidColorBrush(settings.Border);

            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);

            dgshift.FontFamily = new FontFamily(settings.datagridfont);
            LatestData.FontFamily = new FontFamily(settings.datagridfont);
            activatedTickets.FontFamily = new FontFamily(settings.datagridfont);
            soldoutTickets.FontFamily = new FontFamily(settings.datagridfont);
            InactiveTickets.FontFamily = new FontFamily(settings.datagridfont);
            ReturnTickets.FontFamily = new FontFamily(settings.datagridfont);

            lblInstntTotl_Copy.FontFamily = new FontFamily(settings.labelfont);
            lblInstCashot_Copy.FontFamily = new FontFamily(settings.labelfont);
            lbtpup.FontFamily = new FontFamily(settings.labelfont);
            lbtpupcncl.FontFamily = new FontFamily(settings.labelfont);
            lbcredit.FontFamily = new FontFamily(settings.labelfont);
            lbdebit.FontFamily = new FontFamily(settings.labelfont);
            lbtopup.FontFamily = new FontFamily(settings.labelfont);
            lbtopupcancel.FontFamily = new FontFamily(settings.labelfont);
            lblInstntTotl.FontFamily = new FontFamily(settings.labelfont);
            lbnetonlinesalee.FontFamily = new FontFamily(settings.labelfont);
            lblInstCashot.FontFamily = new FontFamily(settings.labelfont);
            lbonlinescshout.FontFamily = new FontFamily(settings.labelfont);

            lbinsTotal.FontFamily = new FontFamily(settings.labelfont);
            lbosale.FontFamily = new FontFamily(settings.labelfont);
            lbinsCashout.FontFamily = new FontFamily(settings.labelfont);
            lboCashout.FontFamily = new FontFamily(settings.labelfont);

            lbusrnme.FontFamily = new FontFamily(settings.labelfont);
            lblctnnme.FontFamily = new FontFamily(settings.labelfont);
            lblegalname.FontFamily = new FontFamily(settings.labelfont);
            lbopendte.FontFamily = new FontFamily(settings.labelfont);

            lbclsedte.FontFamily = new FontFamily(settings.labelfont);
            lbUserName.FontFamily = new FontFamily(settings.labelfont);
            lbLocation.FontFamily = new FontFamily(settings.labelfont);
            lbCompany.FontFamily = new FontFamily(settings.labelfont);

            lbOpenDate.FontFamily = new FontFamily(settings.labelfont);
            lbClosedate.FontFamily = new FontFamily(settings.labelfont);
            dgshift.Foreground = new SolidColorBrush(settings.Font);
            btnBack.Background = new SolidColorBrush(settings.Tab);
            btnBack.Foreground = new SolidColorBrush(settings.Font);

            lbClosedate_Copy.Foreground = new SolidColorBrush(settings.Font);
            lbClosedate_Copy.FontFamily = new FontFamily(settings.labelfont);
            lbclsedte_Copy.FontFamily = new FontFamily(settings.labelfont);

            btEdit.Background = new SolidColorBrush(settings.Tab);
            btEdit.Foreground = new SolidColorBrush(settings.Font);

            btnChangeShiftDate.Background = new SolidColorBrush(settings.Tab);
            btnChangeShiftDate.Foreground = new SolidColorBrush(settings.Font);
            btnChangeShiftDate.Visibility = Visibility.Hidden;
        }

        private void btPdf_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                var response = client.DownloadData(new Uri(String.Format(bl.baseUrl + "/GetShiftReportPDF.aspx?Print=yes&ReportId=" + reportid + "&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + bl.GetAtvmStatus())));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\Shiftreport.pdf", response);

            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\Shiftreport.pdf");
            Process.Start(sInfo);
        }

        public void loading()
        {
            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;

                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }


                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            rl = bl.ViewShift(date1);
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation  
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            if (rl == null && bl.internetconnection() == false)
            {
                string heading1 = "        Poor Connectivity";
                var dialog1 = new DialogBox_BoxNo(heading1);
                dialog1.ShowDialog();
            }
            else
            {
                string dateheading = Convert.ToDateTime(datepicker1.SelectedDate).ToString("MM/dd/yyyy");
                dgshift.Columns[0].Header = "                Shifts on" + " " + dateheading;
                dgshift.ItemsSource = rl;
                btnChangeShiftDate.Visibility = Visibility.Hidden;
            }

        }

        public void loadingdata()
        {
            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWorkloading);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChangedlloading);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompletedloading);
            mWorker.RunWorkerAsync();
            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;

                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }

                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }
        }

        private void worker_DoWorkloading(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            rll = bl.ShiftReportData(reportid);
        }

        private void worker_ProgressChangedlloading(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompletedloading(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            if (rll == null && bl.internetconnection() == false)
            {
                string heading1 = "        Poor Connectivity";
                var dialog1 = new DialogBox_BoxNo(heading1);
                dialog1.ShowDialog();
            }
            else
            {
                lbOpenDate.Content = rll.StartDate;
                lbClosedate.Content = rll.CloseDate;
                lbinsTotal.Content = rll.InstantSales.ToString("C");
                lbinsCashout.Content = rll.InstantCashout.ToString("C");
                lbosale.Content = rll.OnlineSales.ToString("C");
                lboCashout.Content = rll.OnlineCashout.ToString("C");
                lbcredit.Content = rll.Credit.ToString("C");
                lbdebit.Content = rll.Debit.ToString("C");
                lbtopup.Content = rll.Topup.ToString("C");
                lbtopupcancel.Content = rll.TopUpCancel.ToString("C");

                LatestData.ItemsSource = rll.ShiftData;
                activatedTickets.ItemsSource = rll.ActiveData;
                soldoutTickets.ItemsSource = rll.SoldOutData;
                InactiveTickets.ItemsSource = rll.InactiveData;
                ReturnTickets.ItemsSource = rll.ReturnData;
            }
        }

        private void datepicker1_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            spcshiftdata.Visibility = Visibility.Hidden;
            spcshift.Visibility = Visibility.Visible;
            date1 = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
            loading();
        }

        private void btback_Click(object sender, RoutedEventArgs e)
        {
            spcshift.Visibility = Visibility.Visible;
            spcshiftdata.Visibility = Visibility.Hidden;
            datepicker1.SelectedDate = Convert.ToDateTime(date1);
            loading();
        }

        private void DatagridShift_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (dgshift.SelectedItems.Count == 1)
            {
                try
                {
                    r = (ReportIdList)dgshift.SelectedItem;
                    lbClosedate_Copy.Content = r.shift;
                    reportid = r.ReportId.ToString();
                    spcshift.Visibility = Visibility.Hidden;
                    spcshiftdata.Visibility = Visibility.Visible;


                    if (dgshift.SelectedIndex == 0)
                    {
                        // || dgshift.SelectedIndex == dgshift.Items.Count - 1
                        btnChangeShiftDate.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnChangeShiftDate.Visibility = Visibility.Hidden;
                    }

                    if (dgshift.SelectedItem != null)
                    {
                        reportid = Convert.ToString(r.ReportId);
                        loadingdata();
                    }
                    else
                    {
                        spcshiftdata.Visibility = Visibility.Hidden;
                        spcshift.Visibility = Visibility.Visible;
                    }
                }
                catch
                {
                    string heading = "Invalid Selection";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            bool flag = false;
            while (!flag)                         //means flag=false
            {
                var dialog = new AddShiftReport(reportid);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    dialog.Close();
                    loadingdata();
                }
                else
                {
                    flag = true;
                }
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (tab1.IsSelected == true)
            {
                int count = LatestData.SelectedItems.Count;

                if (LatestData.SelectedItem != null)
                {
                    if (count == 1)
                    {
                        try
                        {
                            ReportData rd = (ReportData)LatestData.SelectedItem;
                            var dialog = new DeleteDetails(rd, reportid);
                            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                            {
                                loadingdata();
                            }
                        }
                        catch
                        {
                            string heading = "Invalid Selection";
                            var dialog = new DialogBox_BoxNo(heading);
                            dialog.ShowDialog();
                        }
                    }
                    else
                    {
                        string heading = "Select atleast one";
                        int flag = 2;
                        var dialog = new DialogBox_BoxNo(heading, flag);
                        dialog.ShowDialog();
                    }
                }
                else
                {
                    string heading = "Invalid Selection";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();

                }
            }

            else if (tab5.IsSelected == true)
            {
                if (ReturnTickets.SelectedItem != null && ReturnTickets.SelectedItems.Count == 1)
                {
                    SpecificTicket st = (SpecificTicket)ReturnTickets.SelectedItem;
                    ReportData rd = new ReportData
                    {
                        Box = st.Box,
                        TicketId = st.TicketId,
                        PackNo = st.PackNo,
                        Value = st.Value,
                        TicketInventoryId = st.TicketInventoryId,
                        TicketName = st.TicketName




                    };
                    try
                    {

                        var dialog = new DeleteDetails(rd, reportid);
                        if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                        {
                            loadingdata();
                        }
                    }
                    catch
                    {
                        string heading = "Invalid Selection";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }

                }

            }
            else
            {
                string heading = "Select atleast one";
                int flag = 2;
                var dialog = new DialogBox_BoxNo(heading, flag);
                dialog.ShowDialog();
            }

        }

        private void bteDIT_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new EditShift(rll.OnlineSales, rll.OnlineCashout, rll.InstantCashout, rll.ActiveTicketNo, rll.InventoryTicketNo, rll.CashInHand, rll.Credit, rll.Debit, rll.Topup, rll.TopUpCancel, reportid, rll.Issued, rll.Loan);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {

            }
            loadingdata();
        }

        private void tab1_GotFocus(object sender, RoutedEventArgs e)
        {
            btAdd.Visibility = Visibility.Visible;
            //  btEdit.Visibility = Visibility.Visible;
            btDelete.Visibility = Visibility.Visible;
        }

        private void tab2_GotFocus(object sender, RoutedEventArgs e)
        {
            btAdd.Visibility = Visibility.Hidden;
            //   btEdit.Visibility = Visibility.Hidden;
            btDelete.Visibility = Visibility.Hidden;
        }

        private void tab5_GotFocus(object sender, RoutedEventArgs e)
        {
            btAdd.Visibility = Visibility.Hidden;
            //   btEdit.Visibility = Visibility.Hidden;
            btDelete.Visibility = Visibility.Visible;
        }

        private void btnChangeShiftDate_Click(object sender, RoutedEventArgs e)
        {
            SelectDate dialog = new SelectDate();
            dialog.ResponceData = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");

            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                btnChangeShiftDate.Visibility = Visibility.Hidden;
                Result rs = bl.ChangeShiftDate(reportid, dialog.ResponceData);
                spcshiftdata.Visibility = Visibility.Hidden;
                spcshift.Visibility = Visibility.Visible;
                date1 = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                loading();
            }
        }
    }
}
