﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for SelectColor.xaml
    /// </summary>
    public partial class SelectColor : Page
    {
        private Settings settings = Properties.Settings.Default;
        public SelectColor()
        {
            InitializeComponent();
            btBack.Background = new SolidColorBrush(settings.Tab);
            button1.Background = new SolidColorBrush(settings.Tab);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            btBack.Foreground = new SolidColorBrush(settings.Font);
            button1.Foreground = new SolidColorBrush(settings.Font);
            btReset.Background = new SolidColorBrush(settings.Tab);
            btReset.Foreground = new SolidColorBrush(settings.Font);


            lblogo.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);

            colorPickerTab.SelectedColor = settings.Tab;
            colorPickerTheme.SelectedColor = settings.Theme;
            colorPickerFont.SelectedColor = settings.Font;
            colorPickerActive.SelectedColor = settings.ActiveC;
            colorPickerComplite.SelectedColor = settings.Complite;
            colorPickerInactive.SelectedColor = settings.InactiveC;
            colorPickerReturn.SelectedColor = settings.ReturnC;
            colorPickerDay1_7.SelectedColor = settings.Day1_7;
            colorPickerDay8_14.SelectedColor = settings.Day8_14;
            colorPickerDay15_21.SelectedColor = settings.Day15_21;
            colorPickerDay22_above.SelectedColor = settings.Day22_above;
            colorPickerBackground.SelectedColor = settings.Background;
            colorPickerborder.SelectedColor = settings.Border;
            colorPickerHC1.SelectedColor = settings.HC1_Color;
            colorPickerHC2.SelectedColor = settings.HC2_Color;
            colorPickerHC3.SelectedColor = settings.HC3_Color;
            tbHC1_Value.Text = settings.HC1_Value.ToString();
            tbHC2_Value.Text = settings.HC2_Value.ToString();
            tbHC3_Value.Text = settings.HC3_Value.ToString();
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            if (settings.HomeColorBackground == 0)
                rdSale_backgroundColor.IsChecked = true;
            else
                rdSale_fontcolor.IsChecked = true;


            //colorPickerFont.SelectedColor = settings.Font;
            //colorPickerTheme.SelectedColor = settings.Theme;
            //colorPickerDay1_7.SelectedColor = settings.Day1_7;
            //colorPickerDay8_14.SelectedColor = settings.Day8_14;
            //colorPickerDay15_21.SelectedColor = settings.Day15_21;
            //colorPickerDay22_above.SelectedColor = settings.Day22_above;

        }

        private void colorPickerFont_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerTheme_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerTab_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerDay1_7_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerDay8_14_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerDay15_21_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerDay22_above_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void btBack_Click(object sender, RoutedEventArgs e)     //back button
        {
            NavigationService navService = NavigationService.GetNavigationService(this);
            HomeAdmin nextPage = new HomeAdmin();
            navService.Navigate(nextPage);
        }

        private void button1_Click(object sender, RoutedEventArgs e)        //save button
        {
            int status = 0;


            settings.Tab = colorPickerTab.SelectedColor;
            settings.Theme = colorPickerTheme.SelectedColor;
            settings.Font = colorPickerFont.SelectedColor;
            settings.ReturnC = colorPickerReturn.SelectedColor;
            settings.InactiveC = colorPickerInactive.SelectedColor;
            settings.Complite = colorPickerComplite.SelectedColor;
            settings.ActiveC = colorPickerActive.SelectedColor;
            settings.Day1_7 = colorPickerDay1_7.SelectedColor;
            settings.Day15_21 = colorPickerDay15_21.SelectedColor;
            settings.Day8_14 = colorPickerDay8_14.SelectedColor;
            settings.Day22_above = colorPickerDay22_above.SelectedColor;
            settings.Background = colorPickerBackground.SelectedColor;
            settings.Border = colorPickerborder.SelectedColor;

            settings.HC1_Color = colorPickerHC1.SelectedColor;
            settings.HC2_Color = colorPickerHC2.SelectedColor;
            settings.HC3_Color = colorPickerHC3.SelectedColor;

            double hc1val, hc2val, hc3val;
            bool result = Double.TryParse(tbHC1_Value.Text.Replace("%", ""), out hc1val);
            bool result1 = Double.TryParse(tbHC2_Value.Text.Replace("%", ""), out hc2val);
            bool result2 = Double.TryParse(tbHC3_Value.Text.Replace("%", ""), out hc3val);
            if (result == false)
            {
                var dialog = new Message("Please enter digits only.", "");
                dialog.ShowDialog();
                status = 1;
            }
            else if (hc1val > 100)
            {
                var dialog = new Message("Maximam Value is 100", "");
                dialog.ShowDialog();
                status = 1;
            }


            else if (result1 == false)
            {
                var dialog = new Message("Please enter digits only.", "");
                dialog. ShowDialog();
                status = 1;
            }
            else if (hc2val > 100)
            {
                var dialog = new Message("Maximam Value is 100", "");
                dialog.ShowDialog();
                status = 1;
            }
            else if (hc1val > hc2val)
            {

                var dialog = new Message("Percentage 1 Should less that percentage 2", "");
                dialog.ShowDialog();
                status = 1;
                return;
            }

            else if (result2 == false)
            {
                var dialog = new Message("Please enter digits only.", "");
                dialog.ShowDialog();

                status = 1;
            }
            else if (hc3val > 100)
            {
                var dialog = new Message("Maximam Value is 100", "");
                dialog.ShowDialog();
                status = 1;
            }
            else if (hc2val > hc3val)
            {
                var dialog = new Message("Percentage 2 Should less that percentage 3", "");
                dialog.ShowDialog();
                status = 1;
                return;
            }


            settings.HC1_Value = hc1val;
            settings.HC2_Value = hc2val;
            settings.HC3_Value = hc3val;
            settings.ColorSettingType = 1;
            if (day_font_Satus == true)
            {
                settings.HomeColorBackground = 0;
                settings.HomeColorForground = 1;
            }
            else
            {
                settings.HomeColorBackground = 1;
                settings.HomeColorForground = 0;

            }

            if (status == 0)
            {
                settings.Save();
                NavigationService navService = NavigationService.GetNavigationService(this);
                HomeAdmin nextPage = new HomeAdmin();
                navService.Navigate(nextPage);
            }
        }

        private void ColorPicker_Border(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerHC1_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerHC2_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }

        private void colorPickerHC3_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {

        }



        private void tbHC1_Value_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbHC1_Value.Text = AddSymbol(tbHC1_Value.Text);
            if (tbHC1_Value.Text.Length != 0)
                tbHC1_Value.CaretIndex = tbHC1_Value.Text.Length - 1;
        }

        private string AddSymbol(string text)
        {
            text = text.Replace("%", "");
            if (text != "")
            {
                text += "%";
            }
            return text;
        }

        private void tbHC2_Value_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbHC2_Value.Text = AddSymbol(tbHC2_Value.Text);
            if (tbHC2_Value.Text.Length != 0)
                tbHC2_Value.CaretIndex = tbHC2_Value.Text.Length - 1;
        }

        private void tbHC3_Value_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbHC3_Value.Text = AddSymbol(tbHC3_Value.Text);
            if (tbHC3_Value.Text.Length != 0)
                tbHC3_Value.CaretIndex = tbHC3_Value.Text.Length - 1;
        }

        bool day_font_Satus = true;
        private void rdDay_fontcolor_Checked(object sender, RoutedEventArgs e)
        {
            rdDay_backgroundColor.IsChecked = false;
            rdSale_backgroundColor.IsChecked = true;
            day_font_Satus = true;
        }

        private void rdDay_backgroundColor_Checked(object sender, RoutedEventArgs e)
        {
            rdDay_fontcolor.IsChecked = false;
            rdSale_fontcolor.IsChecked = true;
            day_font_Satus = false;
        }

        private void rdSale_fontcolor_Checked(object sender, RoutedEventArgs e)
        {
            rdSale_backgroundColor.IsChecked = false;
            rdDay_backgroundColor.IsChecked = true;
            day_font_Satus = false;

        }

        private void rdSale_backgroundColor_Checked(object sender, RoutedEventArgs e)
        {
            rdSale_fontcolor.IsChecked = false;
            rdDay_fontcolor.IsChecked = true;
            day_font_Satus = true;
        }

        private void btReset_Click(object sender, RoutedEventArgs e)
        {
            settings.Tab = (Color)ColorConverter.ConvertFromString("#FF000000"); ;
            settings.Theme = (Color)ColorConverter.ConvertFromString("#FF6495ED"); ;
            settings.Font = (Color)ColorConverter.ConvertFromString("#FFFF0000"); ;
            settings.ReturnC = (Color)ColorConverter.ConvertFromString("#FF00FF00"); ;
            settings.InactiveC = (Color)ColorConverter.ConvertFromString("#FFFFFF00"); ;
            settings.Complite = (Color)ColorConverter.ConvertFromString("#FF1E90FF"); ;
            settings.ActiveC = (Color)ColorConverter.ConvertFromString("#FFFF0000"); ;
            settings.Day1_7 = (Color)ColorConverter.ConvertFromString("#FFFFFFFF"); ;
            settings.Day15_21 = (Color)ColorConverter.ConvertFromString("#FFFFFFFF"); ;
            settings.Day8_14 = (Color)ColorConverter.ConvertFromString("#FFFFFFFF"); ;
            settings.Day22_above = (Color)ColorConverter.ConvertFromString("#FFFFFFFF"); ;
            settings.Background = (Color)ColorConverter.ConvertFromString("#00FFFFFF"); ;
            settings.Border = (Color)ColorConverter.ConvertFromString("#FFFF0000"); ;
            settings.HC1_Color = (Color)ColorConverter.ConvertFromString("#00FFFFFF"); ;
            settings.HC2_Color = (Color)ColorConverter.ConvertFromString("#00FFFFFF"); ;
            settings.HC3_Color = (Color)ColorConverter.ConvertFromString("#00FFFFFF"); ;
            settings.ColorSettingType = 0;
            settings.HC1_Value = 33;
            settings.HC2_Value = 66;
            settings.HC3_Value = 100;

            settings.Save();
            NavigationService navService = NavigationService.GetNavigationService(this);
            HomeAdmin nextPage = new HomeAdmin();
            navService.Navigate(nextPage);
        
        }
    }
}
