﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for DeleteWindow.xaml
    /// </summary>
    public partial class DeleteWindow : Window
    {
        private Settings settings = Properties.Settings.Default; 
        private string p;
        Blogic bl = new Blogic();
        private emaillist es;
        private string heading;
        private string username;
        int flag = 0;
        //private ViewTicket a;
        public DeleteWindow()
        {
            InitializeComponent();
            color();
            btyes.Focus();
        }

        public DeleteWindow(string p)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();
            btyes.Focus();
            this.p = p;
            btyes.Background = new SolidColorBrush(settings.Tab);
            btno.Background = new SolidColorBrush(settings.Tab);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            ticketname.Foreground = new SolidColorBrush(settings.Font);
            btyes.Foreground = new SolidColorBrush(settings.Font);
            btno.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);
            //lbtcktname.Content = "Ticket Name:";
            //ticketname.Content = p.ToString();
        }

        public void color()
        {
            btyes.Background = new SolidColorBrush(settings.Tab);
            btno.Background = new SolidColorBrush(settings.Tab);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            ticketname.Foreground = new SolidColorBrush(settings.Font);
            btyes.Foreground = new SolidColorBrush(settings.Font);
            btno.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);
        }

        public DeleteWindow(emaillist es)
        {
            InitializeComponent();
            color();
            btyes.Focus();
            this.es = es;
            ticketname.Visibility = Visibility.Visible;
            lbtcktname.Visibility = Visibility.Visible;
            lbtcktname.Content = "Email:";
            ticketname.Content = es.Email;           
        }

        public DeleteWindow(string heading, string username)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();
            btyes.Focus();
            this.heading = heading;
            this.username = username;        
            lbtcktname.Visibility = Visibility.Visible;
            lblTittle.Content = heading;         
            lbtcktname.Content = username;

            if (heading.Trim() == "Change Password")
            { 
            this.Title ="Change Password";
            }
            if (heading.Trim() == "Delete User")
            {
                flag = 1;   //delete employee
            }
        }

        private void Button_No_Clicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_Yes_clicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            if (flag == 1)    //delete employee
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
            }
            else
            {
                try
                {
                    Application curApp = Application.Current;
                    Window mainWindow = curApp.MainWindow;
                    this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
                    this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

    }
}
