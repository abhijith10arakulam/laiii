﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lai2.ATVM
{
    /// <summary>
    /// Interaction logic for AtvmCloseShift.xaml
    /// </summary>
    public partial class AtvmCloseShift : Page
    {
        private Settings settings = Properties.Settings.Default;
        System.ComponentModel.BackgroundWorker mWorker;
        ReportDataList rl = new ReportDataList();
        Blogic bl = new Blogic();
        List<ScanData> tickets;
        ResultWithOldTicket rs;

        List<ReportDataATVM> closeShiftData;
        public AtvmCloseShift()
        {
            InitializeComponent();
            lblogo.Foreground = new SolidColorBrush(settings.Font);
            btback.Background = new SolidColorBrush(settings.Tab);
            btCloseShift.Background = new SolidColorBrush(settings.Tab);
            btReset.Background = new SolidColorBrush(settings.Tab);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            btback.Foreground = new SolidColorBrush(settings.Font);
            btCloseShift.Foreground = new SolidColorBrush(settings.Font);
            btReset.Foreground = new SolidColorBrush(settings.Font);
            LatestData.BorderBrush = new SolidColorBrush(settings.Border);
            LatestData.FontFamily = new FontFamily(settings.datagridfont);

            lbinsatntsaleval.Foreground = new SolidColorBrush(settings.Font);
            lbtotalcountval.Foreground = new SolidColorBrush(settings.Font);
            tickets = new List<ScanData>();
            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
           new System.Threading.ThreadStart(delegate { }));
            //  pbProcessing.Value = 0;

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();


            try
            {

                {

                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loading.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {

                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string time = DateTime.Now.ToString("hh:mm:sstt");
            rl = bl.TodaysData(date, time);

            closeShiftData = rl.ShiftData.Where(x => x.ActiveAge >= 0).Select(x =>
                new ReportDataATVM
                {
                    ActiveAge = x.ActiveAge,
                    Box = x.Box,
                    CloseNo = x.OpenNo + x.Count,
                    Count = x.Count,
                    InactiveStatus = x.InactiveStatus,
                    MaxNo = x.MaxNo,
                    OpenNo = x.OpenNo,
                    PackNo = x.PackNo,
                    rank = x.rank,
                    ReportId = x.ReportId,
                    RetuenStatus = x.RetuenStatus,
                    SalePercentage = x.SalePercentage,
                    SoldoutStatus = x.SoldoutStatus,
                    TicketId = x.TicketId,
                    TicketInventoryId = x.TicketInventoryId,
                    TicketName = x.TicketName,
                    Total = x.Count * x.Value,
                    TransactionId = x.TransactionId,
                    Value = x.Value
                }
           ).ToList();

        }


        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            // pbProcessing.Value = e.ProgressPercentage;
            //if (pbProcessing.Value == 0.05)
            //{
            //    mWorker.CancelAsync();
            //}
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop Progressbar updatation  
            mWorker.CancelAsync();
            loading.IsBusy = false;
            if (rl.ShiftData == null)
            {
                if (bl.internetconnection() == false)
                {
                    string heading = "     Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    mWorker.RunWorkerAsync();

                    //Duration duration = new Duration(TimeSpan.FromSeconds(20));

                    //DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
                    //pbProcessing.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
                    try
                    {
                        //while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                        {

                            if (!mWorker.CancellationPending)
                            {
                                try
                                {
                                    loading.IsBusy = true;
                                }
                                catch (System.Exception ex)
                                {
                                    // No action required
                                }
                            }
                            else
                            {
                                // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                                //   break;
                            }

                            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                                   new System.Threading.ThreadStart(delegate { }));
                        }
                    }
                    catch { }

                }
            }
            else
            {


                if (rl.ExpiryCheck.Status == false)
                {
                    var dialog = new Expired(rl.ExpiryCheck.Description);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                    }
                    Blogic.ExpireFlag = 1;
                }
                else
                {
                    Blogic.ExpireFlag = 0;

                    Blogic.opendate = rl.StartDate;
                    LatestData.ItemsSource = closeShiftData;//rl.ShiftData.Where(x => x.ActiveAge >= 0).ToList();


                    lbinsatntsaleval.Content = closeShiftData.Sum(x => x.Total).ToString("C");
                    lbtotalcountval.Content = closeShiftData.Sum(x => x.Count).ToString();

                }

            }

        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }

        private void btCloseShift_Click(object sender, RoutedEventArgs e)
        {
            bool flag = false;
            int box = 0;
            List<ReportDataATVM> data = (List<ReportDataATVM>)LatestData.ItemsSource;
            tickets = new List<ScanData>();
            //tickets = data.Where(x => x.CloseNo != 0).Select(x => new ScanData
            //{
            //    PackNo = x.PackNo,
            //    TicketId = x.TicketId,
            //    PackPosition = x.CloseNo

            //}).ToList();

            foreach (ReportDataATVM reportData in data)
            {

                //    if ((reportData.CloseNo != 0) || (reportData.OpenNo == 0 && reportData.CloseNo == 0))
                {
                    tickets.Add(new ScanData
                {
                    PackNo = reportData.PackNo,
                    TicketId = reportData.TicketId,
                    PackPosition = reportData.CloseNo

                });
                    if (reportData.CloseNo < reportData.OpenNo || reportData.CloseNo > reportData.MaxNo + 1)
                    {
                        flag = true;
                        box = reportData.Box;
                    }

                }
            }
            if (!flag)
            {
                mWorker = new System.ComponentModel.BackgroundWorker();
                mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(workerClose_DoWork);
                mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
                mWorker.WorkerReportsProgress = true;
                mWorker.WorkerSupportsCancellation = true;
                mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(workerClose_RunWorkerCompleted);
                mWorker.RunWorkerAsync();


                try
                {

                    {

                        if (!mWorker.CancellationPending)
                        {
                            try
                            {
                                loading.IsBusy = true;
                            }
                            catch (System.Exception ex)
                            {
                                // No action required
                            }
                        }
                        else
                        {

                        }

                        System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                               new System.Threading.ThreadStart(delegate { }));
                    }
                }
                catch { }
            }


            else
            {

                var dialog = new Message("Please Check Box -" + box, "");
                dialog.ShowDialog();
            }


        }

        private void workerClose_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loading.IsBusy = false;
            try
            {
                var dialog = new SkippedBox();
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    loading.IsBusy = true;
                    string time = DateTime.Now.ToString("hh:mm:sstt");
                    string closedate = DateTime.Now.ToString("yyyy/MM/dd");
                    Result rs = bl.CloseShiftStatus_check("0", "0", "0", "0", "0", "0", "0", closedate, "0", "0", "0", "0", closedate, time, "0", "0");

                    if (rs.Status == true)
                    {
                        loading.IsBusy = false;
                        var dia = new DialogBox_BoxNo("Congratulations!!!.\n You have successfully closed your shift");
                        if (dia.ShowDialog() == true && dia.DialogResult == true)
                        { }
                        NavigationService navService = NavigationService.GetNavigationService(this);
                        HomeAdmin nextPage = new HomeAdmin();
                        navService.Navigate(nextPage);
                    }
                    else
                    {
                        loading.IsBusy = false;

                        try
                        {
                            //  MessageBox.Show(r.Description);
                            string heading = rs.Description;
                            var dialog1 = new DialogBox_BoxNo(heading);
                            dialog1.ShowDialog();
                        }
                        catch
                        {

                        }

                    }

                }




                dialog.Close();
            }
            catch { }
        }

        private void workerClose_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            rs = new ResultWithOldTicket();
            rs = bl.CloseShiftAtvm(tickets);

        }
        bool flag = false;
        int value = 0;
        bool flag2 = false;
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            int a;
            if (Int32.TryParse(((TextBox)sender).Text, out a))
            {
                ReportDataATVM obj = (ReportDataATVM)LatestData.CurrentItem;
                if (obj != null)
                {

                    //   if (a > 0)
                    {
                        obj.CloseNo = obj.OpenNo + a;
                        obj.Total = a * obj.Value;
                    }
                    //else
                    //{
                    //    obj.CloseNo = 0;
                    //    obj.Total = 0;
                    //}

                    List<ReportDataATVM> data = (List<ReportDataATVM>)LatestData.ItemsSource;
                    lbinsatntsaleval.Content = data.Sum(x => x.Total).ToString("C");
                    lbtotalcountval.Content = data.Sum(x => x.Count).ToString();

                }
            }
            else
            {
                ((TextBox)sender).Text = "";
            }

            ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
        }

        private void TextBox_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            int a;
            if (Int32.TryParse(((TextBox)sender).Text, out a))
            {

            }
            else
            {
                ((TextBox)sender).Text = "";
            }


        }

        private void btback_Click(object sender, RoutedEventArgs e)
        {
            NavigationService navService = NavigationService.GetNavigationService(this);
            HomeAdmin nextPage = new HomeAdmin();
            navService.Navigate(nextPage);
        }

        private void LatestData_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            List<ReportDataATVM> data = (List<ReportDataATVM>)LatestData.ItemsSource;

            data.ForEach(x => x.CloseNo = x.OpenNo + x.Count);
            data.ForEach(x => x.Total = x.Count * x.Value);
            LatestData.ItemsSource = data;
        }

        private void LatestData_CurrentCellChanged(object sender, EventArgs e)
        {
            //if (flag)
            //{
            //    flag = false;
            //    flag2 = false;
            //    List<ReportData> data = (List<ReportData>)LatestData.ItemsSource;
            //  int index= data.IndexOf((ReportData)LatestData.CurrentItem);
            //    LatestData.Items.Refresh();

            //    //  LatestData.CurrentCell = LatestData.Rows[data.IndexOf((ReportData)LatestData.CurrentItem)].Cells[6];

            //    LatestData.CurrentCell = new DataGridCellInfo(
            //    LatestData.Items[index], LatestData.Columns[6]);
            //   LatestData.BeginEdit();


            //}
            //flag2 = true;
        }

        private void btCloseShift_GotFocus(object sender, RoutedEventArgs e)
        {
            btCloseShift.Background = Brushes.White; ;
        }

        private void btCloseShift_LostFocus(object sender, RoutedEventArgs e)
        {
            btCloseShift.Background = new SolidColorBrush(settings.Tab);
        }

        private void btReset_Click(object sender, RoutedEventArgs e)
        {
            bl.ResetSkipping();
            NavigationService navService = NavigationService.GetNavigationService(this);
            AtvmCloseShift nextPage = new AtvmCloseShift();
            navService.Navigate(nextPage);
        }




    }

    public class TotalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var data = value as ReportData;
            return (data.Count * data.Value).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    public class CloseNoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var data = value as ReportData;
            return (data.Count + data.OpenNo).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

}

