﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using System.Windows.Shapes;

using System.Windows.Interop;
using lai2.Bal;
using lai2.Properties;
using System.Media;
using System.Deployment.Application;
using System.IO;
using Microsoft.Win32;
using System.Net.Mail;
using System.Net;

namespace lai2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LoginList ll = new LoginList();
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();

        public MainWindow()
        {
            InitializeComponent();
            SetAddRemoveProgramsIcon();
            SetStartup();
      
            // MinimizeToTray.Enable(this);
            if (settings.Password == "")
            {
                Frame1.Source = new Uri("Login.xaml", UriKind.RelativeOrAbsolute);
            }
            else
            {
                IndexFinder.index = 0;
                string username = settings.username;
                string password = settings.Password;
                ll = bl.LoginCheck(username, password);


                if (ll.user.UserName != null)
                {
                    if (ll.status == true)                         //for shop admin
                    {
                        string device = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                        string version = "0";
                        if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                        {
                            ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                            version = ad.CurrentVersion.ToString();
                        }

                        bl.SendDeviceDetails(device, version);

                        settings.username = username;
                        settings.Password = password;
                        settings.Save();
                        //Application.Current.Properties.Add("uname", username);             //session ---store the username in a to uname(as we like)
                        if (LoginDetail.user.UserType == "ShopAdmin")
                        {
                            SoundPlayer snd = new SoundPlayer(Properties.Resources.login);
                            snd.Play();

                            Frame1.Source = new Uri("Home.xaml", UriKind.RelativeOrAbsolute);
                            //Frame1.Source = new Uri("POSHOme.xaml", UriKind.RelativeOrAbsolute);

                        }
                        else if (LoginDetail.user.UserType == "Employee")
                        {
                            SoundPlayer snd = new SoundPlayer(Properties.Resources.login);
                            snd.Play();
                            Frame1.Source = new Uri("HomeAdmin.xaml", UriKind.RelativeOrAbsolute);


                        }
                        else if (LoginDetail.user.UserType == "ShopManeger")
                        {
                            SoundPlayer snd = new SoundPlayer(Properties.Resources.login);
                            snd.Play();
                            Frame1.Source = new Uri("HomeAdmin.xaml", UriKind.RelativeOrAbsolute);

                        }
                    }
                    else
                    {
                        settings.Password = "";
                        settings.Save();
                        Frame1.Source = new Uri("Login.xaml", UriKind.RelativeOrAbsolute);

                    }

                }
                else
                {
                    settings.Password = "";
                    settings.Save();
                    Frame1.Source = new Uri("Login.xaml", UriKind.RelativeOrAbsolute);
                
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

    
    private void SetStartup()
    {
            //string filePath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "lai2.exe");
            //RegistryKey rk = Registry.CurrentUser.OpenSubKey
            //    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);


            ////rk.SetValue("lai2", filePath);
            //// else
            //rk.DeleteValue("lai2", false);

        }
        private static void PinUnpinTaskBar( bool pin)
        {
            string filePath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "lai2.exe");
            if (!File.Exists(filePath)) throw new FileNotFoundException(filePath);

            // create the shell application object
            dynamic shellApplication = Activator.CreateInstance(Type.GetTypeFromProgID("Shell.Application"));

            string path = System.IO.Path.GetDirectoryName(filePath);
            string fileName = System.IO.Path.GetFileName(filePath);

            dynamic directory = shellApplication.NameSpace(path);
            dynamic link = directory.ParseName(fileName);

            dynamic verbs = link.Verbs();
            for (int i = 0; i < verbs.Count; i++)
            {
                dynamic verb = verbs.Item(i);
                string verbName = verb.Name.Replace(@"&", string.Empty).ToLower();

                if ((pin && verbName.Equals("pin to taskbar")) || (!pin && verbName.Equals("unpin from taskbar")))
                {

                    verb.DoIt();
                }
            }

            shellApplication = null;
        }


        private static void SetAddRemoveProgramsIcon()
        {
            
          
            //only run if deployed 
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed && ApplicationDeployment.CurrentDeployment.IsFirstRun)
            {
                PinUnpinTaskBar(false);
                PinUnpinTaskBar(true);
                try
                {
                    string iconSourcePath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "favicon.ico");
                    if (!File.Exists(iconSourcePath))
                        return;

                    RegistryKey myUninstallKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
                    string[] mySubKeyNames = myUninstallKey.GetSubKeyNames();
                    for (int i = 0; i < mySubKeyNames.Length; i++)
                    {
                        RegistryKey myKey = myUninstallKey.OpenSubKey(mySubKeyNames[i], true);
                        object myValue = myKey.GetValue("DisplayName");
                        if (myValue != null && myValue.ToString() == "Lottery Artificial Intelligence II")
                        {
                            myKey.SetValue("DisplayIcon", iconSourcePath);
                            break;
                        }
                    }
                }
                catch (Exception ex) { }
            }
        }
    }
}
