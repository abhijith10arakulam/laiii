﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using Microsoft.Win32;
using System.IO;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddPayout : Window
    {
        string Selectedvalue;
        private Settings settings = Properties.Settings.Default;

        int validation = 0;
        int flag;
        Blogic bl = new Blogic();
        public AddPayout()
        {            
            InitializeComponent();
            color();
            Hiddenvalidation();
            txtboxInvoiceNo.Focus();

            cbModePayment.Items.Add("Cash");
            cbModePayment.Items.Add("Cheque");

            List<Vendors> vendor = bl.Vendors_view();

            cbVendorName.ItemsSource = vendor;
            this.cbVendorName.SelectedValuePath = "VendorID";
            this.cbVendorName.DisplayMemberPath = "VendorName";

            List<Departments> dept = bl.viewAddDepartment();

            cbDepartment.ItemsSource = dept;
            this.cbDepartment.SelectedValuePath = "DeptId";
            this.cbDepartment.DisplayMemberPath = "Dept";

            //if (LoginDetail.user.UserType == "ShopAdmin")
            //{
            //    cbselectuser.Items.Add("Employee");

            //   cbselectuser.Items.Add("Shop Manager");             
            //}
            //else if (LoginDetail.user.UserType == "ShopManeger")
            //{
            //    cbselectuser.Items.Add("Employee");
            //}

            //cbselectuser.SelectedItem = "Employee";
        }

        public void color()
        {
            btnregister.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            lbdecription.Foreground = new SolidColorBrush(settings.Font);
            btnregister.Foreground = new SolidColorBrush(settings.Font);           
            cbDepartment.Background = new SolidColorBrush(settings.Tab);
            btnregister.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            cbDepartment.Foreground = new SolidColorBrush(settings.Font);
            cbVendorName.Foreground = new SolidColorBrush(settings.Font);
            cbVendorName.Background = new SolidColorBrush(settings.Tab);
            cbModePayment.Foreground = new SolidColorBrush(settings.Font);
            cbModePayment.Background = new SolidColorBrush(settings.Tab);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            btnregister_Copy.Background = new SolidColorBrush(settings.Tab);
            btnregister_Copy.Foreground=new SolidColorBrush(settings.Font);
        }

        public void Hiddenvalidation()
        {
            lbvalusername.Visibility = Visibility.Hidden;
            lbvalpassword.Visibility = Visibility.Hidden;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Hiddenvalidation();
            validation = 0;
            if (txtboxInvoiceNo.Text == "")
            {
                lbvalpassword.Visibility = Visibility.Visible;
                validation = 1;
            }
            if (txtboxAmount.Text == "")
            {
                lbvalusername.Visibility = Visibility.Visible;
                validation = 1;
            }
            else if (validation == 0)
            {
                //strImage = strImage.Substring(0, 1000);
                 Result r = bl.AddPayout(cbVendorName.SelectedValue.ToString(), cbDepartment.SelectedValue.ToString(), txtboxInvoiceNo.Text, cbModePayment.SelectedItem.ToString(), txtboxAmount.Text, strImage);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        this.DialogResult = true;
                    }
                    else
                    {
                        lbdecription.Content = r.Description;
                    }
                }
            }
        }

        private void CbselectUser_selectionChanged(object sender, SelectionChangedEventArgs e)
        {

            // Selectedvalue =Convert.ToString(cbselectuser.SelectedItem);

            //if (Selectedvalue == "Shop Manager")
            //{
            //    flag = 4;
            //}
            //if (Selectedvalue == "Employee")
            //{
            //    flag = 5;
            //}
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        }

        private void cbVendorName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbVendorName_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbModePayment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        string filepath;
        string strImage;
        private void button_Click(object sender, RoutedEventArgs e)
        {
            strImage= ImageToBase64();
//            OpenFileDialog open = new OpenFileDialog();
//            open.Multiselect = false;
//            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
//            bool? result = open.ShowDialog();

            //            if (result == true)
            //            {
            //                filepath = open.FileName; // Stores Original Path in Textbox    
            //                ImageSource imgsource = new BitmapImage(new Uri(filepath)); // Just show The File In Image when we browse It
            //                Clientimg.Source = imgsource;

            //                FileStream fs = new FileStream(open.FileName, FileMode.Open,
            //FileAccess.Read);

            //                byte[] data = new byte[fs.Length];
            //                fs.Read(data, 0, System.Convert.ToInt32(fs.Length));

            //                fs.Close();


            //            }
        }

        static string base64String = null;
        public string ImageToBase64()
        {


            OpenFileDialog open = new OpenFileDialog();
            open.Multiselect = false;
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            bool? result = open.ShowDialog();

            if (result == true)
            {
                filepath = open.FileName; // Stores Original Path in Textbox    

                //ImageCompression objImg = new ImageCompression();
                //string strToPath =  "E:\\Projects\\Execujet\\TelephoneBillingSystem\\ExecuJets\\images\\to.jpg";
                //objImg.compressImage(filepath, strToPath, 100, 50, "#FFFFFF");
                ImageSource imgsource = new BitmapImage(new Uri(filepath)); // Just show The File In Image when we browse It
                Clientimg.Source = imgsource;
               

                FileStream fs = new FileStream(open.FileName, FileMode.Open,
FileAccess.Read);

                byte[] data = new byte[fs.Length];
                fs.Read(data, 0, System.Convert.ToInt32(fs.Length));

                fs.Close();


            }

        // Clientimg.Source.
         

            string path = filepath;

            using (System.Drawing.Image image = System.Drawing.Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

    }
}
