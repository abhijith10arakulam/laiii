﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for StoreTime.xaml
    /// </summary>
    public partial class StoreTime : Window
    {


        private Settings settings = Properties.Settings.Default;

        int flag;
        
        public StoreTime()
        {
         InitializeComponent();

            rdbtnAscending.FontFamily = new FontFamily(settings.labelfont);
            rdbtnDescending.FontFamily = new FontFamily(settings.labelfont);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            rdbtnAscending.Foreground = new SolidColorBrush(settings.Font);
            rdbtnDescending.Foreground = new SolidColorBrush(settings.Font);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);


            if (LoginDetail.shops[IndexFinder.index].ShopTime == 0)
            {
                rdbtnAscending.IsChecked = true;
            }
            else
            {
                rdbtnDescending.IsChecked = true;
            }

        }

        private void rdbtnAscending_checked(object sender, RoutedEventArgs e)
        {
            flag = 0;    //ascending
        }

        private void rdbtnDescending_checked(object sender, RoutedEventArgs e)
        {
            //  settings.Ticketvalue = 1;
            flag = 1;
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            if (flag == 0)
            {
                LoginDetail.shops[IndexFinder.index].ShopTime = 0;
              
            }
            else if (flag == 1)
            {
                LoginDetail.shops[IndexFinder.index].ShopTime = 1;
                
            }
           Blogic bl=new Blogic();
            bl.UpdateShopTime();
            this.DialogResult = true;
        }

       
    }
}