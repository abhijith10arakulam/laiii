﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using lai2.Bal;
using System.Net;
using lai2.Properties;
using System.Media;
using System.IO;
using System.Diagnostics;
using System.Deployment.Application;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {

        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        //Complete cp  = new Complete();
        //UserDetails ud = new UserDetails();
        LoginList ll = new LoginList();
        public static string username;
        
        public Login()
        {
            
            string fontName = "Mistral";
            float fontSize = 12;

            using (System.Drawing.Font fontTester = new System.Drawing.Font(
                   fontName,
                   fontSize,
                   System.Drawing.FontStyle.Regular,
                   System.Drawing.GraphicsUnit.Pixel))
            {
                if (fontTester.Name != fontName)
                {
                    WebClient client = new WebClient();

                    var response = client.DownloadData(new Uri(String.Format("https://www.realtnetworking.com/Font/MISTRAL.TTF")));

                    string path = @"C:\testdir2\";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    System.IO.File.WriteAllBytes(@"C:\testdir2\MISTRAL.TTF", response);
                    ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\MISTRAL.TTF");
                    Process.Start(sInfo);
                }
               
            }
            InitializeComponent();
            btlogin.Background = new SolidColorBrush(settings.Tab);
            btlogin.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            lbloginresponse.Foreground = Brushes.Red;
            lblogo.Foreground = new SolidColorBrush(settings.Font);
            tbusername.Text = settings.username;
            tbpassword.Password = settings.Password;
            tbpassword.KeyDown += new KeyEventHandler(tbpswrd_keydown);
            Blogic.CurrentRow = 0;
           
           

        }

        void tbpswrd_keydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Loginfunction();
              
            }
        }

        private void Loginfunction()
        {
            lbloginresponse.Visibility = Visibility.Hidden;
            IndexFinder.index = 0;
            username = tbusername.Text;           
            string password = tbpassword.Password;
            ll = bl.LoginCheck(username, password);

            if (ll.user.UserName != null)
            {
                if (ll.status == true)                         //for shop admin
                {
                    string device = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    string version = "0";
                    if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                    {
                        ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                        version = ad.CurrentVersion.ToString();
                    }

                    bl.SendDeviceDetails(device, version);
                
                    settings.username = username;
                    settings.Password = password;
                    settings.Save();
                    //Application.Current.Properties.Add("uname", username);             //session ---store the username in a to uname(as we like)
                    if(LoginDetail.user.UserType == "ShopAdmin")
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.login);
                        snd.Play();
                        NavigationService navService = NavigationService.GetNavigationService(this);    //navigate to first page
                        Home nextPage = new Home();
                        navService.Navigate(nextPage);
                    }
                    else if (LoginDetail.user.UserType == "Employee")
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.login);
                        snd.Play();
                        NavigationService navService = NavigationService.GetNavigationService(this);
                        HomeAdmin nextPage = new HomeAdmin();
                        navService.Navigate(nextPage);
                    }
                    else if (LoginDetail.user.UserType == "ShopManeger")
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.login);
                        snd.Play();
                        NavigationService navService = NavigationService.GetNavigationService(this);
                        HomeAdmin nextPage = new HomeAdmin();
                        navService.Navigate(nextPage);
                    }                    
                }               
                else
                {
                    tbpassword.Password = "";
                    // MessageBox.Show("Invalid Username or Password");
                    lbloginresponse.Visibility = Visibility.Visible;
                    settings.Password = "";
                    settings.Save();
                }
            }
            else if(bl.internetconnection()==true)
            {
                 tbpassword.Password = "";
                    // MessageBox.Show("Invalid Username or Password");
                    lbloginresponse.Visibility = Visibility.Visible;
                    settings.Password = "";
                    settings.Save();
            }
            else
            {
                //  MessageBox.Show("Poor Connectivity");
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
        }       

        private void Button_Click_1(object sender, RoutedEventArgs e)           //Login Button
        {
            Loginfunction();
        }
        //private void ForgotPassword_Clicked(object sender, RoutedEventArgs e)
        //{
        //    int forgotpswrd = 0;
        //    var dialog = new demo(forgotpswrd);
        //    dialog.ShowDialog(); 
        //}

        //private void Newtoapp_Clicked(object sender, RoutedEventArgs e)
        //{
        //    int forgotpswrd = 1;
        //    var dialog = new demo(forgotpswrd);
        //    dialog.ShowDialog();
        //}

      

        private void Label_MouseEnter_1(object sender, MouseEventArgs e)
        {
          //  this.colorPicker.SelectedColor = new SolidColorBrush(Colors.Black);
            lbforgotpassword.Foreground = new SolidColorBrush(Colors.Red);

        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)         //forgotpassword
        {
            //int forgotpswrd = 0;
            //var dialog = new demo(forgotpswrd);
            //dialog.ShowDialog();
            System.Diagnostics.Process.Start("https://www.realtnetworking.com/ForgotPassword.aspx");
        }

        private void lbfrgtpswrdmouseenter(object sender, MouseEventArgs e)
        {
            lbforgotpassword.Foreground = new SolidColorBrush(settings.Font);
          //  textblockforgotpasswrd.TextDecorations = TextDecoration.Underline;
           // lbforgotpassword.TextDecorations = TextDecorations.Underline;
           
        }

        private void lbforgotpaswrdMouse_leave(object sender, MouseEventArgs e)
        {
            lbforgotpassword.Foreground = new SolidColorBrush(Colors.White);
           // lbforgotpassword.TextDecorations = null;
        }

        private void Newtoapp_Clicked(object sender, MouseButtonEventArgs e)        //new to app
        {
            //int forgotpswrd = 1;
            //var dialog = new demo(forgotpswrd);
            //dialog.ShowDialog();
            System.Diagnostics.Process.Start("http://youtube.com/embed/_t-aDMxs0EQ?rel=0");
        }

        private void lbnewtoappenter(object sender, MouseEventArgs e)
        {
            lbnewtoapp.Foreground = new SolidColorBrush(settings.Font);
        }

        private void lbnewtoappleave(object sender, MouseEventArgs e)
        {
            lbnewtoapp.Foreground = new SolidColorBrush(Colors.White);
        }

    }
}
