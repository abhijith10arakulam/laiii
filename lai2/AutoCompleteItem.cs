﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lai2
{
   public class AutoCompleteItem
      
    {
        private string name;
        private int id;

        public AutoCompleteItem()
        {

        }

        public AutoCompleteItem(string name, int id)
        {
            Name = name;
            ID = id;
        }

        public string Name { get; set; }
        public int ID { get; set; }
    }
}

