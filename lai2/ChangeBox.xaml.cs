﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ChangeBox.xaml
    /// </summary>
    public partial class ChangeBox : Window
    {
        Blogic bl = new Blogic();
        private Settings settings = Properties.Settings.Default;
        int flag = 0;
        string value;
        string tcktid;
        string packno;
        string ticketname;
        ResultWithOldTicket rs = new ResultWithOldTicket();
        public ChangeBox()
        {
            InitializeComponent();
            HideLabel();
            color();
            //           cbBox.SelectedItem = 1;
            //cbchangebox.SelectedItem = 1;

            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            var am = DateTime.Now.ToString("tt");

            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;

        }

        public List<int> ResponseData
        {
            get
            {
                List<int> boxes = new List<int>();
                boxes.Add(Convert.ToInt32(cbBox.SelectedItem));
                boxes.Add(Convert.ToInt32(cbchangebox.SelectedItem));
                return boxes;
            }
            set
            {
                List<int> boxes = value;
                for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
                {
                    cbBox.Items.Add(i);
                    cbchangebox.Items.Add(i);
                }
                cbBox.SelectedItem = boxes[0];
                cbchangebox.SelectedItem = boxes[1];

            }
        }
        public void HideLabel()
        {
            lbvalpackno.Visibility = Visibility.Hidden;
            lbvalpackpositionclose.Visibility = Visibility.Hidden;
            lbvalpackpositionopen.Visibility = Visibility.Hidden;
            lbvaltcktid.Visibility = Visibility.Hidden;
            lbvaltcktname.Visibility = Visibility.Hidden;
        }
        public void color()
        {
            btClose.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            cbchangebox.Background = new SolidColorBrush(settings.Tab);
            comboboxhour.Background = new SolidColorBrush(settings.Tab);
            comboboxminute.Background = new SolidColorBrush(settings.Tab);
            comboboxam.Background = new SolidColorBrush(settings.Tab);
            cbBox.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            cbBox.Foreground = new SolidColorBrush(settings.Font);
            cbchangebox.Foreground = new SolidColorBrush(settings.Font);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            comboboxhour.Foreground = new SolidColorBrush(settings.Font);
            comboboxminute.Foreground = new SolidColorBrush(settings.Font);
            comboboxam.Foreground = new SolidColorBrush(settings.Font);
            btClose.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            tbTicketId.Foreground = new SolidColorBrush(settings.Font);
            tbPackno.Foreground = new SolidColorBrush(settings.Font);
            tbTicketName.Foreground = new SolidColorBrush(settings.Font);
            tbPackPositionopen.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            HideLabel();
            if (tbPackno.Content == "")
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPosition.Text == "")
            {
                lbvalpackpositionclose.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPositionopen.Content == "")
            {
                lbvalpackpositionopen.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketId.Content == "")
            {
                lbvaltcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketName.Content == "")
            {
                lbvaltcktname.Visibility = Visibility.Visible;
                flag = 1;
            }

            //if (TryToParse(tbTicketId.Content) == false)
            //{
            //    lbvaltcktid.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (TryToParse(tbPackno.Text) == false)
            //{
            //    lbvalpackno.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            if (TryToParse(tbPackPosition.Text) == false)
            {

                lbvalpackpositionclose.Visibility = Visibility.Visible;
                flag = 1;
            }
            //else if (Convert.ToInt32(tbPackPosition.Text) < Convert.ToInt32(tbPackPositionopen.Content))
            //{

            //    flag = 1;
            //    lbdescription.Content = "Please Check Pack Position";
            //    lbvalpackpositionclose.Visibility = Visibility.Visible;

            //}
            //if (TryToParse(tbPackPositionopen.Text) == false)
            //{
            //    lbvalpackpositionopen.Visibility = Visibility.Visible;
            //    flag = 1;
            //}

            else if (flag == 0)
            {
                ResultWithOldTicket r = new ResultWithOldTicket();
                string packpositionclose = tbPackPosition.Text;
                string changeBox = cbchangebox.SelectedItem.ToString();
                string s = DateTime.Now.ToString("yyyy/MM/dd hh:mm tt");
                string heading = "Change Box in Progress";
                string frombox = cbBox.SelectedItem.ToString();

                r = bl.CheckStatus_ChangeBox(tcktid, packno, packpositionclose, changeBox, s);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading1);
                    dialog.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        string heading1 = "Changed box successfully!";
                        var dialog = new DialogBox_BoxNo(heading1);
                        dialog.ShowDialog();
                        this.DialogResult = true;
                        tbTicketId.Content = "";
                        tbTicketName.Content = "";
                        tbPackno.Content = "";
                        tbPackPosition.Text = "";
                        tbPackPositionopen.Content = "";
                        lbdescription.Content = r.Description;
                    }
                    else if (r.Description == "Another Ticket is Activated in this Box")
                    {
                        var dialog1 = new OldTicketInfo(r);
                        if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
                        {
                            r = bl.CheckStatus_ChangeBox(tcktid, packno, packpositionclose, changeBox, s);
                            if (r.Status == true)
                            {

                                string heading1 = "Changed box successfully!";
                                var dialog = new DialogBox_BoxNo(heading1);
                                dialog.ShowDialog();

                                this.DialogResult = true;
                                // btSubmit.Click += new RoutedEventHandler(btSubmit_Click);
                            }
                        }
                    }
                    else
                    {
                        lbdescription.Content = r.Description;
                    }
                }
            }

        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
            }
            else
            {
                return result;
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.15;
        }

        private void BoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            string box = Convert.ToString(cbBox.SelectedItem);
            rs = bl.AutoFill_ChangeBox(box);
            if (rs == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rs.Status == true)
                {
                    //  lbdescription.Content = rs.Description;
                    tbTicketId.Content = Convert.ToString(rs.OldTicketInfo.TicketId);
                    tbPackno.Content = Convert.ToString(rs.OldTicketInfo.PackNo);
                    tbTicketName.Content = Convert.ToString(rs.OldTicketInfo.TicketName);
                    value = Convert.ToString(rs.OldTicketInfo.Value.ToString("C"));
                    tcktid = Convert.ToString(rs.OldTicketInfo.TicketId);
                    packno = Convert.ToString(rs.OldTicketInfo.PackNo);
                    ticketname = Convert.ToString(rs.OldTicketInfo.TicketName);
                    tbPackPositionopen.Content = Convert.ToString(rs.OldTicketInfo.OpenNo);
                    if (rs.OldTicketInfo.CloseNo == 0)
                    {
                        if (settings.TicketActivationOrder == 0 || Blogic.IsAtvm)
                            tbPackPosition.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                        else
                            tbPackPosition.Text = Convert.ToString(rs.OldTicketInfo.OpenNo - 1);
                    }
                    else
                    {
                        if (settings.TicketActivationOrder == 0 || Blogic.IsAtvm)
                            tbPackPosition.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                        else
                            tbPackPosition.Text = Convert.ToString(rs.OldTicketInfo.CloseNo - 1);
                    }
                }
                else
                {
                    //  lbdescription.Content = rs.Description;
                    tbTicketId.Content = "";
                    tbPackno.Content = "";
                    tbTicketName.Content = "";
                    tbPackPositionopen.Content = "";
                    tbPackPosition.Text = "";
                }
            }
        }
    }
}
