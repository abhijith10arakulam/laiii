﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Message.xaml
    /// </summary>
    public partial class Message : Window
    {
        private Settings settings = Properties.Settings.Default;

        public Message( string discription, string date)
        {
            InitializeComponent();
            tbDiscription.Foreground = new SolidColorBrush(settings.Theme);
            tbDate.Foreground = new SolidColorBrush(settings.Theme);
            btOk.Background = new SolidColorBrush(settings.Tab);

            btOk.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            tbDiscription.Text = discription;
            tbDate.Text = date;

           }

        private void btContinue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true;
            }
            catch {
                DialogResult = false;
            }
        }
    }
}
