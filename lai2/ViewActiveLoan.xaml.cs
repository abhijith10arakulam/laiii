﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewActiveTickets.xaml
    /// </summary>
    public partial class ViewActiveLoan : Page
    {
        System.ComponentModel.BackgroundWorker mWorker; 
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        List<Loans> st = new List<Loans>();
        public ViewActiveLoan()
        {
            InitializeComponent();

            lbstartdate.Content= Blogic.opendate;
            txtlegal.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin.Content = LoginDetail.user.UserName.ToString();

            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin.Foreground = new SolidColorBrush(settings.Font);
            dgLoan.FontFamily = new FontFamily(settings.datagridfont);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            dgLoan.Foreground = new SolidColorBrush(settings.ActiveC);
            dgLoan.BorderBrush = new SolidColorBrush(settings.Border);
            this.Background = new SolidColorBrush(settings.Background);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);

            lblcntnme.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            lblblnme.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            lbopendatee.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            lbusrlgn.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin.FontFamily = new FontFamily(settings.labelfont);
            loadingtelerick.Foreground = new SolidColorBrush(settings.Font);

            tbEmptyBoxes.Foreground = new SolidColorBrush(settings.Font);
            tbEmptyBoxes.Text = bl.EmptyBox_Report().Replace("\"","");
           
            loading();     


        }
       
        public void loading()
        {        
            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
            new System.Threading.ThreadStart(delegate { }));        

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            try
            {             
                if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loadingtelerick.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }            
            catch { }
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (st == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                st = bl.Loans_view();
               // dgLoan.ItemsSource = st;
            }
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
           
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {            
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            st = bl.Loans_view();
            dgLoan.ItemsSource = st;
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }          
    }
}
