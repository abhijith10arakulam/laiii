﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace lai2
{
    class ColorGridBk : IValueConverter
    {
        private Settings settings = Properties.Settings.Default;
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //  return new SolidColorBrush(settings.Font);
            try
            {
                string  insval = (string)value;
                string[] data = insval.Split('/');
               
               if(    settings. HomeColorForground==1)
               {
                   double val  =System .Convert .ToDouble ( data[1]);
                    if (settings.HC1_Value >= val)
                        return new SolidColorBrush(settings.HC1_Color);
                    else if (settings.HC2_Value >= val)
                        return new SolidColorBrush(settings.HC2_Color);
                    else if (settings.HC3_Value >= val)
                        return new SolidColorBrush(settings.HC3_Color);
                    else
                        return new SolidColorBrush(Colors.Coral);

                           }
               else
               {
                   double val = System.Convert.ToDouble(data[0]);
                   if (val == -51)
                   {
                       return new SolidColorBrush(settings.Complite);
                   }
                   else if (val == -52)
                   {
                       return new SolidColorBrush(settings.InactiveC);
                   }
                   else if (val == -53)
                   {
                       return new SolidColorBrush(settings.ReturnC);
                   }

                   else if (val <= 1)
                   {
                       return new SolidColorBrush(settings.ActiveC);
                   }
                   else if (val <= 7)
                   {
                       return new SolidColorBrush(settings.Day1_7);
                   }
                   else if (val <= 14)
                   {
                       return new SolidColorBrush(settings.Day8_14);
                   }
                   else if (val <= 21)
                   {
                       return new SolidColorBrush(settings.Day15_21);
                   }
                   else
                   return new SolidColorBrush(settings.Day22_above);

               
               }
            }

            catch
            { return new SolidColorBrush(Colors.Coral); }
        }
      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
