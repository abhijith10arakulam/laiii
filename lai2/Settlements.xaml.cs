﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Settlements.xaml
    /// </summary>
    public partial class Settlements : Window
    {
        string fromdate;
        string todate;
        Blogic bl = new Blogic();
        public Settlements()
        {
            InitializeComponent();
            fromdatePicker1.SelectedDate =DateTime.Now.AddDays(-7);
            datepickertodate.SelectedDate = DateTime.Now;
           // this.Background = new SolidColorBrush(settings.Background);
        }

        private void datePicker1_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            fromdate = Convert.ToDateTime(fromdatePicker1.SelectedDate).ToString("yyyy/MM/dd");
            todate = Convert.ToDateTime(datepickertodate.SelectedDate).ToString("yyyy/MM/dd");
            ResultWithSettlementTicket rt = new ResultWithSettlementTicket();
            rt = bl.Settlementby_date(fromdate,todate);
            if (rt == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rt.Status == true)
                {
                    dgList.ItemsSource = rt.SettlementTicket;
                }
            }

        }

        private void btWeekSett_Click_1(object sender, RoutedEventArgs e)
        {
            ResultWithSettlementTicket rs = new ResultWithSettlementTicket();
            string date;
            date = DateTime.Now.ToString();
            rs = bl.Settlementby_week(date);
            if (rs == null && bl.internetconnection() == false)
            {
                if (rs.Status == true)
                {
                    dgList.ItemsSource = rs.SettlementTicket;
                }
            }
        }

        private void SettlementbyWeek_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SettlementbyTicket();
            dialog.ShowDialog();
        }
    }
}
