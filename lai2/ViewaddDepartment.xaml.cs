﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewaddEmployee.xaml
    /// </summary>
    public partial class ViewaddDepartment : Window
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        public ViewaddDepartment()
        {
            InitializeComponent();
            color();
            List<Departments> u = bl.viewAddDepartment();
            if (u == null && bl.internetconnection() == false)
            {
                string heading1 = "        Poor Connectivity";
                var dialog1 = new DialogBox_BoxNo(heading1);
                dialog1.ShowDialog();     
            }
            else
            {
                gdDepartment.ItemsSource = u;
            }           
        }
        public void color()
        {
            btnadduser.Background = new SolidColorBrush(settings.Tab);
            btndelete.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            btnadduser.Foreground = new SolidColorBrush(settings.Font);
            btndelete.Foreground = new SolidColorBrush(settings.Font);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            gdDepartment.Foreground = new SolidColorBrush(settings.Font);
            gdDepartment.FontFamily = new FontFamily(settings.datagridfont);
            gdDepartment.BorderBrush = new SolidColorBrush(settings.Border);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AddDepartment();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                List<Departments> u = bl.viewAddDepartment();
                if (u == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();  
                }
                else
                gdDepartment.ItemsSource = u;
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        } 

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (gdDepartment.SelectedItem != null)
            {
                Departments u1 = (Departments)gdDepartment.SelectedItem;
                string heading = "Delete Department";
                string username = u1.Dept.ToString();
                username = "Do you want to delete" + "  " + username +"?";
                var dialog = new DeleteWindow(heading, username);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    string deleteDeptId = Convert.ToString(u1.DeptId);
                    Result r = bl.DeleteAddedDepartment(deleteDeptId);
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            List<Departments> u = bl.viewAddDepartment();
                            if (u == null && bl.internetconnection() == false)
                            {
                                string heading1 = "        Poor Connectivity";
                                var dialog1 = new DialogBox_BoxNo(heading1);
                                dialog1.ShowDialog();
                            }
                            else
                                gdDepartment.ItemsSource = u;
                        }
                    }
                }
            }
                else
                {
                    //  MessageBox.Show("Select One");
                    string heading = "Select One Deptartment";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
        }
    }

