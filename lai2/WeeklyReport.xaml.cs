﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Net;
using System.IO;
using System.Diagnostics;
//using Telerik.Windows.Controls.ChartView;

namespace lai2
{
    /// <summary>
    /// Interaction logic for WeeklyReport.xaml
    /// </summary>
    public partial class WeeklyReport : Page
    {
        System.ComponentModel.BackgroundWorker mWorker;
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        string fromdate;
        string todate;
        private int id;
        List<Report> r = new List<Report>();
        public WeeklyReport()
        {
            InitializeComponent();
            fromdatepicker1.SelectedDate = DateTime.Now.AddDays(-7);
            todatePicker.DisplayDateEnd = DateTime.Now;
            btnGraph.Background = new SolidColorBrush(settings.Tab);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            lbfromdate.Foreground = new SolidColorBrush(settings.Font);
            lbtodate.Foreground = new SolidColorBrush(settings.Font);
            btnGraph.Foreground = new SolidColorBrush(settings.Font);
            dgWeeklyReport.Foreground = new SolidColorBrush(settings.Font);
            tab1.Background = new SolidColorBrush(settings.Tab);
            tab2.Background = new SolidColorBrush(settings.Tab);
            tab3.Background = new SolidColorBrush(settings.Tab);
            tab4.Background = new SolidColorBrush(settings.Tab);
            tab5.Background = new SolidColorBrush(settings.Tab);
            tab6.Background = new SolidColorBrush(settings.Tab);

            tab1.Foreground = new SolidColorBrush(settings.Font);
            tab2.Foreground = new SolidColorBrush(settings.Font);
            tab3.Foreground = new SolidColorBrush(settings.Font);
            tab4.Foreground = new SolidColorBrush(settings.Font);
            tab5.Foreground = new SolidColorBrush(settings.Font);
            tab6.Foreground = new SolidColorBrush(settings.Font);
            dgWeeklyReport.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            dgWeeklyReport.FontFamily = new FontFamily(settings.datagridfont);
            lbfromdate.FontFamily = new FontFamily(settings.labelfont);
            lbtodate.FontFamily = new FontFamily(settings.labelfont);
            tabcntrl.BorderBrush = new SolidColorBrush(settings.Border);
            btnpriintreport.Foreground = new SolidColorBrush(settings.Font);
            btnpriintreport.Background = new SolidColorBrush(settings.Tab);
            todatePicker.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(datepicker1_selectionChanged);
            fromdatepicker1.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(datepicker1_selectionChanged);
            todatePicker.SelectedDate = DateTime.Now;

        }

        private void dgWeeklyReport_LoadingRow(object sender, DataGridRowEventArgs e)
        {

        }

        public void loading()
        {
            System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
               new System.Threading.ThreadStart(delegate { }));
            //  pbProcessing.Value = 0;

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();

            //Duration duration = new Duration(TimeSpan.FromSeconds(20));

            //DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
            //pbProcessing.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
            try
            {
                //while (pbProcessing.Value != 100) //(mWorker.IsBusy)
                {

                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            // pbProcessing.Value = (pbProcessing.Value + 0.05) % 100;
                            // Thread.Sleep(5);
                            //pbProcessing.Value = (pbProcessing.Value + 1);
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
            }
            catch { }
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            r = bl.WeeklyReportView(fromdate, todate);

        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            if (r == null && bl.internetconnection() == false)
            {
                string heading1 = "        Poor Connectivity";
                var dialog1 = new DialogBox_BoxNo(heading1);
                dialog1.ShowDialog();
            }
            else
            {
                dgWeeklyReport.ItemsSource = r;
                List<Report> r2 = new List<Report>();
                if (r.Count != 0)
                {
                    // r2 = new List<Report>();
                    // r2 = r;
                    // r2.Remove(r2[r2.Count - 1]);
                    for (int i = 0; i < r.Count - 1; i++)
                    {
                        r2.Add(r[i]);
                    }
                    c1.ItemsSource = r2;
                    c2.ItemsSource = r2;
                    c3.ItemsSource = r2;
                    c4.ItemsSource = r2;
                    c5.ItemsSource = r2;
                    c6.ItemsSource = r2;
                    c7.ItemsSource = r2;
                    c8.ItemsSource = r2;
                    c9.ItemsSource = r2;
                  //  c10.ItemsSource = r2;
                    c11.ItemsSource = r2;
                    c12.ItemsSource = r2;
                }
                //int index = dgWeeklyReport.Items.Count - 1;
                ////foreach (DataGridRow drw in dgWeeklyReport.Items)
                ////{
                ////    if (drw.GetIndex() == index)
                ////    {
                ////        drw.Foreground = new SolidColorBrush(settings.Theme);
                ////    }
                ////}
                //DataGridRow row = dgWeeklyReport.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
                //if (row!= null)
                //{
                //    row.Foreground = new SolidColorBrush(settings.Theme);
                //}


                string fromdate1 = Convert.ToDateTime(fromdatepicker1.SelectedDate).ToString("MM-dd-yyyy dddd");
                string todate1 = Convert.ToDateTime(todatePicker.SelectedDate).ToString("MM-dd-yyyy dddd");

                double totalsale = 0;
                double totallinstantsale = 0;

                double totalonlinecash = 0;
                double totalinstantcash = 0;

                double totalinstantprofit = 0;
                double totalonlineprofit = 0;

                double totalActualMoneyDrop = 0;
                double totalCalculatedMoneyDrop = 0;

                double shortOrOver = 0;
                double topupcancel = 0;

                double active = 0;
                double settilement = 0;

                for (int i = 0; i < r.Count - 1; i++)
                {
                    totalsale += r[i].OnlineSales;
                    totallinstantsale += r[i].InstantSales;

                    totalonlinecash += r[i].OnlineCashout;
                    totalinstantcash += r[i].InstantCashout;

                    totalinstantprofit += r[i].InstantProfit;
                    totalonlineprofit += r[i].OnlineProfit;

                    totalActualMoneyDrop += r[i].ActualMoneyDrop;
                    totalCalculatedMoneyDrop += r[i].CalculatedMoneyDrop;

                    shortOrOver += r[i].ShortOrOver;
                    topupcancel += r[i].TopUpCancel;
                    active += r[i].ActtiveNo;
                    settilement += r[i].SettlementNo;
                }

                ColumnChartSales.Title = "Instant Sale from " + fromdate1 + " to " + todate1 + " = " + "$" + totallinstantsale
                + Environment.NewLine +
               "Online Sale from " + fromdate1 + " to " + todate1 + " = " + "$" + totalsale;


                ColumnChartCashout.Title = "Instant Cashout from " + fromdate1 + " to " + todate1 + " = " + "$" + totalinstantcash
                 + Environment.NewLine +
                "Online Cashout from " + fromdate1 + " to " + todate1 + " = " + "$" + totalonlinecash;


                ColumnChartProfit.Title = "Instant Profit from " + fromdate1 + " to " + todate1 + " = " + "$" + totalinstantprofit
                 + Environment.NewLine +
                 "Online Profit from " + fromdate1 + " to " + todate1 + " = " + "$" + totalonlineprofit;

                ColumnChartCalculatedMoneyDrop_ActualMoneyDrop.Title = "Actual Money Drop from " + fromdate1 + " to " + todate1 + " = " + "$" + totalActualMoneyDrop
                 + Environment.NewLine +
                 "Calculated Money Drop from " + fromdate1 + " to " + todate1 + " = " + "$" + totalCalculatedMoneyDrop;

                ColumnChartTopUp_TopUpCancel.Title = "Short / Over" + fromdate1 + " to " + todate1 + " = " + "$" + shortOrOver
                 + Environment.NewLine;
                 //"Top Up cancel from " + fromdate1 + " to " + todate1 + " = " + "$" + topupcancel;

                ColumnActive_settilement.Title = "Active tickets " + fromdate1 + " to " + todate1 + " = " + active
                + Environment.NewLine +
                "Settilement " + fromdate1 + " to " + todate1 + " = " + settilement;
            }

        }

        int graphFlag = 0;
        private void btnGraph_Click(object sender, RoutedEventArgs e)
        {
            if (graphFlag == 0)
            {
                graphFlag = 1;
                spData.Visibility = Visibility.Hidden;
                spData1.Visibility = Visibility.Visible;
                btnGraph.Content = "Report";
            }
            else
            {

                graphFlag = 0;
                spData.Visibility = Visibility.Visible;
                spData1.Visibility = Visibility.Hidden;
                btnGraph.Content = "Graph";
            }
        }


        private void datepicker1_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (fromdate != Convert.ToDateTime(fromdatepicker1.SelectedDate).ToString("yyyy/MM/dd") || todate != Convert.ToDateTime(todatePicker.SelectedDate).ToString("yyyy/MM/dd")
                )
            {
                fromdate = Convert.ToDateTime(fromdatepicker1.SelectedDate).ToString("yyyy/MM/dd");
                todate = Convert.ToDateTime(todatePicker.SelectedDate).ToString("yyyy/MM/dd");
                loading();
            }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string st = String.Format("https://www.realtnetworking.com/API/LAI2/WeeklyReportPdf.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&FromDate=" + fromdate + "&ToDate=" + todate + "&Print=yes");
                var response = client.DownloadData(new Uri(String.Format("https://www.realtnetworking.com/API/LAI2/WeeklyReportPdf.aspx?UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&FromDate=" + fromdate + "&ToDate=" + todate + "&Print=yes")));
                //if (response == null && bl.internetconnection() == false)
                //{
                //    string heading1 = "        Poor Connectivity";
                //    var dialog1 = new DialogBox_BoxNo(heading1);
                //    dialog1.ShowDialog();
                //}
                //else
                //{
                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\Dailyreport.pdf", response);
                // }
            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\Dailyreport.pdf");
            Process.Start(sInfo);
        }

        


    }
}
