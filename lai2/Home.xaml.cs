﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Media;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        //Complete cp = new Complete();
        //UserDetails ud = new UserDetails();
        Shop s = new Shop();
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        int flag ;

        public Home()
        {
            InitializeComponent();

            flag=0;
            lbtitleshoplist.FontFamily = new FontFamily(settings.Fontstyle);

            bytlogout.Background = new SolidColorBrush(settings.Tab);
            lbtitleshoplist.Foreground = new SolidColorBrush(settings.Theme);
            bytlogout.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            LatestData.Foreground = new SolidColorBrush(settings.Font);
            lblogo.Foreground = new SolidColorBrush(settings.Font);
            LatestData.BorderBrush = new SolidColorBrush(settings.Border);
            LatestData.ItemsSource = LoginDetail.shops;
            LatestData.FontFamily = new FontFamily(settings.datagridfont);
            btAddExstingShop.Background = new SolidColorBrush(settings.Tab);
            
            btAddExstingShop.Foreground = new SolidColorBrush(settings.Font);

            btRemoveShop.Background = new SolidColorBrush(settings.Tab);

            btRemoveShop.Foreground = new SolidColorBrush(settings.Font);
            
        }

        private void LatestData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Blogic.closeShiftButtonStatus = false;
            if (flag == 0)
            {
                try
                {
                    IndexFinder.index = Convert.ToInt32(LatestData.SelectedIndex);
                    LatestData.Visibility = Visibility.Hidden;
                    NavigationService navService = NavigationService.GetNavigationService(this);
                    HomeAdmin nextPage = new HomeAdmin();
                    navService.Navigate(nextPage);
                }
                catch
                {
                    string heading = "Invalid Selection";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                    LatestData.UnselectAll();
                }
            }

            else
            {
                int index = -1; ;
                try
                {
                    IndexFinder.index = Convert.ToInt32(LatestData.SelectedIndex);
                }
                catch
                {
                    IndexFinder.index = -1;
                }

                if (IndexFinder.index != -1)
                {
                    string heading = "Do you want to Remove Shop " + LoginDetail.shops[IndexFinder.index].LegalName + "?";
                    var dialog = new DialogBox(heading);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        LoginList ll = new LoginList();
                        Result rs = bl.RemoveShop();
                        if (rs.Status == true)
                        {
                            IndexFinder.index = 0;
                            string username = settings.username;
                            string password = settings.Password;
                            try
                            {
                                ll = bl.LoginCheck(username, password);
                                NavigationService navService = NavigationService.GetNavigationService(this);    //navigate to first page
                                Home nextPage = new Home();
                                navService.Navigate(nextPage);
                            }
                            catch
                            {
                                NavigationService navService = NavigationService.GetNavigationService(this);    //navigate to first page
                                Login nextPage = new Login();
                                navService.Navigate(nextPage);

                            }
                        }
                        else
                        {

                            string heading1 = rs.Description;
                            var dialog1 = new DialogBox_BoxNo(heading1);
                            dialog1.ShowDialog();
                            LatestData.UnselectAll();
                        }

                    }

                }
            }
        }
        private void Logoutclicked(object sender, RoutedEventArgs e)
        {
            string heading = "Do you want to Log Out?";
            var dialog = new DialogBox(heading);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                SoundPlayer snd = new SoundPlayer(Properties.Resources.done);
                snd.Play();
                settings.Password = "";
                settings.Save();
                NavigationService navService = NavigationService.GetNavigationService(this);
                Login nextPage = new Login();
                navService.Navigate(nextPage);
            }           
        }

        private void btAddExstingShop_Click(object sender, RoutedEventArgs e)
        {
            LoginList ll = new LoginList();
           
            var dialog = new AddShop();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                IndexFinder.index = 0;
                string username = settings.username;
                string password = settings.Password;
                try
                {
                    ll = bl.LoginCheck(username, password);
                    NavigationService navService = NavigationService.GetNavigationService(this);    //navigate to first page
                    Home nextPage = new Home();
                    navService.Navigate(nextPage);
                }
                catch
                {
                    NavigationService navService = NavigationService.GetNavigationService(this);    //navigate to first page
                    Login nextPage = new Login();
                    navService.Navigate(nextPage);
                
                }

            }
        }

        private void btRemoveShop_Click(object sender, RoutedEventArgs e)
        {
            if (flag == 0)
            {

                flag = 1;
                btRemoveShop.Content = "Cancel";
                btRemoveShop.Background = new SolidColorBrush(settings.Day1_7);
            }
            else
            {
                flag = 0;
                btRemoveShop.Background = new SolidColorBrush(settings.Tab);
                btRemoveShop.Content = "Remove Shop";
            
            }
           
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!settings.IsPasswordChanged && Convert.ToDateTime( LoginDetail.shops[IndexFinder.index].Registerdate)< Convert.ToDateTime("12-1-2016"))
            //{
            //    DialogBox dialog = new DialogBox("Time To Change Your Password");
            //    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            //    {
            //        string title = "Change Password";
            //        string oldpswrd = "Old Password";
            //        string newpswrd = "New Password";
            //        string Confirmpswrd = "Confirm Password";
            //        var dialog1 = new AddBox(title, oldpswrd, newpswrd, Confirmpswrd);
            //        dialog1.ShowDialog();
            //    }
            //}
        }                       
            }
}
