﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewaddEmployee.xaml
    /// </summary>
    /// 
    public class FillCheque1
    {
        public string SlNo { get; set; }
        public string ChequeNo { get; set; }
        public string CompanyPerson { get; set; }
        public string Amount { get; set; }
    }
    public partial class AddCashInHand : Window
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        public AddCashInHand()
        {
            InitializeComponent();
            color();
            //List<User> u = bl.viewAddEmployee();
            //if (u == null && bl.internetconnection() == false)
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();     
            //}
            //else
            //{
            //    gdaddemployeeList.ItemsSource = u;
            //}

            List<FillCheque1> fillCheque = new List<FillCheque1>();

            fillCheque.Add(new FillCheque1() { SlNo = "1", ChequeNo = "4434", CompanyPerson = "Kris G",Amount="$5000" });
            fillCheque.Add(new FillCheque1() { SlNo = "2", ChequeNo = "9037", CompanyPerson = "Shell", Amount = "$300" });
            fillCheque.Add(new FillCheque1() { SlNo = "3", ChequeNo = "5436", CompanyPerson = "Agassi", Amount = "$2000" });
            gdaddemployeeList.ItemsSource = fillCheque;

        }
        public void color()
        {
            btnadduser.Background = new SolidColorBrush(settings.Tab);
            btndelete.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            btnadduser.Foreground = new SolidColorBrush(settings.Font);
            btndelete.Foreground = new SolidColorBrush(settings.Font);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            gdaddemployeeList.Foreground = new SolidColorBrush(settings.Font);
            gdaddemployeeList.FontFamily = new FontFamily(settings.datagridfont);
            gdaddemployeeList.BorderBrush = new SolidColorBrush(settings.Border);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            return;
            var dialog = new AddEmployee();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                List<User> u = bl.viewAddEmployee();
                if (u == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();  
                }
                else
                gdaddemployeeList.ItemsSource = u;
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        } 

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            return;
            if (gdaddemployeeList.SelectedItem != null)
            {
                User u1 = (User)gdaddemployeeList.SelectedItem;
                string heading = "Delete User";
                string username = u1.UserName.ToString();
                username = "Do you want to delete" + "  " + username +"?";
                var dialog = new DeleteWindow(heading, username);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    string deleteUserid = Convert.ToString(u1.UserId);
                    Result r = bl.DeleteAddedEmployees(deleteUserid);
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            List<User> u = bl.viewAddEmployee();
                            if (u == null && bl.internetconnection() == false)
                            {
                                string heading1 = "        Poor Connectivity";
                                var dialog1 = new DialogBox_BoxNo(heading1);
                                dialog1.ShowDialog();
                            }
                            else
                                gdaddemployeeList.ItemsSource = u;
                        }
                    }
                }
            }
                else
                {
                    //  MessageBox.Show("Select One");
                    string heading = "Select One User";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
        }
    }

