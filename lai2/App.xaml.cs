﻿
using Microsoft.Shell;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;

namespace lai2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application,ISingleInstanceApp
    {
        private static readonly Semaphore singleInstanceWatcher;
        private static readonly bool createdNew;

        static App()
        {


        }
        private const string Unique = "LAI2";

        [STAThread]
        public static void Main()
        {
            
            if (SingleInstance<App>.InitializeAsFirstInstance(Unique))
            {
                var application = new App();
                application.InitializeComponent();
              //  try
                {
                    application.Run();
                }
                //catch
                { }
                // Allow single instance code to perform cleanup operations
                SingleInstance<App>.Cleanup();
            }
        }

        #region ISingleInstanceApp Members
        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            // Handle command line arguments of second instance
            return true;
        }
        #endregion

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
