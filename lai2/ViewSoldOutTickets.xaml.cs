﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewSoldOutTickets.xaml
    /// </summary>
    public partial class ViewSoldOutTickets : Page
    {
        System.ComponentModel.BackgroundWorker mWorker; 
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        List<TicketsView> vt = new List<TicketsView>();
        string fromdate ;
        string todate ;
        public ViewSoldOutTickets()
        {
            InitializeComponent();

            lbstartdate.Content = Blogic.opendate;
            txtlegal.Content = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Content = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin.Text = LoginDetail.user.UserName.ToString();

            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin.Foreground = new SolidColorBrush(settings.Font);

            fromdatepicker.SelectedDate = DateTime.Now.AddDays(-7);
            todatepicker.SelectedDate = DateTime.Now;
            todatepicker.DisplayDateEnd = DateTime.Now;

            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            //lbfromdate.Foreground = new SolidColorBrush(settings.Font);
            //lbtodate.Foreground = new SolidColorBrush(settings.Font);
            dgSoldOut.Foreground = new SolidColorBrush(settings.Complite);
            this.Background = new SolidColorBrush(settings.Background);
            dgSoldOut.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            dgSoldOut.FontFamily = new FontFamily(settings.datagridfont);

            lblcntnme.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            lblegalnme.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            lbfromdate.FontFamily = new FontFamily(settings.labelfont);
            lbtodate.FontFamily = new FontFamily(settings.labelfont);
            lbopendatee.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            lbuserlogin.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin.FontFamily = new FontFamily(settings.labelfont);
            loadingtelerick.Foreground = new SolidColorBrush(settings.Font);

            tbEmptyBoxes.Foreground = new SolidColorBrush(settings.Font);
            tbEmptyBoxes.Text = bl.EmptyBox_Report().Replace("\"", "");
           
            loading();
        }

        public void loading()
        {
            try
            {
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                new System.Threading.ThreadStart(delegate { }));
                mWorker = new System.ComponentModel.BackgroundWorker();
                mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
                mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
                mWorker.WorkerReportsProgress = true;
                mWorker.WorkerSupportsCancellation = true;
                mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                mWorker.RunWorkerAsync();
                try
                {
                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loadingtelerick.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
                catch { }
            }
            catch { }
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                vt = bl.ViewSoldOut_Tickets(fromdate, todate);
            }
            catch { }
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            try
            {
                mWorker.CancelAsync();
                loadingtelerick.IsBusy = false;
                dgSoldOut.ItemsSource = vt;
            }
            catch { }
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }       

        private void ToDate_Changed(object sender, SelectionChangedEventArgs e)
        {
            if (fromdatepicker.SelectedDate != null && todatepicker.SelectedDate != null)
            {
                fromdate = Convert.ToDateTime(fromdatepicker.SelectedDate).ToString("yyyy/MM/dd");
                todate = Convert.ToDateTime(todatepicker.SelectedDate).ToString("yyyy/MM/dd");
                loading();
            }            
        }

        private void datagrid_loadingRow(object sender, DataGridRowEventArgs e)
        {
            
        }
    }
}
