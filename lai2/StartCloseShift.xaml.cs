﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for StartCloseShift.xaml
    /// </summary>
    public partial class StartCloseShift : Window
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        ReportDataList rl = new ReportDataList();
        public StartCloseShift()
        {
            InitializeComponent();
            string date = DateTime.Now.ToString("MM-dd-yyyy hh:mm:sstt");
            lbshiftClose.Content = date;
            string date1 = DateTime.Now.ToString("yyyy-MM-dd");
            string time = DateTime.Now.ToString("hh:mm:sstt");
            //rl = bl.TodaysData(date1, time);
            //if (rl == null && bl.internetconnection() == false)
            //{
            //    string heading = "        Poor Connectivity";
            //    var dialog = new DialogBox_BoxNo(heading);
            //    dialog.ShowDialog();
            //}
            //else
            {
                lbshiftopen.Content = Blogic.opendate;//rl.StartDate;
                btback.Background = new SolidColorBrush(settings.Tab);
                btCloseShift.Background = new SolidColorBrush(settings.Tab);
                lbshiftopen.Foreground = new SolidColorBrush(settings.Font);
                lbshiftClose.Foreground = new SolidColorBrush(settings.Font);
                btback.Foreground = new SolidColorBrush(settings.Font);
                btCloseShift.Foreground = new SolidColorBrush(settings.Font);
                lbtitle.Foreground = new SolidColorBrush(settings.Theme);
                if (settings.Background.ToString() != "#00FFFFFF")
                {
                    this.Background = new SolidColorBrush(settings.Background);
                }
                lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
                lbshiftopen.FontFamily = new FontFamily(settings.labelfont);
                lbshiftClose.FontFamily = new FontFamily(settings.labelfont);
                btReset.Background = new SolidColorBrush(settings.Tab);
                btReset.Foreground = new SolidColorBrush(settings.Font);
            }


            if(bl.CheckScan())
            {
                btReset.Visibility = Visibility.Visible;

            }
            else
            {
                btReset.Visibility = Visibility.Hidden;
            }
        }       

        private void btback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private void btCloseShift_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            //var dialog = new CloseShiftInProgress();
            //if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            //{
            //    this.DialogResult = true;
            //}
                   
        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
            }
            catch { }
        }

        private void btReset_Click(object sender, RoutedEventArgs e)
        {

            bl.ResetSkipping();
            this.DialogResult = true;
        }
    }
}
