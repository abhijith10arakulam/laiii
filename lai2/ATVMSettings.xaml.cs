﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ATVMSettings.xaml
    /// </summary>
    public partial class ATVMSettings : Window
    {
        private Settings settings = Properties.Settings.Default;

        int flag;
        public ATVMSettings()
        {
            InitializeComponent();
            InitializeComponent();
            rdbtnEnable.FontFamily = new FontFamily(settings.labelfont);
            rdbtnDisable.FontFamily = new FontFamily(settings.labelfont);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            rdbtnEnable.Foreground = new SolidColorBrush(settings.Font);
            rdbtnDisable.Foreground = new SolidColorBrush(settings.Font);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);


            CheckStatus();
        }
        private void CheckStatus()
        {
            Blogic bl = new Blogic();
            int status =LoginDetail.shops[IndexFinder.index].HasAtvm;

            if (status == 1)
            {
                rdbtnEnable.IsChecked = true;        //Enabled
            }
            else
            {
                rdbtnDisable.IsChecked = true;
            }
        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void rdbtnEnable_checked(object sender, RoutedEventArgs e)
        {
            flag = 1;    //Enabled
        }

        private void rdbtnDisable_checked(object sender, RoutedEventArgs e)
        {
            //  settings.Ticketvalue = 1;
            flag = 0;       //Disabled
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            LoginDetail.shops[IndexFinder.index].HasAtvm = flag;
            Blogic bl = new Blogic();
            Result isAffected = bl.UpdateStatus_ATVMSettings( flag);
            if (isAffected.Status == true)
            {
                this.DialogResult = true;
            }
        }
    }
}
