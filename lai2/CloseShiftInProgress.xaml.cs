﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Media;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace lai2
{
    /// <summary>
    /// Interaction logic for CloseShiftInProgress.xaml
    /// </summary>

    public partial class CloseShiftInProgress : Window
    {

        System.ComponentModel.BackgroundWorker mWorker;
        public static string credit, debit, topup, topupcancel;

        string date;
        string time;
        int returnFlag = 1;
        private int UIvalue;
        string barcode = "";
        StringBuilder mScanData = new StringBuilder();
        KeyConverter mScanKeyConverter = new KeyConverter();
        //  private string p;
        //  private Result res;
        int check = 0;
        int temp = 0;
        int ScanOrder;
        Blogic bl = new Blogic();
        int flag = 0;
        string closedate;
        ResultWithBox rb = new ResultWithBox();
        private Settings settings = Properties.Settings.Default;
        string onlinesale;
        string onlineCashout;
        string Cashout;
        string active;
        string inventory;
        string total;
        string cashinhand;
        string boxtempscan;
        string loan;
        string issued;

        public CloseShiftInProgress()
        {


            InitializeComponent();
            loadingtelerick.IsBusy = false;
            color();
            hiddenvalidation();
            rb = bl.ViewSkippedBox();
            if (rb == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                credit = "0";
                debit = "0";
                topup = "0";
                topupcancel = "0";

                //if (rb.Status == false)
                //{

                //    rb = bl.ViewSkippedBox();
                //    if (rb.BoxList.Count > 0)
                //    {
                //        boxfunctionality();
                //    }
                //    else
                //    {
                //        btNext.Visibility = Visibility.Hidden;
                //        lbs3.Visibility = Visibility.Visible;
                //        lbpleaseenter.Content = "Please hit";
                //        lbnextticket.Content = "Close Shift";
                //        lbs3.Content = "to proceed";

                //        lbs2_Copy.Visibility = Visibility.Visible;

                //        lbs1.Content = "Please hit";
                //        lbs2.Content = "Close Shift";
                //        lbs2_Copy.Content = "to proceed";
                //        //   btncloseshft.Focus();
                //     //   btFinishScan.Focus();
                //    }
                //}
                //if (rb.Status == true)
                //{
                //    boxtempscan = rb.BoxList[temp].ToString();
                //    lbnextScan.Content = rb.BoxList[temp].ToString();
                //    // lbboxmanual.Content = rb.BoxList[temp].ToString();
                //    settings.box = rb.BoxList[temp];
                //    settings.Save();
                //}
                //date = DateTime.Now.ToString("yyyy/MM/dd");
                //time = DateTime.Now.ToString("hh:mm:sstt");
                //shiftdatepicker.DisplayDateStart = Convert.ToDateTime(Blogic.opendate);
                //if (LoginDetail.shops[IndexFinder.index].ShopTime == 0)
                //    shiftdatepicker.SelectedDate = DateTime.Now;
                //else
                //    shiftdatepicker.SelectedDate = Convert.ToDateTime(Blogic.opendate);

                //this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
                //tbcloseno.KeyDown += new KeyEventHandler(tbcloseno_keydown);
                //for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
                //{
                //    cbBox.Items.Add(i);
                //}
            }
        }

        private void tbcloseno_keydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string ticketid = tbtcktid.Text;
                string packno = tbpackno.Text;
                string packposition = tbcloseno.Text;
                if (Trytoparse(packposition) == true && packposition != string.Empty)
                {
                    ResultWithOldTicket r = bl.ManualCloseShift(ticketid, packno, packposition);
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            //   btnsubmitcloseshift.Visibility = Visibility.Hidden;
                            lbboxmanual.Content = "";
                            tbtcktid.Text = "";
                            tbtcktname.Text = "";
                            tbpackno.Text = "";
                            tbopenno.Text = "";
                            tbcloseno.Text = "";
                            tbvalue.Text = "";
                            //tbboxval.Text = "";
                            cbBox.Text = "";
                            rb.BoxList.Remove(r.OldTicketInfo.Box);
                            if (rb.BoxList.Count > 0)
                            {
                                boxfunctionality();
                            }
                            else
                            {

                                rb = bl.ViewSkippedBox();
                                if (rb.BoxList.Count > 0)
                                {
                                    boxfunctionality();
                                }
                                else
                                {
                                    btnsubmitcloseshift.Visibility = Visibility.Hidden;
                                    btnnextmanual.Visibility = Visibility.Hidden;
                                    lbs3.Visibility = Visibility.Visible;
                                    lbpleaseenter.Content = "Please hit";
                                    lbnextticket.Content = "Close Shift";
                                    lbs3.Content = "to proceed";

                                    lbs2_Copy.Visibility = Visibility.Visible;
                                    lbs1.Content = "Please hit";
                                    lbs2.Content = "Close Shift";
                                    lbs2_Copy.Content = "to proceed";
                                    lbnextScan.Content = "";
                                    btNext.Visibility = Visibility.Hidden;
                                 //   btFinishScan.Focus();
                                }
                                // lbnextScan.Content = rb.BoxList[temp].ToString();
                            }
                            //tbtcktid.Text = "";
                            //tbtcktname.Text = "";
                            //tbpackno.Text = "";
                            //tbopenno.Text = "";
                            //tbcloseno.Text = "";
                            //tbvalue.Text = "";
                        }
                        else
                        {
                            string heading = r.Description;
                            int closeshiftstatus = 1;
                            var dialog = new DialogBox_BoxNo(heading, closeshiftstatus);
                            dialog.ShowDialog();
                        }
                    }
                }
                else
                {
                    lbvalcloseno.Visibility = Visibility.Visible;
                }
            }
        }
        public void color()
        {
            btFinishScan.Background = new SolidColorBrush(settings.Tab);
            btNext.Background = new SolidColorBrush(settings.Tab);
            btGoback.Background = new SolidColorBrush(settings.Tab);
            btFinishShift.Background = new SolidColorBrush(settings.Tab);
            btback.Background = new SolidColorBrush(settings.Tab);
            btok.Background = new SolidColorBrush(settings.Tab);
            lbs1.Foreground = new SolidColorBrush(Colors.White);
            lbs2.Foreground = new SolidColorBrush(Colors.White);
            lbnextScan.Foreground = new SolidColorBrush(settings.Font);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            lbpreticket.Foreground = new SolidColorBrush(settings.Font);
            btFinishScan.Foreground = new SolidColorBrush(settings.Font);
            btNext.Foreground = new SolidColorBrush(settings.Font);
            btGoback.Foreground = new SolidColorBrush(settings.Font);
            btFinishShift.Foreground = new SolidColorBrush(settings.Font);
            checkboxadvancedoption.Foreground = new SolidColorBrush(settings.Font);
            lbadvanced.Foreground = new SolidColorBrush(settings.Font);
            btback.Foreground = new SolidColorBrush(settings.Font);
            btok.Foreground = new SolidColorBrush(settings.Font);
            btmanualcloseshift.Foreground = new SolidColorBrush(settings.Font);
            btmanualcloseshift.Background = new SolidColorBrush(settings.Tab);
            btncloseshft.Foreground = new SolidColorBrush(settings.Font);
            btncloseshft.Background = new SolidColorBrush(settings.Tab);
            btnnextmanual.Foreground = new SolidColorBrush(settings.Font);
            btnnextmanual.Background = new SolidColorBrush(settings.Tab);
            lbboxmanual.Foreground = new SolidColorBrush(settings.Font);
            btnsubmitcloseshift.Foreground = new SolidColorBrush(settings.Font);
            btnsubmitcloseshift.Background = new SolidColorBrush(settings.Tab);
            btnbackmanual.Foreground = new SolidColorBrush(settings.Font);
            btnbackmanual.Background = new SolidColorBrush(settings.Tab);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            //tbbox.Foreground = new SolidColorBrush(settings.Font);
            //tbtctname.Foreground = new SolidColorBrush(settings.Font);
            //tbtctvalue.Foreground = new SolidColorBrush(settings.Font);
            //tbtctopenno.Foreground = new SolidColorBrush(settings.Font);
            //tbtctclno.Foreground = new SolidColorBrush(settings.Font);
            //tbTotal.Foreground = new SolidColorBrush(settings.Font);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            lbpreticket.FontFamily = new FontFamily(settings.labelfont);
            lbadvanced.FontFamily = new FontFamily(settings.labelfont);
            checkboxadvancedoption.FontFamily = new FontFamily(settings.labelfont);
            lbdescription.FontFamily = new FontFamily(settings.labelfont);

            //lbpleaseenter.FontFamily = new FontFamily(settings.labelfont);
            //lbnextticket.FontFamily = new FontFamily(settings.labelfont);

            lbpleaseenter.Foreground = new SolidColorBrush(Colors.White);
            lbnextticket.Foreground = new SolidColorBrush(Colors.White);
            lbcurrenttcktdtls.Foreground = new SolidColorBrush(settings.Font);
            lbcurrenttcktdtls.FontFamily = new FontFamily(settings.labelfont);
            lbs2_Copy.Foreground = new SolidColorBrush(Colors.White);
            lbs3.Foreground = new SolidColorBrush(Colors.White);
            cbBox.Background = new SolidColorBrush(settings.Tab);
            cbBox.Foreground = new SolidColorBrush(settings.Font);
            lbBox.FontFamily = new FontFamily(settings.labelfont);
            lbBox.Foreground = new SolidColorBrush(settings.Font);
            btActivate.Foreground = new SolidColorBrush(settings.Font);
            btActivate.Background = new SolidColorBrush(settings.Tab);
            btSubmitBox.Foreground = new SolidColorBrush(settings.Font);
            btSubmitBox.Background = new SolidColorBrush(settings.Tab);

        }

        public void hiddenvalidation()
        {
            //lbvalactive.Visibility = Visibility.Hidden;
            //  lbvaladvancedoption.Visibility = Visibility.Hidden;
            //  lbvalcashinhand.Visibility = Visibility.Hidden;
            //  lbvaldatepicker.Visibility = Visibility.Hidden;
            lbvalinstantcashout.Visibility = Visibility.Hidden;
            //  lbvalinventory.Visibility = Visibility.Hidden;
            lbvalonlinecahout.Visibility = Visibility.Hidden;
            lbvalonlinesale.Visibility = Visibility.Hidden;
            //  lbvaltotal.Visibility = Visibility.Hidden;          
            lbvalactive.Visibility = Visibility.Hidden;
            lbvalCaashinhand.Visibility = Visibility.Hidden;
            lbvalinventory.Visibility = Visibility.Hidden;
            lbvaltotal.Visibility = Visibility.Hidden;
            lbvalcredit.Visibility = Visibility.Hidden;
            lbvaldebit.Visibility = Visibility.Hidden;
            lbvalTopup.Visibility = Visibility.Hidden;
            lbvalTopUpCancel.Visibility = Visibility.Hidden;
            lbvalissue.Visibility = Visibility.Hidden;
            lbvalloan.Visibility = Visibility.Hidden;

        }

        void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key.ToString() == "Return" && returnFlag == 1 & barcode.Length != 0)
                {
                    //bl = new BAL();
                    //returnFlag = 0;
                    if ((barcode.Length <= 24 && barcode.Length >= 22) || (barcode.Length == 20) || (barcode.Length == 18))
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.SCANVOICE);
                        snd.Play();
                        ScanData tt = bl.GetTicket(barcode);
                        setData(tt);
                        // btSubmit.Focus();
                    }

                    barcode = "";

                    if (UIvalue == 0)
                    {
                        // EnableTextBoxes();
                        //if (TbValidation())
                        //{

                        //    //this.DialogResult = true;
                        //}
                        //else
                        //{
                        //    btUISubmit.Visibility = Visibility.Visible;
                        //    btUISubmit.IsEnabled = true;
                        //}
                    }
                }
                // else 
                if (e.Key.ToString().StartsWith("D") && returnFlag == 1)
                {
                    barcode = barcode + e.Key.ToString().Replace("D", string.Empty);

                }
                //listBox1.Items.Add(e.Key.ToString().Replace("D",string.Empty));
            }
            catch { }
        }


        int LastScannedBox = 0;
        private void setData(ScanData tt)
        {
            try
            {
                lbdescription.Content = "";
                string ticketid = Convert.ToString(tt.TicketId);
                string packno = Convert.ToString(tt.PackNo);
                string packpositionopen = Convert.ToString(tt.PackPosition);
                ResultWithOldTicket rt = bl.CloseShiftAuto_fill(ticketid, packno, packpositionopen);
                if (rt == null && bl.internetconnection() == false)
                {
                    string heading = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    if (rt.Status == true)
                    {
                        LastScannedBox = rt.OldTicketInfo.Box;
                        settings.box = rt.OldTicketInfo.Box + 1;
                        if (settings.box >= LoginDetail.shops[IndexFinder.index].BoxCount)
                        {
                            settings.box = 1;

                        }
                        settings.Save();

                        tbbox.Text = Convert.ToString(rt.OldTicketInfo.Box);
                        tbtctname.Text = Convert.ToString(rt.OldTicketInfo.TicketName);
                        tbtctvalue.Text = Convert.ToString(rt.OldTicketInfo.Value.ToString("C"));
                        tbtctopenno.Text = Convert.ToString(rt.OldTicketInfo.OpenNo);
                        tbtctclno.Text = Convert.ToString(rt.OldTicketInfo.CloseNo);
                        tbTotal.Text = Convert.ToString(rt.OldTicketInfo.Total);
                        tbpacknoval.Text = Convert.ToString(rt.OldTicketInfo.PackNo);


                        if (rb.BoxList.Count > 0)
                        {
                            rb.BoxList.Remove(rt.OldTicketInfo.Box);

                            if (Convert.ToString(rt.OldTicketInfo.Box) == boxtempscan)
                            {
                                // temp++;
                                if (rb.BoxList.Count > temp)
                                {
                                    // if (Convert.ToString(rb.BoxList[temp]) != "")
                                    {
                                        lbnextScan.Content = rb.BoxList[temp].ToString();
                                        boxtempscan = rb.BoxList[temp].ToString();
                                    }
                                }
                                else if (rb.BoxList.Count != 0)
                                {
                                    temp = 0;
                                    lbnextScan.Content = rb.BoxList[temp].ToString();
                                }
                            }
                        }
                        if (rb.BoxList.Count == 0)
                        {

                            rb = bl.ViewSkippedBox();
                            if (rb.BoxList.Count == 0)
                            {
                                lbs2_Copy.Visibility = Visibility.Visible;
                                lbs1.Content = "Please hit";
                                lbs2.Content = "Close Shift";
                                lbs2_Copy.Content = "to proceed";
                                btNext.Visibility = Visibility.Hidden;
                                lbnextScan.Visibility = Visibility.Hidden;

                                btnnextmanual.Visibility = Visibility.Hidden;
                                lbs3.Visibility = Visibility.Visible;
                                lbpleaseenter.Content = "Please hit";
                                lbnextticket.Content = "Close Shift";
                                lbs3.Content = "to proceed";
                                lbboxmanual.Visibility = Visibility.Hidden;
                                btnsubmitcloseshift.Visibility = Visibility.Hidden;
                              //  btFinishScan.Focus();
                            }
                            else
                            {

                                boxfunctionality();
                            }
                        }

                    }
                    else if ((rt.Status == false) && ((rt.Description.Trim() == "Please Active This Ticket") || ((rt.Description.Trim() == "Update Inventory"))))
                    {

                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                        string tcktid = Convert.ToString(rt.OldTicketInfo.TicketId);
                        string pckno = Convert.ToString(rt.OldTicketInfo.PackNo);
                        string pckposopen = Convert.ToString(rt.OldTicketInfo.OpenNo);
                        if (LastScannedBox != 0)
                        {
                            settings.box = LastScannedBox + 1;
                            settings.Save();
                            if (settings.box >= LoginDetail.shops[IndexFinder.index].BoxCount)
                            {
                                settings.box = 1;
                                settings.Save();
                            }

                        }
                        else
                        {
                            if (rb.BoxList.Count > temp)
                            {
                                settings.box = rb.BoxList[temp];
                                settings.Save();
                            }
                        }
                        var dialog = new ActiveTickets(tcktid, pckno, pckposopen);
                        if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                        {
                            setData(tt);
                        }
                    }
                    else
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                        //  MessageBox.Show(rt.Description);
                        //  lbdescription.Content = rt.Description;

                        var dialog2 = new Message(rt.Description, rt.DateTime);
                        if (dialog2.ShowDialog() == true && dialog2.DialogResult == true)
                        {
                        }
                    }
                }
                if (rb.BoxList.Count == 0)
                {
                    rb = bl.ViewSkippedBox();
                    if (rb.BoxList.Count == 0)
                    {
                        lbs2_Copy.Visibility = Visibility.Visible;
                        lbs1.Content = "Please hit";
                        lbs2.Content = "Close Shift";
                        lbs2_Copy.Content = "to proceed";
                        btNext.Visibility = Visibility.Hidden;
                        lbnextScan.Visibility = Visibility.Hidden;

                        btnnextmanual.Visibility = Visibility.Hidden;
                        lbs3.Visibility = Visibility.Visible;
                        lbpleaseenter.Content = "Please hit";
                        lbnextticket.Content = "Close Shift";
                        lbs3.Content = "to proceed";
                        lbboxmanual.Visibility = Visibility.Hidden;
                        btnsubmitcloseshift.Visibility = Visibility.Hidden;
                     //   btFinishScan.Focus();
                    }
                    else
                    { boxfunctionality(); }
                }
            }
            catch { }

        }
        public void boxfunctionality()
        {
            //if (lbnextScan.Content != "" || lbboxmanual.Content != "")
            if (rb.Status == true)
            {
                // temp++;
                if (rb.BoxList.Count > temp)
                {
                    boxtempscan = rb.BoxList[temp].ToString();
                    lbnextScan.Content = rb.BoxList[temp].ToString();
                    lbboxmanual.Content = rb.BoxList[temp].ToString();
                    settings.box = rb.BoxList[temp];
                    settings.Save();
                    //if (lbboxmanual.Content != "")
                    if (rb.Status == true)
                    {
                        ResultWithOldTicket rs = new ResultWithOldTicket();
                        string box = Convert.ToString(lbboxmanual.Content);
                        rs = bl.AutoFill_BeggingNo(box);
                        if (rs == null && bl.internetconnection() == false)
                        {
                            string heading = "        Poor Connectivity";
                            var dialog = new DialogBox_BoxNo(heading);
                            dialog.ShowDialog();
                        }
                        else
                        {
                            if (rs.Status == true)
                            {
                                btnsubmitcloseshift.Visibility = Visibility.Visible;
                                //  lbdescription.Content = rs.Description;
                                tbtcktid.Text = Convert.ToString(rs.OldTicketInfo.TicketId);
                                tbpackno.Text = Convert.ToString(rs.OldTicketInfo.PackNo);
                                tbtcktname.Text = Convert.ToString(rs.OldTicketInfo.TicketName);
                                tbopenno.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                                // tbcloseno.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                                tbcloseno.Text = tbopenno.Text;
                                tbvalue.Text = Convert.ToString(rs.OldTicketInfo.Value.ToString("C"));
                                //      tbboxval.Text = box;
                                cbBox.Text = box;
                                tbcloseno.CaretIndex = tbcloseno.Text.Length;

                            }
                            else
                            {
                                //  lbdescription.Content = rs.Description;
                                btmanualcloseshift.Visibility = Visibility.Hidden;
                                btnsubmitcloseshift.Visibility = Visibility.Hidden;
                                tbtcktid.Text = "";
                                tbpackno.Text = "";
                                tbtcktname.Text = "";
                                tbvalue.Text = "";
                                tbopenno.Text = "";
                                tbcloseno.Text = "";
                                //   tbboxval.Text = "";
                                cbBox.Text = "";

                            }
                        }
                    }
                }
                else if (rb.BoxList.Count <= temp)
                {
                    // temp = 0;

                    boxtempscan = rb.BoxList[temp].ToString();
                    lbnextScan.Content = rb.BoxList[temp].ToString();
                    lbboxmanual.Content = rb.BoxList[temp].ToString();

                    //if (lbboxmanual.Content != "")
                    if (rb.Status == true)
                    {
                        ResultWithOldTicket rs = new ResultWithOldTicket();
                        string box = Convert.ToString(lbboxmanual.Content);
                        rs = bl.AutoFill_BeggingNo(box);
                        if (rs == null && bl.internetconnection() == false)
                        {
                            string heading = "        Poor Connectivity";
                            var dialog = new DialogBox_BoxNo(heading);
                            dialog.ShowDialog();
                        }
                        else
                        {
                            if (rs.Status == true)
                            {
                                //  lbdescription.Content = rs.Description;
                                tbtcktid.Text = Convert.ToString(rs.OldTicketInfo.TicketId);
                                tbpackno.Text = Convert.ToString(rs.OldTicketInfo.PackNo);
                                tbtcktname.Text = Convert.ToString(rs.OldTicketInfo.TicketName);
                                tbvalue.Text = Convert.ToString(rs.OldTicketInfo.Value.ToString("C"));
                                tbopenno.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                                //  tbcloseno.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                                tbcloseno.Text = tbopenno.Text;
                                //       tbboxval.Text = box;
                                cbBox.Text = box;
                                tbcloseno.CaretIndex = tbcloseno.Text.Length;

                            }
                            else
                            {
                                //  lbdescription.Content = rs.Description;
                                btmanualcloseshift.Visibility = Visibility.Hidden;
                                btnsubmitcloseshift.Visibility = Visibility.Hidden;
                                tbtcktid.Text = "";
                                tbpackno.Text = "";
                                tbtcktname.Text = "";
                                tbvalue.Text = "";
                                tbopenno.Text = "";
                                tbcloseno.Text = "";
                                tbbox.Text = "";
                            }
                        }
                    }
                }
            }
        }

        private void btNext_Click(object sender, RoutedEventArgs e)
        {
            if (rb.BoxList.Count > 1)
            {
                //temp++;
                rb.BoxList.Remove(rb.BoxList[0]);
                boxfunctionality();
                btNext.Focusable = false;
            }
            else
            {
                rb = bl.ViewSkippedBox();
                boxfunctionality();
            }
        }

        private void ButtonFinish_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dialog = new SkippedBox();
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    spCdata.Visibility = Visibility.Hidden;
                    spCFinish.Visibility = Visibility.Visible;
                    spCmore.Visibility = Visibility.Hidden;
                    spmanualcloseshift.Visibility = Visibility.Hidden;

                }
                else
                    btFinishScan.Focusable = false;

                temp = 0;
                rb = bl.ViewSkippedBox();
                boxfunctionality();

                dialog.Close();
            }
            catch { }
        }

        private void tbOnlineSale_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }

        private void tbOnlinePayOut_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btGoback_Click(object sender, RoutedEventArgs e)
        {
            spCdata.Visibility = Visibility.Visible;
            spCFinish.Visibility = Visibility.Hidden;
            spCmore.Visibility = Visibility.Hidden;
        }

        private void btFinishShift_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            hiddenvalidation();
            //string onlinesale = tbOnlineSale.Text;
            //string onlineCashout = tbOnlineCashOut.Text;
            //string Cashout = tbCashOut.Text;
            //string cashinhand = tbcashinhand.Text;

            active = tbactive.Text;
            inventory = tbinventory.Text;
            total = tbtotal.Text;
            loan = tbloan.Text;
            issued = tbissue.Text;

            if (tbOnlineSale.Text == "")
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbOnlineCashOut.Text == "")
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbCashOut.Text == "")
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(onlinesale) == false)
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(onlineCashout) == false)
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(Cashout) == false)
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (tbinventory.Text != "" && TryToParse(tbinventory.Text) == false)
            {
                lbvalinventory.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbactive.Text != "" && TryToParse(tbactive.Text) == false)
            {
                lbvalactive.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbcashinhand.Text != "" && TryToParse(cashinhand) == false)
            {
                lbvalCaashinhand.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbissue.Text != "" && TryToParse(tbissue.Text) == false)
            {
                lbvalissue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbloan.Text != "" && TryToParse(tbloan.Text) == false)
            {
                lbvalloan.Visibility = Visibility.Visible;
                flag = 1;
            }
            else if (flag == 0)
            {
                if (tbactive.Text == "")
                {
                    active = "0";
                }
                else if (tbactive.Text.Contains("."))
                {
                    active = tbactive.Text.Split('.')[0];

                }
                if (tbinventory.Text == "")
                {
                    inventory = "0";
                }
                else if (tbinventory.Text.Contains("."))
                {
                    inventory = tbinventory.Text.Split('.')[0];

                }
                if (tbtotal.Text == "")
                {
                    total = "0";
                }
                if (tbcashinhand.Text == "")
                {
                    cashinhand = "0";
                }

                if (tbcredit.Text == "")
                {
                    credit = "0";
                }
                else
                {
                    if (TryToParse(credit) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalcredit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbdebit.Text == "")
                {
                    debit = "0";
                }
                else
                {
                    if (TryToParse(debit) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvaldebit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbtopup.Text == "")
                {
                    topup = "0";
                }
                else
                {
                    if (TryToParse(topup) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalTopup.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbtopupcancel.Text == "")
                {
                    topupcancel = "0";
                }

                else
                {
                    if (TryToParse(topupcancel) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalTopUpCancel.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }
                if (tbloan.Text == "")
                {
                    loan = "0";
                }
                else
                {
                    if (TryToParse(tbloan.Text) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalcredit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }
                if (tbissue.Text == "")
                {
                    issued = "0";
                }
                else
                {
                    if (TryToParse(tbissue.Text) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalcredit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (check == 0)
                {
                    if (LoginDetail.shops[IndexFinder.index].ShopTime == 1 && Convert.ToDateTime(Blogic.opendate).Date != DateTime.Now.Date)
                    {

                      //  string msg = "This shift will go to previous day. ";
                        var dialog = new DialogWithCalender();
                        dialog.ResponseData = Convert.ToDateTime(closedate);

                        if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                        {
                            closedate = dialog.ResponseData.ToString("MM-dd-yyyy");
                            loading();
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        loading();
                    }
                }
            }
        }

        private void btOk_Click(object sender, RoutedEventArgs e)
        {
            //credit = tbcredit.Text;
            //debit = tbdebit.Text;
            //topup = tbtopup.Text;
            //topupcancel = tbtopupcancel.Text;

            if (tbcredit.Text != "" && tbdebit.Text != "" && tbtopup.Text != "" && tbtopupcancel.Text != "")
            {
                spCdata.Visibility = Visibility.Hidden;
                spCFinish.Visibility = Visibility.Visible;
                spCmore.Visibility = Visibility.Hidden;
                checkboxadvancedoption.IsChecked = true;
            }
            else
            {
                //  MessageBox.Show("enter the fields");
                string heading = "Enter the fields";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
        }

        private void btBack_Click(object sender, RoutedEventArgs e)
        {
            if (tbcredit.Text == "" && tbdebit.Text == "" && tbtopup.Text == "" && tbtopupcancel.Text == "")
            { checkboxadvancedoption.IsChecked = false; }
            spCdata.Visibility = Visibility.Hidden;
            spCFinish.Visibility = Visibility.Visible;
            spCmore.Visibility = Visibility.Hidden;
        }

        private void AdvancedOptionChecked(object sender, RoutedEventArgs e)
        {
            if (checkboxadvancedoption.IsChecked == true)
            {
                //  checkboxadvancedoption.IsChecked = false;
                spCdata.Visibility = Visibility.Hidden;
                spCFinish.Visibility = Visibility.Hidden;
                spCmore.Visibility = Visibility.Visible;
            }
        }


        private void tbtotal_gotfocus(object sender, RoutedEventArgs e)
        {
            //if ((tbactive.Text != "") && (tbinventory.Text != ""))
            //{
            //    if ((TryToParse(tbactive.Text) == true) && (TryToParse(tbinventory.Text) == true))
            //    {
            //        int sum = Convert.ToInt32(tbactive.Text) + Convert.ToInt32(tbinventory.Text);
            //        tbtotal.Text = Convert.ToString(sum);
            //    }
            //    else
            //    {
            //        string heading = "   Please enter Numeric";
            //        var dialog = new DialogBox_BoxNo(heading);
            //        dialog.ShowDialog();
            //    }
            //}
        }

        private void tbinventory_textchanged(object sender, TextChangedEventArgs e)
        {
            if ((tbactive.Text != "") && (tbinventory.Text != "") && (tbissue.Text != ""))
            {
                if ((TryToParse(tbactive.Text) == true) && (TryToParse(tbinventory.Text) == true) && (TryToParse(tbissue.Text) == true))
                {

                    tbtotal.Text = Convert.ToString(Convert.ToDouble(tbactive.Text) + Convert.ToDouble(tbinventory.Text)+Convert.ToDouble(tbissue.Text));
                }
                else
                {
                    string heading = "Please enter Numeric";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
            else if (tbinventory.Text == "")
            {
                tbtotal.Text = "";
            }
        }

        private void shiftdatepicker_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            closedate = Convert.ToDateTime(shiftdatepicker.SelectedDate).ToString("yyyy/MM/dd");
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            if (value == null)
            {
                return false;
            }
            double number;
            bool result = double.TryParse(value, out number);
            if (result)
            {
                return result;

            }
            else
            {
                return result;
            }
        }

        private void btnmanualcloseshift_click(object sender, RoutedEventArgs e)
        {
            boxfunctionality();
            spmanualcloseshift.Visibility = Visibility.Visible;
            spCdata.Visibility = Visibility.Hidden;

            if (spmanualcloseshift.Visibility == Visibility.Visible)
            {

                if (rb.Status == false)
                {
                    btnnextmanual.Visibility = Visibility.Hidden;
                    btnsubmitcloseshift.Visibility = Visibility.Hidden;
                }

                if (rb.BoxList.Count > 0)
                {
                    lbboxmanual.Content = rb.BoxList[temp];
                    //if (lbboxmanual.Content != "")
                    if (rb.Status == true)
                    {
                        ResultWithOldTicket rs = new ResultWithOldTicket();
                        if (rs == null && bl.internetconnection() == false)
                        {
                            string heading = "        Poor Connectivity";
                            var dialog = new DialogBox_BoxNo(heading);
                            dialog.ShowDialog();
                        }
                        else
                        {
                            string box = Convert.ToString(lbboxmanual.Content);
                            rs = bl.AutoFill_BeggingNo(box);

                            if (rs.Status == true)
                            {
                                //  lbdescription.Content = rs.Description;
                                tbtcktid.Text = Convert.ToString(rs.OldTicketInfo.TicketId);
                                tbpackno.Text = Convert.ToString(rs.OldTicketInfo.PackNo);
                                tbtcktname.Text = Convert.ToString(rs.OldTicketInfo.TicketName);
                                tbopenno.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                                // tbcloseno.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                                tbvalue.Text = Convert.ToString(rs.OldTicketInfo.Value.ToString("C"));
                                //   tbboxval.Text = box;
                                cbBox.Text = box;
                                tbcloseno.Text = tbopenno.Text;
                                tbcloseno.CaretIndex = tbcloseno.Text.Length;

                            }
                            else
                            {
                                //  lbdescription.Content = rs.Description;
                                tbtcktid.Text = "";
                                tbpackno.Text = "";
                                tbtcktname.Text = "";
                                tbvalue.Text = "";
                                tbopenno.Text = "";
                                tbcloseno.Text = "";
                                tbbox.Text = "";
                            }
                        }
                    }
                }
            }
        }

        private void btbackmanual_Click(object sender, RoutedEventArgs e)
        {
            boxfunctionality();
            spmanualcloseshift.Visibility = Visibility.Hidden;
            spCdata.Visibility = Visibility.Visible;
        }

        public static bool Trytoparse(string value)
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            return result;
        }

        private void btmanualsubmit_Click(object sender, RoutedEventArgs e)
        {
            string ticketid = tbtcktid.Text;
            string packno = tbpackno.Text;
            string packposition = tbcloseno.Text;
            if (Trytoparse(packposition) == true && packposition != string.Empty)
            {
                ResultWithOldTicket r = bl.ManualCloseShift(ticketid, packno, packposition);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        //   btnsubmitcloseshift.Visibility = Visibility.Hidden;
                        lbboxmanual.Content = "";
                        tbtcktid.Text = "";
                        tbtcktname.Text = "";
                        tbpackno.Text = "";
                        tbopenno.Text = "";
                        tbcloseno.Text = "";
                        tbvalue.Text = "";
                        //    tbboxval.Text = "";
                        cbBox.Text = "";
                        rb.BoxList.Remove(r.OldTicketInfo.Box);
                        if (rb.BoxList.Count > 0)
                        {
                            boxfunctionality();
                        }
                        else
                        {

                            rb = bl.ViewSkippedBox();
                            if (rb.BoxList.Count == 0)
                            {

                                btnsubmitcloseshift.Visibility = Visibility.Hidden;
                                btnnextmanual.Visibility = Visibility.Hidden;
                                lbs3.Visibility = Visibility.Visible;
                                lbpleaseenter.Content = "Please hit";
                                lbnextticket.Content = "Close Shift";
                                lbs3.Content = "to proceed";

                                lbs2_Copy.Visibility = Visibility.Visible;
                                lbs1.Content = "Please hit";
                                lbs2.Content = "Close Shift";
                                lbs2_Copy.Content = "to proceed";
                                lbnextScan.Content = "";
                                btNext.Visibility = Visibility.Hidden;
                               // btncloseshft.Focus();
                                // lbnextScan.Content = rb.BoxList[temp].ToString();
                            }

                            else
                            {
                                boxfunctionality();
                            }
                            //tbtcktid.Text = "";
                            //tbtcktname.Text = "";
                            //tbpackno.Text = "";
                            //tbopenno.Text = "";
                            //tbcloseno.Text = "";
                            //tbvalue.Text = "";
                        }
                    }
                    else
                    {
                        string heading = r.Description;
                        int closeshiftstatus = 1;
                        var dialog = new DialogBox_BoxNo(heading, closeshiftstatus);
                        dialog.ShowDialog();
                    }
                }
            }
            else
            {
                lbvalcloseno.Visibility = Visibility.Visible;
            }

        }

        private void netonlinesale_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbOnlineSale.Text.Replace("$", "");
            if (s != "")
            {
                tbOnlineSale.Text = "$" + s;
            }
            onlinesale = tbOnlineSale.Text.Replace("$", "");
            tbOnlineSale.CaretIndex = onlinesale.Length + 1;
        }

        private void Onlinecashes_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbOnlineCashOut.Text.Replace("$", "");
            if (s != "")
            {
                tbOnlineCashOut.Text = "$" + s;
            }
            onlineCashout = tbOnlineCashOut.Text.Replace("$", "");
            tbOnlineCashOut.CaretIndex = onlineCashout.Length + 1;
        }


        private void tbinstantsale_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbCashOut.Text.Replace("$", "");
            if (s != "")
            {
                tbCashOut.Text = "$" + s;
            }
            Cashout = tbCashOut.Text.Replace("$", "");
            tbCashOut.CaretIndex = Cashout.Length + 1;
        }

        private void tbcashinhand_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbcashinhand.Text.Replace("$", "");
            if (s != "")
            {
                tbcashinhand.Text = "$" + s;
            }
            cashinhand = tbcashinhand.Text.Replace("$", "");
            tbcashinhand.CaretIndex = cashinhand.Length + 1;
        }

        private void tbcredit_textchanged(object sender, TextChangedEventArgs e)
        {
            string s = tbcredit.Text.Replace("$", "");
            if (s != "")
            {
                tbcredit.Text = "$" + s;

            }
            credit = tbcredit.Text.Replace("$", "");
            tbcredit.CaretIndex = credit.Length + 1;
        }

        private void tbdebit_textchanged(object sender, TextChangedEventArgs e)
        {
            string s = tbdebit.Text.Replace("$", "");
            if (s != "")
            {
                tbdebit.Text = "$" + s;
            }
            debit = tbdebit.Text.Replace("$", "");
            tbdebit.CaretIndex = debit.Length + 1;
        }


        private void tbtopup_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbtopup.Text.Replace("$", "");
            if (s != "")
            {
                tbtopup.Text = "$" + s;
            }
            topup = tbtopup.Text.Replace("$", "");
            tbtopup.CaretIndex = topup.Length + 1;

        }

        private void tbtopupcancel_textchanged(object sender, TextChangedEventArgs e)
        {
            string s = tbtopupcancel.Text.Replace("$", "");
            if (s != "")
            {
                tbtopupcancel.Text = "$" + s;
            }
            topupcancel = tbtopupcancel.Text.Replace("$", "");
            tbtopupcancel.CaretIndex = topupcancel.Length + 1;

        }

        private void Tbcredit_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbcredit.Text != "")
            {
                string s = tbcredit.Text;
                if (s.Contains("."))
                {
                    credit = s;
                }
                else
                {
                    tbcredit.Text = s + ".00";
                }
                credit = tbcredit.Text.Replace(".00", "");
                credit = credit.Replace("$", "");
            }
        }

        private void tbdebit_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbdebit.Text != "")
            {
                string s = tbdebit.Text;
                if (s.Contains("."))
                {
                    debit = s;
                }
                else
                {
                    tbdebit.Text = s + ".00";
                }
                debit = tbdebit.Text.Replace(".00", "");
                debit = debit.Replace("$", "");
            }
        }

        private void tbtopup_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbtopup.Text != "")
            {
                string s = tbtopup.Text;
                if (s.Contains("."))
                {
                    topup = s;
                }
                else
                {
                    tbtopup.Text = s + ".00";
                }
                topup = tbtopup.Text.Replace(".00", "");
                topup = topup.Replace("$", "");
            }
        }

        private void tbtopupcancel_topup(object sender, RoutedEventArgs e)
        {
            if (tbtopupcancel.Text != "")
            {
                string s = tbtopupcancel.Text;
                if (s.Contains("."))
                {
                    topupcancel = s;
                }
                else
                {
                    tbtopupcancel.Text = s + ".00";
                }
                topupcancel = tbtopupcancel.Text.Replace(".00", "");
                topupcancel = topupcancel.Replace("$", "");
            }
        }

        private void tbonlinesale_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbOnlineSale.Text != "")
            {
                string s = tbOnlineSale.Text;
                if (s.Contains("."))
                {
                    onlinesale = s;
                }
                else
                {
                    tbOnlineSale.Text = s + ".00";
                }
                onlinesale = tbOnlineSale.Text.Replace(".00", "");
                onlinesale = onlinesale.Replace("$", "");
            }
        }

        private void tbOnlineCashOut_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbOnlineCashOut.Text != "")
            {
                string s = tbOnlineCashOut.Text;
                if (s.Contains("."))
                {
                    onlineCashout = s;
                }
                else
                {
                    tbOnlineCashOut.Text = s + ".00";
                }
                onlineCashout = tbOnlineCashOut.Text.Replace(".00", "");
                onlineCashout = onlineCashout.Replace("$", "");
            }
        }

        private void tbCashOut_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbCashOut.Text != "")
            {
                string s = tbCashOut.Text;
                if (s.Contains("."))
                {
                    Cashout = s;
                }
                else
                {
                    tbCashOut.Text = s + ".00";
                }
                Cashout = tbCashOut.Text.Replace(".00", "");
                Cashout = Cashout.Replace("$", "");
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        }


        public void loading()
        {
            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;
                        spCFinish.Visibility = Visibility.Hidden;
                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }


                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //     rsFinish = bl.CloseShiftStatus_check(onlinesale, onlineCashout, Cashout, active, inventory, total, cashinhand, closedate, credit, debit, topup, topupcancel, date, time);
            rsFinish = bl.CloseShiftStatus_check(onlinesale, onlineCashout, Cashout, active, inventory, total, cashinhand, closedate, credit, debit, topup, topupcancel, date, time, loan, issued);


        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        Result rsFinish;
        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            spCFinish.Visibility = Visibility.Visible;

            if (rsFinish == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rsFinish.Status == true)
                {
                    // MessageBox.Show(r.Description);
                    this.DialogResult = true;
                }
                else
                {

                    try
                    {
                        //  MessageBox.Show(r.Description);
                        string heading = rsFinish.Description;
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    catch
                    {

                    }

                }
            }

        }

        private void tbcloseno_TextChanged(object sender, TextChangedEventArgs e)
        {
            // btmanualcloseshift.Focus();
        }

        private void tbcashinhand_LostFocus(object sender, RoutedEventArgs e)
        {

            if (tbcashinhand.Text != "")
            {
                string s = tbcashinhand.Text;
                if (s.Contains("."))
                {
                    cashinhand = s;
                }
                else
                {
                    tbcashinhand.Text = s + ".00";
                }
                cashinhand = tbcashinhand.Text.Replace(".00", "");
                cashinhand = cashinhand.Replace("$", "");
            }

        }

        private void tbcloseno_GotFocus(object sender, RoutedEventArgs e)
        {
            tbcloseno.CaretIndex = tbcloseno.Text.Length;
        }

        private void tbcloseno_GotFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            tbcloseno.CaretIndex = tbcloseno.Text.Length;


        }

        private void btFinishShift_GotFocus(object sender, RoutedEventArgs e)
        {
            btFinishShift.Background = Brushes.White;
        }

        private void btFinishShift_LostFocus(object sender, RoutedEventArgs e)
        {
            btFinishShift.Background = new SolidColorBrush(settings.Tab);
        }

        private void btActivate_Click(object sender, RoutedEventArgs e)
        {
            SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
            snd.Play();
            var dialog = new ActiveTickets();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                dialog.Close();
                cbBoxChanged();
            }
        }

        private void btSubmitBox_Click(object sender, RoutedEventArgs e)
        {
            ResultWithOldTicket rs = new ResultWithOldTicket();
            string box = Convert.ToString(cbBox.SelectedItem.ToString());
            rs = bl.AutoFill_BeggingNo(box);
            if (rs == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rs.Status == true)
                {
                    btnsubmitcloseshift.Visibility = Visibility.Visible;
                    //  lbdescription.Content = rs.Description;
                    tbtcktid.Text = Convert.ToString(rs.OldTicketInfo.TicketId);
                    tbpackno.Text = Convert.ToString(rs.OldTicketInfo.PackNo);
                    tbtcktname.Text = Convert.ToString(rs.OldTicketInfo.TicketName);
                    tbopenno.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                    // tbcloseno.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                    tbcloseno.Text = tbopenno.Text;
                    tbvalue.Text = Convert.ToString(rs.OldTicketInfo.Value.ToString("C"));
                    //      tbboxval.Text = box;
                    cbBox.Text = box;
                    tbcloseno.CaretIndex = tbcloseno.Text.Length;

                }
                else
                {
                    //  lbdescription.Content = rs.Description;
                    btmanualcloseshift.Visibility = Visibility.Hidden;
                    btnsubmitcloseshift.Visibility = Visibility.Hidden;
                    tbtcktid.Text = "";
                    tbpackno.Text = "";
                    tbtcktname.Text = "";
                    tbvalue.Text = "";
                    tbopenno.Text = "";
                    tbcloseno.Text = "";
                    //       tbboxval.Text = "";
                    cbBox.Text = "";

                }
            }
        }

        private void cbBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbBoxChanged();

        }

        private void cbBoxChanged()
        {
            cbBox.IsEnabled = false;
            ResultWithOldTicket rs = new ResultWithOldTicket();
            //   string box = Convert.ToString(cbBox.SelectedItem.ToString());
            string box = Convert.ToString(cbBox.SelectedItem);
            rs = bl.AutoFill_BeggingNo(box);
            if (rs == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rs.Status == true)
                {
                    btnsubmitcloseshift.Visibility = Visibility.Visible;
                    //  lbdescription.Content = rs.Description;
                    tbtcktid.Text = Convert.ToString(rs.OldTicketInfo.TicketId);
                    tbpackno.Text = Convert.ToString(rs.OldTicketInfo.PackNo);
                    tbtcktname.Text = Convert.ToString(rs.OldTicketInfo.TicketName);
                    tbopenno.Text = Convert.ToString(rs.OldTicketInfo.OpenNo);
                    // tbcloseno.Text = Convert.ToString(rs.OldTicketInfo.CloseNo);
                    tbcloseno.Text = tbopenno.Text;
                    tbvalue.Text = Convert.ToString(rs.OldTicketInfo.Value.ToString("C"));
                    //      tbboxval.Text = box;
                    cbBox.Text = box;
                    tbcloseno.CaretIndex = tbcloseno.Text.Length;

                }
                else
                {
                    //  lbdescription.Content = rs.Description;
                    //  btmanualcloseshift.Visibility = Visibility.Hidden;
                    //   btnsubmitcloseshift.Visibility = Visibility.Hidden;
                    tbtcktid.Text = "";
                    tbpackno.Text = "";
                    tbtcktname.Text = "";
                    tbvalue.Text = "";
                    tbopenno.Text = "";
                    tbcloseno.Text = "";
                    //   tbboxval.Text = "";
                    cbBox.Text = "";

                }
            }
            cbBox.IsEnabled = true;
        }











    }
}
