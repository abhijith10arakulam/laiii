﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace lai2
{
    public class AnalysisColor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            try
            {
                string val = (string)value;

                if (val == "Add Box" || val == "Add Ticket")
                {
                    return new SolidColorBrush(Colors.Green);
                }
                else if (val == "Reduce Box" || val == "Reduce Ticket")
                {
                    return new SolidColorBrush(Colors.Red);
                }
                else
                {
                    return new SolidColorBrush(Colors.Yellow);
                }


            }
            catch
            { return new SolidColorBrush(Colors.Coral); }
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
