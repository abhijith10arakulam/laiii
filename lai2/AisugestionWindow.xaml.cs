﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AisugestionWindow.xaml
    /// </summary>
    public partial class AisugestionWindow : Window
    {
        Blogic bl = new Blogic();
        private Settings settings = Properties.Settings.Default;
        ResultWithBox r;
        public AisugestionWindow()
        {
            InitializeComponent();
            color();
            cbBoxActive.Items.Add("Box");
            cbBoxActive.SelectedValue = "Box";           
           r = new ResultWithBox();
            r = bl.EmptyBox();
            if (r == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (r.Status == true)
                {
                    for (int i = 0; i < r.BoxList.Count; i++)
                    {
                        cbBoxActive.Items.Add(r.BoxList[i]);
                    }                  
                }
            }
        }
        public void color()
        {
            cbBoxActive.Foreground = new SolidColorBrush(settings.Font);
            cbBoxActive.Background = new SolidColorBrush(settings.Tab);
            btAiActive.Foreground = new SolidColorBrush(settings.Font);
            btAiActive.Background = new SolidColorBrush(settings.Tab);
            dgDataActive.Foreground = new SolidColorBrush(settings.Font);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            dgDataActive.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            dgDataActive.FontFamily = new FontFamily(settings.datagridfont);
        }

        private void cbboxactive_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string box = cbBoxActive.SelectedItem.ToString();
            if (TryToParse(box) == true)
            {
                List<AITicket> ai = bl.Aisugestion(box);
                if (ai == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();  
                }
                else
                dgDataActive.ItemsSource = ai;
            }
        }

        private void btAiActive_Click(object sender, RoutedEventArgs e)
        {
            string box = cbBoxActive.SelectedItem.ToString();
            if (TryToParse(box) == true)
            {
                bool flag = false;
                while (!flag)                         //means flag=false
                {
                    var dialog = new ActiveTickets(box);
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {                        
                        dialog.Close();
                    }
                    else
                    {
                        flag = true;
                    }
                }


                r = bl.EmptyBox();
                if (r == null && bl.internetconnection() == false)
                {
                    string heading = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        for (int i = 0; i < r.BoxList.Count; i++)
                        {
                            cbBoxActive.Items.Add(r.BoxList[i]);
                        }
                    }
                }
            }
        }

        private static bool TryToParse(string content)
        {
            int number;
            bool value = Int32.TryParse(content, out number);
            return value;        
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        } 

    }
}
