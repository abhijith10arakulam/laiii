﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewInventory.xaml
    /// </summary>
    public partial class ViewInventory : Page
    {
        System.ComponentModel.BackgroundWorker mWorker; 
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        ViewTicketList vtl = new ViewTicketList();
        public ViewInventory()
        {
            InitializeComponent();
            btInventoryEdit.Background = new SolidColorBrush(settings.Tab);
            btInventoryDeleteAll.Background = new SolidColorBrush(settings.Tab);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);           
            lbTotal.Foreground = new SolidColorBrush(settings.Font);
            btInventoryEdit.Foreground = new SolidColorBrush(settings.Font);
            btInventoryDeleteAll.Foreground = new SolidColorBrush(settings.Font);
            dgInventory.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            lbTotal_Copy.Foreground = new SolidColorBrush(settings.Font);           
            btPrintreport.Foreground = new SolidColorBrush(settings.Font);
            btPrintreport.Background = new SolidColorBrush(settings.Tab);
            dgInventory.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            dgInventory.FontFamily = new FontFamily(settings.datagridfont);
            lbtotaltickets.FontFamily = new FontFamily(settings.labelfont);
            lbTotal.FontFamily = new FontFamily(settings.labelfont);
            lbtotaltickets_Copy.FontFamily = new FontFamily(settings.labelfont);
            lbTotal_Copy.FontFamily = new FontFamily(settings.labelfont);
            loadingtelerick.Foreground = new SolidColorBrush(settings.Font);


            if (LoginDetail.user.UserType == "Employee")
            {
                btInventoryDeleteAll.Visibility = Visibility.Hidden;
                btInventoryEdit.Visibility = Visibility.Hidden;
            }
                loading();
        }  
       

        public void loading()
        {
            try
            {
                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                new System.Threading.ThreadStart(delegate { }));
                mWorker = new System.ComponentModel.BackgroundWorker();
                mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
                mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
                mWorker.WorkerReportsProgress = true;
                mWorker.WorkerSupportsCancellation = true;
                mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                mWorker.RunWorkerAsync();
                try
                {
                    if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loadingtelerick.IsBusy = true;
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
                }
                catch { }
            }
            catch { }
        }


        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            vtl = bl.ViewInventoryDetails();
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            try
            {
                mWorker.CancelAsync();
                loadingtelerick.IsBusy = false;
                lbTotal_Copy.Content = vtl.TotalInventoryValue.ToString("C");
                lbTotal.Content = vtl.NoTicketInInvetory;
                dgInventory.ItemsSource = vtl.ViewInventoryList;
            }
            catch { }
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }       
      
        private void ViewInventorydelete(object sender, RoutedEventArgs e)
        {        
            string deleteData="";
            if (dgInventory.SelectedItem != null)
            {
                string heading = "Delete Inventory In Progress";
                string username = "Do you want to delete selected Ticket(s)?";
                var dialog = new DeleteWindow(heading,username);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    for (int i = 0; i < dgInventory.SelectedItems.Count; i++)
                    {

                        deleteData += ((ViewTicket)dgInventory.SelectedItems[i]).TicketInventoryId + ",";
                    }
                    deleteData = deleteData.Substring(0, deleteData.Length - 1);
                    Result r = bl.DeleteInventory(deleteData);
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            this.NavigationService.Refresh();
                        }
                    }
                }
            }
            else
            {
              //  MessageBox.Show("Select one");
                string heading = "        Select One Ticket";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }    

        }

        private void PrintReport(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                string date = DateTime.Now.ToString();
                string link = "https://www.realtnetworking.com/API/LAI2/AllInventoryPdf.aspx?ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + "&UserId=" + LoginDetail.user.UserId + "&Print=yes" + "&Date=" + date;
                var response = client.DownloadData(new Uri(String.Format(link)));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\ViewInventory.pdf", response);

            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\ViewInventory.pdf");
            Process.Start(sInfo);   
        }

        private void btInventoryDeleteAll_Click(object sender, RoutedEventArgs e)
        {
           
           
                string heading = "Delete Inventory In Progress";
                string username = "Do you want to delete All Ticket(s)?";
                var dialog = new DeleteWindow(heading, username);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                   
                    Result r = bl.DeleteInventory("DeleteALL");
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            this.NavigationService.Refresh();
                        }
                    }
                }
            
            

        }

               
    }
}
