﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for FixSoldOut.xaml
    /// </summary>
    public partial class FixSoldOut : Window
    {
        Blogic bl = new Blogic();
        int flag = 0;
        private ReportData r;
        private Settings settings = Properties.Settings.Default;
        int typeId = 0;
        public FixSoldOut()
        {
            InitializeComponent();
            color();
            HideLabel();
        }

        public FixSoldOut(ReportData r)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();
            HideLabel();
            this.r = r;
            tbTicketId.Content = r.TicketId.ToString();
            tbPackno.Content = r.PackNo.ToString();
            tbTicketName.Content = r.TicketName;
            tbbox.Content = r.Box.ToString();
            lbtitlefixsoldout.Content = "Fix Sold Out Tickets";
            tbPackPositionopen.Text = r.OpenNo.ToString();
            tbPackPosition.Text = r.CloseNo.ToString();
            tbPackPosition.Visibility = Visibility.Hidden;
            tbPackPositionopen.Visibility = Visibility.Hidden;
            lbPackpos.Visibility = Visibility.Hidden;
            lbPackposopen.Visibility = Visibility.Hidden;
        }
        public FixSoldOut(ReportData r, string type)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            color();
            HideLabel();
            this.r = r;
            tbTicketId.Content = r.TicketId.ToString();
            tbPackno.Content = r.PackNo.ToString();
            tbTicketName.Content = r.TicketName;
            tbbox.Content = r.Box.ToString();
            tbPackPositionopen.Text = r.OpenNo.ToString();
            tbPackPosition.Text = r.CloseNo.ToString();
       
            if (type == "Return")
            {
                this.Title = "Fix Return Tickets";
                lbtitlefixsoldout.Content = "Fix Return Tickets";
                tbPackPosition.Visibility = Visibility.Hidden;
                tbPackPositionopen.Visibility = Visibility.Hidden;
                lbPackpos.Visibility = Visibility.Hidden;
                lbPackposopen.Visibility = Visibility.Hidden;

            }
            if (type == "Inactive")
            {
                this.Title = "Fix Inactive Tickets CloseNo";
                lbtitlefixsoldout.Content = "Fix Inactive Tickets CloseNo ";
                tbPackPosition.Visibility = Visibility.Visible;
                tbPackPositionopen.Visibility = Visibility.Visible;
                typeId = 2;// For identify inactive
            }

        }
        public void color()
        {
            btClose.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            lbtitlefixsoldout.Foreground = new SolidColorBrush(settings.Theme);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            btClose.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            tbTicketId.Foreground = new SolidColorBrush(settings.Font);
            tbPackno.Foreground = new SolidColorBrush(settings.Font);
            tbTicketName.Foreground = new SolidColorBrush(settings.Font);
            tbbox.Foreground = new SolidColorBrush(settings.Font);
            lbtitlefixsoldout.FontFamily = new FontFamily(settings.Fontstyle);
            lbdescription.FontFamily = new FontFamily(settings.labelfont);
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        public void HideLabel()
        {
            lbvalpackno.Visibility = Visibility.Hidden;
            lbvaltcktid.Visibility = Visibility.Hidden;
            lbvaltcktPacckPosition.Visibility = Visibility.Hidden;
            lbvalbox.Visibility = Visibility.Hidden;
            
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            HideLabel();
            //if (tbbox.Content == "")
            //{
            //    lbvalbox.Visibility = Visibility.Visible;
            //    flag = 1;
            //}

            //if (tbTicketId.Content == "")
            //{
            //    lbvaltcktid.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (tbTicketName.Content == "")
            //{
            //    lbvaltcktname.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (tbPackno.Content == "")
            //{
            //    lbvalpackno.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (TryToParse(tbPackno.Content) == false)
            //{
            //    lbvalpackno.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (TryToParse(tbTicketId.Text) == false)
            //{
            //    lbvaltcktid.Visibility = Visibility.Visible;
            //    flag = 1;
            //}
            //if (TryToParse(tbbox.Text) == false)
            //{
            //    lbvalbox.Visibility = Visibility.Visible;
            //    flag = 1;
            //}

            if (typeId == 0)
            {

                string ticketid = r.TicketId.ToString();
                string packno = r.PackNo.ToString();
                string ticketname = r.TicketName;
                string box = r.Box.ToString();
                string packposition = "0";

                if (r.OpenNo == 0 && r.CloseNo == r.MaxNo + 1)
                {
                    Result rs = bl.backtoinventory(r.TicketId, r.PackNo);
                    if (rs == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else if (rs.Status == true)
                    {
                        this.DialogResult = true;
                    }
                    else
                    { }

                }
                else
                {
                    ResultWithOldTicket rt = bl.FixSoldOut(ticketid, packno, packposition);
                    if (rt == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (rt.Status == true)
                        {
                            this.DialogResult = true;
                        }
                        else if (rt.Description == "Another Ticket is Activated in this Box")
                        {
                            int fixsoldoutstatus = 1;
                            var dialog = new OldTicketInfo(rt, fixsoldoutstatus);
                            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                            {
                                ResultWithOldTicket rt1 = bl.FixSoldOut(ticketid, packno, packposition);
                                if (rt1 == null && bl.internetconnection() == false)
                                {
                                    string heading1 = "        Poor Connectivity";
                                    var dialog1 = new DialogBox_BoxNo(heading1);
                                    dialog1.ShowDialog();
                                }
                                else
                                {
                                    if (rt1.Status == true)
                                    {
                                        this.DialogResult = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            else
            {
                string ticketid = r.TicketId.ToString();
                string packno = r.PackNo.ToString();
                string ticketname = r.TicketName;
                string box = r.Box.ToString();
                string packposition = Convert.ToString(tbPackPosition.Text);

                if (TryToParse(packposition))
                {
                    Result rt = bl.FixInactiveCloseNo(ticketid, packno, packposition);
                    if (rt == null && bl.internetconnection() == false)
                    {
                        string heading1 = "        Poor Connectivity";
                        var dialog1 = new DialogBox_BoxNo(heading1);
                        dialog1.ShowDialog();
                    }
                    else
                    {
                        if (rt.Status == true)
                        {
                            this.DialogResult = true;
                        }

                    }
                }
                else
                {
                    lbvaltcktPacckPosition.Visibility = Visibility.Visible;
                }
            }
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.34;
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
            }
            else
            {
                return result;
            }
        }
    }
}
