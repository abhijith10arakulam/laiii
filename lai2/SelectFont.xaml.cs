﻿using ColorFont;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for SelectFont.xaml
    /// </summary>
    public partial class SelectFont : Window
    {
        private Settings settings = Properties.Settings.Default;
        FontInfo selectedFont;
        int flag;
        public SelectFont()
        {
            InitializeComponent();
            color();
        }

        private void color()
        {
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
           
            txtText.FontFamily = new FontFamily(settings.Fontstyle);
            txtdatagrid.FontFamily = new FontFamily(settings.datagridfont);
            txtlabel.FontFamily = new FontFamily(settings.labelfont);

            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            ldheadingfontchoser.Foreground = new SolidColorBrush(settings.Font);
            ldheadingfontchoser.FontFamily = new FontFamily(settings.labelfont);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            lddatagridFontchoser.Foreground = new SolidColorBrush(settings.Font);
            lddatagridFontchoser.FontFamily = new FontFamily(settings.labelfont);
            ldlabelfontchoser.Foreground = new SolidColorBrush(settings.Font);
            ldlabelfontchoser.FontFamily = new FontFamily(settings.labelfont);
            btnselectfont.Foreground = new SolidColorBrush(settings.Font);
            btnselectfont.Background = new SolidColorBrush(settings.Tab);
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnselectfont_Copy.Foreground = new SolidColorBrush(settings.Font);
            btnselectfont_Copy.Background = new SolidColorBrush(settings.Tab);
            btnlabel_click.Foreground = new SolidColorBrush(settings.Font);
            btnlabel_click.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            btnback.Background = new SolidColorBrush(settings.Tab);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            flag = 0;   //heading font 

            ColorFont.ColorFontDialog fntDialog = new ColorFont.ColorFontDialog();
            fntDialog.Owner = this;
            fntDialog.Font = FontInfo.GetControlFont(this.txtText);
            if (fntDialog.ShowDialog() == true)
            {
                selectedFont = fntDialog.Font;
                if (selectedFont != null)
                {
                    FontInfo.ApplyFont(this.txtText, selectedFont);                   
                }
            }
        }      

        private void btndatas_click(object sender, RoutedEventArgs e)
        {
            flag = 1; //datagrid font  

            ColorFont.ColorFontDialog fntDialog = new ColorFont.ColorFontDialog();
            fntDialog.Owner = this;
            fntDialog.Font = FontInfo.GetControlFont(this.txtdatagrid);
            if (fntDialog.ShowDialog() == true)
            {
                selectedFont = fntDialog.Font;
                if (selectedFont != null)
                {
                    FontInfo.ApplyFont(this.txtdatagrid, selectedFont);                   
                }
            }
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Btnlabel_click(object sender, RoutedEventArgs e)
        {
            flag = 2; //label font

            ColorFont.ColorFontDialog fntDialog = new ColorFont.ColorFontDialog();
            fntDialog.Owner = this;
            fntDialog.Font = FontInfo.GetControlFont(this.txtlabel);
            if (fntDialog.ShowDialog() == true)
            {
                selectedFont = fntDialog.Font;
                if (selectedFont != null)
                {
                    FontInfo.ApplyFont(this.txtlabel, selectedFont);                    
                }
            }
        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        }

        private void Btnsave_click(object sender, RoutedEventArgs e)
        {
            if (flag == 0)      //heading font
            {
                settings.Fontstyle = selectedFont.Family.ToString();
            }

            if (flag == 1)          //datagrid font
            {
                settings.datagridfont = selectedFont.Family.ToString();
            }
            if (flag == 2)          //label font
            {
                settings.labelfont = selectedFont.Family.ToString();
            }
            settings.Save();
            this.DialogResult = true;
        }
    }
}
