﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for SkippedBox.xaml
    /// </summary>
    public partial class SkippedBox : Window
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        int flag;
        int flag1;
        public SkippedBox()
        {
            InitializeComponent();
            color();
            flag = 0;
            flag1 = 0;
            btCloseShift.Visibility = Visibility.Hidden;
            ResultWithBox rb = new ResultWithBox();
             rb = bl.ViewSkippedBox();
             if (rb == null && bl.internetconnection() == false)
             {
                 string heading = "        Poor Connectivity";
                 var dialog = new DialogBox_BoxNo(heading);
                 dialog.ShowDialog();
             }
             else
             {
                 if (rb.Status == true)
                 {
                     LatestData.ItemsSource = rb.BoxList;
                 }               
             }
             btcontinue.Focus();
             
        }
        public void color()
        {
            btAISuggestion.Background = new SolidColorBrush(settings.Tab);
            btGoback.Background = new SolidColorBrush(settings.Tab);
            btCloseShift.Background = new SolidColorBrush(settings.Tab);
            lbskippedbox.Foreground = new SolidColorBrush(settings.Theme);
            lbemptybox.Foreground = new SolidColorBrush(settings.Theme);
            btAISuggestion.Foreground = new SolidColorBrush(settings.Font);
            btGoback.Foreground = new SolidColorBrush(settings.Font);
            btCloseShift.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btcontinue.Foreground = new SolidColorBrush(settings.Font);
            btcontinue.Background = new SolidColorBrush(settings.Tab);
            LatestData.BorderBrush = new SolidColorBrush(settings.Border);
            lbskippedbox.FontFamily = new FontFamily(settings.Fontstyle);
            lbemptybox.FontFamily = new FontFamily(settings.Fontstyle);
            LatestData.FontFamily = new FontFamily(settings.datagridfont);
        }

        private void btGoback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private void btCloseShift_Click(object sender, RoutedEventArgs e)
        {
           
            this.DialogResult = true;
             }     

        private void btAISuggestion_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AisugestionWindow();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                ResultWithBox r = new ResultWithBox();
                r = bl.EmptyBox();
                if (r.Status == true)
                {
                    LatestData.ItemsSource = r.BoxList;
                }
            }
            else
            {
                ResultWithBox r = new ResultWithBox();
                r = bl.EmptyBox();
                if (r.Status == true)
                {
                    LatestData.ItemsSource = r.BoxList;
                }
            }

        }

        private void LatestData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                                                                                        
        }

        private void btContinue_Click(object sender, RoutedEventArgs e)
        {
          
            if (flag == 0)
            {

                if (LatestData.Items.Count >= LoginDetail.shops[IndexFinder.index].BoxCount * .9)
                {
                    var dialog = new Conformation();
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        this.Title = "Empty Box";
                        btcontinue.Visibility = Visibility.Hidden;
                        lbskippedbox.Visibility = Visibility.Hidden;
                        btAISuggestion.Visibility = Visibility.Hidden;
                        lbemptybox.Visibility = Visibility.Visible;
                        btCloseShift.Visibility = Visibility.Visible;
                        ResultWithBox r = new ResultWithBox();
                        r = bl.EmptyBox();
                        if (r == null && bl.internetconnection() == false)
                        {
                            string heading = "        Poor Connectivity";
                            var dialog1= new DialogBox_BoxNo(heading);
                            dialog1.ShowDialog();
                        }
                        else
                        {
                            if (r.Status == true)
                            {
                                LatestData.ItemsSource = r.BoxList;
                                flag++;
                            }
                        }
                    }
                    else
                    {
                        this.DialogResult = false;
                    }
                }
                else
                {
                    this.Title = "Empty Box";
                    btcontinue.Visibility = Visibility.Hidden;
                    lbskippedbox.Visibility = Visibility.Hidden;
                    btAISuggestion.Visibility = Visibility.Hidden;
                    lbemptybox.Visibility = Visibility.Visible;
                    btCloseShift.Visibility = Visibility.Visible;
                    ResultWithBox r = new ResultWithBox();
                    r = bl.EmptyBox();
                    if (r == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (r.Status == true)
                        {
                            LatestData.ItemsSource = r.BoxList;
                            flag++;
                        }
                    }
                }
            }


            btCloseShift.Focus();
            //else
            //{
            //    this.DialogResult = true;
            //}
        }
    }
}
