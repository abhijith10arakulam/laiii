﻿using lai2.Bal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Deployment;
using System.Deployment.Application;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Support.xaml
    /// </summary>
    public partial class Support : Window
    {
        public Support()
        {
            InitializeComponent();
            lbSupport_val.Content = Bal.LoginDetail.shops[IndexFinder.index].SupportId;
            lbExpare_val.Content = Bal.LoginDetail.shops[IndexFinder.index].Expiredate;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                lblVersion.Content =  ad.CurrentVersion.ToString();
            }
          //  else
            //    lblVer.Text = "V" + Application.ProductVersion.ToString();

        }
      

        private void lblogin_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://realtnetworking.com/Support.aspx");

            Process.Start(sInfo);
        }
    }
   
}
