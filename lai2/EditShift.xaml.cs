﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace lai2
{
    /// <summary>
    /// Interaction logic for EditShift.xaml
    /// </summary>
    public partial class EditShift : Window
    {
        public static string credit, debit, topup, topupcancel;

        string date;
        string time;
        int returnFlag = 1;
        private int UIvalue;
        string barcode = "";
        //  private Result res;
        int check = 0;
        int temp = 0;
        int ScanOrder;
        Blogic bl = new Blogic();
        int flag = 0;
        string closedate;
        private Settings settings = Properties.Settings.Default;
        string onlinesale;
        string onlineCashout;
        string Cashout;
        string active;
        string inventory;
        string total;
        string cashinhand;
        string boxtempscan;
        string reportId;
        string loan;
        string issued;
        public void color()
        {

            btGoback.Background = new SolidColorBrush(settings.Tab);
            btFinishShift.Background = new SolidColorBrush(settings.Tab);
            btback.Background = new SolidColorBrush(settings.Tab);
            btok.Background = new SolidColorBrush(settings.Tab);

            lbTitile.Foreground = new SolidColorBrush(settings.Theme);

            btGoback.Foreground = new SolidColorBrush(settings.Font);
            btFinishShift.Foreground = new SolidColorBrush(settings.Font);
            checkboxadvancedoption.Foreground = new SolidColorBrush(settings.Font);
            lbadvanced.Foreground = new SolidColorBrush(settings.Font);
            btback.Foreground = new SolidColorBrush(settings.Font);
            btok.Foreground = new SolidColorBrush(settings.Font);

            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            //tbbox.Foreground = new SolidColorBrush(settings.Font);
            //tbtctname.Foreground = new SolidColorBrush(settings.Font);
            //tbtctvalue.Foreground = new SolidColorBrush(settings.Font);
            //tbtctopenno.Foreground = new SolidColorBrush(settings.Font);
            //tbtctclno.Foreground = new SolidColorBrush(settings.Font);
            //tbTotal.Foreground = new SolidColorBrush(settings.Font);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            lbadvanced.FontFamily = new FontFamily(settings.labelfont);
            checkboxadvancedoption.FontFamily = new FontFamily(settings.labelfont);

            //lbpleaseenter.FontFamily = new FontFamily(settings.labelfont);
            //lbnextticket.FontFamily = new FontFamily(settings.labelfont);

            //vignesh
            btOnlineCashOut.Foreground = new SolidColorBrush(settings.Font);
            btOnlineSale.Foreground = new SolidColorBrush(settings.Font);
            btCashOut.Foreground = new SolidColorBrush(settings.Font);
            btUpdateData.Foreground = new SolidColorBrush(settings.Font);
            btCredit.Foreground = new SolidColorBrush(settings.Font);
            btDebit.Foreground = new SolidColorBrush(settings.Font);
            btTopup.Foreground = new SolidColorBrush(settings.Font);
            btTopupCancel.Foreground = new SolidColorBrush(settings.Font);
            btOnlineCashOut.Background = new SolidColorBrush(settings.Tab);
            btOnlineSale.Background = new SolidColorBrush(settings.Tab);
            btCashOut.Background = new SolidColorBrush(settings.Tab);
            btUpdateData.Background = new SolidColorBrush(settings.Tab);
            btCredit.Background = new SolidColorBrush(settings.Tab);
            btDebit.Background = new SolidColorBrush(settings.Tab);
            btTopup.Background = new SolidColorBrush(settings.Tab);
            btTopupCancel.Background = new SolidColorBrush(settings.Tab);

        }
        public EditShift(double onlinesale, double onlineCashout, double Cashout, double active, double inventory, double cashinhand, double credit, double debit, double topup, double topupcancel, string reportId, double issued, double loan)
        {
            InitializeComponent();
            hiddenvalidation();

            color();
            tbOnlineSale.Text = Convert.ToString(onlinesale);
            tbOnlineCashOut.Text = Convert.ToString(onlineCashout);
            tbCashOut.Text = Convert.ToString(Cashout);
            tbactive.Text = Convert.ToString(active);
            tbinventory.Text = Convert.ToString(inventory);
            tbcashinhand.Text = Convert.ToString(cashinhand);

            tbcredit.Text = Convert.ToString(credit);
            tbdebit.Text = Convert.ToString(debit);
            tbtopup.Text = Convert.ToString(topup);
            tbtopupcancel.Text = Convert.ToString(topupcancel);

            //vignesh
            tbissue.Text = Convert.ToString(issued);
            tbloan.Text = Convert.ToString(loan);

            if (credit != 0 || debit != 0 || topup != 0 || topupcancel != 0)
            {
                checkboxadvancedoption.IsChecked = true;
            }
            this.reportId = reportId;

            spCFinish.Visibility = Visibility.Visible;
            spCmore.Visibility = Visibility.Hidden;
        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;


            tbcashinhand_LostFocus(sender, e);
            tbCashOut_lostfocus(sender, e);
            Tbcredit_lostfocus(sender, e);
            tbdebit_lostfocus(sender, e);
            tbOnlineCashOut_lostfocus(sender, e);
            tbonlinesale_lostfocus(sender, e);
            tbtopup_lostfocus(sender, e);
            tbtopupcancel_topup(sender, e);


        }

        private void btFinishShift_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            hiddenvalidation();
            //string onlinesale = tbOnlineSale.Text;
            //string onlineCashout = tbOnlineCashOut.Text;
            //string Cashout = tbCashOut.Text;
            //string cashinhand = tbcashinhand.Text;

            active = tbactive.Text;
            inventory = tbinventory.Text;
            total = tbtotal.Text;
            issued = tbissue.Text;
            loan = tbloan.Text;
            if (tbOnlineSale.Text == "")
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbOnlineCashOut.Text == "")
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbCashOut.Text == "")
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(onlinesale) == false)
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(onlineCashout) == false)
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(Cashout) == false)
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (tbinventory.Text != "" && TryToParse(tbinventory.Text) == false)
            {
                lbvalinventory.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbactive.Text != "" && TryToParse(tbactive.Text) == false)
            {
                lbvalactive.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbcashinhand.Text != "" && TryToParse(cashinhand) == false)
            {
                lbvalCaashinhand.Visibility = Visibility.Visible;
                flag = 1;
            }

            //vignesh
            if (tbissue.Text != "" && TryToParse(issued) == false)
            {
                lbvalissue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbloan.Text != "" && TryToParse(loan) == false)
            {
                lbvalloan.Visibility = Visibility.Visible;
                flag = 1;
            }
            else if (flag == 0)
            {
                if (tbactive.Text == "")
                {
                    active = "0";
                }
                if (tbinventory.Text == "")
                {
                    inventory = "0";
                }
                if (tbtotal.Text == "")
                {
                    total = "0";
                }
                if (tbcashinhand.Text == "")
                {
                    cashinhand = "0";
                }

                if (tbcredit.Text == "")
                {
                    credit = "0";
                }
                else
                {
                    if (TryToParse(credit) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalcredit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbdebit.Text == "")
                {
                    debit = "0";
                }
                else
                {
                    if (TryToParse(debit) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvaldebit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbtopup.Text == "")
                {
                    topup = "0";
                }
                else
                {
                    if (TryToParse(topup) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalTopup.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbtopupcancel.Text == "")
                {
                    topupcancel = "0";
                }

                else
                {
                    if (TryToParse(topupcancel) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalTopUpCancel.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                //vignesh
                //if (tbissue.Text == "")
                //{
                //    issued = "0";
                //}
                //else
                //{
                //    if (TryToParse(issued) == true)
                //    {
                //        check = 0;
                //    }
                //    else
                //    {
                //        lbvalissue.Visibility = Visibility.Visible;
                //        check = 1;
                //    }
                //}
                //if (tbloan.Text == "")
                //{
                //    loan = "0";
                //}
                //else
                //{
                //    if (TryToParse(loan) == true)
                //    {
                //        check = 0;
                //    }
                //    else
                //    {
                //        lbvalloan.Visibility = Visibility.Visible;
                //        check = 1;
                //    }
                //}
                if (check == 0)
                {
                    loading();
                }
            }
        }
        Result rs;


        private void loading()
        {
            rs = bl.EditReportData(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Edited");
                dialog.ShowDialog();

                DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Editing Failed");
                dialog.ShowDialog();

            }
        }
        private void tbtotal_gotfocus(object sender, RoutedEventArgs e)
        { }
        private void AdvancedOptionChecked(object sender, RoutedEventArgs e)
        {
            //if (checkboxadvancedoption.IsChecked == true)
            //{
            //    //  checkboxadvancedoption.IsChecked = false;

            //    spCFinish.Visibility = Visibility.Hidden;
            //    spCmore.Visibility = Visibility.Visible;
            //}
        }


        private void netonlinesale_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbOnlineSale.Text.Replace("$", "");
            if (s != "")
            {
                tbOnlineSale.Text = "$" + s;
            }
            onlinesale = tbOnlineSale.Text.Replace("$", "");
            tbOnlineSale.CaretIndex = onlinesale.Length + 1;
        }

        private void Onlinecashes_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbOnlineCashOut.Text.Replace("$", "");
            if (s != "")
            {
                tbOnlineCashOut.Text = "$" + s;
            }
            onlineCashout = tbOnlineCashOut.Text.Replace("$", "");
            tbOnlineCashOut.CaretIndex = onlineCashout.Length + 1;
        }


        private void tbinstantsale_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbCashOut.Text.Replace("$", "");
            if (s != "")
            {
                tbCashOut.Text = "$" + s;
            }
            Cashout = tbCashOut.Text.Replace("$", "");
            tbCashOut.CaretIndex = Cashout.Length + 1;
        }

        private void tbcashinhand_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbcashinhand.Text.Replace("$", "");
            if (s != "")
            {
                tbcashinhand.Text = "$" + s;
            }
            cashinhand = tbcashinhand.Text.Replace("$", "");
            tbcashinhand.CaretIndex = cashinhand.Length + 1;
        }

        private void tbcredit_textchanged(object sender, TextChangedEventArgs e)
        {
            string s = tbcredit.Text.Replace("$", "");
            if (s != "")
            {
                tbcredit.Text = "$" + s;

            }
            credit = tbcredit.Text.Replace("$", "");
            tbcredit.CaretIndex = credit.Length + 1;
        }

        private void tbdebit_textchanged(object sender, TextChangedEventArgs e)
        {
            string s = tbdebit.Text.Replace("$", "");
            if (s != "")
            {
                tbdebit.Text = "$" + s;
            }
            debit = tbdebit.Text.Replace("$", "");
            tbdebit.CaretIndex = debit.Length + 1;
        }


        private void tbtopup_textchanged(object sender, TextChangedEventArgs e)
        {

            string s = tbtopup.Text.Replace("$", "");
            if (s != "")
            {
                tbtopup.Text = "$" + s;
            }
            topup = tbtopup.Text.Replace("$", "");
            tbtopup.CaretIndex = topup.Length + 1;

        }

        private void tbtopupcancel_textchanged(object sender, TextChangedEventArgs e)
        {
            string s = tbtopupcancel.Text.Replace("$", "");
            if (s != "")
            {
                tbtopupcancel.Text = "$" + s;
            }
            topupcancel = tbtopupcancel.Text.Replace("$", "");
            tbtopupcancel.CaretIndex = topupcancel.Length + 1;

        }

        public void TextFormating()
        {
            //object sender=tbactive;

            // Tbcredit_lostfocus(object sender, RoutedEventArgs r );

        }

        private void Tbcredit_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbcredit.Text != "")
            {
                string s = tbcredit.Text;
                if (s.Contains("."))
                {
                    credit = s;
                }
                else
                {
                    tbcredit.Text = s + ".00";
                }
                credit = tbcredit.Text.Replace(".00", "");
                credit = credit.Replace("$", "");
            }
        }

        private void tbdebit_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbdebit.Text != "")
            {
                string s = tbdebit.Text;
                if (s.Contains("."))
                {
                    debit = s;
                }
                else
                {
                    tbdebit.Text = s + ".00";
                }
                debit = tbdebit.Text.Replace(".00", "");
                debit = debit.Replace("$", "");
            }
        }

        private void tbtopup_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbtopup.Text != "")
            {
                string s = tbtopup.Text;
                if (s.Contains("."))
                {
                    topup = s;
                }
                else
                {
                    tbtopup.Text = s + ".00";
                }
                topup = tbtopup.Text.Replace(".00", "");
                topup = topup.Replace("$", "");
            }
        }

        private void tbtopupcancel_topup(object sender, RoutedEventArgs e)
        {
            if (tbtopupcancel.Text != "")
            {
                string s = tbtopupcancel.Text;
                if (s.Contains("."))
                {
                    topupcancel = s;
                }
                else
                {
                    tbtopupcancel.Text = s + ".00";
                }
                topupcancel = tbtopupcancel.Text.Replace(".00", "");
                topupcancel = topupcancel.Replace("$", "");
            }
        }

        private void tbonlinesale_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbOnlineSale.Text != "")
            {
                string s = tbOnlineSale.Text;
                if (s.Contains("."))
                {
                    onlinesale = s;
                }
                else
                {
                    tbOnlineSale.Text = s + ".00";
                }
                onlinesale = tbOnlineSale.Text.Replace(".00", "");
                onlinesale = onlinesale.Replace("$", "");
            }
        }

        private void tbOnlineCashOut_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbOnlineCashOut.Text != "")
            {
                string s = tbOnlineCashOut.Text;
                if (s.Contains("."))
                {
                    onlineCashout = s;
                }
                else
                {
                    tbOnlineCashOut.Text = s + ".00";
                }
                onlineCashout = tbOnlineCashOut.Text.Replace(".00", "");
                onlineCashout = onlineCashout.Replace("$", "");
            }
        }

        private void tbCashOut_lostfocus(object sender, RoutedEventArgs e)
        {
            if (tbCashOut.Text != "")
            {
                string s = tbCashOut.Text;
                if (s.Contains("."))
                {
                    Cashout = s;
                }
                else
                {
                    tbCashOut.Text = s + ".00";
                }
                Cashout = tbCashOut.Text.Replace(".00", "");
                Cashout = Cashout.Replace("$", "");
            }
        }

        public void hiddenvalidation()
        {
            //lbvalactive.Visibility = Visibility.Hidden;
            //  lbvaladvancedoption.Visibility = Visibility.Hidden;
            //  lbvalcashinhand.Visibility = Visibility.Hidden;
            //  lbvaldatepicker.Visibility = Visibility.Hidden;
            lbvalinstantcashout.Visibility = Visibility.Hidden;
            //  lbvalinventory.Visibility = Visibility.Hidden;
            lbvalonlinecahout.Visibility = Visibility.Hidden;
            lbvalonlinesale.Visibility = Visibility.Hidden;
            //  lbvaltotal.Visibility = Visibility.Hidden;          
            lbvalactive.Visibility = Visibility.Hidden;
            lbvalCaashinhand.Visibility = Visibility.Hidden;
            lbvalinventory.Visibility = Visibility.Hidden;
            lbvaltotal.Visibility = Visibility.Hidden;
            lbvalcredit.Visibility = Visibility.Hidden;
            lbvaldebit.Visibility = Visibility.Hidden;
            lbvalTopup.Visibility = Visibility.Hidden;
            lbvalTopUpCancel.Visibility = Visibility.Hidden;
            //vignesh
            lbvalissue.Visibility = Visibility.Hidden;
            lbvalloan.Visibility = Visibility.Hidden;

        }

        private void btGoback_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void tbcashinhand_LostFocus(object sender, RoutedEventArgs e)
        {

            if (tbcashinhand.Text != "")
            {
                string s = tbcashinhand.Text;
                if (s.Contains("."))
                {
                    cashinhand = s;
                }
                else
                {
                    tbcashinhand.Text = s + ".00";
                }
                cashinhand = tbcashinhand.Text.Replace(".00", "");
                cashinhand = cashinhand.Replace("$", "");
            }

        }

        private void tbcloseno_GotFocus(object sender, RoutedEventArgs e)
        {
            // tbcloseno.CaretIndex = tbcloseno.Text.Length;
        }
        public static bool Trytoparse(string value)
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            return result;
        }
        private static bool TryToParse(string value)        //number to number conversion
        {
            if (value == null)
            {
                return false;
            }
            double number;
            value = value.Replace("$", "");
            if (value == "")
                value = "0";
            bool result = double.TryParse(value, out number);
            if (result)
            {
                return result;

            }
            else
            {
                return result;
            }
        }
        private void tbinventory_textchanged(object sender, TextChangedEventArgs e)
        {
            if ((tbactive.Text != "") && (tbinventory.Text != ""))
            {
                if ((TryToParse(tbactive.Text) == true) && (TryToParse(tbinventory.Text) == true))
                {
                    int sum = Convert.ToInt32(tbactive.Text) + Convert.ToInt32(tbinventory.Text);
                    tbtotal.Text = Convert.ToString(sum);
                }
                else
                {
                    string heading = "Please enter Numeric";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
            else if (tbinventory.Text == "")
            {
                tbtotal.Text = "";
            }
        }


        private void btOk_Click(object sender, RoutedEventArgs e)
        {
            //credit = tbcredit.Text;
            //debit = tbdebit.Text;
            //topup = tbtopup.Text;
            //topupcancel = tbtopupcancel.Text;
            flag = 0;
            if (TryToParse(tbcredit.Text) == false)
            {
                lbvalcredit.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbdebit.Text) == false)
            {
                lbvaldebit.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbtopup.Text) == false)
            {
                lbvalTopup.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbtopupcancel.Text) == false)
            {
                lbvalTopUpCancel.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (flag == 0)
            {
                if (tbcredit.Text != "" && tbdebit.Text != "" && tbtopup.Text != "" && tbtopupcancel.Text != "")
                {
                    //spCdata.Visibility = Visibility.Hidden;
                    spCFinish.Visibility = Visibility.Visible;
                    spCmore.Visibility = Visibility.Hidden;
                    checkboxadvancedoption.IsChecked = true;
                }
                else
                {
                    //  MessageBox.Show("enter the fields");
                    string heading = "Enter the fields";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
            }
        }

        private void btBack_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;

            if (TryToParse(tbcredit.Text) == false)
            {
                lbvalcredit.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbdebit.Text) == false)
            {
                lbvaldebit.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbtopup.Text) == false)
            {
                lbvalTopup.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbtopupcancel.Text) == false)
            {
                lbvalTopUpCancel.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (flag == 0)
            {
                if (tbcredit.Text == "" && tbdebit.Text == "" && tbtopup.Text == "" && tbtopupcancel.Text == "")
                { checkboxadvancedoption.IsChecked = false; }
                else
                {
                    checkboxadvancedoption.IsChecked = true;
                }
                //  spCdata.Visibility = Visibility.Hidden;
                spCFinish.Visibility = Visibility.Visible;
                spCmore.Visibility = Visibility.Hidden;
            }
        }

        private void btFinishShift_GotFocus(object sender, RoutedEventArgs e)
        {
            btFinishShift.Background = Brushes.White;
        }

        private void btFinishShift_LostFocus(object sender, RoutedEventArgs e)
        {
            btFinishShift.Background = new SolidColorBrush(settings.Tab);
        }

        private void checkboxadvancedoption_Click(object sender, RoutedEventArgs e)
        {
            spCFinish.Visibility = Visibility.Hidden;
            spCmore.Visibility = Visibility.Visible;
        }

        //vignesh
        private void btOnlineSale_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            hiddenvalidation();
            active = tbactive.Text.Replace("$", "");
            inventory = tbinventory.Text.Replace("$", "");
            total = tbtotal.Text.Replace("$", "");
            issued = tbissue.Text.Replace("$", "");
            loan = tbloan.Text.Replace("$", "");
            if (tbOnlineSale.Text == "")
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            else if (flag == 0)
            {
                onlinesaleupdate();
            }
        }
        private void onlinesaleupdate()
        {
            rs = bl.UpdateOnlineSales(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

              //  DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btOnlineCashOut_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            hiddenvalidation();
            active = tbactive.Text.Replace("$", "");
            inventory = tbinventory.Text.Replace("$", "");
            total = tbtotal.Text.Replace("$", "");
            issued = tbissue.Text.Replace("$", "");
            loan = tbloan.Text.Replace("$", "");
            if (tbOnlineCashOut.Text == "")
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            else if (flag == 0)
            {
                onlinecashoutupdate();
            }
        }

        private void onlinecashoutupdate()
        {
            rs = bl.UpdateOnlineCashout(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

             //   DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btCashOut_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            hiddenvalidation();
            active = tbactive.Text.Replace("$", "");
            inventory = tbinventory.Text.Replace("$", "");
            total = tbtotal.Text.Replace("$", "");
            issued = tbissue.Text.Replace("$", "");
            loan = tbloan.Text.Replace("$", "");
            if (tbCashOut.Text == "")
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }
            else if (flag == 0)
            {
                instantcashoutupdate();
            }
        }
        private void instantcashoutupdate()
        {
            rs = bl.UpdateInstantCashout(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

            //    DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btUpdateData_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            hiddenvalidation();
            active = tbactive.Text.Replace("$", "");
            inventory = tbinventory.Text.Replace("$", "");
            total = tbtotal.Text.Replace("$", "");
            issued = tbissue.Text.Replace("$", "");
            loan = tbloan.Text.Replace("$", "");
            if (tbOnlineSale.Text == "")
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbOnlineCashOut.Text == "")
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbCashOut.Text == "")
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(onlinesale) == false)
            {
                lbvalonlinesale.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(onlineCashout) == false)
            {
                lbvalonlinecahout.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(Cashout) == false)
            {
                lbvalinstantcashout.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (tbinventory.Text != "" && TryToParse(tbinventory.Text) == false)
            {
                lbvalinventory.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbactive.Text != "" && TryToParse(tbactive.Text) == false)
            {
                lbvalactive.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbcashinhand.Text != "" && TryToParse(cashinhand) == false)
            {
                lbvalCaashinhand.Visibility = Visibility.Visible;
                flag = 1;
            }

            //vignesh
            if (tbissue.Text != "" && TryToParse(issued) == false)
            {
                lbvalissue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbloan.Text != "" && TryToParse(loan) == false)
            {
                lbvalloan.Visibility = Visibility.Visible;
                flag = 1;
            }
            else if (flag == 0)
            {
                if (tbactive.Text == "")
                {
                    active = "0";
                }
                if (tbinventory.Text == "")
                {
                    inventory = "0";
                }
                if (tbtotal.Text == "")
                {
                    total = "0";
                }
                if (tbcashinhand.Text == "")
                {
                    cashinhand = "0";
                }

                if (tbcredit.Text == "")
                {
                    credit = "0";
                }
                else
                {
                    if (TryToParse(credit) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalcredit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbdebit.Text == "")
                {
                    debit = "0";
                }
                else
                {
                    if (TryToParse(debit) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvaldebit.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbtopup.Text == "")
                {
                    topup = "0";
                }
                else
                {
                    if (TryToParse(topup) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalTopup.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                if (tbtopupcancel.Text == "")
                {
                    topupcancel = "0";
                }

                else
                {
                    if (TryToParse(topupcancel) == true)
                    {
                        check = 0;
                    }
                    else
                    {
                        lbvalTopUpCancel.Visibility = Visibility.Visible;
                        check = 1;
                    }
                }

                //vignesh
                //if (tbissue.Text == "")
                //{
                //    issued = "0";
                //}
                //else
                //{
                //    if (TryToParse(issued) == true)
                //    {
                //        check = 0;
                //    }
                //    else
                //    {
                //        lbvalissue.Visibility = Visibility.Visible;
                //        check = 1;
                //    }
                //}
                //if (tbloan.Text == "")
                //{
                //    loan = "0";
                //}
                //else
                //{
                //    if (TryToParse(loan) == true)
                //    {
                //        check = 0;
                //    }
                //    else
                //    {
                //        lbvalloan.Visibility = Visibility.Visible;
                //        check = 1;
                //    }
                //}
                if (check == 0)
                {
                    updatereport();
                }

            }
        }
        private void updatereport()
        {
            rs = bl.UpdateReport(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId,loan,issued);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

               // DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btCredit_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            credit = tbcredit.Text.Replace("$","");
            if (TryToParse(tbcredit.Text) == false)
            {
                lbvalcredit.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (flag == 0)
            {
                updatecredit();
            }
        }
        private void updatecredit()
        {
            rs = bl.UpdateCredit(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

              //  DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btDebit_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            debit = tbdebit.Text.Replace("$","");
            if (TryToParse(tbdebit.Text) == false)
            {
                lbvaldebit.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (flag == 0)
            {
                updatedebit();
            }
        }

        private void updatedebit()
        {
            rs = bl.UpdateDebit(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

              //  DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btTopup_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            topup = tbtopup.Text.Replace("$", "");
            if (TryToParse(tbtopup.Text) == false)
            {
                lbvalTopup.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (flag == 0)
            {
                updatetopup();
            }
        }
        private void updatetopup()
        {
            rs = bl.UpdateTopup(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

             //   DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }

        private void btTopupCancel_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            topupcancel = tbtopupcancel.Text.Replace("$", "");
            if (TryToParse(tbtopupcancel.Text) == false)
            {
                lbvalTopUpCancel.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (flag == 0)
            {
                updatetopupcancel();
            }
        }
        private void updatetopupcancel()
        {
            rs = bl.UpdateTopupCancel(onlinesale, onlineCashout, Cashout, active, inventory, cashinhand, credit, debit, topup, topupcancel, reportId);
            if (rs.Status == true)
            {
                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Successfully Saved");
                dialog.ShowDialog();

            //    DialogResult = true;
            }
            else
            {

                DialogBox_BoxNo dialog = new DialogBox_BoxNo("Saving Failed");
                dialog.ShowDialog();

            }
        }
    }
}
