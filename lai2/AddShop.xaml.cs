﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AddShop.xaml
    /// </summary>
    public partial class AddShop : Window
    {
        private Settings settings = Properties.Settings.Default;
        int validation = 0;
        int flag;
        Blogic bl = new Blogic();
        public AddShop()
        {
            InitializeComponent();
            color();
            Hiddenvalidation();
            txtboxusername.Focus();
        }

        public void color()
        {
            btnregister.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            lbdecription.Foreground = new SolidColorBrush(settings.Font);
            btnregister.Foreground = new SolidColorBrush(settings.Font);
           // txtboxShopEmail .Background = new SolidColorBrush(settings.Tab);
            btnregister.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
          //  txtboxShopEmail.Foreground = new SolidColorBrush(settings.Font);
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            btnregister_Copy.Background = new SolidColorBrush(settings.Tab);
            btnregister_Copy.Foreground = new SolidColorBrush(settings.Font);
        }

        public void Hiddenvalidation()
        {
            lbvalusername.Visibility = Visibility.Hidden;
            lbvalpassword.Visibility = Visibility.Hidden;
            lbvalShopEmail .Visibility = Visibility.Hidden;

        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 1.95;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.5;
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        public string EmailValid(string email)
        {
            // Allows only valid EmailID
            string pattern = @"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$";
            System.Text.RegularExpressions.Match match = Regex.Match(email.Trim(), pattern, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return ("1");
            }
            else
            {

                return null;
            }
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Hiddenvalidation();
            validation = 0;
            if (txtboxpassword.Password == "")
            {
                lbvalpassword.Visibility = Visibility.Visible;
                validation = 1;
            }
            if (txtboxusername.Text == "")
            {
                lbvalusername.Visibility = Visibility.Visible;
                validation = 1;
            }
            if (txtboxShopEmail .Text == "")
            {
                lbvalShopEmail .Visibility = Visibility.Visible;
                validation = 1;
            }
            if (EmailValid(txtboxShopEmail.Text) != "1")
            {
                lbvalShopEmail.Visibility = Visibility.Visible;
                validation = 1;
            }

            else if (validation == 0)
            {
                string username = txtboxusername.Text;
                string password = txtboxpassword.Password;
                string shopEmail = txtboxShopEmail.Text;
                Result r = bl.AddShop(username, password, shopEmail);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        this.DialogResult = true;
                    }
                    else
                    {
                        lbdecription.Content = r.Description;
                    }
                }
            }
        }


    }
}
