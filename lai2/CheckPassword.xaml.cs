﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
namespace lai2
{
    /// <summary>
    /// Interaction logic for CheckPassword.xaml
    /// </summary>
    public partial class CheckPassword : Window
    {
        private Settings settings = Properties.Settings.Default;
        public CheckPassword()
        {
            InitializeComponent();
            color();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            string password = txtPassword.Password.ToString();
            string currentPassword=settings.Password.ToString();
            if(password==currentPassword)
            {
                DialogResult = true;
            }
            else
            {
                MessageBox.Show("Password Incorrect");
                DialogResult = false;
            }
        }
        public void color()
        {
            buttonSumbit.Background = new SolidColorBrush(settings.Tab);
            buttonSumbit.Foreground = new SolidColorBrush(settings.Font);
            buttonBack.Background = new SolidColorBrush(settings.Tab);
            buttonBack.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbPassword.Foreground = new SolidColorBrush(settings.Theme);
            lbPassword.FontFamily = new FontFamily(settings.Fontstyle);
        }

        private void Back_click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
