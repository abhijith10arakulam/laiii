﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AutoSettledTicketSoldOut.xaml
    /// </summary>
    public partial class AutoSettledTicketSoldOut : Window
    {
        private Settings settings = Properties.Settings.Default;

        int flag;
        public AutoSettledTicketSoldOut()
        {
            InitializeComponent();
            rdbtnAscending.FontFamily = new FontFamily(settings.labelfont);
            rdbtnDescending.FontFamily = new FontFamily(settings.labelfont);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            rdbtnAscending.Foreground = new SolidColorBrush(settings.Font);
            rdbtnDescending.Foreground = new SolidColorBrush(settings.Font);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);


            CheckStatus();
        }

        private void CheckStatus()
        {
            Blogic bl = new Blogic();
            int status = bl.GetStatus_AutoSettled_SoldOut(LoginDetail.shops[IndexFinder.index].ShopId);

            if (status == 1)
            {
                rdbtnAscending.IsChecked = true;        //Enabled
            }
            else
            {
                rdbtnDescending.IsChecked = true;
            }
        }


        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void rdbtnAscending_checked(object sender, RoutedEventArgs e)
        {
            flag = 1;    //Yes
        }

        private void rdbtnDescending_checked(object sender, RoutedEventArgs e)
        {            
            flag = 0;       //No
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            Blogic bl = new Blogic();
            Result isAffected = bl.UpdateStatus_AutoSettled_SoldOut(LoginDetail.shops[IndexFinder.index].ShopId, flag);
            if (isAffected.Status == true)
            {
                this.DialogResult = true;
            }
        }
    }
}
