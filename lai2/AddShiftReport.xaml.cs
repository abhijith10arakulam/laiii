﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for AddShiftReport.xaml
    /// </summary>
    public partial class AddShiftReport : Window
    {
        private Settings settings = Properties.Settings.Default;

        string TicketValue;
        private int UIvalue;
        //bool mLeftCtrlDown = false;
        //bool mScanShiftDown = false;
        //bool mScanning = false;
        int returnFlag = 1;

        string barcode = "";
        StringBuilder mScanData = new StringBuilder();
        KeyConverter mScanKeyConverter = new KeyConverter();
        //  private string p;
        //  private Result res;

        int ScanOrder;
        Blogic bl = new Blogic();
        int flag = 0;
        private string box;
        private string ticketname;
        private string tcktvalue;
        private string openno;
        private string closeno;
        private string total;
        private string tcktid;
        private string pckno;
        private string pckposopen;
        int temp;
        private int zero;
        public static int maxno;
        private string reportid;
        public AddShiftReport()
        {
            InitializeComponent();
            this.DataContext = new ViewModel();
        }

        public AddShiftReport(string reportid)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.reportid = reportid;
            InitializeComponent();
            Colour();
            InVisibleLabel();
            this.DataContext = new ViewModel();
            if (settings.checkbox == 1)
            {
                CheckBox1.IsChecked = true;
            }
            else
            {
                CheckBox1.IsChecked = false;
            }
           // rdbtnascending.IsChecked = true;
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            var am = DateTime.Now.ToString("tt");
            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;

            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                cbboxcount.Items.Add(i);
            }
            //cbboxcount.SelectedItem = 1;
            // tbPackPositionopen.Text = "0";           
            cbboxcount.SelectedItem = settings.box;

            if (settings.box >= LoginDetail.shops[IndexFinder.index].BoxCount+1)
            {
                settings.box = 1;
                cbboxcount.SelectedItem = settings.box;
            }
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);

            if (settings.TicketActivationOrder == 0 || Blogic.IsAtvm)
            {
                rdbtnascending.IsChecked = true;
                ScanOrder = 0;
            }
            else
            {
                rdbtndescending.IsChecked = true;
                ScanOrder = 1;
            }
        }

        public void InVisibleLabel()
        {
            lbvaldtcktid.Visibility = Visibility.Hidden;
            lbvaldtcktname.Visibility = Visibility.Hidden;
            lbvaldpackposOPen.Visibility = Visibility.Hidden;
            lbvaldtcktpackno.Visibility = Visibility.Hidden;
            lbvaldtcktvalue.Visibility = Visibility.Hidden;
            //lbvaldBox.Visibility = Visibility.Hidden;
            lbvaldpackposClose.Visibility = Visibility.Hidden;
        }

        public void Colour()
        {
          //  btfullpackSoldout.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            btClose.Background = new SolidColorBrush(settings.Tab);
            cbboxcount.Background = new SolidColorBrush(settings.Tab);
            comboboxhour.Background = new SolidColorBrush(settings.Tab);
            comboboxam.Background = new SolidColorBrush(settings.Tab);
            comboboxminute.Background = new SolidColorBrush(settings.Tab);
            cbboxcount.Foreground = new SolidColorBrush(settings.Font);
            comboboxhour.Foreground = new SolidColorBrush(settings.Font);
            comboboxam.Foreground = new SolidColorBrush(settings.Font);
            comboboxminute.Foreground = new SolidColorBrush(settings.Font);

            lbactivationheading.Foreground = new SolidColorBrush(settings.Theme);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
          //  btfullpackSoldout.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            btClose.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbactivationheading.FontFamily = new FontFamily(settings.Fontstyle);
        }

        private void TicketId_textchanged(object sender, TextChangedEventArgs e)
        {
           if( bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
          // if (tbTicketId.Text.Length >= 3)
            {
                if (TryToParse(tbTicketId.SearchText) == true)
                {
                    //if (ScanOrder == 1)
                    //{
                    //    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.Text);
                    //    if (ts.Status == true)
                    //    {
                    //        maxno = ts.TicketInfo[0].MaxNo;
                    //        tbPackPositionopen.Text = Convert.ToString(maxno);
                    //    }
                    //}

                    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.SearchText);
                    if (ts == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (ts.Status == true)
                        {
                            tbPackno.SearchText = Convert.ToString(ts.TicketInfo[0].PackNo);
                            tbTicketName.Text = Convert.ToString(ts.TicketInfo[0].TicketName);
                            tbValue.Text = Convert.ToString(ts.TicketInfo[0].Value);
                            maxno = ts.TicketInfo[0].MaxNo;
                        }
                        else
                        {
                            //tbPackno.Text = "";
                            //tbTicketName.Text = "";
                            //tbValue.Text = "";
                            //tbPackPositionopen.Text = "";
                            //tbBox.Text = "";                  
                        }
                    }
                }
            }
            if (tbTicketId.SearchText == "")
            {
                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackno.SearchText = "";
                lbdescription.Visibility = Visibility.Hidden;
            }
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)        //Ascending Clicked
        {
            ScanOrder = 0;
            //if (settings.ActiveScan == 0)
            //{
            //    tbPackPositionopen.Text = "0";
            //    tbPackPositionclose.Text = Convert.ToString(maxno);
            //}
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)        //Descending Clicked
        {
            ScanOrder = 1;
            //if (settings.ActiveScan == 0)
            //{
            //    tbPackPositionopen.Text = Convert.ToString(maxno);
            //    tbPackPositionclose.Text = "0";
            //}
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {
            flag = 0;
            InVisibleLabel();
            if (tbTicketName.Text == "")
            {
                lbvaldtcktname.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketId.SearchText == "")
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackno.SearchText == "")
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbValue.Text == "")
            {
                lbvaldtcktvalue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPositionopen.Text == "")
            {
                lbvaldpackposOPen.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackPositionclose.Text == "")
            {
                lbvaldpackposClose.Visibility = Visibility.Visible;
                flag = 1;
            }   

              if (TryToParse(tbPackPositionclose.Text) == false)
            {
                lbvaldpackposClose.Visibility = Visibility.Visible;
                flag = 1;
            }

              if (TryToParse(tbTicketId.SearchText) == false)
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
              if ((TryToParse(tbPackno.SearchText) == false))
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if ((TryToParse(tbPackPositionopen.Text) == false))
            {
                lbvaldpackposOPen.Visibility = Visibility.Visible;
                flag = 1;
            }
            if ((TryToParse(TicketValue) == false))
            {
                lbvaldtcktvalue.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
            {
                lbvaldtcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckPackNoLenght(tbPackno.SearchText.Length))
            {
                lbvaldtcktpackno.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (CheckBox1.IsChecked == true)
            {
                if (flag == 0)
                {
                    if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                  //  if (tbTicketId.Text.Length >= 3 && tbTicketId.Text.Length <= 5 && (tbPackno.Text.Length == 6 || tbPackno.Text.Length == 7))
                    {
                        string heading = "Do you want to Add?";
                        string ticketname = tbTicketName.Text;
                        string value = tbValue.Text;
                        string box = cbboxcount.SelectedItem.ToString();
                        var dialog = new ConfirmationDetails(heading, ticketname, value, box);
                        if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                        {
                            lbdescription.Content = "";
                            // tbValue.Text = tbValue.Text.Replace("$", "");
                            string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                            String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + " " + comboboxam.SelectedItem.ToString();
                            ResultWithOldTicket rt = bl.AddShiftReport_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, tbPackPositionclose.Text, cbboxcount.SelectedItem.ToString(), reportid, s, ScanOrder);
//                            ResultWithOldTicket rt = bl.AddShiftReport_Tickets(tbTicketId.Text, tbPackno.Text, tbPackPositionopen.Text, tbPackPositionclose.Text, cbboxcount.SelectedItem.ToString(), reportid, s, ScanOrder);
                        
                            if (rt == null && bl.internetconnection() == false)
                            {
                                string heading1 = "        Poor Connectivity";
                                var dialog1 = new DialogBox_BoxNo(heading1);
                                dialog1.ShowDialog();
                            }
                            else
                            {
                                if (rt.Status == true)
                                {
                                    if (settings.box < LoginDetail.shops[IndexFinder.index].BoxCount)
                                    {
                                        settings.box = Convert.ToInt32(cbboxcount.SelectedItem);
                                        settings.box++;
                                        settings.Save();
                                    }
                                    else
                                    {
                                        settings.box = 1;
                                        settings.Save();
                                        cbboxcount.SelectedItem = settings.box;
                                    }
                                    lbdescription.Content = rt.Description;
                                    tbTicketId.SearchText = "";
                                    tbPackno.SearchText = "";
                                    tbTicketName.Text = "";
                                    tbPackPositionopen.Text = "";
                                    tbValue.Text = "";

                                    string heading1 = "Added successfully!";
                                    var dia = new DialogBox_BoxNo(heading1);
                                    dia.ShowDialog();
                                    this.DialogResult = true;
                                }
                                else
                                {
                                    tbTicketId.Focus();
                                    lbdescription.Visibility = Visibility.Visible;
                                    lbdescription.Content = rt.Description;
                                    flag = 0;
                                }
                            }
                        }
                        else
                        {
                            tbTicketId.Focus();
                        }
                    }
                }
            }

            else
            {
                var dialog = new DialogBox_BoxNo();
                dialog.ShowDialog();
                //  MessageBox.Show("Please Select Box No");
            }
        }

        void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return" && returnFlag == 1 & barcode.Length != 0)
            {
                //bl = new BAL();
                //returnFlag = 0;
                if ((barcode.Length <= 24 && barcode.Length >= 22) || (barcode.Length == 20)||(barcode.Length == 18))
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
                    snd.Play();
                    ScanData tt = bl.GetTicket(barcode);
                    setData(tt);
                    // btSubmit.Focus();
                }

                barcode = "";

                if (UIvalue == 0)
                {
                    // EnableTextBoxes();
                    //if (TbValidation())
                    //{

                    //    //this.DialogResult = true;
                    //}
                    //else
                    //{
                    //    btUISubmit.Visibility = Visibility.Visible;
                    //    btUISubmit.IsEnabled = true;
                    //}
                }
            }

            if (e.Key.ToString().StartsWith("D") && returnFlag == 1)
            {
                barcode = barcode + e.Key.ToString().Replace("D", string.Empty);

            }
            //listBox1.Items.Add(e.Key.ToString().Replace("D",string.Empty));
        }

        private void setData(ScanData tt)
        {
            tbTicketId.SearchText = Convert.ToString(tt.TicketId);
            tbPackno.SearchText = tt.PackNo;
            if (settings.ActiveScan == 1)
            {
                tbPackPositionopen.Text = Convert.ToString(tt.PackPosition);
                tbPackPositionclose.Text = Convert.ToString(tt.PackPosition);
            }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void tbValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = tbValue.Text.Replace("$", "");
            if (s != "")
            {
                tbValue.Text = "$" + s;
            }
            TicketValue = tbValue.Text.Replace("$", "");
        }

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                return result;
                //if (value == null) value = "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        }
        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1;
        }
        private void cbboxcount_selctionchanged(object sender, SelectionChangedEventArgs e)
        {
            cbboxcount.Focusable = false;
        }
        public void tbTicketIdChangeEvent()
        {
            var data = (AutoCompleteItem)tbTicketId.SelectedItem;

            if (data != null)
            {
                tbTicketId.SearchText = data.Name;
            }
            if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
            //   if (tbTicketId.Text.Length >= 3)
            {
                if (TryToParse(tbTicketId.SearchText) == true)
                {
                    //if (ScanOrder == 1)
                    //{
                    //    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.Text);
                    //    if (ts.Status == true)
                    //    {
                    //        maxno = ts.TicketInfo[0].MaxNo;
                    //        tbPackPositionopen.Text = Convert.ToString(maxno);
                    //    }
                    //}
                    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.SearchText);
                    if (ts == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (ts.Status == true)
                        {
                            InVisibleLabel();
                            tbPackno.SearchText = Convert.ToString(ts.TicketInfo[0].PackNo);
                            tbTicketName.Text = Convert.ToString(ts.TicketInfo[0].TicketName);
                            tbValue.Text = Convert.ToString(ts.TicketInfo[0].Value);
                            maxno = ts.TicketInfo[0].MaxNo;
                            //ticketnametemp = tbTicketName.Text;
                            //expdate = ts.TicketInfo[0].ExpDate;
                            //canupdateFlag = 1;
                            //if (ScanOrder == 0 && settings.ActiveScan == 0)
                            //{
                            //    asndngtemp++;
                            //    tbPackPositionopen.Text = "000";
                            //}
                        }
                        else
                        {
                            //tbPackno.SearchText = "";
                            //tbTicketName.Text = "";
                            //tbValue.Text = "";
                            //tbPackPositionopen.Text = "";
                            //tbBox.Text = "";                  
                        }
                    }
                }
            }

            if (tbTicketId.SearchText == "")
            {
                InVisibleLabel();
                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackno.SearchText = "";
                maxno = new int();
                lbdescription.Visibility = Visibility.Hidden;
                //canupdateFlag = 0;
            }
        }
        private void tbTicketId_SearchTextChanged(object sender, EventArgs e)
        {
            tbTicketIdChangeEvent();
        }

        private void tbTicketId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tbTicketIdChangeEvent();

        }

        private void tbTicketId_LostFocus(object sender, RoutedEventArgs e)
        {
            var data = tbTicketId.SearchText;
            List<GetPackNo> items = new List<GetPackNo>();
            items = bl.GetPackNos(data);
            tbPackno.ItemsSource = items;


        }

        private void tbPackno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tbPackno.SelectedItem != null)
                tbPackno.SearchText = ((GetPackNo)tbPackno.SelectedItem).PackNo;

        }

        private void tbTicketId_KeyUp(object sender, KeyEventArgs e)
        {

        }

    }
}
