﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Checkboxsettings.xaml
    /// </summary>
    public partial class Checkboxsettings : Window
    {
        private Settings settings = Properties.Settings.Default;
        public Checkboxsettings()
        {
            InitializeComponent();
            if (settings.checkbox == 1)
            {
                rdbtnchecked.IsChecked = true;
            }
            else
            {
                rdbtnchecked_Copy.IsChecked = true;
            }
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            rdbtnchecked.Foreground = new SolidColorBrush(settings.Font);          
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);
            rdbtnchecked_Copy.Foreground = new SolidColorBrush(settings.Font);
            rdbtnchecked.FontFamily = new FontFamily(settings.labelfont);
            rdbtnchecked_Copy.FontFamily = new FontFamily(settings.labelfont);
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            if (rdbtnchecked.IsChecked == true)
            {
                settings.checkbox = 1;
            }
            else
            {
                settings.checkbox = 0;
            }
            settings.Save();
            this.DialogResult = true;
        }

        private void rdbtnzero_checked(object sender, RoutedEventArgs e)
        {
           
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
        }

        private void rdbtnuncheck_checked(object sender, RoutedEventArgs e)
        {
            //if (rdbtnchecked_Copy.IsChecked == true)
            //{
            //    settings.checkbox = 0;
            //}
            //else
            //{
            //    settings.checkbox = 1;
            //}
        }
    }
}
