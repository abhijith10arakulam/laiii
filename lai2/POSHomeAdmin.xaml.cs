﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Media;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using lai2.Bal;
using lai2.Properties;
using System.Windows.Threading;
using System.ComponentModel;
using lai2.ATVM;
namespace lai2
{
    /// <summary>
    /// Interaction logic for HomeAdmin.xaml
    /// </summary>
    public partial class POSHomeAdmin : Page
    {
        private BackgroundWorker bw = new BackgroundWorker();
        Blogic bl = new Blogic();
        private Settings settings = Properties.Settings.Default;
        System.ComponentModel.BackgroundWorker mWorker;
        // public System.Windows.Media.SolidColorBrush background;
        int loading;
        DispatcherTimer timer = new DispatcherTimer();
        int aiFlag = 0;
        public POSHomeAdmin()
        {

            InitializeComponent();

          
            Blogic.aifalg = 0;
            Blogic.ExpireFlag = 0;
            Blogic.CurrentRow = 0;

            if (LoginDetail.shops[IndexFinder.index].HasAtvm==0)
            {
                btAtvm.Visibility = Visibility.Hidden;
               
            }
            if (Blogic.IsAtvm)
            {
                btAtvm.Content = "Switch to LAI II";
                lblogo.Content = "ITVM";
                //lbApp.Content = "ITVM";
            }
            else
            {
                btAtvm.Content = "Switch to ITVM";
                lblogo.Content = "Lottery Artificial Intelligence II ";
                //lbApp.Content = "LAI II";
            }

            if (LoginDetail.shops[IndexFinder.index].HasAtvm == 0)
            {
                lbApp.Visibility = Visibility.Hidden;
              
            }
          
            if (Blogic.IsAtvm)
            {
                ChengeBoxCount.IsEnabled = false;
                ActivationOrder.IsEnabled = false;
                ATVMSettings.IsEnabled = false;
                Help.Visibility = Visibility.Hidden;
                btartificialintelligence.Visibility = Visibility.Hidden;
            }

            btartificialintelligence.Background = new SolidColorBrush(settings.Tab);
            btcloseshift.Background = new SolidColorBrush(settings.Tab);
            btshiftinprogress.Background = new SolidColorBrush(settings.Tab);
            btupdateinventory.Background = new SolidColorBrush(settings.Tab);
            btprintreport.Background = new SolidColorBrush(settings.Tab);
            btviewinventory.Background = new SolidColorBrush(settings.Tab);
            btactivetckets.Background = new SolidColorBrush(settings.Tab);
            //btinactive.Background = new SolidColorBrush(settings.Tab);
            //btreturn.Background = new SolidColorBrush(settings.Tab);
            //btsoldout.Background = new SolidColorBrush(settings.Tab);
            //btsettlement.Background = new SolidColorBrush(settings.Tab);
            //shoplistbtn.Background = new SolidColorBrush(settings.Tab);
            btAtvm.Background = new SolidColorBrush(settings.Tab);
            btPOS.Background = new SolidColorBrush(settings.Tab);

            btartificialintelligence.Foreground = new SolidColorBrush(settings.Font);
            btcloseshift.Foreground = new SolidColorBrush(settings.Font);
            btPOS.Foreground = new SolidColorBrush(settings.Font);

            AddVendor.Background = new SolidColorBrush(settings.Tab);
            AddVendor.Foreground = new SolidColorBrush(settings.Font);

            btshiftinprogress.Foreground = new SolidColorBrush(settings.Font);
            btupdateinventory.Foreground = new SolidColorBrush(settings.Font);
            btprintreport.Foreground = new SolidColorBrush(settings.Font);
            btviewinventory.Foreground = new SolidColorBrush(settings.Font);
            btactivetckets.Foreground = new SolidColorBrush(settings.Font);
            //btinactive.Foreground = new SolidColorBrush(settings.Font);
            //btreturn.Foreground = new SolidColorBrush(settings.Font);
            //btsoldout.Foreground = new SolidColorBrush(settings.Font);
            //btsettlement.Foreground = new SolidColorBrush(settings.Font);
            //shoplistbtn.Foreground = new SolidColorBrush(settings.Font);
            lblogo.Foreground = new SolidColorBrush(settings.Font);
            btAtvm.Foreground = new SolidColorBrush(settings.Font);

            this.Background = new SolidColorBrush(settings.Background);
            if(!Blogic.closeShiftButtonStatus)
            btcloseshift.Visibility = Visibility.Hidden;
            else
            btcloseshift.Visibility = Visibility.Visible;

            //adminframe.Source = new Uri("DataList.xaml", UriKind.RelativeOrAbsolute);
            //adminframe.Source = new Uri("POSDataList.xaml", UriKind.RelativeOrAbsolute);
            adminframe.Source = new Uri("POSDataListView.xaml", UriKind.RelativeOrAbsolute);
            if (LoginDetail.user.UserType == "ShopAdmin")
            {
               // shoplistbtn.Visibility = Visibility.Visible;
            }
            else
            {

               // shoplistbtn.Visibility = Visibility.Hidden;
            }
            if (LoginDetail.user.UserType == "Employee")
            {
                Settings.IsEnabled = false;//.Hidden;
                //shoplistbtn.Visibility = Visibility.Hidden;
                Report.IsEnabled = false;
                AddUser.IsEnabled = false;//Visibility.Hidden;
                menu_recoverLastShift.Visibility = Visibility.Hidden;
                CheckStatus_PrintReport();
                fixbeggingno.Visibility = Visibility.Hidden;
            }          
            timer.Interval = TimeSpan.FromSeconds(20);
            timer.Tick += timer_Tick;
            timer.Start();           
        }


        private void CheckStatus_PrintReport()
        {
            Blogic bl = new Blogic();
            int status = bl.GetStatus_PrintReport(LoginDetail.shops[IndexFinder.index].ShopId);
            
            if (status == 1)
            {
                btprintreport.Visibility = Visibility.Visible;        //Enabled
            }
            else
            {
                btprintreport.Visibility = Visibility.Hidden;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
           timer.Stop();
            try
            {
                if (Blogic.aishop == null)
                {
                    bw.WorkerReportsProgress = true;
                    bw.WorkerSupportsCancellation = true;
                    bw.DoWork += new DoWorkEventHandler(bw_DoWork);

                    bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);

                    if (bw.IsBusy != true)
                    {
                        bw.RunWorkerAsync();
                    }
                }
            }
            catch { }




        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timer.Stop();

         //   bw.IsBusy = false;
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Blogic.aishop = bl.MostSellingTicket("Weekly", Convert.ToDateTime(Blogic.opendate).ToString("yyyy-MM-dd"), "shop");
                Blogic.aishopDaily = bl.MostSellingTicket("Daily", Convert.ToDateTime(Blogic.opendate).ToString("yyyy-MM-dd"), "shop");
                Blogic.aishopMontly = bl.MostSellingTicket("Monthly", Convert.ToDateTime(Blogic.opendate).ToString("yyyy-MM-dd"), "shop");
                Blogic.aiNextweekorder = bl.AI_NextweekOrderDetails();
               
            }
            catch
            { }
        }

        //private void Grid_Loaded_1(object sender, RoutedEventArgs e)
        //{
        //    Duration duration = new Duration(TimeSpan.FromSeconds(20));
        //    DoubleAnimation doubleanimation = new DoubleAnimation(400.0, duration);
        //    PBar.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
        //}

        private void HomeClicked(object sender, RoutedEventArgs e)
        {
            NavigationService navService = NavigationService.GetNavigationService(this);
            POSHomeAdmin nextPage = new POSHomeAdmin();
            navService.Navigate(nextPage);
           
        }

        private void LogOutClicked(object sender, RoutedEventArgs e)
        {
            string heading = "Do you want to Log Out?";
            var dialog = new DialogBox(heading);

            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                SoundPlayer snd = new SoundPlayer(Properties.Resources.done);
                snd.Play();
                settings.Password = "";
                settings.Save();
                NavigationService navService = NavigationService.GetNavigationService(this);
                Login nextPage = new Login();
                navService.Navigate(nextPage);
            }
        }

        private void ExitClicked(object sender, RoutedEventArgs e)
        {
            string heading = "Do you want to Exit?";
            var dialog = new DialogBox(heading);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                Environment.Exit(0);
            }
        }

        private void btHome_Click(object sender, RoutedEventArgs e)             //Home button
        {
            NavigationService navService = NavigationService.GetNavigationService(this);
            HomeAdmin nextPage = new HomeAdmin();
           
            navService.Navigate(nextPage);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)           //view inventory
        {
            adminframe.Source = new Uri("ViewInventory.xaml", UriKind.RelativeOrAbsolute);
        }
        int count = 0;
        private void Button_Click_2(object sender, RoutedEventArgs e)           //update inventory
        {
            if (bl.internetconnection() == true)
            {
                int count = 0;
                bool flag = false;

                adminframe.Source = new Uri("ViewActiveLoan.xaml", UriKind.RelativeOrAbsolute);
                var dialog = new AddLoan();
                //dialog.ShowDialog();

                while (!flag)                         //means flag=false
                {
                    count++;
                    if (count == 1)
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                    }
                    dialog = new AddLoan();
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        adminframe.NavigationService.Refresh();
                        dialog.Close();
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }

        }

        private void btRefresh_Click(object sender, RoutedEventArgs e)              //refresh clicked
        {
            adminframe.NavigationService.Refresh();
            
        }

        private void shoplistclick(object sender, RoutedEventArgs e)                //shoplistclicked
        {
            //if (bl.internetconnection() == true)
            //{
            NavigationService navService = NavigationService.GetNavigationService(this);
            Home nextPage = new Home();
            navService.Navigate(nextPage);
            Blogic.aiNextweekorder = null;
            Blogic.aishopMontly = null;
            Blogic.aishopDaily = null;
            Blogic.aishop = null;
            timer.Stop();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }



        private void ActivateTickets_click(object sender, RoutedEventArgs e)            //Activate Tickets Clicked
        {
            if (bl.internetconnection() == true)
            {
                int count = 0;
                bool flag = false;

                adminframe.Source = new Uri("ViewActivePayout.xaml", UriKind.RelativeOrAbsolute);
                var dialog = new AddPayout();
                //dialog.ShowDialog();

                while (!flag)                         //means flag=false
                {
                    count++;
                    if (count == 1)
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                    }
                    dialog = new AddPayout();
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        adminframe.NavigationService.Refresh();
                        dialog.Close();
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }
        }
        private void AddVendor_Click(object sender, RoutedEventArgs e)
        {
          
            if (bl.internetconnection() == true)
            {
                int count = 0;
                bool flag = false;

                adminframe.Source = new Uri("ViewActiveVendor.xaml", UriKind.RelativeOrAbsolute);
                var dialog = new AddVendor();
                //dialog.ShowDialog();

                while (!flag)                         //means flag=false
                {
                    count++;
                    if (count == 1)
                    {
                        SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                        snd.Play();
                    }
                    dialog = new AddVendor();
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {
                        adminframe.NavigationService.Refresh();
                        dialog.Close();
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }
        }
        private void InactiveTickets_clicked(object sender, RoutedEventArgs e)    //Inactive Tickets Clicked      
        {
            //if (bl.internetconnection() == true)
            //{
            int count = 0;
            adminframe.Source = new Uri("ViewInactive.xaml", UriKind.RelativeOrAbsolute);
            bool flag = false;
            while (!flag)
            {
                count++;
                if (count == 1)
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();
                }
                var dialog = new InactiveWindow();
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    adminframe.NavigationService.Refresh();
                    dialog.Close();
                }
                else
                {
                    flag = true;
                }
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void ReturnTickets_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            int count = 0;
            adminframe.Source = new Uri("ViewReturnTickets.xaml", UriKind.RelativeOrAbsolute);
            bool flag = false;
            while (!flag)
            {
                count++;
                if (count == 1)
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();
                }
                var dialog = new ReturnTickets();
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    adminframe.NavigationService.Refresh();
                    dialog.Close();
                }
                else
                {
                    flag = true;
                }
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void SoldOutTickets_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            int count = 0;
            adminframe.Source = new Uri("ViewSoldOutTickets.xaml", UriKind.RelativeOrAbsolute);
            bool flag = false;
            while (!flag)
            {
                count++;
                if (count == 1)
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();
                }
                var dialog = new SoldoutTickets();
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    adminframe.NavigationService.Refresh();
                    dialog.Close();
                }
                else
                {
                    flag = true;
                }
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void Settlements_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            int count = 0;
            adminframe.Source = new Uri("ViewSettlements.xaml", UriKind.RelativeOrAbsolute);
            bool flag = false;
            while (!flag)
            {
                count++;
                if (count == 1)
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();
                }
                var dialog = new SettlementbyTicket();
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
                    adminframe.NavigationService.Refresh();
                    dialog.Close();
                }
                else
                {
                    flag = true;
                }
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void FixBeggingNo_Clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            int box = 1;
            adminframe.Source = new Uri("DataList.xaml", UriKind.RelativeOrAbsolute);
            bool flag = false;
            while (!flag)
            {
                if (box > LoginDetail.shops[IndexFinder.index].BoxCount)
                    box = 1;
                var dialog = new FixBegingNo(box);
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
             //       adminframe.NavigationService.Refresh();
                    dialog.Close();
                    Blogic.CurrentRow = box-1;

                    box = dialog.ResponseData+1;

                    adminframe.NavigationService.Refresh();
             
                }
                else
                {
                    flag = true;
                }
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void FixSoldout_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            adminframe.Source = new Uri("ViewFixSoldOut.xaml", UriKind.RelativeOrAbsolute);
            //else 
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void ChangeBox_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{

            adminframe.Source = new Uri("DataList.xaml", UriKind.RelativeOrAbsolute);

            List<int> boxes = new List<int>();
            boxes.Add(1);
            boxes.Add(2);
            bool flag = false;
            while (!flag)
            {

                var dialog = new ChangeBox();
                dialog.ResponseData = boxes;
                if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                {
        //            adminframe.NavigationService.Refresh();
                    dialog.Close();
                    boxes = dialog.ResponseData;
                    Blogic.CurrentRow = boxes[1];
                    boxes[0] = boxes[0] + 1;
                    boxes[1] = boxes[1] + 1;
                    if (boxes[0] > LoginDetail.shops[IndexFinder.index].BoxCount)
                        boxes[0] = 1;
                    if (boxes[1] > LoginDetail.shops[IndexFinder.index].BoxCount)
                        boxes[1] = 1;

                    
                    adminframe.NavigationService.Refresh();
                }
                else
                {
                    flag = true;
                }
            }
            //}
            //else 
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void ShiftInProgress_clicked(object sender, RoutedEventArgs e)
        {


      LoginList ll=      bl.LoginCheck(settings.username, settings.Password);

      if (ll.user.UserName != null)
      {
          Blogic.closeShiftButtonStatus = true;


          btcloseshift.Visibility = Visibility.Visible;

          NavigationService navService = NavigationService.GetNavigationService(this);
          HomeAdmin nextPage = new HomeAdmin();
          navService.Navigate(nextPage);
      }
            else
      {

          settings.Password = "";
          settings.Save();
          NavigationService navService = NavigationService.GetNavigationService(this);
          Login nextPage = new Login();
          navService.Navigate(nextPage);
      }
          
        }

        private void CloseShift_clicked(object sender, RoutedEventArgs e)
        {
            //NavigationService navService1 = NavigationService.GetNavigationService(this);
            //HomeAdmin nextPage1 = new HomeAdmin();
            //navService1.Navigate(nextPage1);

            if (bl.LoginCheck(settings.username, settings.Password).status)
            {
                Blogic.closeShiftButtonStatus = false;
                var dialog = new POSStartCloseShift();
                if (!Blogic.IsAtvm)
                {
                    if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    {


                        var dialog1 = new POSCloseShiftInProgress();
                        if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
                        {
                            var dia = new DialogBox_BoxNo(" Congratulations!!!.\n You have successfully closed your shift");
                            if (dia.ShowDialog() == true && dia.DialogResult == true)
                            { }
                            NavigationService navService = NavigationService.GetNavigationService(this);
                            POSHomeAdmin nextPage = new POSHomeAdmin();
                            navService.Navigate(nextPage);

                        }
                        else
                        {


                            NavigationService navService = NavigationService.GetNavigationService(this);
                            POSHomeAdmin nextPage = new POSHomeAdmin();
                            navService.Navigate(nextPage);

                        }
                    }


                }
                else
                {
                    NavigationService navService = NavigationService.GetNavigationService(this);
                    AtvmCloseShift nextPage = new AtvmCloseShift();
                    navService.Navigate(nextPage);
                }
            }
            else
            {
                  NavigationService navService = NavigationService.GetNavigationService(this);
                    Login nextPage = new Login();
                    navService.Navigate(nextPage);
                }
            }
        

        private void Button_Click_3(object sender, RoutedEventArgs e)         //artificial Intelligence Clicked
        {
            //if (bl.internetconnection() == true)
            //{
            try
            {
                //if (Blogic.aifalg == 0)
                {
                    Blogic.aifalg = 1;
                    //shoplistbtn.Visibility = Visibility.Hidden;
                    spcbn.Visibility = Visibility.Hidden;
                    adminframe.Source = new Uri("AI.xaml", UriKind.RelativeOrAbsolute);
                }
            }
            catch
            { }
            //}
            //else 
            //{
            //string heading1 = "        Poor Connectivity";
            //var dialog1 = new DialogBox_BoxNo(heading1);
            //dialog1.ShowDialog();
            //}         

        }

        private void Email_settings_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            var dialog = new Email_settings();
            dialog.ShowDialog();
            //}
            //else
            //{
            //string heading1 = "        Poor Connectivity";
            //var dialog1 = new DialogBox_BoxNo(heading1);
            //dialog1.ShowDialog();
            //}
        }

        private void InventoryAge_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            string title = "Update Inventory Age";
            string type = "Inventory Age";
            var dialog = new AddBox(title, type);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
            //}
            //else
            //{
            //string heading1 = "        Poor Connectivity";
            //var dialog1 = new DialogBox_BoxNo(heading1);
            //dialog1.ShowDialog();
            // }
        }

        private void BoxAge_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            string title = "Update Box Age";
            string type = "Box Age";
            var dialog = new AddBox(title, type);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void Change_password_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            string title = "Change Password";
            string oldpswrd = "Old Password";
            string newpswrd = "New Password";
            string Confirmpswrd = "Confirm Password";
            var dialog = new AddBox(title, oldpswrd, newpswrd, Confirmpswrd);
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void AddEmployee_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            var dialog = new ViewaddEmployee();
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }
        private void AddDepartment_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            var dialog = new ViewaddDepartment();
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }
        private void AddVendor_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            var dialog = new ViewaddVendor();
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }
        private void ProfitPercentage_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            string title = "Update Profit Percentage";
            string type = "Instant Profit";
            var dialog = new AddBox(title, type);
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void BoxCount_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            string title = "Update Box Count";
            string type = "Box Number";
            var dialog = new AddBox(title, type);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
            
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void ShiftReport_Clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            //shoplistbtn.Visibility = Visibility.Hidden;
            spcbn.Visibility = Visibility.Hidden;
            adminframe.Source = new Uri("DailyReport.xaml", UriKind.RelativeOrAbsolute);
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void WeeklyReport_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            int id = 0;
            //shoplistbtn.Visibility = Visibility.Hidden;
            spcbn.Visibility = Visibility.Hidden;
            //adminframe.Source = new Uri("WeeklyReport.xaml", UriKind.RelativeOrAbsolute);
            WeeklyReport WR = new WeeklyReport();
            adminframe.NavigationService.Navigate(WR);
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void MonthlyReport_clicked(object sender, RoutedEventArgs e)
        {
            //int id = 1;
            //shoplistbtn.Visibility = Visibility.Hidden;
            //spcbn.Visibility = Visibility.Hidden;  
            //WeeklyReport WR = new WeeklyReport(id);
            //adminframe.NavigationService.Navigate(WR);

        }

        private void ActivescanClicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            var dialog = new ActiveScan();
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void ChangeColour_clicked(object sender, RoutedEventArgs e)
        {
            NavigationService navService = NavigationService.GetNavigationService(this);
            SelectColor nextPage = new SelectColor();
            navService.Navigate(nextPage);
        }

        private void Barcode_clicked(object sender, RoutedEventArgs e)
        {


            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork_Barcde);

            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted_Barcde);
            mWorker.RunWorkerAsync();
            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;
                       
                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }


                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }
           

        }

        private void worker_RunWorkerCompleted_Barcde(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\About%20Usermanual.pdf");
            Process.Start(sInfo);
        }

        private void worker_DoWork_Barcde(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                var response = client.DownloadData(new Uri(String.Format("https://www.realtnetworking.com/API/Documents/User%20Manual.pdf")));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\About%20Usermanual.pdf", response);
                //}
            }
            catch
            { return; }

        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\About%20Application.pdf");
            Process.Start(sInfo);
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                var response = client.DownloadData(new Uri(String.Format("https://www.realtnetworking.com/API/Documents/UsermanualWindows.pdf")));

                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\About%20Application.pdf", response);
                // }
            }
            catch
            { return; }

        }

        private void Application_clicked(object sender, RoutedEventArgs e)
        {
            //  System.Diagnostics.Process.Start("https://www.realtnetworking.com/API/Documents/Usermanual Windows.pdf");


            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);

            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;
                      
                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }


                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }

        }

        private void AboutLaiClicked(object sender, RoutedEventArgs e)
        {

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork_about);

            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted_about);
            mWorker.RunWorkerAsync();

            try
            {
                if (!mWorker.CancellationPending)
                {
                    try
                    {
                        loadingtelerick.IsBusy = true;
                        
                    }
                    catch (System.Exception ex)
                    {
                        // No action required
                    }
                }


                System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                       new System.Threading.ThreadStart(delegate { }));
            }
            catch { }
        
          
        }

        private void worker_RunWorkerCompleted_about(object sender, RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\About%20LAI_website.pdf");
            Process.Start(sInfo);
        }

        private void worker_DoWork_about(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {
                var response = client.DownloadData(new Uri(String.Format("https://www.realtnetworking.com/API/Documents/AboutLAI.pdf")));
                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\About%20LAI_website.pdf", response);
                //  }
            }
            catch
            { return; }
        }

        private void ViewInventory_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            adminframe.Source = new Uri("ViewInventory.xaml", UriKind.RelativeOrAbsolute);
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void ViewInactive_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            adminframe.Source = new Uri("ViewInactive.xaml", UriKind.RelativeOrAbsolute);
            //}
            // else
            // {
            // string heading1 = "        Poor Connectivity";
            // var dialog1 = new DialogBox_BoxNo(heading1);
            // dialog1.ShowDialog();
            // }
        }

        private void ViewReturn_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            adminframe.Source = new Uri("ViewReturnTickets.xaml", UriKind.RelativeOrAbsolute);
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();F
            //}
        }

        private void ViewSoldout_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            adminframe.Source = new Uri("ViewSoldOutTickets.xaml", UriKind.RelativeOrAbsolute);
            //}
            //string heading1 = "        Poor Connectivity";
            //var dialog1 = new DialogBox_BoxNo(heading1);
            //dialog1.ShowDialog();
        }

        private void ViewActive_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            adminframe.Source = new Uri("ViewActiveTickets.xaml", UriKind.RelativeOrAbsolute);
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void btprintreport_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            try
            {

                string link=bl.baseUrl+"/GetShiftReportPDF.aspx?Print=yes&ReportId=new&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId+bl.GetAtvmStatus();
                Console.WriteLine(link);

                var response = client.DownloadData(new Uri(String.Format(bl.baseUrl + "/GetShiftReportPDF.aspx?Print=yes&ReportId=new&UserId=" + LoginDetail.user.UserId + "&ShopId=" + LoginDetail.shops[IndexFinder.index].ShopId + bl.GetAtvmStatus())));
                //if (response == null && bl.internetconnection() == false)
                //{
                //    string heading1 = "        Poor Connectivity";
                //    var dialog1 = new DialogBox_BoxNo(heading1);
                //    dialog1.ShowDialog();
                //}
                //else
                //{
                string path = @"C:\testdir2\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                System.IO.File.WriteAllBytes(@"C:\testdir2\YourPDF.pdf", response);
                // }
            }
            catch
            { return; }

            ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\YourPDF.pdf");
            Process.Start(sInfo);
        }

        private void ViewSettled_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            adminframe.Source = new Uri("ViewSettlements.xaml", UriKind.RelativeOrAbsolute);
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void Airesult_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            string title = "Update AI Result Percentage";
            string type = "AI result Percentage";
            var dialog = new AddBox(title, type);
            dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }

        private void TicketValueOrder_clicked(object sender, RoutedEventArgs e)
        {
            //if (bl.internetconnection() == true)
            //{
            var dialog = new TicketValueOrder();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();              
            //}
        }



        private void selectfont_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SelectFont();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
        }

        private void Checkbox_clicked(object sender, RoutedEventArgs e)
        {
            var dialog = new Checkboxsettings();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
            //var dialog = new Loadingdemo();
            //dialog.ShowDialog();
        }

        private void Support_Click(object sender, RoutedEventArgs e)
        {
             
            var dialog = new Support();

            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            { }
        }

        private void FactoryReset_Click(object sender, RoutedEventArgs e)
        {
            string heading = "Do you want to Reset?";
            var dialog = new DialogBox(heading);

            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                var dialogCheck = new CheckPassword();
                if (dialogCheck.ShowDialog() == true && dialogCheck.DialogResult == true)
                {
                    heading = "Warning!!! " + "You will loss all your  " + Environment.NewLine + " Data Permanently!!!. ";
                    var dialog1 = new DialogBox(heading);
                    if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
                    {
                        Result rs = bl.FactoryReset();
                        settings.box = 1;
                        settings.Save();
                        NavigationService navService = NavigationService.GetNavigationService(this);
                        HomeAdmin nextPage = new HomeAdmin();
                        navService.Navigate(nextPage);
                    }
                }
            }

        }

        private void gdAction_MouseEnter(object sender, MouseEventArgs e)
        {
            if (Blogic.ExpireFlag == 1)
            {
                btcloseshift.IsEnabled = false;
                gdAction.IsEnabled = false;
            }
            else
            {
                btcloseshift.IsEnabled = true;
                gdAction.IsEnabled = true;
            }
        }

        private void btcloseshift_MouseEnter(object sender, MouseEventArgs e)
        {
            if (Blogic.ExpireFlag == 1)
            {
                btAtvm.IsEnabled = false;
                btcloseshift.IsEnabled = false;
                gdAction.IsEnabled = false;
            }
            else
            {
                btAtvm.IsEnabled = true;
                btcloseshift.IsEnabled = true;
                gdAction.IsEnabled = true;
            }
        }
       
        private void ActivationOrder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Activation_Order();

            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            { }

        }

        private void SettlementDays_Click(object sender, RoutedEventArgs e)
        {
            string title = "Update Settlement Days";
            string type = "Settlement Days";
            var dialog = new AddBox(title, type);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
                adminframe.NavigationService.Refresh();
            }
        }

        private void menu_recoverLastShift_Click(object sender, RoutedEventArgs e)
        {
            Blogic bl = new Blogic();
            Result r = new Result();


            var dialg=new DialogBox("Do You Want To Recover The Last Shift?");
            if (dialg.ShowDialog() == true && dialg.DialogResult == true)
            {
                var dialogCheck = new CheckPassword();
                if (dialogCheck.ShowDialog() == true && dialogCheck.DialogResult == true)
                {
                    r = bl.RecoverLastShift();
                    if (r.Status == true)
                    {
                        NavigationService navService = NavigationService.GetNavigationService(this);
                        HomeAdmin nextPage = new HomeAdmin();
                        navService.Navigate(nextPage);
                    }
                    else
                    {
                        NavigationService navService = NavigationService.GetNavigationService(this);
                        HomeAdmin nextPage = new HomeAdmin();
                        navService.Navigate(nextPage);
                    }
                }
            }
        }

        private void printreport_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new PrintReport_Settings();
            if(dialog.ShowDialog()==true && dialog.DialogResult==true)
            {

            }
        }

        private void autosettledticketsoldout_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AutoSettledTicketSoldOut();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {
             
            }
        }

        private void ShopTimeClicked(object sender, RoutedEventArgs e)
        {
            var dialog = new StoreTime();
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {

            }
        }



        #region Atvm


      
     

       private void btAtvm_clicked(object sender, RoutedEventArgs e)
       {
           if(Blogic.IsAtvm)
           {
               Blogic.IsAtvm = false;
               LoginDetail.shops[IndexFinder.index].BoxCount =Blogic. TempBoxCount;
           }
           else
           {
               Blogic.IsAtvm = true;
               Blogic.TempBoxCount = LoginDetail.shops[IndexFinder.index].BoxCount;
               LoginDetail.shops[IndexFinder.index].BoxCount = 24;

           }
           NavigationService navService = NavigationService.GetNavigationService(this);
           HomeAdmin nextPage = new HomeAdmin();
           navService.Navigate(nextPage);
       }

        #endregion

       private void ATVMClicked(object sender, RoutedEventArgs e)
       {
           var dialog = new ATVMSettings();
           if (dialog.ShowDialog() == true && dialog.DialogResult == true)
           {
               NavigationService navService = NavigationService.GetNavigationService(this);
               HomeAdmin nextPage = new HomeAdmin();
               navService.Navigate(nextPage);
           }
       }

       private void adminframeNavigated(object sender, NavigationEventArgs e)
       {
           NavigationCommands.BrowseBack.InputGestures.Clear();
           NavigationCommands.BrowseForward.InputGestures.Clear();

       }

       private void TeamviewerClicked(object sender, RoutedEventArgs e)
       {
           mWorker = new System.ComponentModel.BackgroundWorker();
           mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork_TeamViewer);

           mWorker.WorkerReportsProgress = true;
           mWorker.WorkerSupportsCancellation = true;
           mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted_TeamViewer);
           mWorker.RunWorkerAsync();
           try
           {
               if (!mWorker.CancellationPending)
               {
                   try
                   {
                       loadingtelerick.IsBusy = true;

                   }
                   catch (System.Exception ex)
                   {
                       // No action required
                   }
               }


               System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                      new System.Threading.ThreadStart(delegate { }));
           }
           catch { }
           
       }

       private void worker_DoWork_TeamViewer(object sender, System.ComponentModel.DoWorkEventArgs e)
       {
           WebClient client = new WebClient();
           try
           {
               var response = client.DownloadData(new Uri(String.Format("http://download.teamviewer.com/download/version_9x/TeamViewer_Setup.exe")));

               string path = @"C:\testdir2\";
               if (!Directory.Exists(path))
               {
                   Directory.CreateDirectory(path);
               }
               System.IO.File.WriteAllBytes(@"C:\testdir2\TeamViewer_Setup.exe", response);
               //}
           }
           catch
           { return; }

       }

       private void worker_RunWorkerCompleted_TeamViewer(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
       {
           mWorker.CancelAsync();
           loadingtelerick.IsBusy = false;
           ProcessStartInfo sInfo = new ProcessStartInfo(@"C:\testdir2\TeamViewer_Setup.exe");
           Process.Start(sInfo);
       }

        private void btPOS_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void btPOS_Click(object sender, RoutedEventArgs e)
        {
            LoginList ll = bl.LoginCheck(settings.username, settings.Password);

            if (ll.user.UserName != null)
            {
                Blogic.closeShiftButtonStatus = true;


                btcloseshift.Visibility = Visibility.Visible;

                NavigationService navService = NavigationService.GetNavigationService(this);
                HomeAdmin nextPage = new HomeAdmin();
                navService.Navigate(nextPage);
            }
            else
            {

                settings.Password = "";
                settings.Save();
                NavigationService navService = NavigationService.GetNavigationService(this);
                Login nextPage = new Login();
                navService.Navigate(nextPage);
            }
        }

        private void ViewVendor_clicked(object sender, RoutedEventArgs e)
        {

            adminframe.Source = new Uri("ViewActiveVendor.xaml", UriKind.RelativeOrAbsolute);

            ////if (bl.internetconnection() == true)
            ////{
            //var dialog = new ViewaddVendor();
            //dialog.ShowDialog();
            //}
            //else
            //{
            //    string heading1 = "        Poor Connectivity";
            //    var dialog1 = new DialogBox_BoxNo(heading1);
            //    dialog1.ShowDialog();
            //}
        }
        private void ViewPayout_clicked(object sender, RoutedEventArgs e)
        {
            adminframe.Source = new Uri("ViewActivePayout.xaml", UriKind.RelativeOrAbsolute);
            ////if (bl.internetconnection() == true)
            ////{
            //var dialog = new ViewaddPayout();
            //dialog.ShowDialog();
            ////}
            ////else
            ////{
            ////    string heading1 = "        Poor Connectivity";
            ////    var dialog1 = new DialogBox_BoxNo(heading1);
            ////    dialog1.ShowDialog();
            ////}
        }
        private void ViewLoan_clicked(object sender, RoutedEventArgs e)
        {
            adminframe.Source = new Uri("ViewActiveLoan.xaml", UriKind.RelativeOrAbsolute);

            ////if (bl.internetconnection() == true)
            ////{
            //var dialog = new ViewaddLoan();
            //dialog.ShowDialog();
            ////}
            ////else
            ////{
            ////    string heading1 = "        Poor Connectivity";
            ////    var dialog1 = new DialogBox_BoxNo(heading1);
            ////    dialog1.ShowDialog();
            ////}
        }

     
    }
}
