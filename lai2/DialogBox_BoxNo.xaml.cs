﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for DialogBox_BoxNo.xaml
    /// </summary>
    public partial class DialogBox_BoxNo : Window
    {
        private Settings settings = Properties.Settings.Default;
        private string heading;
        int flag=0;
        private int closeshiftstatus;
        public DialogBox_BoxNo()
        {
            InitializeComponent();
            btnStatus1.Focus();
            btnStatus1.Background = new SolidColorBrush(settings.Tab);
            btnStatus1.Foreground = new SolidColorBrush(settings.Font);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.Text = "Please check the Box to confirm";
            lblTittle.FontFamily = new FontFamily(settings.labelfont);
        }

        public DialogBox_BoxNo(string heading)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnStatus1.Focus();
            this.heading = heading;
            lblTittle.Text = heading;
            btnStatus1.Background = new SolidColorBrush(settings.Tab);
            btnStatus1.Foreground = new SolidColorBrush(settings.Font);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.labelfont);
            if (heading != null)
            {

                if (heading.Trim() == "Poor Connectivity")
                {
                    flag = 1;
                }
                else if (heading.Trim() == "Select One User")
                {
                    flag = 2;
                }
                else if (heading.Trim() == "Invalid Selection")
                {
                    flag = 3;
                }
            }
        }
        public DialogBox_BoxNo(string heading, int closeshiftstatus)
        {
            // TODO: Complete member initialization
            InitializeComponent();  
            btnStatus1.Focus();
            this.heading = heading;
            this.closeshiftstatus = closeshiftstatus;
            lblTittle.Text = heading;
            btnStatus1.Background = new SolidColorBrush(settings.Tab);
            btnStatus1.Foreground = new SolidColorBrush(settings.Font);
            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.labelfont);

        }     

     
        private void btnStatus1_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            if (flag == 1)
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
            }
            else if (flag == 2)         //add user 
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;            
            }

            else if (flag == 3)             //invalid selection
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;       
           
            }
            else if (closeshiftstatus == 1)
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;      
            }

            else if (closeshiftstatus == 2)
            {
                Application curApp = Application.Current;
                Window mainWindow = curApp.MainWindow;
                this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
                this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
            }
            else
            {
                //Application curApp = Application.Current;
                //Window mainWindow = curApp.MainWindow;
                //this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.16;
                //this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.4;
            }
        }
    }
}
