﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace lai2
{
    class RowForgroundChange : IValueConverter
    {
        private Settings settings = Properties.Settings.Default;
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //  return new SolidColorBrush(settings.Font);
            try
            {
                string val = (string)value;
                if(val!="Total")
                return new SolidColorBrush(settings.Font);
                else
                    return new SolidColorBrush(Colors.White);

            }
            catch
            { return new SolidColorBrush(settings.Font); }
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
