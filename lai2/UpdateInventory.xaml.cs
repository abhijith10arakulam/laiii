﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;
using System.Media;

namespace lai2
{
    /// <summary>
    /// Interaction logic for UpdateInventory.xaml
    /// </summary>
    public partial class UpdateInventory : Window
    {
        private Settings settings = Properties.Settings.Default;
        private int UIvalue;
        //bool mLeftCtrlDown = false;
        //bool mScanShiftDown = false;
        //bool mScanning = false;
        int returnFlag = 1;
        TicketDetails returnTicket = new TicketDetails();
        string barcode = "";
        StringBuilder mScanData = new StringBuilder();
        KeyConverter mScanKeyConverter = new KeyConverter();
        private string p;
        private Result res;

        Blogic bl = new Blogic();
        int flag = 0;
        string TicketValue;
        int count;

        bool statusflag = false;

        public UpdateInventory()
        {
            InitializeComponent();
            HiddenLabel();
            //prvstcketdetailslabel.Visibility = Visibility.Hidden;
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
            SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
            snd.Play();
            // tbMinno.Text = "000";
            color();
            UIvalue = 1;

        }

        public UpdateInventory(int count)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.count = count;
            HiddenLabel();
            color();
            SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
            snd.Play();
            //prvstcketdetailslabel.Visibility = Visibility.Hidden;
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
            // tbMinno.Text = "000";
            lbscancount.Content = count;
            UIvalue = 1;

        }

        public UpdateInventory(int count,Result rs)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.count = count;
            HiddenLabel();
            color();
            tbExpDate.DisplayDateStart=System.DateTime.Now;
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
            if(count!=0)
            lbscancount.Content = count;
            UIvalue = 0;
            statusflag = true;
            if (rs.Status == true)
            {
                statuslabel.Visibility = Visibility.Visible;
                statuslabel.Text = rs.Description;
                //statuslabel.Foreground =            
            }
            else
            {
               
                if (rs.DateTime != null)
                {
                    statuslabel.Visibility = Visibility.Visible;
                    statuslabel.Text = rs.DateTime ;
                    //var dialog = new Message(rs.Description, rs.DateTime);
                    //if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                    //{
                    //}
                }
                else
                {
                    statuslabel.Visibility = Visibility.Visible;
                    statuslabel.Text = rs.Description;
                }
            }
        }

        public TicketDetails ResponseData
        {
            get
            {
                
                returnTicket.PackNo = tbPackno.Text;
                returnTicket.TicketName = tbTicketName.Text;
                returnTicket.Value = Convert.ToInt32(TicketValue);
                returnTicket.MaxNo = Convert.ToInt32 (tbMaxno.Text);
                returnTicket.TicketId = Convert.ToString(tbTicketId.Text);
                if (cbExpDate.IsChecked == false && Convert.ToDateTime(tbExpDate.SelectedDate).Year > 2013)
                {
                    returnTicket.ExpDate = Convert.ToDateTime(tbExpDate.SelectedDate).ToString("MM-dd-yyyy");
                }
                else
                {
                    returnTicket.ExpDate = "never";
                }
                  return returnTicket;
            }

            set
            {
                returnTicket = value;
                if (returnTicket.TicketId != "")
                {
                    tbTicketId.Text = Convert.ToString(returnTicket.TicketId);
                    tbPackno.Text = returnTicket.PackNo;
                }
            }
        }
        public void color()
        {
           
            btUISubmit.Background = new SolidColorBrush(settings.Tab);
            label1.Foreground = new SolidColorBrush(settings.Theme);
            statuslabel.Foreground = Brushes.White;            
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            lbscan.Foreground = new SolidColorBrush(settings.Font);
            btUISubmit.Foreground = new SolidColorBrush(settings.Font);
            lbscancount.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            label1.FontFamily = new FontFamily(settings.Fontstyle);
            statuslabel.FontFamily = new FontFamily(settings.labelfont);

        }

        public void HiddenLabel()
        {
            lbvalMaxno.Visibility = Visibility.Hidden;
            lbvalminno.Visibility = Visibility.Hidden;
            lbvalpackno.Visibility = Visibility.Hidden;
            lbvaltckname.Visibility = Visibility.Hidden;
            lbvaltcktid.Visibility = Visibility.Hidden;
            lbvalValue.Visibility = Visibility.Hidden;
        }

        void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return" && returnFlag == 1 & barcode.Length != 0)
            {
                //bl = new BAL();
                //returnFlag = 0;
                if ((barcode.Length <= 24 && barcode.Length >= 22) || (barcode.Length == 20)||(barcode.Length == 18))
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
                    snd.Play();
                    ScanData tt = bl.GetTicket(barcode);

                    setData(tt);
                    // btSubmit.Focus();
                }

                barcode = "";

              //  if (UIvalue == 0)
                {
                    // EnableTextBoxes();
                    //if (TbValidation())
                    //{

                    //    //this.DialogResult = true;
                    //}
                    //else
                    //{
                    //    btUISubmit.Visibility = Visibility.Visible;
                    //    btUISubmit.IsEnabled = true;
                    //}
                }


            }
            // else 
            if (e.Key.ToString().StartsWith("D") && returnFlag == 1)
            {
                barcode = barcode + e.Key.ToString().Replace("D", string.Empty);

            }
            //listBox1.Items.Add(e.Key.ToString().Replace("D",string.Empty));
        }

        private void setData(ScanData tt)
        {
            tbTicketId.Text = Convert.ToString(tt.TicketId);
            tbPackno.Text = tt.PackNo;

            flag = CheckValidation();

            if (UIvalue == 1)
            {

                if (flag == 0)
                {
                    Submit();
                    // btUISubmit.Focusable = true;
                }
                else
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();
                }
            }
            else
            {

                if (flag == 0)
                {
                    DialogResult = true;
                    // btUISubmit.Focusable = true;
                }
                else
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                    snd.Play();
                }
            }
        }

        private void Ticketid_textchanged(object sender, TextChangedEventArgs e)
        {
          
            tbPackno.Text = "";
            tbTicketName.Text = "";
            tbValue.Text = "";
            tbMinno.Text = "";
            tbMaxno.Text = "";
          
          
            tbExpDate.IsEnabled = true;
            cbExpDate.IsEnabled = true;
            cbExpDate.IsChecked = false;
            string ticketid = tbTicketId.Text;
            if (TryToParse(tbTicketId.Text) == true)
            {
                if(bl.CheckTicketIdLenght(ticketid.Length))
               // if (ticketid.Length >= 3 && ticketid.Length <= 5)
                {
                    TicketStatus td = bl.CheckTicketInfo(tbTicketId.Text);
                    if (td == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (td.Status == true)
                        {
                            if (td.TicketInfo[0].TicketId == Convert.ToString(tbTicketId.Text))
                            {
                                tbTicketName.Text = td.TicketInfo[0].TicketName;
                                tbValue.Text = Convert.ToString(td.TicketInfo[0].Value);
                                tbMinno.Text = "000";
                                tbMaxno.Text = Convert.ToString(td.TicketInfo[0].MaxNo);
                                if (td.TicketInfo[0].ExpDate == "never" || td.TicketInfo[0].ExpDate == ""|| td.TicketInfo[0].ExpDate ==null)
                                    cbExpDate.IsChecked = true;
                                else
                          
                                    tbExpDate.SelectedDate = Convert.ToDateTime(td.TicketInfo[0].ExpDate);

                                tbExpDate.IsEnabled = false;
                                cbExpDate.IsEnabled = false;
                                if (!statusflag)
                                {
                                    statuslabel.Visibility = Visibility.Hidden;
                                }
                                statusflag = false;
                            }
                        }
                        else
                        {
                           // SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                            //snd.Play();
                            tbPackno.Text = "";
                            tbTicketName.Text = "";
                            tbValue.Text = "";
                            tbMinno.Text = "";
                            tbMaxno.Text = "";
                            

                            tbExpDate.IsEnabled = true ;
                            cbExpDate.IsEnabled = true;
                            cbExpDate.IsChecked = false;
                            statuslabel.Text = "This is a New Ticket - Please enter all  details one time";
                              statuslabel.Visibility = Visibility.Visible;
                              statuslabel.FontSize = 13;
                        }
                    }
                }
                if (tbTicketId.Text == "")
                {
                    tbTicketName.Text = "";
                    tbValue.Text = "";
                   
                    tbExpDate.IsEnabled = true;
                    cbExpDate.IsEnabled = true;
                    cbExpDate.IsChecked = false;
                   
                    statuslabel.Visibility = Visibility.Hidden;
                }
            }
        }

        private void btUISubmit_Click(object sender, RoutedEventArgs e)
        {
            flag = CheckValidation();
            if (flag == 0)
            {
                DialogResult = true;
              //  Submit();
            }
            else
            {
                SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                snd.Play();
            }
        }



        private void Finsh()
        {
           
                DialogResult = true;
         

        }
        private void Submit()
        {
            if (bl.CheckTicketIdLenght(tbTicketId.Text.Length) && bl.CheckPackNoLenght(tbPackno.Text.Length))
          //  if ((tbPackno.Text.Length == 6 || tbPackno.Text.Length == 7) && (tbTicketId.Text.Length >= 3 && tbTicketId.Text.Length <= 5))
            {
                //  string heading = "Do you want to update?";
                string ticketname = tbTicketName.Text;
                string value = tbValue.Text;
                //var dialog = new ConfirmationDetails(heading, ticketname, value);
                //if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                //{
                HiddenLabel();
                Result r = new Result();
                string date = DateTime.Now.ToString("yyyy-MM-dd hh:mm tt");
                string expDate = "never";
                if (cbExpDate.IsChecked == false && Convert.ToDateTime(tbExpDate.SelectedDate).Year>2013)
                {
                    expDate =Convert .ToDateTime ( tbExpDate.SelectedDate).ToString ("MM-dd-yyyy");
                }

                r = bl.updatestatus(tbTicketId.Text, tbPackno.Text, tbTicketName.Text, tbMaxno.Text, tbMinno.Text, TicketValue, date, expDate);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        tbMaxno.Text = "";
                        tbMinno.Text = "";
                        tbPackno.Text = "";
                        tbTicketId.Text = "";
                        tbTicketName.Text = "";
                        tbValue.Text = "";
                        tbExpDate = new DatePicker();
                      
                        tbExpDate.IsEnabled = true;
                        cbExpDate.IsEnabled = true;
                        cbExpDate.IsChecked = false;
                        //prvstcketdetailslabel.Visibility = Visibility.Visible;
                        statuslabel.Visibility = Visibility.Visible;
                        statuslabel.Text = r.Description;
                        Blogic.updateInventoryfalg = 0;

                        this.DialogResult = true;
                    }
                    else
                    {
                        //prvstcketdetailslabel.Visibility = Visibility.Visible;
                        btUISubmit.Focusable = false;
                        statuslabel.Visibility = Visibility.Visible;
                        statuslabel.Text = r.Description;
                        tbMaxno.Text = "";
                        tbMinno.Text = "";
                        tbPackno.Text = "";
                        tbTicketId.Text = "";
                        tbTicketName.Text = "";
                        tbValue.Text = "";
                       
                         
                        tbExpDate.IsEnabled = true;
                        cbExpDate.IsEnabled = true;
                        cbExpDate.IsChecked = false;
                        
                    }
                }
            }
        }

        private int CheckValidation()
        {
            flag = 0;
            HiddenLabel();
            if (tbMaxno.Text == "")
            {
                lbvalMaxno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbMinno.Text == "")
            {
                lbvalminno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbPackno.Text == "")
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketId.Text == "")
            {
                lbvaltcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbTicketName.Text == "")
            {
                lbvaltckname.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (tbValue.Text == "")
            {
                lbvalValue.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (TryToParse(tbMaxno.Text) == false)
            {
                lbvalMaxno.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (TryToParse(tbMinno.Text) == false)
            {
                lbvalminno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbPackno.Text) == false)
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(tbTicketId.Text) == false)
            {
                lbvaltcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (TryToParse(TicketValue) == false)
            {
                lbvalValue.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (!bl.CheckTicketIdLenght(tbTicketId.Text.Length))
            {
                lbvaltcktid.Visibility = Visibility.Visible;
                flag = 1;
            }
            if (!bl.CheckPackNoLenght(tbPackno .Text.Length))
            {
                lbvalpackno.Visibility = Visibility.Visible;
                flag = 1;
            }

            if (cbExpDate.IsChecked != true && tbExpDate.SelectedDate < DateTime.Now)
            {

                flag = 1;
                statuslabel.Text = "Ticket Expired";
            }
            return flag;

        }

        private void tbValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbValue.Text != "")
            {

                if (TryToParse(tbValue.Text.Replace("$", "")) == true)
                {
                    string s = tbValue.Text.Replace("$", "");
                    if (Convert.ToInt32(s) != 0)
                    {
                        //if (s != "")
                        //{
                        //    tbValue.Text = "$" + s;
                        //    string val = tbValue.Text.Replace("$", "");
                        //    int value = Convert.ToInt32(val);
                        //    tbMaxno.Text = Convert.ToString((300 / value) - 1);
                        //}
                        TicketValue = tbValue.Text.Replace("$", "");
                        tbMinno.Text = "0";
                        tbValue.CaretIndex = TicketValue.Length + 1;
                    }
                }
            }
            else
            {
                //            tbValue.Text = "0";

            }

            }
        

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {

                return result;
                //if (value == null) value= "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.10;
        }

        private void btUISubmit_LostFocus(object sender, RoutedEventArgs e)
        {
            btUISubmit.Background = new SolidColorBrush(settings.Tab);
        }

        private void btUISubmit_GotFocus(object sender, RoutedEventArgs e)
        {
            btUISubmit.Background = Brushes.White;
        }

        private void cbExpDate_Checked(object sender, RoutedEventArgs e)
        {
            if (cbExpDate.IsChecked == true)
           {

                tbExpDate.IsEnabled = false;
            }
            
               

        }

        private void cbExpDate_Unchecked(object sender, RoutedEventArgs e)
        {
            if (cbExpDate.IsChecked == false)
            {

                tbExpDate.IsEnabled = true;
            }

        }

        private void tbTicketName_LostFocus(object sender, RoutedEventArgs e)
        {
            tbTicketName.Text = tbTicketName.Text.ToUpper();
        }

       


    }
}