﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Conformation.xaml
    /// </summary>
    public partial class ConfirmationPOS : Window
    {
        private Settings settings = Properties.Settings.Default;

        public ConfirmationPOS()
        {
            InitializeComponent();
            btGoback.Background = new SolidColorBrush(settings.Tab);
            
            btGoback.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btcontinue.Foreground = new SolidColorBrush(settings.Font);
            btcontinue.Background = new SolidColorBrush(settings.Tab);
            
        }

         public ConfirmationPOS(string data)
        {
            InitializeComponent();
            btGoback.Background = new SolidColorBrush(settings.Tab);

            btGoback.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            btcontinue.Foreground = new SolidColorBrush(settings.Font);
            btcontinue.Background = new SolidColorBrush(settings.Tab);
            txtData.Text = data;
            btcontinue.Content = "Yes";
            btGoback.Content = "No";
        }
        private void btGoback_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btContinue_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;

        }
    }
}
