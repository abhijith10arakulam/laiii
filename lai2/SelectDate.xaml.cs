﻿using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for SelectDate.xaml
    /// </summary>
    public partial class SelectDate : Window
    {

        private Settings settings = Properties.Settings.Default;

        int flag;
        public string ResponceData
        {

            get
            {

                return Convert.ToDateTime( dpTodate.SelectedDate).ToString("yyyy-MM-dd");

            }
            set
            {

                dpTodate.SelectedDate =Convert.ToDateTime( value);
            }

        }
        public SelectDate()
        {
            InitializeComponent();

            lblTittle.Foreground = new SolidColorBrush(settings.Theme);
            btnback.Background = new SolidColorBrush(settings.Tab);
            btnback.Foreground = new SolidColorBrush(settings.Font);
            btnsave.Background = new SolidColorBrush(settings.Tab);
            btnsave.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lblTittle.FontFamily = new FontFamily(settings.Fontstyle);


           

        }

    
        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;

          
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
           
            this.DialogResult = true;
        }


    }
}