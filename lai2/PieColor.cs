﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace lai2
{
    class PieColor : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            try
            {
                int val = (int)value;

                if (val == 1)
                {
                    return new SolidColorBrush(Colors.Blue);
                }
                else if (val == 2)
                {
                    return new SolidColorBrush(Colors.Magenta);
                }
                else if (val == 3)
                {
                    return new SolidColorBrush(Colors.Green);
                }
                else if (val == 4)
                {
                    return new SolidColorBrush(Colors.Purple);
                }
                else if (val == 5)
                {
                    return new SolidColorBrush(Colors.Yellow);
                }
                else if (val == 6)
                {
                    return new SolidColorBrush(Colors.OrangeRed);
                }
                else if (val == 7)
                {
                    return new SolidColorBrush(Colors.MistyRose);
                }
                else if (val == 8)
                {
                    return new SolidColorBrush(Colors.LimeGreen);
                }
                else if (val == 9)
                {
                    return new SolidColorBrush(Colors.DarkGray);
                }
                else if (val == 10)
                {
                    return new SolidColorBrush(Colors.PaleGreen);
                }
                else if (val == 11)
                {
                    return new SolidColorBrush(Colors.Plum);
                }
                else if (val == 12)
                {
                    return new SolidColorBrush(Colors.RosyBrown);
                }
                else if (val == 13)
                {
                    return new SolidColorBrush(Colors.Tomato);
                }
                else if (val == 14)
                {
                    return new SolidColorBrush(Colors.Wheat);

                }
                else if (val == 15)
                {
                    return new SolidColorBrush(Colors.YellowGreen);

                }

                else if (val == 16)
                {
                    return new SolidColorBrush(Colors.OliveDrab);

                }



                else
                {
                    return new SolidColorBrush(Colors.SeaShell);
                }

            }
            catch
            { return new SolidColorBrush(Colors.Coral); }
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
