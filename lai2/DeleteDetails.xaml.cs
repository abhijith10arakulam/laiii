﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for DeleteDetails.xaml
    /// </summary>
    public partial class DeleteDetails : Window
    {
        private Settings settings = Properties.Settings.Default;  
        Blogic bl = new Blogic();
        private ViewTicket vt;
        private List<ViewTicket> vt1;
        private ReportDataList rd;
        private string reportid;
        private ReportData rd1;
        public DeleteDetails()
        {
            InitializeComponent(); 
        }

        public DeleteDetails(ViewTicket vt)
        {
            InitializeComponent();        
            // TODO: Complete member initialization  

            btnDelete.Background = new SolidColorBrush(settings.Tab);
            btnEditInventory.Background = new SolidColorBrush(settings.Tab);
            this.Background = new SolidColorBrush(settings.Background);
            this.vt = vt;
            txtTicktId.Text =Convert.ToString(vt.TicketId);
            txtPackNo.Text = Convert.ToString(vt.PackNo);
            txtTicketName.Text=Convert.ToString(vt.TicketName);
         //   txtValue.Text = Convert.ToString(vt.Value);
        }

        public DeleteDetails(List<ViewTicket> vt1)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            btnDelete.Background = new SolidColorBrush(settings.Tab);
            btnEditInventory.Background = new SolidColorBrush(settings.Tab);
            this.Background = new SolidColorBrush(settings.Background);
            this.vt1 = vt1;
           
        }

        //public DeleteDetails(ReportData rd, string reportid)
        //{
        //    // TODO: Complete member initialization
        //    InitializeComponent();
        //    //label4.Foreground = new SolidColorBrush(settings.Theme);
        //    //btnEditInventory.Foreground = new SolidColorBrush(settings.Font);
        //    //btnDelete.Foreground = new SolidColorBrush(settings.Font);
        //    //btnEditInventory.Background = new SolidColorBrush(settings.Tab);
        //    //btnDelete.Background = new SolidColorBrush(settings.Tab);
        //    //lbdescription.Foreground = new SolidColorBrush(settings.Font);


        //    //this.rd = rd;
        //    //this.reportid = reportid;
        //    //txtTicktId.Text = rd.TicketId.ToString() ;
        //    //txtPackNo.Text = rd.PackNo.ToString();
        //    //txtTicketName.Text = rd.TicketName.ToString();
        //    //txtbox.Text = rd.Box.ToString();
        //    //txtopenno.Text = rd.OpenNo.ToString();
        //    //txtcloseno.Text = rd.CloseNo.ToString();


        //}

        public DeleteDetails(ReportData rd1, string reportid)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.rd1 = rd1;
            this.reportid = reportid;
            label4.FontFamily = new FontFamily(settings.Fontstyle);
            label4.Foreground = new SolidColorBrush(settings.Theme);
            btnEditInventory.Foreground = new SolidColorBrush(settings.Font);
            btnDelete.Foreground = new SolidColorBrush(settings.Font);
            btnEditInventory.Background = new SolidColorBrush(settings.Tab);
            btnDelete.Background = new SolidColorBrush(settings.Tab);
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            txtTicktId.Text = rd1.TicketId.ToString();
            txtPackNo.Text = rd1.PackNo.ToString();
            txtTicketName.Text = rd1.TicketName.ToString();
            txtbox.Text = rd1.Box.ToString();
            txtopenno.Text = rd1.OpenNo.ToString();
            txtcloseno.Text = rd1.CloseNo.ToString();
        }



        private void deletebttnclicked(object sender, RoutedEventArgs e)
        {
            string ticketinventoryid = rd1.TicketInventoryId.ToString();
            string transactionid = rd1.TransactionId.ToString();

            string heading = "Do you want to delete?";
            string ticketname = rd1.TicketName;
            string value = rd1.Value.ToString();
            string box = rd1.Box.ToString();
            var dialog = new ConfirmationDetails(heading, ticketname, value, box);
            if (dialog.ShowDialog() == true && dialog.DialogResult == true)
            {

                Result r = bl.DeleteReport(ticketinventoryid, transactionid, reportid);
                if (r == null && bl.internetconnection() == false)
                {
                    string heading1 = "        Poor Connectivity";
                    var dialog1 = new DialogBox_BoxNo(heading1);
                    dialog1.ShowDialog();
                }
                else
                {
                    if (r.Status == true)
                    {
                        string heading1 = "Deleted successfully!";
                        var dia = new DialogBox_BoxNo(heading1);
                        dia.ShowDialog();
                        this.DialogResult = true;
                    }
                    else
                    {
                        lbdescription.Visibility = Visibility.Visible;
                        lbdescription.Content = r.Description;
                    }
                }
            }
        }
       

        private void btnback_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.2;
        }     
     
    }
}
