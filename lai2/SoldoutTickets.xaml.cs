﻿using lai2.Bal;
using lai2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for SoldoutTickets.xaml
    /// </summary>
    public partial class SoldoutTickets : Window
    {
        private Settings settings = Properties.Settings.Default;
        Blogic bl = new Blogic();
        private int UIvalue;
        //bool mLeftCtrlDown = false;
        //bool mScanShiftDown = false;
        //bool mScanning = false;
        int returnFlag = 1;
        string TicketValue;

        string barcode = "";
        StringBuilder mScanData = new StringBuilder();
        KeyConverter mScanKeyConverter = new KeyConverter();
        //  private string p;
        //  private Result res;

        int statustemp=1;
        int ScanOrder;
        string ticketnametemp;
        string valuetemp;
        private string ticketid;
        private string ticketname;
        private string packno;
        private string box;
        private string openno;
        int flag = 0;
        public static int temp;
        public SoldoutTickets()
        {                      
            InitializeComponent();
            HideLabel();
            color();
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            var am = DateTime.Now.ToString("tt");

            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now; 
         
            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                tbbox.Items.Add(i);
            }
            tbbox.SelectedIndex = 0;

            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
            BindTicketId();
        }

        private void BindTicketId()
        {
            int operation = 2;
            AutoComplete ac = new AutoComplete();
            ac = bl.AutoCompliteData(operation);
            tbTicketId.ItemsSource = ac.ticketIdList;
        }


        public void HideLabel()
        {
            lbvalClose.Visibility = Visibility.Hidden;
            lbvalpackno.Visibility = Visibility.Hidden;
            lbvalPackOpen.Visibility = Visibility.Hidden;
            lbvaltcktid.Visibility = Visibility.Hidden;
            lbvalticcketname.Visibility = Visibility.Hidden;
            lbvalValue.Visibility = Visibility.Hidden;
            lbvalbox.Visibility = Visibility.Hidden;
        }
      
        public void color()
        {
            lbdescription.Foreground = new SolidColorBrush(settings.Font);
            comboboxhour.Foreground = new SolidColorBrush(settings.Font);
            comboboxam.Foreground = new SolidColorBrush(settings.Font);
            comboboxminute.Foreground = new SolidColorBrush(settings.Font);
            comboboxhour.Background = new SolidColorBrush(settings.Tab);
            comboboxam.Background = new SolidColorBrush(settings.Tab);
            comboboxminute.Background = new SolidColorBrush(settings.Tab);
            btClose.Background = new SolidColorBrush(settings.Tab);
            btSubmit.Background = new SolidColorBrush(settings.Tab);
            lbtitle.Foreground = new SolidColorBrush(settings.Theme);
            //btfullpackSoldout.Background = new SolidColorBrush(settings.Tab);
            //btfullpackSoldout.Foreground = new SolidColorBrush(settings.Font);           
            btClose.Foreground = new SolidColorBrush(settings.Font);
            btSubmit.Foreground = new SolidColorBrush(settings.Font);
            tbbox.Background = new SolidColorBrush(settings.Tab);
            tbbox.Foreground = new SolidColorBrush(settings.Font);
            if (settings.Background.ToString() != "#00FFFFFF")
            {
                this.Background = new SolidColorBrush(settings.Background);
            }
            lbtitle.FontFamily = new FontFamily(settings.Fontstyle);
            lbdescription.FontFamily = new FontFamily(settings.labelfont);
        }

        public SoldoutTickets(string ticketid, string ticketname, string packno, string box, string openno)
        {
            InitializeComponent();
            tbTicketId.IsEnabled = false;
            tbPackno.IsEnabled = false;
            tbTicketName.IsEnabled = false;
            tbValue.IsEnabled = false;
            tbPackPosition.IsEnabled = false;
            tbPackPositionopen.IsEnabled = false;
            // TODO: Complete member initialization
          
            color();
             HideLabel();
             this.ticketid = ticketid;
             this.ticketname = ticketname;
             this.packno = packno;
             this.box = box;
             this.openno = openno;
            var hr = Convert.ToInt32(DateTime.Now.ToString("hh"));
            var mm = Convert.ToInt32(DateTime.Now.ToString("mm"));
            var am = DateTime.Now.ToString("tt");
            comboboxam.Items.Add("AM");
            comboboxam.Items.Add("PM");
            for (int i = 1; i <= 12; i++)
            {
                comboboxhour.Items.Add(i);
            }
            for (int i = 0; i <= 59; i++)
            {
                comboboxminute.Items.Add(i);
            }
            for (int i = 1; i <= LoginDetail.shops[IndexFinder.index].BoxCount; i++)           //drop down for box count
            {
                tbbox.Items.Add(i);
            }
            tbbox.SelectedItem = box;    
            comboboxhour.SelectedItem = hr;
            comboboxminute.SelectedItem = mm;
            comboboxam.SelectedItem = am;
            datepicker1.SelectedDate = DateTime.Now;          
            
            tbTicketId.SearchText = ticketid;
            tbTicketName.Text = ticketname;
            tbPackno.SearchText = packno;
            tbPackPositionopen.Text = openno;
            this.PreviewKeyUp += new KeyEventHandler(MainWindow_PreviewKeyUp);
            BindTicketId();
           // temp = 1;
        }

        void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return" && returnFlag == 1 & barcode.Length != 0)
            {
                //bl = new BAL();
                //returnFlag = 0;
                if ((barcode.Length <= 24 && barcode.Length >= 22) || (barcode.Length == 20)||(barcode.Length == 18))
                {
                    SoundPlayer snd = new SoundPlayer(Properties.Resources.scan);
                    snd.Play();
                    ScanData tt = bl.GetTicket(barcode);                    
                    setData(tt);
                    // btSubmit.Focus();
                }

                barcode = "";

                if (UIvalue == 0)
                {
                    // EnableTextBoxes();
                    //if (TbValidation())
                    //{

                    //    //this.DialogResult = true;
                    //}
                    //else
                    //{
                    //    btUISubmit.Visibility = Visibility.Visible;
                    //    btUISubmit.IsEnabled = true;
                    //}
                }
            }
            // else 
            if (e.Key.ToString().StartsWith("D") && returnFlag == 1)
            {
                barcode = barcode + e.Key.ToString().Replace("D", string.Empty);

            }
            //listBox1.Items.Add(e.Key.ToString().Replace("D",string.Empty));
        }

        private void setData(ScanData tt)
        {
            tbTicketId.SearchText = Convert.ToString(tt.TicketId);
            tbPackno.SearchText = tt.PackNo;
          //  tbPackPositionopen.Text = Convert.ToString(tt.PackPosition);
        }

        private void tbPackno_TextChanged(object sender, EventArgs e)
        {
            if (tbTicketId.SearchText != "" && tbPackno.SearchText != "")
            {
                if ((TryToParse(tbTicketId.SearchText) == true) && (TryToParse(tbPackno.SearchText) == true))
                {
                    if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                   //                    if (tbTicketId.SearchText.Length >= 3 && tbTicketId.SearchText.Length <= 5 && (tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7))
                    {
                        string description = "This Ticket is  not in Activated list";
                        ResultWithOldTicket ts = bl.InActiveTickets_Fill(tbTicketId.SearchText, tbPackno.SearchText);
                        if (ts == null && bl.internetconnection() == false)
                        {
                            string heading = "        Poor Connectivity";
                            var dialog = new DialogBox_BoxNo(heading);
                            dialog.ShowDialog();
                        }
                        else
                        {
                            if (ts.Status == true && ts.Description == "Success")
                            {
                                HideLabel();
                                statustemp = 1;
                                btSubmit.Content = "Sold Out";
                                tbPackno.SearchText = Convert.ToString(ts.OldTicketInfo.PackNo);
                                tbTicketName.Text = Convert.ToString(ts.OldTicketInfo.TicketName);
                                tbValue.Text = Convert.ToString(ts.OldTicketInfo.Value);
                                tbPackPositionopen.Text = Convert.ToString(ts.OldTicketInfo.OpenNo);                               
                                tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.MaxNo);                               
                                if (ts.OldTicketInfo.Box != 0)
                                {
                                    tbbox.SelectedItem = Convert.ToInt32(ts.OldTicketInfo.Box);
                                }
                                else if (ts.OldTicketInfo.Box == 0)
                                {
                                    tbbox.SelectedIndex = 0;
                                }
                                ticketnametemp = tbTicketName.Text;
                            }

                            else if (ts.Status == true && ts.Description.Trim() == description)
                            {
                                HideLabel();
                                statustemp = 2;
                                btSubmit.Content = "Full Pack Sold Out";
                                tbPackno.SearchText = Convert.ToString(ts.OldTicketInfo.PackNo);
                                tbTicketName.Text = Convert.ToString(ts.OldTicketInfo.TicketName);
                                tbValue.Text = Convert.ToString(ts.OldTicketInfo.Value);
                                tbPackPositionopen.Text = Convert.ToString(ts.OldTicketInfo.OpenNo);
                                // tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.CloseNo);
                                tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.MaxNo);
                                //tbbox.Text = Convert.ToString(ts.OldTicketInfo.Box);
                                ticketnametemp = tbTicketName.Text;
                                if (ts.OldTicketInfo.Box != 0)
                                {
                                    tbbox.SelectedItem = Convert.ToInt32(ts.OldTicketInfo.Box);
                                }
                                else if (ts.OldTicketInfo.Box == 0)
                                {
                                    tbbox.SelectedIndex = 0;
                                }
                            }

                            else
                            {
                                lbdescription.Content = ts.Description;
                                tbPackno.SearchText = Convert.ToString(ts.OldTicketInfo.PackNo);
                                tbTicketName.Text = Convert.ToString(ts.OldTicketInfo.TicketName);
                                tbValue.Text = Convert.ToString(ts.OldTicketInfo.Value);
                                tbPackPositionopen.Text = Convert.ToString(ts.OldTicketInfo.OpenNo);
                                tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.MaxNo);
                                if (ts.OldTicketInfo.Box != 0)
                                {
                                    tbbox.SelectedItem = Convert.ToInt32(ts.OldTicketInfo.Box);
                                }
                                else if (ts.OldTicketInfo.Box == 0)
                                {
                                    tbbox.SelectedIndex = 0;
                                }
                                ticketnametemp = tbTicketName.Text;
                            }
                        }
                    }
                }
            }

            if (tbPackno.SearchText == "")
            {
                HideLabel();
                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackPositionopen.Text = "";
                lbdescription.Visibility = Visibility.Hidden;
                btSubmit.Content = "Submit";
            }
        }


        private void tbTicketId_TextChanged(object sender, EventArgs e)
        {
            if (tbTicketId.SearchText != "" && tbPackno.SearchText != "")
            {
                if ((TryToParse(tbTicketId.SearchText) == true) && (TryToParse(tbPackno.SearchText) == true))
                {
                    if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                    //   if (tbTicketId.SearchText.Length >= 3 && tbTicketId.SearchText.Length <= 5 && (tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7))
                    {
                        ResultWithOldTicket ts = bl.InActiveTickets_Fill(tbTicketId.SearchText, tbPackno.SearchText);
                        if (ts == null && bl.internetconnection() == false)
                        {
                            string heading = "        Poor Connectivity";
                            var dialog = new DialogBox_BoxNo(heading);
                            dialog.ShowDialog();
                        }
                        else
                        {
                            if (ts.Status == true && ts.Description == "Success")
                            {
                                HideLabel();
                                statustemp = 1;
                                btSubmit.Content = "Sold Out";
                                tbPackno.SearchText = Convert.ToString(ts.OldTicketInfo.PackNo);
                                tbTicketName.Text = Convert.ToString(ts.OldTicketInfo.TicketName);
                                tbValue.Text = Convert.ToString(ts.OldTicketInfo.Value);
                                tbPackPositionopen.Text = Convert.ToString(ts.OldTicketInfo.OpenNo);
                                // tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.CloseNo);
                                tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.MaxNo);
                                //tbbox.Text = Convert.ToString(ts.OldTicketInfo.Box);
                                ticketnametemp = tbTicketName.Text;
                                if (ts.OldTicketInfo.Box != 0)
                                {
                                    tbbox.SelectedItem = Convert.ToInt32(ts.OldTicketInfo.Box);
                                }
                                else if (ts.OldTicketInfo.Box == 0)
                                {
                                    tbbox.SelectedIndex = 0;
                                }
                            }

                            else if (ts.Status == true && ts.Description == "This Ticket is not in Activated list")
                            {
                                HideLabel();
                                statustemp = 2;
                                btSubmit.Content = "Full Pack Sold Out";
                                tbPackno.SearchText = Convert.ToString(ts.OldTicketInfo.PackNo);
                                tbTicketName.Text = Convert.ToString(ts.OldTicketInfo.TicketName);
                                tbValue.Text = Convert.ToString(ts.OldTicketInfo.Value);
                                tbPackPositionopen.Text = Convert.ToString(ts.OldTicketInfo.OpenNo);
                                // tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.CloseNo);
                                tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.MaxNo);
                                //tbbox.Text = Convert.ToString(ts.OldTicketInfo.Box);
                                ticketnametemp = tbTicketName.Text;
                                if (ts.OldTicketInfo.Box != 0)
                                {
                                    tbbox.SelectedItem = Convert.ToInt32(ts.OldTicketInfo.Box);
                                }
                                else if (ts.OldTicketInfo.Box == 0)
                                {
                                    tbbox.SelectedIndex = 0;
                                }
                            }
                            else
                            {
                                lbdescription.Content = ts.Description;
                                tbPackno.SearchText = Convert.ToString(ts.OldTicketInfo.PackNo);
                                tbTicketName.Text = Convert.ToString(ts.OldTicketInfo.TicketName);
                                tbValue.Text = Convert.ToString(ts.OldTicketInfo.Value);
                                tbPackPositionopen.Text = Convert.ToString(ts.OldTicketInfo.OpenNo);
                                tbPackPosition.Text = Convert.ToString(ts.OldTicketInfo.MaxNo);
                                if (ts.OldTicketInfo.Box != 0)
                                {
                                    tbbox.SelectedItem = Convert.ToInt32(ts.OldTicketInfo.Box);
                                }
                                else if (ts.OldTicketInfo.Box == 0)
                                {
                                    tbbox.SelectedIndex = 0;
                                }
                                ticketnametemp = tbTicketName.Text;
                            }
                        }
                    }
                }
            }

            else
            {
                tbTicketIdChangeEvent();
            }
            if (tbTicketId.SearchText == "")
            {
                HideLabel();
                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackPositionopen.Text = "";
                lbdescription.Visibility = Visibility.Hidden;
                btSubmit.Content = "Submit";
            }               
           
        }


        private void tbValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = tbValue.Text.Replace("$", "");
            if (s != "")
            {
                tbValue.Text = "$" + s;
                 valuetemp = tbValue.Text;
            }
            TicketValue = tbValue.Text.Replace("$", "");
            tbValue.CaretIndex = TicketValue.Length + 1;
        }

        private void btSubmit_Click(object sender, RoutedEventArgs e)
        {           
          flag = 0;
          HideLabel();
          btSubmit.Focusable = false;
          if (statustemp == 1)
          {
              if (tbbox.Text == "")
              {
                  lbvalbox.Visibility = Visibility.Visible;
                  flag = 1;
              }

              if (tbTicketName.Text == "")
              {
                  lbvalticcketname.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (tbPackPosition.Text == "")
              {
                  lbvalClose.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (tbPackno.SearchText == "")
              {
                  lbvalpackno.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (tbPackPositionopen.Text == "")
              {
                  lbvalPackOpen.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (tbTicketId.SearchText == "")
              {
                  lbvaltcktid.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (tbValue.Text == "")
              {
                  lbvalValue.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(tbTicketId.SearchText) == false)
              {
                  lbvaltcktid.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if ((TryToParse(tbPackno.SearchText) == false))
              {
                  lbvalpackno.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if ((TryToParse(tbPackPosition.Text) == false))
              {
                  lbvalClose.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(tbPackPositionopen.Text) == false)
              {
                  lbvalPackOpen.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(tbbox.Text) == false)
              {
                  lbvalbox.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(TicketValue) == false)
              {
                  lbvalValue.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (!bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
              {
                  lbvaltcktid.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (!bl.CheckPackNoLenght(tbPackno.SearchText.Length))
              {
                  lbvalpackno.Visibility = Visibility.Visible;
                  flag = 1;
              }
              else if (flag == 0)
              {
                  if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                //  if (tbTicketId.SearchText.Length >= 3 && tbTicketId.SearchText.Length <= 5 && (tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7))
                  {
                      SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                      snd.Play();
                      string heading = "Are you sure the ticket is soldout??";
                      string ticketname = tbTicketName.Text;
                      string value = tbValue.Text;
                      string box = tbbox.SelectedItem.ToString();
                      var dialog = new ConfirmationDetails(heading, ticketname, value,box);
                      if (dialog.ShowDialog() == true && dialog.DialogResult == true)
                      {
                          lbdescription.Visibility = Visibility.Hidden;
                          // tbValue.Text = tbValue.Text.Replace("$", "");
                          string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                          String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + " " + comboboxam.SelectedItem.ToString();

                          Result rt = bl.SoldOut_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPosition.Text, s);
                          if (rt == null && bl.internetconnection() == false)
                          {
                              string heading1 = "        Poor Connectivity";
                              var dialog1 = new DialogBox_BoxNo(heading1);
                              dialog1.ShowDialog();
                          }
                          else
                          {
                              if (rt.Status == true)
                              {
                                  lbdescription.Visibility = Visibility.Visible;
                                  lbdescription.Content = rt.Description;
                                  tbTicketId.SearchText = "";
                                  tbPackno.SearchText = "";
                                  tbTicketName.Text = "";
                                  tbPackPositionopen.Text = "";
                                  tbValue.Text = "";
                                  this.DialogResult = true;
                              }
                              else
                              {
                                  // MessageBox.Show(rt.Description);
                                  lbdescription.Visibility = Visibility.Visible;
                                  lbdescription.Content = rt.Description;
                                  tbTicketId.Focus();
                              }
                          }
                      }
                      else
                      {
                          tbTicketId.Focus();
                      }
                  }
              }
          }

          else if (statustemp == 2)
          {
              if (tbTicketId.SearchText == "")
              {
                  lbvaltcktid.Visibility = Visibility.Visible;
                  flag = 1;
              }
              else if (tbPackno.SearchText == "")
              {
                  lbvalpackno.Visibility = Visibility.Visible;
                  flag = 1;
              }
              else if (tbbox.Text == "")
              {
                  lbvalbox.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(tbTicketId.SearchText) == false)
              {
                  lbvaltcktid.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(tbPackno.SearchText) == false)
              {
                  lbvalpackno.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (TryToParse(tbbox.Text) == false)
              {
                  lbvalbox.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (!bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
              {
                  lbvaltcktid.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (!bl.CheckPackNoLenght(tbPackno.SearchText.Length))
              {
                  lbvalpackno.Visibility = Visibility.Visible;
                  flag = 1;
              }
              if (flag == 0)
              {
                  if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length) && bl.CheckPackNoLenght(tbPackno.SearchText.Length))
                 // if (tbTicketId.SearchText.Length >= 3 && tbTicketId.SearchText.Length <= 5 && (tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7))
                  {
                      SoundPlayer snd = new SoundPlayer(Properties.Resources.Whistlewav);
                      snd.Play();

                      string heading = "Do you want to Sold Out full pack?";
                      int soldoutstatus = 1;
                      string ticketname = ticketnametemp;
                      string value = valuetemp;
                      string box = tbbox.SelectedItem.ToString();

                      var dialog1 = new ConfirmationDetails(heading, soldoutstatus, ticketname, value, box);
                      if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
                      {
                          lbdescription.Content = "";
                          tbValue.Text = tbValue.Text.Replace("$", "");

                          string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
                          String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + " " + comboboxam.SelectedItem.ToString();
                          ResultWithOldTicket rt = bl.FullPackSoldOut_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, box, s, ScanOrder);
                          if (rt == null && bl.internetconnection() == false)
                          {
                              string heading1 = "        Poor Connectivity";
                              var dialog2 = new DialogBox_BoxNo(heading1);
                              dialog2.ShowDialog();
                          }
                          else
                          {
                              if (rt.Status == true)
                              {
                                  lbdescription.Content = rt.Description;
                                  tbTicketId.SearchText = "";
                                  tbPackno.SearchText = "";
                                  tbTicketName.Text = "";
                                  tbPackPositionopen.Text = "";
                                  tbValue.Text = "";
                                  this.DialogResult = true;
                              }
                              else
                              {
                                  lbdescription.Visibility = Visibility.Visible;
                                  lbdescription.Content = rt.Description;
                              }
                          }
                      }
                  }
              }
          }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void windowloaded(object sender, RoutedEventArgs e)
        {
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2.2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 1.05;
        } 

        private static bool TryToParse(string value)        //number to number conversion
        {
            int number;
            bool result = Int32.TryParse(value, out number);
            if (result)
            {
                return result;
                //  Console.WriteLine("Converted '{0}' to {1}.", value, number);
            }
            else
            {
                return result;
                //if (value == null) value = "";
                //Console.WriteLine("Attempted conversion of '{0}' failed.", value);
            }
        }


        private void tbTicketId_LostFocus(object sender, RoutedEventArgs e)
        {
            var data = tbTicketId.SearchText;
            List<GetPackNo> items = new List<GetPackNo>();
            items = bl.GetPackNos(data, 3);
            tbPackno.ItemsSource = items;


        }

        private void tbPackno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                tbPackno.SearchText = ((GetPackNo)tbPackno.SelectedItem).PackNo;
            }
            catch { }

        }

        private void tbTicketId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                tbTicketId.SearchText = ((string)tbTicketId.SelectedItem);
            }
            catch { }
        }



        //private void btFullpackSoldOut_Click(object sender, RoutedEventArgs e)
        //{
        //    HideLabel();
        //    flag = 0;
        //    if (tbTicketId.SearchText == "")
        //    {
        //        lbvaltcktid.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    else if (tbPackno.SearchText == "")
        //    {
        //        lbvalpackno.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    else if (tbbox.Text=="")
        //    {
        //    lbvalbox.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    if (TryToParse(tbTicketId.SearchText)==false)
        //    {
        //        lbvaltcktid.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    if (TryToParse(tbPackno.SearchText) == false)
        //    {
        //        lbvalpackno.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    if (TryToParse(tbbox.Text) == false)
        //    {
        //        lbvalbox.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    if (!bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
        //    {
        //        lbvaltcktid.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    if (!bl.CheckPackNoLenght(tbPackno .Text.Length))
        //    {
        //        lbvalpackno.Visibility = Visibility.Visible;
        //        flag = 1;
        //    }
        //    if (flag == 0)
        //    {
        //        if (tbTicketId.SearchText.Length == 3 || tbTicketId.SearchText.Length == 4 && tbPackno.SearchText.Length == 6 || tbPackno.SearchText.Length == 7)
        //        {
        //            string heading = "            Do you want to Soldout?";
        //            int soldoutstatus = 1;
        //            string ticketname = ticketnametemp;
        //            string value = valuetemp;
        //            string box = tbbox.SelectedItem.ToString();

        //            var dialog1 = new ConfirmationDetails(heading, soldoutstatus, ticketname, value, box);
        //            if (dialog1.ShowDialog() == true && dialog1.DialogResult == true)
        //            {
        //                lbdescription.Content = "";
        //                tbValue.Text = tbValue.Text.Replace("$", "");

        //                string date = Convert.ToDateTime(datepicker1.SelectedDate).ToString("yyyy/MM/dd");
        //                String s = date + " " + comboboxhour.SelectedItem.ToString() + ":" + comboboxminute.SelectedItem.ToString() + " " + comboboxam.SelectedItem.ToString();
        //                ResultWithOldTicket rt = bl.FullPackSoldOut_Tickets(tbTicketId.SearchText, tbPackno.SearchText, tbPackPositionopen.Text, box, s, ScanOrder);
        //                if (rt.Status == true)
        //                {
        //                    lbdescription.Content = rt.Description;
        //                    tbTicketId.SearchText = "";
        //                    tbPackno.SearchText = "";
        //                    tbTicketName.Text = "";
        //                    tbPackPositionopen.Text = "";
        //                    tbValue.Text = "";
        //                    this.DialogResult = true;
        //                }

        //                else
        //                {
        //                    lbdescription.Visibility = Visibility.Visible;
        //                    lbdescription.Content = rt.Description;
        //                }
        //            }
        //        }
        //    }

        //} 


        public void tbTicketIdChangeEvent()
        {



            if (bl.CheckTicketIdLenght(tbTicketId.SearchText.Length))
          
            {
                if (TryToParse(tbTicketId.SearchText) == true)
                {
                    
                    TicketStatus ts = bl.ActiveTickets_Fill(tbTicketId.SearchText);
                    if (ts == null && bl.internetconnection() == false)
                    {
                        string heading = "        Poor Connectivity";
                        var dialog = new DialogBox_BoxNo(heading);
                        dialog.ShowDialog();
                    }
                    else
                    {
                        if (ts.Status == true)
                        {

                            tbPackno.SearchText = Convert.ToString(ts.TicketInfo[0].PackNo);
                            tbTicketName.Text = Convert.ToString(ts.TicketInfo[0].TicketName);
                            tbValue.Text = Convert.ToString(ts.TicketInfo[0].Value);

                        }
                        else
                        {
                                     
                        }
                    }
                }
            }

            if (tbTicketId.SearchText == "")
            {

                tbTicketName.Text = "";
                tbValue.Text = "";
                tbPackno.SearchText = "";



            }
        }

    }
}
