﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lai2
{
    /// <summary>
    /// Interaction logic for Expired.xaml
    /// </summary>
    public partial class Expired : Window
    {
        public Expired()
        {
            InitializeComponent();
        }
        public Expired(string discription)
        {
            InitializeComponent();
            tbDiscription.Text = discription;
        }

        private void tbDiscription_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://realtnetworking.com/Login.aspx");

            Process.Start(sInfo);

        }
    }
}
