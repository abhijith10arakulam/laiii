﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using lai2.Bal;
using lai2.Properties;

namespace lai2
{
    /// <summary>
    /// Interaction logic for ViewSettlements.xaml
    /// </summary>
    public partial class ViewSettlements : Page
    {
        ResultWithSettlementTicket rt = new ResultWithSettlementTicket();
        System.ComponentModel.BackgroundWorker mWorker; 
        private Settings settings = Properties.Settings.Default;
        string fromdate;
        string todate;
        Blogic bl = new Blogic();
        int temp = 0;
        public ViewSettlements()
        {
            InitializeComponent();

            lbstartdate.Content = Blogic.opendate;
            txtlegal.Text = LoginDetail.shops[IndexFinder.index].LegalName.ToString();
            txtlocationname.Text = LoginDetail.shops[IndexFinder.index].LocationName.ToString();
            txtuserlogin.Text = LoginDetail.user.UserName.ToString();

            lbstartdate.Foreground = new SolidColorBrush(settings.Font);
            txtlegal.Foreground = new SolidColorBrush(settings.Font);
            txtlocationname.Foreground = new SolidColorBrush(settings.Font);
            txtuserlogin.Foreground = new SolidColorBrush(settings.Font);


            fromdatePicker1.SelectedDate = DateTime.Now.AddDays(-7);
           
          //  datepickertodate.DisplayDateEnd = DateTime.Now;
            fromdatePicker1.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(datePicker1_SelectedDateChanged);
            datepickertodate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(datePicker1_SelectedDateChanged);

            datepickertodate.SelectedDate = DateTime.Now;
            btWeekSett.Background = new SolidColorBrush(settings.Tab);
            lbTitile.Foreground = new SolidColorBrush(settings.Theme);
            //lbfromdate.Foreground = new SolidColorBrush(settings.Font);
            //lbtodate.Foreground = new SolidColorBrush(settings.Font);
            btWeekSett.Foreground = new SolidColorBrush(settings.Font);
            dgList.Foreground = new SolidColorBrush(settings.Font);
            this.Background = new SolidColorBrush(settings.Background);
            dgList.BorderBrush = new SolidColorBrush(settings.Border);
            lbTitile.FontFamily = new FontFamily(settings.Fontstyle);
            dgList.FontFamily = new FontFamily(settings.datagridfont);

            lblctnnme.FontFamily = new FontFamily(settings.labelfont);
            txtlocationname.FontFamily = new FontFamily(settings.labelfont);
            txtlegal.FontFamily = new FontFamily(settings.labelfont);
            lblegalnmae.FontFamily = new FontFamily(settings.labelfont);
            lbfromdate.FontFamily = new FontFamily(settings.labelfont);
            lbtodate.FontFamily = new FontFamily(settings.labelfont);
            lbopndate.FontFamily = new FontFamily(settings.labelfont);
            lbstartdate.FontFamily = new FontFamily(settings.labelfont);
            lburlogn.FontFamily = new FontFamily(settings.labelfont);
            txtuserlogin.FontFamily = new FontFamily(settings.labelfont);

           
            loading();
        }

        public void loading()
        {    

            mWorker = new System.ComponentModel.BackgroundWorker();
            mWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(worker_DoWork);
            mWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(worker_ProgressChanged);
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();         
            try
            { 
                if (!mWorker.CancellationPending)
                    {
                        try
                        {
                            loadingtelerick.IsBusy = true;
                            // pbProcessing.Value = (pbProcessing.Value + 0.05) % 100;
                            // Thread.Sleep(5);
                            //pbProcessing.Value = (pbProcessing.Value + 1);
                        }
                        catch (System.Exception ex)
                        {
                            // No action required
                        }
                    }
                    else
                    {
                        // MessageBox.Show(this, "Process cancelled", "Cancel Process", MessageBoxButton.OK);
                        //   break;
                    }

                    System.Windows.Threading.Dispatcher.CurrentDispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                           new System.Threading.ThreadStart(delegate { }));
             
            }
            catch { }
        }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            rt = bl.Settlementby_date(fromdate, todate);
        }

        private void worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
          
        }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mWorker.CancelAsync();
            loadingtelerick.IsBusy = false;
            if (rt == null && bl.internetconnection() == false)
            {
                string heading = "        Poor Connectivity";
                var dialog = new DialogBox_BoxNo(heading);
                dialog.ShowDialog();
            }
            else
            {
                if (rt.Status == true)
                {
                    for (int i = 0; i < rt.SettlementTicket.Count; i++)
                    {
                        if (rt.SettlementTicket[i].SettlementStatus == 1)
                        {
                            rt.SettlementTicket[i].SettlementStatusChar = "Settled";
                        }
                        else
                        {
                            rt.SettlementTicket[i].SettlementStatusChar = "AutoSettled";
                        }
                    }
                    dgList.ItemsSource = rt.SettlementTicket;
                }
            }
        }

        private void cmdStop_Click(object sender, RoutedEventArgs e)
        {
            mWorker.CancelAsync();
        }          

        private void datePicker1_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                fromdate = Convert.ToDateTime(fromdatePicker1.SelectedDate).ToString("yyyy/MM/dd");
                if (datepickertodate.SelectedDate > fromdatePicker1.SelectedDate)
                {
                    todate = Convert.ToDateTime(datepickertodate.SelectedDate).ToString("yyyy/MM/dd");
                    loading();
                }
            }
            catch { }          
           
        }

        private void btWeekSett_Click_1(object sender, RoutedEventArgs e)
        {
            if (temp ==0)
            {
                temp = 1;
                ResultWithSettlementTicket rs = new ResultWithSettlementTicket();
                string date;
                date = DateTime.Now.ToString();
                rs = bl.Settlementby_week(date);
                if (rs == null && bl.internetconnection() == false)
                {
                    string heading = "        Poor Connectivity";
                    var dialog = new DialogBox_BoxNo(heading);
                    dialog.ShowDialog();
                }
                else
                {
                    if (rs.Status == true)
                    {
                        for (int i = 0; i < rs.SettlementTicket.Count; i++)
                        {
                            if (rs.SettlementTicket[i].SettlementStatus == 1)
                            {
                                rs.SettlementTicket[i].SettlementStatusChar = "Settled";
                            }
                            else
                            {
                                rs.SettlementTicket[i].SettlementStatusChar = "AutoSettled";
                            }
                        }
                        dgList.ItemsSource = rs.SettlementTicket;
                    }
                }
            }
            else if (temp == 1)
            {
                temp = 0;
                loading();
            }
        }
    }
}
